<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    kartik\select2\Select2,
    kartik\grid\GridView,
    yii\widgets\Breadcrumbs,
    frontend\components\helpers\FunctionHelper;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
$modelU = common\models\pay\PayRequests::FOR_USER_ADMIN;
?>

    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">

        <div class="user-form row">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <h5 class="card-header">Senf credits user</h5>
                    <div class="card-body">
                        <?php $form = ActiveForm::begin(); ?>

                            <?= $form->field($model, 'to_user_id')->widget(Select2::classname(), [
                                'data' => $usersArray,
                                'options' => ['placeholder' => Yii::t('app', 'Select...')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ]
                            ]); ?>

                            <?= $form->field($model, 'amount')->input('number'); ?>
                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                  </div>
            </div>
        </div>
        <div>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'responsive' => false,
                'striped' => false,
                'responsiveWrap' => false,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'module',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->getModule();
                        },
                    ],
                    [
                        'attribute' => 'amount',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return FunctionHelper::numberFormat($model->amount);
                        },
                    ],
                    [
                        'label' => 'Type Request',
                        'format' => 'raw',
                        'value' => function ($model) use ($modelU) {
                            return $model->from_user_id == $modelU ? 'plus' : 'minus';
                        },
                    ],
                    [
                        'label' => 'With user',
                        'format' => 'raw',
                        'value' => function ($model) use ($modelU) {
                            return $model->from_user_id == $modelU ? ($model->toUserModel ? $model->toUserModel->getNameAndSurname() : '') :  ($model->fromUserModel ? $model->fromUserModel->getNameAndSurname() : '');
                        },
                    ],
                    'created_at'
                ],
            ]); ?>
        </div>
        
    </section>
