<?php

namespace backend\modules\users\controllers;

use Yii,
    yii\web\Controller,
    common\models\pay\PayRequests,
    common\models\User,
    frontend\modules\profile\models\SearchPayRequests;

/**
 * Default controller for the `users` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionCreditRequests()
    {
        $model = new PayRequests();
        $model->from_user_id = PayRequests::FOR_USER_ADMIN;
        $model->module = PayRequests::MODULE_ADMIN_REQUEST;
        $model->typePay = PayRequests::TYPE_PAY_CREDIT;
        if ($model->load(Yii::$app->request->post())) {
            if (PayRequests::creditsPayRequest($model->from_user_id, $model->to_user_id, $model->amount, null, PayRequests::OPERATION_TYPE_ADMIN_SENT, PayRequests::MODULE_ADMIN_REQUEST, 'admin sent credits')) {
                Yii::$app->session->setFlash('successSave');
                $model = new PayRequests();
            } else {
                exit;
            }
        }
        
        $searchModel = new SearchPayRequests();
        $searchModel->typePay = PayRequests::TYPE_PAY_CREDIT;
        $searchModel->userFilterid = PayRequests::FOR_USER_ADMIN;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('_credits_request', [
            'model' => $model,
            'usersArray' => User::getAllInArrayMap(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
