<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\university\Courses;
use common\models\User;
use common\models\UserRole;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .checkbox-style .form-group {
        display:inline-block;
        float:right;
    }
</style>
<div class="classes-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="checkbox-style">
        <?= $form->field($model, 'status', [
            'template' => ' {input} <label for="classes-status">' . Yii::t('app', 'Status') . '</label> {error}{hint}'
        ])->checkbox(['label' => null, 'class' => 'checkbox']); ?>
    </div>
    
    <div class="clearfix"></div>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div style="width:150px;height:150px;">
        <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
        <img src="<?= $model->getImages(); ?>" class="previewClassImage img-thumbnail"><br>
        <a class="btn btn-sm btn-success addClassImage" style="width:100%">Change image</a><br><br>
    </div>

    <?= $form->field($model, 'course_id')->widget(Select2::classname(), [
        'data' => Courses::getAllInArrayMap(),
        'options' => ['placeholder' => Yii::t('app', 'Select...')],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]); ?>
    
    <?= $form->field($model, 'professor')->widget(Select2::classname(), [
        'data' => User::getAllInArrayMap(UserRole::ROLE_PROFFESOR),
        'options' => ['placeholder' => Yii::t('app', 'Select...')],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
