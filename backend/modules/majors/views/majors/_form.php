<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\University;

/* @var $this yii\web\View */
/* @var $model common\models\university\Majors */
/* @var $form yii\widgets\ActiveForm */
?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>
<div class="majors-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <div style="width:150px;height:150px;">
        <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
        <img src="<?= $model->getImages(); ?>" class="previewMajorImage img-thumbnail"><br>
        <a class="btn btn-sm btn-success addMajorImage" style="width:100%">Change image</a><br><br>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'url_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'university_id')->dropDownList(University::getAllUniversityInArrayMap()) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
