<?php

namespace backend\modules\majors\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\university\Majors;

/**
 * SearchMajors represents the model behind the search form of `common\models\university\Majors`.
 */
class SearchMajors extends Majors
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'creator', 'university_id'], 'integer'],
            [['name', 'description', 'created_at', 'updated_at', 'url_name', 'img_src'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Majors::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status' => $this->status,
            'creator' => $this->creator,
            'university_id' => $this->university_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'url_name', $this->url_name])
            ->andFilterWhere(['like', 'img_src', $this->img_src]);

        return $dataProvider;
    }
}
