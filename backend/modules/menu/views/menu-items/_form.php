<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MenuItems;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\MenuItems */
/* @var $form yii\widgets\ActiveForm */
?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>

<div class="menu-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="checkbox-style">
        <?= $form->field($model, 'status', [
            'template' => ' {input} <label for="menuitems-status">' . Yii::t('app', 'Status') . '</label> {error}{hint}'
        ])->checkbox(['label' => null, 'class' => 'checkbox']); ?>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div style="width:150px;height:150px;">
        <?= $form->field($model, 'logo')->hiddeninput()->label(false); ?>
        <img src="<?= $model->getImages(); ?>" class="previewMenuImage img-thumbnail"><br>
        <a class="btn btn-sm btn-success addMenuImage" style="width:100%">Change image</a><br><br>
    </div>

    <?= $form->field($model, 'type')->dropDownList(MenuItems::$arrayTypes, ['prompt' => Yii::t('app', 'Select...')]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'class_attr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->widget(Select2::classname(), [
        'data' => MenuItems::getParentArray(),
        'options' => ['placeholder' => Yii::t('app', 'Select...')],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]); ?>
    
    <div class="col-sm-2 no-padding">
        <?= $form->field($model, 'sort')->input('number') ?>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
