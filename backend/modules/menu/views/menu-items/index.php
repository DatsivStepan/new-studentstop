<?php

use yii\helpers\Html,
    kartik\grid\GridView,
    yii\widgets\Breadcrumbs,
    common\models\MenuItems;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\menu\models\MenuItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menu Items');
$this->params['breadcrumbs'][] = $this->title;
?>
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        
            <div class="menu-items-index">

                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <p>
                    <?= Html::a(Yii::t('app', 'Create Menu Items'), ['create'], ['class' => 'btn btn-success']) ?>
                </p>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [

                        [
                            'label' => 'Logo',
                            'attribute' => 'logo',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Html::tag('img', '', [
                                    'src' => $model->getImages(),
                                    'class' => 'img-thumbnail',
                                    'style' => 'width:100px;',
                                ]);
                            }
                        ],
                        'name',
                        [
                            'attribute' => 'type',
                            'format' => 'raw',
                            'filterType' => 'kartik\select2\Select2',
                            'filterWidgetOptions' => [
                                'model' => $searchModel,
                                'attribute' => 'type',
                                'data' => MenuItems::$arrayTypes,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Select...'),
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ],
                            'value' => function ($model) {
                                return array_key_exists($model->type, MenuItems::$arrayTypes) ? 
                                        MenuItems::$arrayTypes[$model->type] : null;
                            }
                        ],
                        'url:url',
                        'sort',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            </div>
        

    </section>