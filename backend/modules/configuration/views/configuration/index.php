<?php

use yii\helpers\Html,
    kartik\grid\GridView,
    yii\widgets\Breadcrumbs,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\configuration\models\SearchConfiguration */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Configurations');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
    
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    
    </section>

    <!-- Main content -->
    <section class="invoice">
        
        <div class="configuration-index">

            <?php //= $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a(Yii::t('app', 'Create Configuration'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => [
                    'id',
                    'name',
                    'key',
                    'value',
                    'created_at',
                    // 'updated_at',

                    [
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>',
                                Url::to(['/configuration/configuration/update', 'id' => $model->id]),
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'data-pjax' => 0
                                ]
                            );
                        }
                    ],
                ],
            ]); ?>
        </div>

    </section>