<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\university\Majors;

/* @var $this yii\web\View */
/* @var $model common\models\university\Courses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="courses-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'major_id')->dropDownList(Majors::getAllMajorsInArrayMap()) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
