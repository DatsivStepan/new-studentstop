<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;
use common\models\University;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\university\models\SearchUniversity */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Universities');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice"> 
        
        <div class="university-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a(Yii::t('app', 'Create University'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'label' => 'Logo',
                        'attribute' => 'image',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::tag('img', '', [
                                'src' => $model->getImages(),
                                'class' => 'img-thumbnail',
                                'style' => 'width:100px;',
                            ]);
                        }
                    ],
                    'name',
                    [
                        'attribute' => 'id_owner',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->ownerModel ? $model->ownerModel->username : null;
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->status ? University::$statusArray[$model->status] : 
                                University::$statusArray[University::STATUS_NOT_ACTIVE];
                        }
                    ],
                    [
                        'attribute' => 'region_id',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->regionModel ? $model->regionModel->name : null;
                        }
                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </section>
