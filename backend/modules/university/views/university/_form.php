<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use common\models\location\Countries;
use common\models\location\States;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\University */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="university-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="checkbox-style">
        <?= $form->field($model, 'status', [
            'template' => ' {input} <label for="university-status">' . Yii::t('app', 'Status') . '</label> {error}{hint}'
        ])->checkbox(['label' => null, 'class' => 'checkbox']); ?>
    </div>

    <?= $form->field($model, 'id_owner')->dropDownList(User::getAllInArrayMap()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'history')->widget(TinyMce::className(), [
            'options' => ['rows' => 6,'id'=>'eventsContent'],
            'language' => 'en_GB',
            'clientOptions' => [
                    'plugins' => [],
                    'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            ]
    ]); ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'year_founded')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country_id')->dropDownList([Countries::CODE_USA => 'USA']) ?>

    <?= $form->field($model, 'region_id')->dropDownList(States::getAllByCountryId(Countries::CODE_USA)) ?>

    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facebook_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'linkedin_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'twitter_link')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
