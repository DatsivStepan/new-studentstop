<?php

namespace backend\modules\cm_categories\controllers;

use yii\web\Controller;

/**
 * Default controller for the `cm_categories` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
