<?php

namespace backend\modules\question_categories\controllers;

use Yii,
    yii\web\Controller,
    common\models\business\QuestionsAnswers,
    backend\modules\question_categories\models\AnswersSearch,
    yii\web\NotFoundHttpException,
    yii\web\Response,
    common\models\pay\PayRequests;

/**
 * Default controller for the `cm_categories` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionOnConfirmation()
    {
        $searchModel = new AnswersSearch();
        $searchModel->status = QuestionsAnswers::STATUS_ON_CONFIRM;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('confirmation-answers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     */
    public function actionShowAnswer($id)
    {
        $this->layout = '/modal';
        $model = $this->findAnswerModel($id);
        return $this->render('view_answer', [
            'model' => $model,
        ]);
    }
    
    /**
     * @param integer $id
     * @return array
     */
    public function actionChangeAnswerStatus($id, $status)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findAnswerModel($id);

        $resStatus = false;
        $model->status = (int)$status;
        if ($model->save()) {
            if (($model->status == QuestionsAnswers::STATUS_REGECT) && ($model->questionModel)) {
                PayRequests::creditsPayRequest($model->user_id, $model->questionModel->user_id, $model->credit, $model->id, PayRequests::OPERATION_TYPE_QUESTION_ANSWER, 'Return money, answer wrong');
            }
            $resStatus = true;
        }

        return ['status' => $resStatus];
    }
    
    /**
     * Finds the QuestionsAnswers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QuestionsAnswers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findAnswerModel($id)
    {
        if (($model = QuestionsAnswers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
