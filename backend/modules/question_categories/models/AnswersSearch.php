<?php

namespace backend\modules\question_categories\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\business\QuestionsAnswers;

/**
 * QuestionsSearch represents the model behind the search form about `common\models\business\Questions`.
 */
class AnswersSearch extends QuestionsAnswers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'question_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['answer', 'show_answer', 'money', 'credit'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'question_id' => $this->question_id,
        ]);

        return $dataProvider;
    }
}
