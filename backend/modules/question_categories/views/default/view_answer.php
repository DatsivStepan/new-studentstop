<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
?>
<div id="answer-payed-block" data-answer_id="<?= $model->id; ?>" data-credit="<?= (float)$model->credit; ?>" data-money="<?= (float)$model->money; ?>">
    <h3 class="text-center"><?= $model->questionModel ? $model->questionModel->title : ''; ?></h3>
    <p><b>Short answer: </b><?= $model->show_answer; ?></p>
    <div class="text-center">
        <p><b>Full answer:</b><?= $model->answer; ?></p>
    </div>
</div>