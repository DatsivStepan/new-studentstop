<?php

use yii\helpers\Html,
    kartik\grid\GridView,
    yii\widgets\Breadcrumbs,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cm_categories\models\ClassifiedMarketCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Answers on confirmation');
$this->params['breadcrumbs'][] = $this->title;
?>
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        
        <div class="classified-market-categories-index">


            <?= GridView::widget([
                'id' => 'answersList',
                'dataProvider' => $dataProvider,
                'pjax' => true,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'label' => 'Question',
                        'attribute' => 'question_id',
                        'value' => function ($model) {
                            return $model->questionModel ? 
                                Html::a($model->questionModel->title, null, ['class' => 'js-show-question', 'data-id' => $model->question_id]) : 
                                '';
                        },
                        'format' => 'raw',
                    ],
                    [
                        'label' => 'Question',
                        'attribute' => 'question_id',
                        'value' => function ($model) {
                            return $model->getPrice();
                        }
                    ],
                    'show_answer',
                    [
                        'label' => 'Created',
                        'attribute' => 'created_at'
                    ],
                    [
                        'value' => function ($model) {
                            return Html::button('S', ['class' => 'btn btn-xs btn-primary js-show-answer', 'title' => 'Show']) . ' ' .
                                Html::button('G', ['class' => 'btn btn-xs btn-success js-confirm', 'title' => 'GOOT!']) . ' ' .
                                Html::button('B', ['class' => 'btn btn-xs btn-danger js-reject', 'title' => 'BAT!']);
                        },
                        'format' => 'raw',
                    ],
                ],
            ]); ?>
        </div>

    </section>