<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm,
    common\models\business\QuestionsCategories,
    kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\business\QuestionsCategories */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="classified-market-categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="checkbox-style">
        <?= $form->field($model, 'status', [
            'template' => ' {input} <label for="questionscategories-status">' . Yii::t('app', 'Status') . '</label> {error}{hint}'
        ])->checkbox(['label' => null, 'class' => 'checkbox']); ?>
    </div>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'img_src')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->widget(Select2::classname(), [
        'id' => 'dfdf',
        'data' => QuestionsCategories::getAllParentCategoris(),
        'options' => ['placeholder' => Yii::t('app', 'Select...')],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ])->label('Parent Category'); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
