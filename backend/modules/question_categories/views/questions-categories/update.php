<?php

use yii\helpers\Html,
    yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\services\ClassifiedMarketCategories */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Questions Categories',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questions Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">

        <div class="classified-market-categories-update">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
        
    </section>
