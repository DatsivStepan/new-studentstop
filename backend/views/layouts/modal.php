<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\assets\MobileAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<?= Html::csrfMetaTags() ?>
<?php $this->head() ?>
<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
<?php $this->endPage() ?>