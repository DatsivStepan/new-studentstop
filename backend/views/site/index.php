<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Studentstop
        <small>#007612</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Invoice</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 text-center">
          <h1 class="page-header text-center">
            <i class="fa fa-globe"></i> StudentStop AdminPanel
          </h1>
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
    <div class="clearfix"></div>