<?php

use yii\helpers\Html;
use common\models\Configuration;
use common\components\AdminMenuUrlManager;
$modelMenuUrl = new AdminMenuUrlManager();
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/admin/images/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= \Yii::$app->user->identity->username; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ADMIN NAVIGATION</li>
        <li>
          <a href="/admin">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Pages</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('pages/pages/create'); ?>"><a href="/admin/pages/pages/create"><i class="fa fa-circle-o"></i> Add page</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('pages/pages/index'); ?>"><a href="/admin/pages/pages/index"><i class="fa fa-circle-o"></i> List pages</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>University</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('university/university/create'); ?>"><a href="/admin/university/university/create"><i class="fa fa-circle-o"></i> Add University</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('university/university/index'); ?>"><a href="/admin/university/university/index"><i class="fa fa-circle-o"></i> List Universities</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Majors</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('majors/majors/create'); ?>"><a href="/admin/majors/majors/create"><i class="fa fa-circle-o"></i> Add Major</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('majors/majors/index'); ?>"><a href="/admin/majors/majors/index"><i class="fa fa-circle-o"></i> List Majors</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Courses</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('courses/courses/create'); ?>"><a href="/admin/courses/courses/create"><i class="fa fa-circle-o"></i> Add Course</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('courses/courses/index'); ?>"><a href="/admin/courses/courses/index"><i class="fa fa-circle-o"></i> List Courses</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Classes</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('classes/classes/create'); ?>"><a href="/admin/classes/classes/create"><i class="fa fa-circle-o"></i> Add Class</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('classes/classes/index'); ?>"><a href="/admin/classes/classes/index"><i class="fa fa-circle-o"></i> List Classes</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Users</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('users/users/create'); ?>"><a href="/admin/users/users/create"><i class="fa fa-circle-o"></i> Add user</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('users/users/index'); ?>"><a href="/admin/users/users/index"><i class="fa fa-circle-o"></i> List users</a></li>
            <li></li>
            <li class="<?= $modelMenuUrl->checkUrl('users/user-role/create'); ?>"><a href="/admin/users/user-role/create"><i class="fa fa-circle-o"></i> Add user role</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('users/user-role/index'); ?>"><a href="/admin/users/user-role/index"><i class="fa fa-circle-o"></i> List users roles</a></li>
            <li></li>
            <li class="<?= $modelMenuUrl->checkUrl('users/default/credit-requests'); ?>"><a href="/admin/users/default/credit-requests"><i class="fa fa-circle-o"></i> Credits</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Menu</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('menu/menu-items/create'); ?>"><a href="/admin/menu/menu-items/create"><i class="fa fa-circle-o"></i> Add item</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('menu/menu-items/index'); ?>"><a href="/admin/menu/menu-items/index"><i class="fa fa-circle-o"></i> List items</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Classified Market Categories</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('cm_categories/classified-market-categories/create'); ?>">
                <a href="/admin/cm_categories/classified-market-categories/create">
                    <i class="fa fa-circle-o"></i> Add category
                </a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('cm_categories/classified-market-categories/index'); ?>"><a href="/admin/cm_categories/classified-market-categories/index"><i class="fa fa-circle-o"></i> List categories</a></li>
          </ul>
        </li>
        <li class="treeview">
            <a href="#">
              <i class="fa fa-users"></i>
              <span>Question bank</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="<?= $modelMenuUrl->checkUrl('question_categories/questions-categories/create'); ?>">
                  <a href="/admin/question_categories/questions-categories/create">
                      <i class="fa fa-circle-o"></i> Add category
                  </a>
              </li>
              <li class="<?= $modelMenuUrl->checkUrl('question_categories/questions-categories/index'); ?>"><a href="/admin/question_categories/questions-categories/index"><i class="fa fa-circle-o"></i> List categories</a></li>
              <li><hr></li>
              <li class="<?= $modelMenuUrl->checkUrl('question_categories/default/on-confirmation'); ?>"><a href="/admin/question_categories/default/on-confirmation"><i class="fa fa-circle-o"></i> On confirmation</a></li>
            </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Rents Categories</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('rents_categories/rents_categories/create'); ?>">
                <a href="/admin/rents_categories/rents-categories/create">
                    <i class="fa fa-circle-o"></i> Add category
                </a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('rents_categories/rents-categories/index'); ?>">
                <a href="/admin/rents_categories/rents-categories/index"><i class="fa fa-circle-o"></i> List categories</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs"></i>
            <span>Settings</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('configuration/configuration/index'); ?>">
                <a href="/admin/configuration/configuration/index"><i class="fa fa-circle-o"></i> Configuration</a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('language/source-message/index'); ?>">
                <a href="/admin/language/source-message/index"><i class="fa fa-circle-o"></i> Translation</a>
            </li>
            <li class="<?= $modelMenuUrl->checkUrl('language/language/index'); ?>">
                <a href="/admin/language/language/index"><i class="fa fa-circle-o"></i> Languages</a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>Posts</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('posts/posts/create'); ?>"><a href="/admin/posts/posts/create"><i class="fa fa-circle-o"></i> Add post</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('posts/posts/index'); ?>"><a href="/admin/posts/posts/index"><i class="fa fa-circle-o"></i> List posts</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>