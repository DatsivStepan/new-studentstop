$(document).ready(function(){
    $(document).on('click','.cartOrderPlus',function(){
        var k = parseInt($('.sectionCartOrder table tbody tr').length)+1;
        var now_t = $.now();
        $('.sectionCartOrder table tbody').append('<tr class="cartOrder'+now_t+'">'+
                '<td>'+k+'</td>'+
                '<td><input type="url"  name="Order['+now_t+'][src]"  class="cart_order_src formStyle"  data-order_id="'+now_t+'"  placeholder="Введіть ссилку на aliexpress..." ></td>'+
                '<td><textarea name="Order['+now_t+'][comment]"  class="cart_order_comment formStyle"  data-order_id="'+now_t+'" placeholder="Введіть коментар до заказу..." rows="1" style="max-height: 110px;max-width:500px;margin: 0px"></textarea></td>'+
                '<td><span class="glyphicon glyphicon-trash deleteCartOrder" data-order_id="'+now_t+'"></span></td>'+
            '</tr>');
    })
    
    
    $(document).on('click', '.deleteCartOrder', function(){
        var order_id = $(this).data('order_id');
        $('.cartOrder'+order_id).remove();
    })
    
    $(document).on('click', '.deleteCartProduct', function(){
        var product_id = $(this).data('product_id');
        $('.cartProduct'+product_id).remove();
    })
    
    
    $(".product_count").bind('keyup mouseup', function () {
        var product_id = $(this).data('product_id');
        var product_price = $(this).data('product_price');
        var product_count = $(this).val();
        $('.cartProduct'+product_id).find('.product_sum').text((parseInt(product_price) * parseInt(product_count))+'грн');
        refreshCartSum()
    })
    
    function refreshCartSum(){
        var product_sum = 0;
        $('.productsCart table tbody tr').each(function( index ) {
            console.log($(this).find('.product_count').val());
            var product_count = $(this).find('.product_count').val()
            var product_price = $(this).find('.product_count').data('product_price')
            console.log(product_count);
            console.log(product_price);
            product_sum = product_sum + (product_price * product_count);
            $('.cartProductSum').html('Сума - <b>'+product_sum+'грн</b>')
        })
    }
    
})