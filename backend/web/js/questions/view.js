$(function(){
    
        function gootStatus (id) {
            swal({
                title: "Are you sure?",
                text: "you want confirm goot status",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        method: 'get',
                        url: '/admin/question_categories/default/change-answer-status/' + id,
                        dataType: 'json',
                        data:{status:3},
                    }).done(function (response) {
                        if (response.status) {
                            $modal.conrirmModal.close();
                            $("#answersList").yiiGridView("applyFilter");
                        }
                    })
                }
            })
        }

        function batStatus (id) {
            swal({
                title: "Are you sure?",
                text: "you want confirm bat status",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        method: 'get',
                        url: '/admin/question_categories/default/change-answer-status/' + id,
                        dataType: 'json',
                        data:{status:4},
                    }).done(function (response) {
                        if (response.status) {
                            $modal.conrirmModal.close();
                            $("#answersList").yiiGridView("applyFilter");
                        }
                    })
                }
            })
        }
    
    
        $(document).on('click', '.js-show-answer', function(){
            var id  = $(this).closest('tr').data('key')
            $.confirm({
                id: 'conrirmModal',
                title: 'Apply Question',
                content: 'url:/admin/question_categories/default/show-answer/' + id,
                columnClass: 'm',
                buttons: {
                    ok: {
                        text: 'GOOT!',
                        btnClass: 'btn btn-sm btn-success',
                        action: function (saveButton) {
                            gootStatus(id);
                            return false;
                        }
                    },
                    ok2: {
                        text: 'BAT!',
                        btnClass: 'btn btn-sm btn-danger',
                        action: function (saveButton) {
                            batStatus(id);
                            return false;
                        }
                    }
                },
            });
        })
})