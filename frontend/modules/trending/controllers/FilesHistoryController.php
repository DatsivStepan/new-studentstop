<?php

namespace frontend\modules\trending\controllers;

use Yii,
    yii\web\Controller,
    frontend\modules\university\models\SearchClasses,
    frontend\modules\business\models\QuestionsSearch,
    frontend\modules\university\models\SearchUniversity;

/**
 * Default controller for the `trending` module
 */
class FilesHistoryController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionFiles()
    {
        return $this->render('files', [
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionMarketplace()
    {
        return $this->render('marketplace', [
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionPersonalFiles()
    {
        return $this->render('personal-files', [
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionSelling()
    {
        return $this->render('selling', [
        ]);
    }
}
