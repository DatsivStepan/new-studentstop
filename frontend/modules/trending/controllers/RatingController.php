<?php

namespace frontend\modules\trending\controllers;

use Yii,
    yii\web\Controller,
    frontend\modules\university\models\SearchClasses,
    frontend\modules\business\models\QuestionsSearch,
    frontend\modules\university\models\SearchUniversity,
    frontend\models\UserSearch;

/**
 * Default controller for the `trending` module
 */
class RatingController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionUniversities()
    {
        $searchModel = new SearchUniversity();
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchByRating();

        return $this->render('universities', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionClasses()
    {
        $searchModel = new SearchClasses();
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchClassesRating();

        return $this->render('classes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionUsers()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('users', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionVirtualClass()
    {
        return $this->render('index');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionQuestionBank()
    {
        $searchModel = new QuestionsSearch();
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchRating();

        return $this->render('questions_bank', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
