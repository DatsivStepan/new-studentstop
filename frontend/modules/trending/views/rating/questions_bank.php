<?php
    use yii\helpers\Html,
        kartik\grid\GridView,
        kartik\rating\StarRating;
?>

<?= $this->render('tabs'); ?>

<?= GridView::widget([
    'id' => 'questionList',
    'dataProvider' => $dataProvider,
    'pjax' => true,
    //'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'title',
            'value' => function ($model) {
                return Html::a($model->title, null, ['class' => 'js-show-question', 'data-id' => $model->id]);
            },
            'format' => 'raw',
        ],
        [
            'label' => 'Created',
            'attribute' => 'created_at'
        ],
        [
            'label' => 'Price',
            'value' => function ($model) {
                return $model->getPrice();
            }
        ],
        [
            'value' => function ($model) {
                return StarRating::widget([
                        'name' => 'rating_q_'. $model->id,
                        'value' => $model->getRating(),
                        'disabled' => true,
                        'pluginOptions' => [
                            'size' => 'xs',
                            'min' => 0,
                            'max' => 5
                        ],
                    ]);
            },
            'format' => 'raw',
        ],
    ],
]); ?>
