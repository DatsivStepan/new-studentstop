<?php
use yii\helpers\Html,
    kartik\grid\GridView,
    kartik\rating\StarRating;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?= $this->render('tabs'); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'responsive' => false,
    'striped' => false,
    'responsiveWrap' => false,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Class',
            'attribute' => 'name',
            'value' => function ($model) {
                return $model->getName();
            },
            'format' => 'raw'
        ],
        [
            'label' => 'Rank',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->getRate(true);
            },
        ],
        [
            'label' => 'Class count',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->getClassCount();
            },
        ],
        [
            'label' => 'User count',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->getUserCount();
            },
        ],
        [
            'label' => 'Rating',
            'format' => 'raw',
            'value' => function ($model) {
                return '<div class="custom-stars">'.StarRating::widget([
                        'name' => 'rating_' . $model->id,
                        'value' => $model->getRating(),
                        'disabled' => true,
                        'pluginOptions' => [
                                'size' => 'xs',
                                'min' => 0,
                                'max' => 5
                        ],
                    ])."</div>";
            },
            'contentOptions' => ['class' => 'professor-rating']
        ]
    ],
]); ?>
