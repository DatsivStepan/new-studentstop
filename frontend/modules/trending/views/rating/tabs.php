<?php
    use yii\helpers\Html,
        yii\helpers\Url;
    use common\components\AdminMenuUrlManager;
    $modelMenuUrl = new AdminMenuUrlManager();
?>
<div class="text-center">
    <ul class="nav nav-pills">
        <li class="nav-item">
            <?= Html::a('Universities', Url::to(['/trending/rating/universities']), ['class' => 'nav-link ' . $modelMenuUrl->checkUrl('trending/rating/universities')])?>
        </li>
        <li class="nav-item">
            <?= Html::a('Classes', Url::to(['/trending/rating/classes']), ['class' => 'nav-link ' . $modelMenuUrl->checkUrl('trending/rating/classes')])?>
        </li>
        <li class="nav-item">
            <?= Html::a('Users', Url::to(['/trending/rating/users']), ['class' => 'nav-link ' . $modelMenuUrl->checkUrl('trending/rating/users')])?>
        </li>
        <li class="nav-item">
            <?= Html::a('Virtual classes', Url::to(['/trending/rating/virtual-class']), ['class' => 'nav-link ' . $modelMenuUrl->checkUrl('trending/rating/virtual-class')])?>
        </li>
        <li class="nav-item">
            <?= Html::a('Question Bank', Url::to(['/trending/rating/question-bank']), ['class' => 'nav-link ' . $modelMenuUrl->checkUrl('trending/rating/question-bank')])?>
        </li>
    </ul>
</div>