<?php
    use yii\helpers\Html,
        yii\helpers\Url;

    use common\components\AdminMenuUrlManager;
    $modelMenuUrl = new AdminMenuUrlManager();
?>
<div class="text-center">
    <ul class="nav nav-pills">
        <li class="nav-item">
            <?= Html::a('Files', Url::to(['/trending/files-history/files']), ['class' => 'nav-link ' . $modelMenuUrl->checkUrl('trending/files-history/files')])?>
        </li>
        <li class="nav-item">
            <?= Html::a('Marketplace', Url::to(['/trending/files-history/marketplace']), ['class' => 'nav-link ' . $modelMenuUrl->checkUrl('trending/files-history/marketplace')])?>
        </li>
        <li class="nav-item">
            <?= Html::a('Personal Files', Url::to(['/trending/files-history/personal-files']), ['class' => 'nav-link ' . $modelMenuUrl->checkUrl('trending/files-history/personal-files')])?>
        </li>
        <li class="nav-item">
            <?= Html::a('Selling', Url::to(['/trending/files-history/selling']), ['class' => 'nav-link ' . $modelMenuUrl->checkUrl('trending/files-history/selling')])?>
        </li>
    </ul>
</div>