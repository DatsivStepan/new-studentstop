<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    kartik\date\DatePicker,
    yii\widgets\Pjax,
    yii\bootstrap\Alert;

//$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBLaan_dFaE2Nxr9HkJwZJOSHYUeKo4c');

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\SearchModels */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div id="user-info-block">
    <?php Pjax::begin(['id' => 'pjax-form-user-info', 'enablePushState' => false]); ?>
        <?= Html::a('<- Back', null, ['class' => 'js-hide-modal'])?>
        <br>
        <br>

        <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
            <script>
                $modal.userAboutModal.close();
                swal({
                    title: "Congratulations! Profile updated!",
                    html: true,
                    icon: "success",
                    confirmButtonText: "Ok",
                  });
            </script>
        <?php } ?>

        <?php if (($type != 'update') && ($model->id != Yii::$app->user->id)) { ?>
            <?= Html::button($model->checkFriendRequestType(), [
                    'class' => 'js-friend-request-button student-card-btn',
                    'data-user_id' => Yii::$app->user->id,
                    'data-friend_id' => $model->id,
                ]); ?>
            <?= Html::button('Mail to user', ['class' => 'js-mail-to-user', 'data-myId' => Yii::$app->user->id, 'data-user_id' => $model->id]); ?>
        <?php } ?>

        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#maptab" role="tab">Map</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="profile" role="tabpanel">
                    <div class="col-sm-12" style="padding:10px;">
                        <?php if ($type == 'update') { ?>

                            <?php $form = ActiveForm::begin(['id' => 'form', 'options' => ['data-pjax' => 'pjax-form-user-info']]); ?>
                                <?= $form->field($userInfo, 'position_lat')->hiddenInput()->label(false); ?>
                                <?= $form->field($userInfo, 'position_lng')->hiddenInput()->label(false); ?>

                                <div style="width:200px;margin:0 auto;">
                                    <?= $form->field($userInfo, 'avatar')->hiddeninput()->label(false); ?>
                                    <img src="<?= $userInfo->getAvatar(); ?>" class="js-preview-userinfo-image img-thumbnail"><br>
                                    <a class="btn btn-sm btn-success js-add-userinfo-image" style="width:100%">Change image</a><br><br>
                                </div>

                                <?= $form->field($userInfo, 'name')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($userInfo, 'surname')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($userInfo, 'birthday')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Enter birth date ...'],
                                    'type' => DatePicker::TYPE_INPUT,
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'yyyy-mm-dd',
                                    ]
                                ]); ?>

                                <?= $form->field($userInfo, 'address')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($userInfo, 'mobile')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($userInfo, 'about')->textarea() ?>

                            <?php ActiveForm::end(); ?>
                        <?php } else { ?>
                            <div style="width:200px;margin:0 auto;">
                                <img src="<?= $userInfo->getAvatar(); ?>" class="img-thumbnail"><br>
                            </div>

                            <h3><?= $userInfo->getNameAndSurname(); ?></h3>
                            <p><?= $userInfo->birthday; ?></p>
                            <p><?= $userInfo->address; ?></p>
                            <p><?= $userInfo->mobile; ?></p>
                            <p><?= $userInfo->about; ?></p>
                        <?php } ?>
                    </div>
            </div>
            <div class="tab-pane" id="maptab" role="tabpanel">
                  <div id="map" style="height:500px;"></div>
            </div>
        </div>
    <?php Pjax::end();  ?>
</div>
<script>
    var autocomplete;
    var latInput = 'userinfo-position_lat';
    var lngInput = 'userinfo-position_lng';
    
    function initAutocomplete() {

        var mapOptions = {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13,
            minZoom: 8,
            maxZoom: 12,
        };

        var lat = document.getElementById(latInput).value;
        var lng = document.getElementById(lngInput).value;
        var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};

        if (lat && lng) {
            mapOptions.center.lat = parseFloat(lat);
            mapOptions.center.lng = parseFloat(lng);
        }

        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('userinfo-address')),
            {types: ['geocode']});

        autocomplete.addListener('place_changed', fillInAddress);

        function fillInAddress() {
            var place = autocomplete.getPlace();

            document.getElementById(latInput).value = place.geometry.location.lat();
            document.getElementById(lngInput).value = place.geometry.location.lng();
        }
    }
    
    $(function(){
        initAutocomplete();
        $('#user-info-block').on('click', '.js-hide-modal', function(){
            $modal.userAboutModal.close()
        })

        $dropzone.initializationDropzoneEleemnt('userinfo', '#userinfo-avatar', 'image/*');
    })
</script>