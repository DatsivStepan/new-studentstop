<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\university\models\SearchCourses */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row no-gutters courses-index">
        <div class="col-12 left-content">
            <div class="classes-box">
                <div class="classes-head">My Classes</div>
                <?php Pjax::begin(['id' => 'pjax-form', 'enablePushState' => false]); ?>
                    <div style="display:none;">
                        <?php $form = ActiveForm::begin(['id' => 'form-search-classes', 'options' => ['data-pjax' => 'pjax-form']]); ?>
                        <?= $form->field($searchModel, 'course_id')->textInput(); ?>
                            <?php ActiveForm::end(); ?>
                    </div>
                    <?php if ($dataProvider->getModels()) { ?>
                        <div class="js-classes-block classes-block my-scroll">
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'responsive' => false,
                                'striped' => false,
                                'responsiveWrap' => false,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    'name',
                                    [
                                        'attribute' => 'professor',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            $professor = $model->professorModel;
                                            return $professor ? 
                                                    Html::tag('img', '', [
                                                        'src' => $professor->getAvatar(),
                                                        'class' => 'professor-avatar',
                                                    ]).
                                                    Html::tag('div', $professor->getNameAndSurname(), [
                                                        'class' => 'professor-name',
                                                    ])
                                                    : null;
                                        },
                                        'contentOptions' => ['class' => 'professor-data']
                                    ],
                                    [
                                        'attribute' => 'status',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return $model->status;
                                        },
                                        'contentOptions' => ['class' => 'professor-status']
                                    ],
                                    [
                                        'label' => 'Students',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return $model->getStudentsCount();
                                        },
                                        'contentOptions' => ['class' => 'professor-students']
                                    ],
                                    [
                                        'label' => 'Rank',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return $model->getRank();
                                        },
                                        'contentOptions' => ['class' => 'professor-rank']
                                    ],
                                    [
                                        'label' => 'Rating',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return '<div class="custom-stars">'.StarRating::widget([
                                                    'name' => 'rating_' . $model->id,
                                                    'value' => $model->getRating(),
                                                    'disabled' => true,
                                                    'pluginOptions' => [
                                                            'size' => 'xs',
                                                            'min' => 0,
                                                            'max' => 5
                                                    ],
                                                ])."</div>";
                                        },
                                        'contentOptions' => ['class' => 'professor-rating']
                                    ],
                                    [
                                        'label' => '',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            $joinStatus = $model->getJoinClassText();

                                            return Html::a($joinStatus['text'], 
                                                $joinStatus['link'],
                                                [
                                                    'class' => 'js-join-classes action-btn',
                                                    'target' => '_blank',
                                                    'data-id' => $model->id,
                                                ]);
                                        },
                                        'contentOptions' => ['class' => 'professor-btn']
                                    ],
                                ],
                            ]); ?>
                        </div>
                    <?php } else { ?>
                        <p class="text-center">YOU DONT HAVE ANY CLASSES YET. START BUILDING YOUR FIRST COURSE <?= Html::a('(+)', yii\helpers\Url::to(['/university/classes/my-classes']), ['class' => 'btn btn-xs btn-primary'])?></p>
                    <?php } ?>
                <?php Pjax::end();  ?>
            </div>
        </div>
    </div>