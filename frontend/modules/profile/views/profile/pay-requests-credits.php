<?php

use yii\helpers\Html,
    kartik\grid\GridView,
    yii\widgets\Pjax,
    yii\widgets\ActiveForm,
    kartik\rating\StarRating,
    frontend\components\helpers\FunctionHelper;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\university\models\SearchCourses */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Credits');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row no-gutters courses-index">
        <div class="col-12 left-content">
            <div class="classes-box">
                <div class="classes-head">Total Credits - <?= $modelU->getCreditsAmount(); ?> credits <?= Html::button('Buy Credits', ['class' => 'btn btn-sm btn-info']); ?></div>
                    <?php if ($dataProvider->getModels()) { ?>
                        <div class="js-classes-block classes-block my-scroll">
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'responsive' => false,
                                'striped' => false,
                                'responsiveWrap' => false,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    [
                                        'attribute' => 'module',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return $model->getModule();
                                        },
                                    ],
                                    [
                                        'attribute' => 'amount',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return FunctionHelper::numberFormat($model->amount);
                                        },
                                    ],
                                    [
                                        'attribute' => 'amount',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return FunctionHelper::numberFormat($model->amount);
                                        },
                                    ],
                                    [
                                        'label' => 'Type Request',
                                        'format' => 'raw',
                                        'value' => function ($model) use ($modelU) {
                                            return $model->to_user_id == $modelU->id ? 'plus' : 'minus';
                                        },
                                    ],
                                    [
                                        'label' => 'From user',
                                        'format' => 'raw',
                                        'value' => function ($model) use ($modelU) {
                                            return $model->from_user_id == common\models\pay\PayRequests::FOR_USER_ADMIN ? 'Administration site' : ($model->from_user_id == $modelU->id ? ($model->toUserModel ? $model->toUserModel->getNameAndSurname() : '') :  ($model->fromUserModel ? $model->fromUserModel->getNameAndSurname() : ''));
                                        },
                                    ],
                                    'created_at'
                                ],
                            ]); ?>
                        </div>
                    <?php } else { ?>
                        <p class="text-center">YOU DONT HAVE Operations</p>
                    <?php } ?>
            </div>
        </div>
    </div>