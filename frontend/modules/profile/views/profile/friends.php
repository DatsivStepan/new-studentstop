<?php

use yii\helpers\Html,
    kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\SearchModels */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Profile friends');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row no-gutters">
    <div class="col-12 left-content">
        <div class="row no-gutters students-box">
            <?php foreach ($dataProvider->getModels() as $model) { ?>
            <?php $friend = $model->friendModel; ?>
            <div class="col-auto student-card">
                <div class="student-card-content">
                    <div class="student-card-avatar" style="background: url(<?= $friend->getAvatar(); ?>) center/cover no-repeat;">
                    </div>
                    <div class="student-card-name online">
                        <?= $friend->getNameAndSurname(); ?>
                    </div>
                    <div class="student-card-status"> Status:
                        <span> <?= $friend->getRole(); ?></span>
                    </div>
                    <div class="student-card-rating"> 
                        <div class="rating-title">
                            Rank: <?= $friend->getRankCount(); ?>
                        </div>
                        <div class="custom-stars">
                            <?= StarRating::widget([
                                'name' => 'rating_20',
                                'value' => $friend->getRankCount(),
                                'disabled' => true,
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'min' => 0,
                                    'max' => 5
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div>
                        <?= Html::button($friend->checkFriendRequestType(), [
                            'class' => 'js-friend-request-button student-card-btn',
                            'data-user_id' => Yii::$app->user->id,
                            'data-friend_id' => $friend->id,
                        ])?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>