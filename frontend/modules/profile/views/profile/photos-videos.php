<?php

use yii\helpers\Html,
    yii\helpers\Url,
    yii\widgets\Pjax,
    yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\SearchModels */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Photos & Videos');
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::to(['/css/plugins/dropzone.css']));
$this->registerJsFile(Url::to(['/js/plugins/dropzone.js']));
?>
<style>
    .removePhoto {
        z-index: 9999;
        position: absolute;
        right: -10px;
        top: -10px;
    }
</style>
<div id="user-files-index">

    <?php Pjax::begin(['id' => 'pjax-form-files', 'enablePushState' => false]); ?>

        <div class="drag-and-drop user_photos" action="/site/upload-photo?type=profile" class="">
            <div class="dz-message needsclick">
                <h4>Drop files here or click to upload.</h4>
            </div>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'form', 'options' => ['data-pjax' => 'pjax-form-files']]); ?>
            <?= Html::hiddenInput('uploadsfiles', '', [])?>
            <?= Html::submitButton(Yii::t('profile', 'Save'), ['class' => 'btn btn-info']) ?>
        <?php ActiveForm::end(); ?>

        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#image">Images</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#video">Videos</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane container active" id="image">
                <?php if ($images) { ?>
                    <?php foreach ($images as $image) { ?>
                        <img src="/<?= $image->file_src; ?>" style="width:150px;">
                    <?php } ?>
                <?php } else { ?>
                        <p class="text-center">not result</p>
                <?php } ?>
            </div>

            <div class="tab-pane container fade" id="video">
                <?php if ($videos) { ?>
                    <?php foreach ($videos as $video) { ?>
                        <video width="150" controls>
                            <source src="/<?= $video->file_src; ?>">
                        </video>
                    <?php } ?>
                <?php } else { ?>
                        <p class="text-center">not result</p>
                <?php } ?>
            </div>
        </div>

        <script>
            $(function(){
                var photosArray = {};
                if($('.user_photos').length){

                    var profDropzone = new Dropzone($('.user_photos')[0], { // Make the whole body a dropzone
                        //maxFilesize: 50,
                        dictDefaultMessage: 'message',
                        acceptedFiles:'image/*,video/*',
                    });

                    profDropzone.on("complete", function(response) {
                        if (response.status == 'success') {
                            var fileType = response.type;

                            if(typeof photosArray[response.xhr.response] == 'undefined'){
                                photosArray[response.xhr.response] = {};
                            }

                            photosArray[response.xhr.response] = fileType;
                            $(response.previewElement).prepend('<i class="fa fa-times removePhoto fa-2x" title="delete" data-name="'+response.name+'"></i>');
                            $('#form [name=uploadsfiles]').val(JSON.stringify(photosArray));
                        }
                    });
                    profDropzone.on("removedfile", function(response) {
                        var removeItem = response.xhr.response;
                        delete(photosArray[removeItem]);
                        $('#form [name=uploadsfiles]').val(JSON.stringify(photosArray));
                    });

                    $('#user-files-index').on('click','.removePhoto',function(){
                        var shos = $(this).parent();

                        var name_photo = $(this).data('name');
                        $.each( profDropzone.files, function( key, value ) {
                            if(value.name == name_photo){
                                profDropzone.removeFile(profDropzone.files[key]);
                            }
                        });
                    });
                }
            })
        </script>
    
    <?php Pjax::end(); ?>

</div>