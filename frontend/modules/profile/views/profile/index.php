<?php

use yii\helpers\Html,
    yii\grid\GridView,
    common\models\user\UserProfileEvents;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\SearchModels */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = $model->getNameAndSurname();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?> timeline</h1>

    <div class="row">
    <?php if ($models = $dataProvider->getModels()) { ?>
        <?php foreach ($models as $model) { ?>
            <div class="col-sm-3">
                <h4><?= $model->userModel ? Html::a($model->userModel->getNameAndSurname(), $model->userModel->getProfileLink()) : ''; ?></h4>
                <?php
                    switch ($model->events_type) {
                        case UserProfileEvents::TYPE_ADD_PHOTO:
                            if ($userFiles = $model->userFilesModel) {
                            ?>
                                <p>add photo</p>
                                <p><?= $model->getDateCreate(); ?></p>
                                <img src="/<?= $userFiles->file_src; ?>" class="img-thumbnail" style="width:150px;">
                            <?php
                            }
                            break;
                        case UserProfileEvents::TYPE_ADD_VIDEO:
                            if ($userFiles = $model->userFilesModel) {
                            ?>
                                <p><?= $model->getDateCreate(); ?></p>
                                <video width="150" controls>
                                    <source src="/<?= $userFiles->file_src; ?>">
                                </video>
                            <?php
                            }
                            break;
                        case UserProfileEvents::TYPE_CHANGE_PROFILE_DATA:
                            ?>
                            <p><?= $model->getDateCreate(); ?></p>
                            <p><?= $model->note; ?></p>
                        <?php 
                            break;
                        case UserProfileEvents::TYPE_CHANGE_PROFILE_AVATAR:
                            ?>
                                <p><?= $model->getDateCreate(); ?></p>
                                <p>Change avatar</p>
                                <img src="/<?= $model->note; ?>" class="img-thumbnail" style="width:150px;">
                            <?php
                        default:
                            break;
                    }  
                ?>
            </div>
        <?php } ?>
    <?php } ?>
    </div>
</div>
