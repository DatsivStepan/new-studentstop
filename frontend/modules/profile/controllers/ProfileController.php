<?php

namespace frontend\modules\profile\controllers;

use Yii,
    yii\web\Response,
    yii\web\Controller,
    common\models\User,
    yii\filters\VerbFilter,
    common\models\user\UserInfo,
    common\models\user\UserFiles,
    yii\web\NotFoundHttpException,
    common\models\user\UserFriend,
    frontend\modules\profile\models\SearchModels,
    frontend\modules\profile\models\SearchFriends,
    frontend\modules\university\models\SearchClasses,
    frontend\modules\profile\models\SearchUserProfileEvents,
    frontend\modules\profile\models\SearchPayRequests,
    common\models\pay\PayRequests;

/**
 * ProfileController implements the CRUD actions for User model.
 */
class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        Yii::$app->params['menuType'] = \common\models\MenuItems::TYPE_PROFILE;
        
        return parent::beforeAction($action);
    }
    
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        $model = $this->findModel($id);
        $searchModel = new SearchUserProfileEvents();
        $searchModel->user_id = $id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetMyFriends($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $result = ['status' => false, 'data' => null];
        $value = Yii::$app->request->post('value');
        if ($data = UserFriend::getMyFriendArray(Yii::$app->user->id, $value)) {
            $result = ['status' => true, 'data' => $data];
        }
        return $result;
    }

    public function actionCredits($id = null, $operationType = null, $operationId = null)
    {
        $model = $this->findModel($id);

        $searchModel = new SearchPayRequests();
        $searchModel->typePay = PayRequests::TYPE_PAY_CREDIT;
        $searchModel->userFilterid = $model->id;
        $searchModel->operationType = $operationType;
        $searchModel->id = $operationId;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('pay-requests-credits', [
            'modelU' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMoney($id = null, $operationType = null, $operationId = null)
    {
        $model = $this->findModel($id);

        $searchModel = new SearchPayRequests();
        $searchModel->typePay = [PayRequests::TYPE_PAY_MONEY, PayRequests::TYPE_PAY_PAYPAL];
        $searchModel->userFilterid = $model->id;
        $searchModel->operationType = $operationType;
        $searchModel->id = $operationId;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('pay-requests-money', [
            'modelU' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer|null $id 
     * @return mixed
     */
    public function actionFriends($id = null)
    {
        $model = $this->findModel($id);

        $searchModel = new SearchFriends();
        $searchModel->user_id = $model->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('friends', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCoursesClasses($id = null)
    {
        $model = $this->findModel($id);

        $searchModel = new SearchClasses();
        $dataProvider = $searchModel->searchMyClasses();

        return $this->render('classes', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAbout($id = null)
    {
        $model = $this->findModel($id);
        $userInfo = UserInfo::findOne(['id_user' => $model->id]);
        if (!$userInfo) {
            $userInfo = new UserInfo();
        }
        if (Yii::$app->request->isPost && $userInfo->load(Yii::$app->request->post())) {
            if ($userInfo->save()) {
                Yii::$app->session->setFlash('successSave');
            } else {
                Yii::$app->session->setFlash('notSaveData');
            }
        }

        $this->layout = '/modal';
        return $this->render('about', [
            'model' => $model,
            'userInfo' => $userInfo,
            'type' => $model->id == Yii::$app->user->id ? 'update' : 'view',
        ]);
    }

    public function actionPhotosVideos($id = null)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost && array_key_exists('uploadsfiles', Yii::$app->request->post())) {
            if ($files = json_decode(Yii::$app->request->post('uploadsfiles'))) {
                foreach ($files as $src => $mType) {
                    $modelF = new UserFiles;
                    $modelF->user_id = $model->id;
                    $modelF->file_src = $src;
                    $type = null;
                    if (strpos($mType, 'video') !== false) {
                        $type = 'video';
                    }
                    if (strpos($mType, 'image') !== false) {
                        $type = 'image';
                    }
                    if ($type != null) {
                        $modelF->type = $type;
                        $modelF->save();
                    }
                }
            }
        }

        return $this->render('photos-videos', [
            'model' => $model,
            'images' => UserFiles::find()->where(['user_id' => $model->id, 'type' => 'image'])->all(),
            'videos' => UserFiles::find()->where(['user_id' => $model->id, 'type' => 'video'])->all()
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id = null)
    {
        if (($model = User::findOne($id ? $id : Yii::$app->user->id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
