<?php

namespace frontend\modules\profile\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\user\UserProfileEvents;

/**
 * SearchModels represents the model behind the search form about `common\models\User`.
 */
class SearchUserProfileEvents extends UserProfileEvents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'events_id', 'events_type', 'status'], 'integer'],
            [['created_at', 'updated_at', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()
            ->alias('ue');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ue.id' => $this->id,
            'ue.user_id' => $this->user_id,
        ]);

        return $dataProvider;
    }
}
