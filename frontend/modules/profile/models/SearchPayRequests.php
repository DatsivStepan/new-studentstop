<?php

namespace frontend\modules\profile\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\pay\PayRequests;

/**
 * SearchModels represents the model behind the search form about `common\models\User`.
 */
class SearchPayRequests extends PayRequests
{
    public $userFilterid;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'typePay', 'module', 'from_user_id', 'to_user_id', 'objectId', 'userFilterid'], 'integer'],
            [['amount'], 'number'],
            [['note'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['operationType'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()
                ->alias('pay')
                ->andWhere(['typePay' => $this->typePay]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'OR',
            ['pay.from_user_id' => $this->userFilterid],
            ['pay.to_user_id' => $this->userFilterid]
        ]);

        $query->andFilterWhere([
            'pay.id' => $this->id,
            'pay.operationType' => $this->operationType,
        ]);

        return $dataProvider;
    }
}
