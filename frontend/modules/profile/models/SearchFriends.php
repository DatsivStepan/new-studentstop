<?php

namespace frontend\modules\profile\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\user\UserFriend;

/**
 * SearchModels represents the model behind the search form about `common\models\User`.
 */
class SearchFriends extends UserFriend
{
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'user_id', 'friend_id'
                ],
                'integer'
            ],
            [
                [
                    'username'
                ],
                'string'
            ],
            [
                [
                    'created_at', 'updated_at'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
                ->alias('uf')
                ->joinWith(['friendModel f']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'uf.id' => $this->id,
            'uf.user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'f.username', $this->username]);

        return $dataProvider;
    }
}
