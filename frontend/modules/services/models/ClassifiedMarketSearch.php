<?php

namespace frontend\modules\services\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\services\ClassifiedMarket;

/**
 * ClassifiedMarketSearch represents the model behind the search form about `common\models\services\ClassifiedMarket`.
 */
class ClassifiedMarketSearch extends ClassifiedMarket
{
    public $favorites;
    public $searchValue;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id', 'user_id', 'university_id', 'category_id',
                    'favorites'
                ],
                'integer'
            ],
            [
                [
                    'title', 'image_src', 'content', 'address', 'created_at', 'updated_at', 'searchValue'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = ClassifiedMarket::find()
            ->alias('cm')
            ->select(['cm.*', 'type' => 'favorites.type'])
            ->joinWith(['favoritesModel favorites']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->favorites) {
            $query->andFilterWhere([
                'favorites.user_id' => Yii::$app->user->id
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cm.id' => $this->id,
            'cm.user_id' => $this->user_id,
            'cm.university_id' => $this->university_id,
            'cm.category_id' => $this->category_id,
        ]);
        
        if ($this->searchValue) {
            $query->andFilterWhere([
                "OR",
                ['like', 'cm.title', $this->searchValue],
                ['like', 'cm.content', $this->searchValue],
                ['like', 'cm.address', $this->searchValue],
            ]);
        }
        $query->andFilterWhere(['like', 'cm.title', $this->title])
            ->andFilterWhere(['like', 'cm.content', $this->content])
            ->andFilterWhere(['like', 'cm.address', $this->address]);

        return $dataProvider;
    }
}
