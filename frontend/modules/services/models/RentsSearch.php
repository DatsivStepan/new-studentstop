<?php

namespace frontend\modules\services\models;

use Yii,
    yii\base\Model,
    yii\data\ActiveDataProvider,
    common\models\services\Rents,
    common\models\services\RentsBooking,
    yii\db\Expression;

/**
 * RentsSearch represents the model behind the search form about `common\models\services\Rents`.
 */
class RentsSearch extends Rents
{
    public $booking_from;
    public $booking_to;
    public $favorites;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id', 'user_id', 'university_id', 'type', 'type_pay', 'publish', 'status',
                    'favorites', 'category_id'
                ],
                'integer'
            ],
            [
                [
                    'title', 'content', 'img_src', 'address', 'lat', 'lng', 'created_at', 'updated_at',
                    'booking_from', 'booking_to'
                ],
                'safe'
            ],
            [['credits', 'money'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        if ($this->booking_from || $this->booking_to) {
            if ($this->booking_from && $this->booking_to) {
                if ($this->booking_to < $this->booking_from) {
                    $bookingFromT = $this->booking_from;
                    $this->booking_from = $this->booking_to;
                    $this->booking_to = $bookingFromT;
                }

                $interval = date_diff(date_create($this->booking_from), date_create($this->booking_to));
                if (($interval->days < 1) && ($interval->h < 1)) {
                    $this->booking_to = date('Y-m-d H:i', strtotime($this->booking_from . ' +1 hour'));
                }
            }
        }
        
        $query = Rents::find()
            ->alias('rents')
            ->select([
                'rents.*',
                'bookC' => $this->booking_from && $this->booking_to ? '(SELECT count(`b`.id) FROM '.\common\models\services\RentsBooking::tableName().
                        ' b WHERE b.rent_id = rents.id AND ('
                . '(`b`.`id` IS NULL) OR '
                . '(`b`.`reserve_from` BETWEEN "' . $this->booking_from . '" AND "' . $this->booking_to . '") OR (`b`.`reserve_to` BETWEEN "' . $this->booking_from . '" AND "' . $this->booking_to . '") OR '
                . '((`b`.`reserve_from` < "' . $this->booking_from . '") AND (`b`.`reserve_to` > "' . $this->booking_to . '") )'
                . ')'
                . ')' : new Expression("0")
                ])
            ->joinWith([
                'rentsBookingModel book',
                'favoritesModel favorites'
            ])
                ->andWhere([
                    'OR',
                    ['book.id' => null],
                    ['book.rent_type' => RentsBooking::TYPE_RENT]
                ])
                ->distinct();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->having(['bookC' => 0]);

//        var_dump($query->createCommand());exit;
        
        if ($this->favorites) {
            $query->andFilterWhere([
                'favorites.user_id' => Yii::$app->user->id
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rents.id' => $this->id,
            'rents.user_id' => $this->user_id,
            'rents.university_id' => $this->university_id,
            'rents.category_id' => $this->category_id,
        ]);
//
        $query->andFilterWhere(['like', 'address', $this->address]);
//            ->andFilterWhere(['like', 'content', $this->content])
//            ->andFilterWhere(['like', 'img_src', $this->img_src])
//            ->andFilterWhere(['like', 'address', $this->address])
//            ->andFilterWhere(['like', 'lat', $this->lat])
//            ->andFilterWhere(['like', 'lng', $this->lng]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function mySearch()
    {
        $query = self::find()
                ->alias('rents')
                ->select(['rents.*', 'type' => 'favorites.type'])
                ->joinWith(['favoritesModel favorites'])
                ->andWhere(['rents.user_id' => Yii::$app->user->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
