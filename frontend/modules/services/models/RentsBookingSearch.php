<?php

namespace frontend\modules\services\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\services\RentsBooking;

/**
 * RentsSearch represents the model behind the search form about `common\models\services\Rents`.
 */
class RentsBookingSearch extends RentsBooking
{
    public $showType;
    public $rentOwnerId;
    public $rentBookingOwnerId;
    public $payType;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'rent_id', 'user_id', 'status', 'rentOwnerId', 'payType', 'rentBookingOwnerId'
                ],
                'integer'
            ],
            [
                [
                    'date_reserve', 'reserve_from', 'reserve_to', 'rating_count',
                    'rating_comment', 'created_at', 'updated_at', 'showType'
                ],
                'safe'
            ],
            [['rent_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($rentType = null)
    {
        $query = self::find()
                ->alias('rb')
                ->joinWith(['rentModel r', 'rentHouseModel rh']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if ($this->showType == RentsBooking::SHOW_TYPE_RECENT) {
            $query->andFilterWhere([
                '>=', 'rb.reserve_to', date('Y-m-d H:i')
            ]);
        } else if ($this->showType == RentsBooking::SHOW_TYPE_ARCHIVE) {
            $query->andFilterWhere([
                '<', 'rb.reserve_to', date('Y-m-d H:i')
            ]);
        }

        if ($this->rentOwnerId) {
            $query->andFilterWhere([
                'OR',
                ['r.user_id' => $this->rentOwnerId, 'rb.rent_type' => RentsBooking::TYPE_RENT],
                ['rh.user_id' => $this->rentOwnerId, 'rb.rent_type' => RentsBooking::TYPE_HOUSE],
            ]);
        }
            
        if ($this->rentBookingOwnerId) {
            $query->andFilterWhere(['rb.user_id' => $this->rentBookingOwnerId]);
        }

        if ($this->payType) {
            $query->andFilterWhere([
                'OR',
                ['r.type_pay' => $this->payType, 'rb.rent_type' => RentsBooking::TYPE_RENT],
                ['rh.type_pay' => $this->payType, 'rb.rent_type' => RentsBooking::TYPE_HOUSE],
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rb.id' => $this->id,
//            'rb.user_id' => $this->user_id,
            'rb.rent_id' => $this->rent_id,
            'rb.rent_type' => $rentType
        ]);

        return $dataProvider;
    }

}
