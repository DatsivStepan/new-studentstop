<?php

use yii\helpers\Html,
    yii\helpers\Url,
    common\models\Lang;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\services\models\ClassifiedMarketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$lgType = Lang::CATEGORY_TYPE_CLASS_MARKET;

$this->title = Yii::t($lgType, 'Classified Markets');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classified-market-index class-market">

    <?= $this->render('_search', [
        'model' => $searchModel,
        'button' => false
    ]); ?>

    <div class="row all-classfied">
        <?php if ($categories) { ?>
            <?php foreach ($categories as $category) { ?>
                <?php if ($childCategories = $category->getChildCategory(3)) { ?>
                    <div class="col-sm-3 classified-block js-category-block">
                        <div class="classified-block-my">
                            <h2 class="js-category-name"><?= $category->name; ?></h2>
                            <?php foreach ($childCategories as $c_category) { ?>
                                <div class="row another-cl">
                                    <div class="col-10 name-category">
                                         <?= Html::a($c_category->name, $c_category->getLink(), ['class' => '']); ?>
                                    </div>
                                   <div class="col-2 number-block">
                                       <div class="number"><?= $c_category->getProductCount(); ?></div>
                                   </div>
                                </div>
                            <?php } ?>
                            <?php if ($category->getChildCategoryCount() > 3) { ?>
                                <div class="button-cl" style="cursor:pointer;">
                                    <?= Html::a(Yii::t($lgType, 'more'), null, [
                                        'class' => 'js-show-more-categories',
                                        'data-key' => $category->id
                                    ])?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>

</div>


<script>
    $(function(){
        $(document).on('click', '.js-show-more-categories', function() {
            var id = $(this).data('key')
            var categoryName = $(this).closest('.js-category-block').find('.js-category-name').text();
            $.dialog({
                id: 'moreCategoriesModal',
                title: categoryName,
                content: 'url:<?= Url::to(['/services/classified-market/more-categoires']) ?>/' + id,
                columnClass: 'm view-block modal-block'
            });
        })
    })
</script>