<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\services\ClassifiedMarket */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Classified Market',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Classified Markets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="classified-market-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
