<?php
use kartik\rating\StarRating,
    yii\helpers\Html,
    common\models\Favorites;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="col-sm-3 classif-search-block js-item-content-block" data-id="<?= $model->id; ?>">
    <div class="searc-block-my">
        <img class="image" src="<?= $model->getImages(); ?>" style="width:100%;">
        <div class="icon-block icon-star">
            <?php $fStatus = Favorites::getFavoriteStatus(Favorites::TYPE_CLASSIFIELD_MARKET, $model->id); ?>
            <?= Html::tag('i', '', [
                'class' => 'glyphicon glyphicon-star cm-favorite-star js-favorite-star-button ' . ($fStatus ? 'active' : ''),
                'data-type' => Favorites::TYPE_CLASSIFIELD_MARKET
            ]); ?>
        </div>
        <h2><?= $model->title; ?></h2>
        <?= StarRating::widget([
            'name' => 'rating_20',
            'value' => $model->getRating(),
            'disabled' => true,
            'pluginOptions' => [
                'size' => 'xs',
                'min' => 0,
                'max' => 5
            ],
        ]); ?>
        <div class="row">
            <div class="col-6">
                <div class="row">
                    <div class="col-12 classif-text-block text-bl">
                        <p><?= $model->getAuthorName(); ?></p>
                    </div>
                    <div class="col-12 date text-bl">
                        <p><?= $model->getDateCreate('d/m/Y'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-6 button-block">
                <?= Html::button('View', [
                    'class' => 'view-button btn btn-info js-show-classified-market',
                    'data-key' => $model->id,
                    'data-title' => $model->title
                ]); ?>
            </div>
        </div>
    </div>
</div>

