<?php

use yii\helpers\Html,
    common\models\Favorites,
    kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model common\models\services\ClassifiedMarket */

$this->title = $model->title;
?>
<div class="classified-market-view">
    <div class="row" style="margin: 0px;">
        <div class="col-12" style="text-align: center;">
            <img class="image" src="<?= $model->getImages(); ?>" style="width:100px;">
        </div>
    </div>
    <div class="row view-bl">
        <div class="col-6 col-sm-1" style="text-align: center;"><img class="image" src="<?= $model->getAuthorAvatar(); ?>"></div>
        <div class="col-6 col-sm-4  classif-text-block text-bl">
            <p><?= $model->getAuthorName(); ?></p>
        </div>
        <div class="col-6 col-sm-5 star-block"><?= StarRating::widget([
            'id' => 'rating_view_'.$model->id,
            'name' => 'rating_view_'.$model->id,
            'value' => $model->getRating(),
            'disabled' => true,
            'pluginOptions' => [
                'size' => 'xs',
                'min' => 0,
                'max' => 5
            ],
        ]); ?></div>
        <div class="col-6 col-sm-2 text-bl"> <p><?= $model->getDateCreate('d/m/Y'); ?></p></div>
    </div>
    <div class="row" style="margin: 0px;">
        <?php if (!$model->isOwner()) { ?>
            <?= Html::button('Contact request', ['class' => 'js-contact btn btn-primary']) ?>
        <?php } ?>
        <div class="col-12 info">
            <p><?= $model->content; ?></p>
        </div>
    </div>
</div>
