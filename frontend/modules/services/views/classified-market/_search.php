<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\services\models\ClassifiedMarketSearch */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(Url::to(['/js/services/classefield_market.js']));
?>

<section class="class-market-section">
    <div class="classified-market-search">

        <?php $form = ActiveForm::begin([
            'id' => 'classified-market-search',
            'method' => 'get',
            'action' => ['/services/classified-market/search' . ($model->category_id ? '/' . $model->category_id : '')]
        ]); ?>

            <div class="row">
                <div class="form-block">
                    <?= $form->field($model, 'searchValue')->textInput(['placeholder' => 'Enter search text ...'])->label(false) ?>
                </div>
                <div class="button-block">
                    <?= Html::submitButton('Search', [
                        'class' => 'btn btn-info',
                    ])?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</section>

