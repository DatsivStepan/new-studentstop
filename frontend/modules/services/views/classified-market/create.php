<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\services\ClassifiedMarket */

$this->title = Yii::t('app', 'Create Classified Market');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Classified Markets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classified-market-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
