<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    yii\widgets\Pjax,
    common\models\services\ClassifiedMarketCategories,
    kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\services\ClassifiedMarket */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(['id' => 'pjax-form-market-create', 'enablePushState' => false]); ?>
    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            if ($modal.classifieldMarketCreateModal) {
                $modal.classifieldMarketCreateModal.close();
            }
            if ($modal.classifieldMarketUpdateModal) {
                $modal.classifieldMarketUpdateModal.close();
            }
            
            location.reload()
        </script>
    <?php } ?>
    <div class="classified-market-form">

        <?php $form = ActiveForm::begin(['id' => 'form-market', 'options' => ['data-pjax' => 'pjax-form-class-create']]); ?>

            <div class="row" style="margin-left: 16px;margin-right: 16px;">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3 col-xl-2 text-modal">
                            <text>Image</text>
                        </div>
                        <div class="col-sm-6 col-xl-7">
                            <div>
                                <?= $form->field($model, 'image_src')->hiddeninput()->label(false); ?> 
                                <img src="<?= $model->getImages(); ?>" class="js-preview-market-image img-thumbnail"><br>
                                <a class="btn btn-sm btn-success js-add-market-image" style="width:100%">Change image</a><br><br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-5 text-modal">
                            <text>Content</text>
                        </div>
                        <div class="col-12">
                            <?= $form->field($model, 'content')->textarea(['rows' => 6])->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left: 16px;margin-right: 16px;">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3 col-xl-2 text-modal">
                            <text>Category ID</text>
                        </div>
                        <div class="col-sm-9 col-xl-10">
                            <?= $form->field($model, 'category_id')->widget(Select2::classname(), ['data' => ClassifiedMarketCategories::getAllByGroupCategories(),'options' => ['placeholder' => Yii::t('app', 'Select...')],'pluginOptions' => ['allowClear' => true
                                        ]])->label(false); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-2 text-modal">
                             <text>Title</text>
                        </div>
                        <div class="col-sm-10">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false); ?>
                        </div>
                    </div>    
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3 col-xl-2 text-modal">
                            <text>Address</text>
                        </div>
                        <div class="col-sm-9 col-xl-10">
                            <?= $form->field($model, 'address')->textInput(['maxlength' => true])->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>

        <?php ActiveForm::end(); ?>
        
    </div>
    <script>
        $(function(){
            $dropzone.initializationDropzoneEleemnt('market', '#classifiedmarket-image_src', 'image/*');
        })
    </script>
<?php Pjax::end(); ?>
