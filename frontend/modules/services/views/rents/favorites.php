<?php

use yii\helpers\Html,
    yii\grid\GridView,
    yii\widgets\Pjax,
    kartik\rating\StarRating,
    yii\helpers\Url,
    common\models\Favorites;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\services\models\ClassifiedMarketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My favorites rents');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .cm-favorite-star:hover{
        -ms-transform: scale(1.2, 1.2); /* IE 9 */
        -webkit-transform: scale(1.2, 1.2); /* Safari */
        transform: scale(1.2, 1.2);
        cursor:pointer;
    }
</style>
<div class="classified-market-index">

    <?= $this->render('_search', [
        'model' => $searchModel,
        'button' => true
    ]); ?>

    <div class="row search-block favorites-block"> 
        <?php if ($models = $dataProvider->getModels()) { ?>
            <?php foreach ($models as $model) { ?>
                <?= $this->render('_item_template', [
                    'model' => $model,
                    'booking_from' => null,
                    'booking_to' => null,
                    'class' => 'col-sm-3'
                ]); ?>
            <?php } ?>
        </div>
        <?php } else { ?>
        
        <?php } ?>
    </div>