<?php

use yii\helpers\Html,
    yii\grid\GridView,
    yii\widgets\Pjax,
    kartik\rating\StarRating,
    yii\helpers\Url,
    common\models\Favorites;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\services\models\ClassifiedMarketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rents');
$this->params['breadcrumbs'][] = $this->title;
$booking_from = $searchModel->booking_from;
$booking_to = $searchModel->booking_to;
?>
<style>
    .cm-favorite-star:hover{
        -ms-transform: scale(1.2, 1.2); /* IE 9 */
        -webkit-transform: scale(1.2, 1.2); /* Safari */
        transform: scale(1.2, 1.2);
        cursor:pointer;
    }
</style>
<div class="classified-market-index custom-rents-search-cont">

    <?= $this->render('_search', [
        'model' => $searchModel,
        'button' => true
    ]); ?>
    <div class="classified-market-index custom-my-requests desktop-display-none">
        <section class="rents-menu">
            <ul class="nav nav-tabs">
                <li class="nav-item" style="width:50%;text-align:center;">
                    <a class="nav-link active">MAP</a>
                </li>
                <li class="nav-item" style="width:50%; text-align:center;">
                    <a class="nav-link">INFO</a>
                </li>
            </ul>
        </section>
    </div>
    <div class="row search-block">
        <div id="map" style="height:100vh;width:100%;">
            
        </div>
    </div>
</div>
<div class="rents-right-block rents-right-block-c-s" style="width: 322px;
    background: white;
    float: right;
    margin-top: 0;
    transform: translateX(220px);
    transition: transform 0.6s ease-in-out;
    z-index: 10;
    box-shadow: 0 0 20px rgba(28, 43, 66, 0.25);position: fixed;
    top: -10px;
    z-index: 10;
    min-height: 100vh;
    height: 100%;
    right: 209px;
    overflow: auto">

        <?php if ($models = $dataProvider->getModels()) { ?>
            <?php foreach ($models as $model) { ?>
                <?= $this->render('_item_template', [
                    'model' => $model,
                    'booking_from' => $booking_from,
                    'booking_to' => $booking_to,
                    'class' => null
                ]); ?>
                <hr>
            <?php } ?>
        <?php } else { ?>
                <p>Not found</p>
        <?php } ?>
    </div>
<script>
    var map;

    function initAutocomplete() {

        var mapOptions = {
            center: {lat: 41.9012969, lng: 12.4957764},
            zoom: 10,
            minZoom: 4,
            maxZoom: 12,
        };
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
    }

    $(function () {
        initAutocomplete();
        loadUserMarjker();

        function loadUserMarjker(latlng) {
            $(".js-item-content-block").each(function (index) {
                var lat = $(this).find('.js-lat').val()
                var lng = $(this).find('.js-lng').val()
//                var price = $(this).find('.js-price').val()
//                if (!price) {
//                    price = 0;
//                }
                if (lat && lng) {
                    var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
//                    var image = {
//                        url: '/images/icons/map_icon.png',
//                        size: new google.maps.Size(40, 30),
//                        labelOrigin: new google.maps.Point(20, 12)
//                    };

                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
//                        labelClass: "marker-labels",
//                        label: {
//                            text: '€' + price,
//                            color: 'white',
//                            fontSize: '16px',
//                            fontWeight: "bold"
//                        },
//                        icon: image,
                        zIndex: 1
                    });
                    marker.addListener('click', function () {
                        console.log('df')
                    });
                }
                map.setZoom(12);
                if (latlng) {
                    map.setCenter(latlng);
                }
            })
        }
    })
</script>