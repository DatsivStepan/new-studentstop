<?php

use yii\helpers\Html,
    yii\helpers\Url;

use common\components\AdminMenuUrlManager;
$modelMenuUrl = new AdminMenuUrlManager();

/* @var $this yii\web\View */
/* @var $model common\models\services\Rents */

?>
<section class="rents-menu">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link <?= $modelMenuUrl->checkUrl('services/rents/my-rents'); ?>" href="<?= Url::to(['/services/rents/my-rents']); ?>">Rents</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?= $modelMenuUrl->checkUrl('services/rents/my-requests'); ?>" href="<?= Url::to(['/services/rents/my-requests']); ?>">Request</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?= $modelMenuUrl->checkUrl('services/rents/my-booking-r'); ?>" href="<?= Url::to(['/services/rents/my-booking-r']); ?>">Boookings</a>
        </li>
        <li class="nav-item">
            <?= Html::button('Add rent', ['class' => 'btn btn-primary js-create-rent']); ?>
        </li>
    </ul>
</section>