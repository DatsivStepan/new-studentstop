<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    common\models\services\RentsCategories,
    kartik\select2\Select2,
    yii\widgets\Pjax,
    common\models\services\Rents,
    common\models\services\RentsBooking;

/* @var $this yii\web\View */
/* @var $model common\models\services\Rents */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Pjax::begin(['id' => 'pjax-form-create-rent', 'enablePushState' => false]); ?>

    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            $modal.bookingModal.close();
            parent.location.reload();
//            $('#listVitrualClasses').yiiGridView("applyFilter");
        </script>
    <?php } ?>

<div class="rents-form">

    <?php $form = ActiveForm::begin(['id' => 'form-create-rent', 'options' => ['data-pjax' => 'pjax-form-create-rent']]); ?> 
    <div class="row status-block">
        <div class="col-2 text-bl status">
            <?= $form->field($model, 'status')->hiddenInput(['value' => RentsBooking::STATUS_NEW]); ?>
        </div>
        <div class="col-10 text-bl">
            <?php
            $interval = date_diff(date_create($model->reserve_from), date_create($model->reserve_to));
            $hours = ($interval->days * 24) + $interval->h;
        ?>
        <h5><?= $model->reserve_from; ?> - <?= $model->reserve_to; ?></h5>
        </div>
    </div>
    <div class="row qwection">
        <div class="col-12" style="text-align: center;">
            <h3>Are you sure?</h3>
        </div>
        <div class="col-12" style="text-align: center;">
             <h5>You have 1111 credits, pay - <?= $hours * $rent->credits; ?></h5>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end(); ?>
