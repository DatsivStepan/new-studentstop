<?php

use yii\helpers\Html,
    yii\helpers\Url,
    frontend\components\helpers\FunctionHelper,
    common\models\business\QuestionsAnswers,
    kartik\rating\StarRating,
    yii\widgets\ActiveForm,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
?>
<div id="rent-rating-block">

    <?php Pjax::begin(['id' => 'pjax-form-rating', 'enablePushState' => false]); ?>
        <?php if (Yii::$app->session->hasFlash('rSuccessSave')) { ?>
            <script>
                if ($modal.addRentRatingModal) {
                    $modal.addRentRatingModal.close();
                    location.reload()
                }
            </script>
        <?php } ?>

        <?php $form = ActiveForm::begin(['id' => 'form-rating-rent', 'options' => ['data-pjax' => 'pjax-form-rating']]); ?>
            <div class="col-sm-12 col-xl-12 text-center" style="font-size:16px !important;">
                <?= $form->field($newRating, 'rating')->widget(StarRating::classname(), [
                    'pluginOptions' => [
                        'step' => '1',
                        'size'=>'xs',
                        'showCaption' => false,
                    ],
                ])->label(false); ?>
            </div>
        <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>

</div>