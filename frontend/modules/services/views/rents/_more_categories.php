<?php

use yii\helpers\Html

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\services\models\ClassifiedMarketSearch */

?>
<div class="classified-market-index class-market">

    <div class="row all-classfied  more-block">
        <?php if ($childCategories = $model->getChildCategory(100)) { ?>
            <div class="classified-modal-block  js-category-block col-12">
                <div class="classified-block-my">
                    <?php foreach ($childCategories as $c_category) { ?>
                        <div class="row another-cl">
                            <div class="col-10 name-category">
                                 <?= Html::a($c_category->name, $c_category->getLink(), ['class' => '']); ?>
                            </div>
                           <div class="col-2 number-block">
                               <div class="number"><?= $c_category->getProductCount(); ?></div>
                           </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
