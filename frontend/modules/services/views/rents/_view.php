<?php

use yii\helpers\Html,
    yii\widgets\DetailView,
    kartik\rating\StarRating,
    yii\helpers\Url,
    common\models\services\Rents,
    frontend\components\helpers\FunctionHelper;

/* @var $this yii\web\View */
/* @var $model common\models\services\Rents */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rents-view">
    <div class="col-12 name">
         <h2><?= $model->title; ?></h2>
    </div>
    <div class="rents-view-modal row" style="background-color: #f3f3f3; padding:  10px 30px; margin: 0px;">

        <div class="col-12" >
            <div class="row">
                <div class="col-12 col-sm-1">
                    <img class="image" src="<?= $model->getAuthorAvatar(); ?>">
                </div>
                <div class="col-12 col-sm-3 classif-text-block text-bl">
                <p><?= $model->getAuthorName(); ?></p>
                </div>
                <div class="col-sm-2 date text-bl">
                    <p><?= $model->getDateCreate('d/m/Y'); ?></p>
                </div>
                <div class="col-sm-3 text-bl">
                    <p><?= $model->getPrice(); ?></p>
                </div>
                <div class="col-sm-3" style="padding-top: 10px;">
                     <?= StarRating::widget([
                        'id' => 'show_rents_r_' . $model->id,
                        'name' => 'show_rents_r_' . $model->id,
                        'value' => $model->getRating(),
                        'disabled' => true,
                        'pluginOptions' => [
                            'size' => 'xs',
                            'min' => 0,
                            'max' => 5
                        ],
                    ]); ?>
                </div>
            </div>


        </div>
    </div>
    <div class="tab-menu">
        <ul class="nav nav-pills">
            <li class="nav-item first">
                <a class="nav-link active" data-toggle="pill" href="#description">Descriptions</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#map_t">Map</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#images">Images</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#calendar">Calendar</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#review">Reviews</a>
            </li>
            <li class="nav-item" style="<?= $booking_from && $booking_to && !$model->isOwner() ? : 'display:none;'; ?>">
                <a class="nav-link" data-toggle="pill" href="#book">Book</a>
            </li>
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane container active" id="description"><?= $model->getContent(); ?></div>

        <div class="tab-pane container fade" id="map_t">
            <div id="map_v" class="map-block" style="width:100%;height:300px;"></div>
        </div>
        
        <div class="tab-pane container fade" id="images">
            <?php if ($images = $model->imagesModel) { ?>
                <?php foreach ($images as $image) { ?>
                    <img class="img-fluid img-thumbnail" src="<?= Url::to(['/' . $image->img_src]); ?>" style="width:100px;height:100px;">
                <?php } ?>
            <?php } else { ?>
                <p class="text-center">No images</p>
            <?php } ?>
        </div>
        <div class="tab-pane container fade" id="calendar">
            <div style="position:relative;width:60%;margin:0 auto;">
                
                    <?= \yii2fullcalendar\yii2fullcalendar::widget([
                        'id' => 'calendar_w_'.$model->id,
                        'clientOptions' => [
                            'getDate' => date('Y-m-d',time()),
                            'defaultDate' => date('Y-m-d',time()),
                            'today' => false,
                        ],
                        'ajaxEvents' => Url::to(['/services/rents/get-rent-bookings/' . $model->id]),
                    ]); ?>
            </div>
        </div>
        <div class="tab-pane container fade" id="review">
            <?php if ($ratings = $model->getRatingRequest(\common\models\services\RentsRating::PERSON_TYPE_OWNER_RENT)) { ?>
                <?php foreach ($ratings as $rating) { ?>
                    <div class="row">
                        <div class="col-6 classif-text-block text-bl">
                            <div class="my-rents-popap-reviews-img">
                                <img src="<?= $rating->rentsBookingModel && $rating->rentsBookingModel->user ? $rating->rentsBookingModel->user->getAvatar() : ''; ?>">
                                <span><?= $rating->rentsBookingModel && $rating->rentsBookingModel->user  ? $rating->rentsBookingModel->user->getNameAndSurname() : ''; ?></span>
                            </div>
                        </div>
                        <div class="col-6 classif-text-block text-bl my-rents-popap-reviews-rate" style="padding-top:10px;">
                            <?= StarRating::widget([
                                'id' => 'show_reting_q_' . $rating->id,
                                'name' => 'show_reting_q_' . $rating->id,
                                'value' => $rating->rating,
                                'disabled' => true,
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'min' => 0,
                                    'max' => 5
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <hr>
                <?php } ?>
            <?php } else { ?>
                <p class="text-center">No reviews</p>
            <?php } ?>
        </div>
        <div class="tab-pane container fade book-block-all" id="book">
            <?php if ($booking_from && $booking_to) { ?>
                <?php 
                    $interval = date_diff(date_create($booking_to), date_create($booking_from));
                    $hours = ($interval->days * 24) + $interval->h;
                ?>
                <div class="row book-block" style="margin:0px">
                    <div class="col-sm-4">
                        <text>Rent date for:</text>
                        <span> <?= $booking_from; ?></span>
                    </div>
                    <div class="col-sm-4">
                        <text>Rent date to: </text>
                        <span><?= $booking_to; ?></span>
                    </div>
                    <div class="col-sm-4">
                        <text>Rent hour: </text>
                        <span> <?= $hours; ?></span>
                    </div>
                    <div class="col-sm-4">
                        <text>Total price($): </text>
                        <span><?= FunctionHelper::numberFormat($hours * $model->money); ?></span>
                        
                    </div>
                    <div class="col-sm-4">
                        <text>Total price(credits): </text>
                        <span><?= FunctionHelper::numberFormat($hours * $model->credits); ?></span>
                    </div>
                </div>
                <div style="text-align: center;padding-top: 10px;">
                    <?php if ($model->type_pay == Rents::PAY_TYPE_INSTANT) { ?>
                        <?= Html::a('Pay with Paypal', null, [
                            'class' => 'paypal-btn js-pay-paypal btn btn-danger',
                            'data-money' => $hours * $model->money
                        ])?>
                        <?= Html::a('Pay credits', null, [
                            'class' => 'creditbtn js-pay-credits btn btn-danger',
                            'data-credit' => $hours * $model->credits
                        ])?>
                    <?php } else if ($model->type_pay == Rents::PAY_TYPE_REQUEST) { ?>
                        <?= Html::a('Send Request', null, ['class' => 'creditbtn js-send-request btn btn-danger'])?>
                    <?php } ?>
               </div>
            <?php } ?> 
                
        </div>
    </div>

</div>

<script>
   var map;
   var lat = '<?= $model->lat; ?>';
   var lng = '<?= $model->lng; ?>'; 
    function initAutocompleteV() {

        var mapOptions = {
            center: {lat: 37.2581948, lng: -104.655005},
            zoom: 4,
            minZoom: 4
        };
        map = new google.maps.Map(document.getElementById('map_v'), mapOptions);

        latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
        if (lat && lng) {
            var marker = new google.maps.Marker({
                position: latlng,
                map: map
            });
            map.setZoom(12);
            map.setCenter(latlng);
        }
    }
        
    $(function() {
        initAutocompleteV();

        $('.nav-item a[href="#calendar"]').on('shown.bs.tab', function(event){
            $('#calendar_w_<?= $model->id; ?>').find( '.fc-agendaWeek-button' ).trigger('click')
            $('#calendar_w_<?= $model->id; ?>').find( '.fc-month-button' ).trigger('click')
        })

        $('.rents-view').on('click', '.js-pay-paypal', function(){
            var thisE = $(this);
            var id = '<?= $model->id; ?>';
            thisE.attr('disabled', true);
            var moneyNeed = thisE.data('money');
            
            var booking_from = '<?= $booking_from; ?>';
            var booking_to = '<?= $booking_to; ?>';
            swal({
                title: "Are you sure?",
                text: "you want payed  " + parseFloat(moneyNeed) + '$',
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    location.href = '/payment-paypal/pay-for-rent-house-book?rentId=' + id + '&bookingFrom=' + booking_from + '&bookingTo=' + booking_to
                }
                thisE.attr('disabled', false);
            })
        });

        $('.rents-view').on('click', '.js-pay-credits', function() {
            var thisE = $(this);
            var id = '<?= $model->id; ?>';
            thisE.attr('disabled', true);
            var creditsNeed = thisE.data('credit');
            
            var booking_from = '<?= $booking_from; ?>';
            var booking_to = '<?= $booking_to; ?>';
            $.ajax({
                method: 'post',
                url: '/site/check-credits-count',
                dataType: 'json'
            }).done(function (response) {
                thisE.attr('disabled', false);
                if (parseFloat(creditsNeed) > parseFloat(response.amount)) {
                    swal({
                        title: "Not enough credits",
                        text: 'You have ' + parseFloat(response.amount) + ' credits, but you need to ' + parseFloat(creditsNeed),
                        html: true,
                        icon: "error",
                        confirmButtonText: "Ok",
                    });
                } else {
                    swal({
                        title: "Are you sure?",
                        text: "you want payed  " + parseFloat(creditsNeed) + ' credits',
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                        buttons: ["Cancel", "Yes, Payed"],
                    }).then((willPayed) => {
                        if (willPayed) {
                            $.ajax({
                                method: 'post',
                                url: '/services/rents/pay-for-booking/',
                                dataType: 'json',
                                data:{
                                    rent_id : id,
                                    reserve_from: booking_from,
                                    reserve_to: booking_to
                                }
                            }).done(function (response) {
                                if (response.status) {
                                    swal("Booking payed")
                                        .then((value) => {
                                            location.reload();
                                        });
                                }
                            });
                        }
                    })
                }
            });
        })

        $('.rents-view').on('click', '.js-send-request', function() {
            swal({
                title: "Are you sure?",
                text: "you want send pay request",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((sendRequest) => {
                if (sendRequest) {
                    var id = '<?= $model->id; ?>';
                    var booking_from = '<?= $booking_from; ?>';
                    var booking_to = '<?= $booking_to; ?>';
                    $.ajax({
                        method: 'post',
                        url: '<?= Url::to(['/services/rents/send-pay-request']); ?>/' + id,
                        dataType: 'json',
                        data:{
                            reserve_from : booking_from,
                            reserve_to : booking_to
                        }
                    }).done(function (response) {
                        if (response.status) {
                            swal("Request sended!", {
                                icon: "success",
                            });
                            location.reload()
                        }
                    })
                }
            })
        });

    })
</script>