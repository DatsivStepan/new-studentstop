<?php

use yii\helpers\Html,
    kartik\grid\GridView,
    yii\widgets\Pjax,
    kartik\rating\StarRating,
    yii\helpers\Url,
    common\models\Favorites,
    common\models\services\RentsBooking,
    common\models\services\RentsRating;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\services\models\ClassifiedMarketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My requests');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .cm-favorite-star:hover{
        -ms-transform: scale(1.2, 1.2); /* IE 9 */
        -webkit-transform: scale(1.2, 1.2); /* Safari */
        transform: scale(1.2, 1.2);
        cursor:pointer;
    }
</style>
<div class="my-requests-block custom-my-requests">

    <?= $this->render('_search', [
        'model' => isset($searchModel) ? $searchModel : new \frontend\modules\services\models\RentsSearch(),
        'button' => true
    ]); ?>
    <?= $this->render('_my-account-menu'); ?>

    <div class="row search-block rents-table-my-book-r">
        <?= GridView::widget([
            'dataProvider' => $dataRProvider,
            'responsive' => false,
            'striped' => false,
            'responsiveWrap' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'rent_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->rentModel ? $model->rentModel->title : '';
                    },
                    'contentOptions' => ['class' => 'professor-data']
                ],
                'reserve_from',
                'reserve_to',
                [
                    'format' => 'raw',
                    'value' => function ($model) {
                        return RentsBooking::$translateStatus[$model->status];
                    },
                ],
                [
                    'format' => 'raw',
                    'value' => function ($model) {
                        $modelR = $model->getRatingModel();
                        return $modelR ?
                            StarRating::widget([
                                'id' => 'show_reting_answer_' . $model->id,
                                'name' => 'show_reting_answer_' . $model->id,
                                'value' => $modelR->rating,
                                'disabled' => true,
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'min' => 0,
                                    'max' => 5
                                ],
                            ]) :
                            Html::button('Add rating tenant', ['class' => 'btn btn-primary js-add-rating-owner-booking']);
                    },
                ],
            ],
        ]); ?>
    </div>
</div>

<script>
    $(function(){
        $('.my-requests-block').on('click', '.js-add-rating-owner-booking', function() {
            var id = $(this).closest('tr').data('key')
            $.confirm({
                id: 'addRentRatingModal',
                title: 'Add rating',
                content: 'url:<?= Url::to(['/services/rents/add-rating']) ?>/' + id,
                columnClass: 'xl view-block modal-block '
            });
        });
    })
</script>