<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    common\models\services\RentsCategories,
    kartik\select2\Select2,
    yii\widgets\Pjax,
    common\models\services\Rents,
    common\models\services\RentsBooking,
    kartik\grid\GridView,
    yii\helpers\Url,
    kartik\rating\StarRating,
    common\models\services\RentsRating;

/* @var $this yii\web\View */
/* @var $model common\models\services\Rents */
/* @var $form yii\widgets\ActiveForm */
$type = Yii::$app->request->get('type');
?>
<section class="my-bookings-block">
    
    <ul class="nav nav-tabs custom-books-tabs">
        <li class="nav-item">
            <a class="nav-link <?= empty($type) ? 'active' : ''; ?>" href="<?= Url::to(['/services/rents/my-booking']); ?>">Request</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?= $type == RentsBooking::SHOW_TYPE_RECENT ? 'active' : ''; ?>" href="?type=<?= RentsBooking::SHOW_TYPE_RECENT; ?>">Recent</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?= $type == RentsBooking::SHOW_TYPE_ARCHIVE ? 'active' : ''; ?>" href="?type=<?= RentsBooking::SHOW_TYPE_ARCHIVE; ?>">Archive</a>
        </li>
    </ul>

    <div class="rents-form table-block-container rents-table-my-book">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'responsive' => false,
            'striped' => false,
            'responsiveWrap' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'rent_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->rentModel ? $model->rentModel->title : '';
                    },
                    'contentOptions' => ['class' => 'professor-data']
                ],
                'reserve_from',
                'reserve_to',
                [
                    'label' => 'Hours rent',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->getRentHoursCount();
                    },
                ],
                [
                    'label' => 'Price',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->getPrice();
                    },
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->isPayed() ? 'Payed' : ($model->status == RentsBooking::STATUS_CONFIRM ? (
                                Html::button('Pay with Paypal', [
                                    'class' => 'paypal-btn js-pay-paypal btn btn-danger',
                                    'data-money' => $model->getNeedMoneyPayCount()
                                ]) . ' ' .
                                Html::button('Pay credits', [
                                    'class' => 'creditbtn js-pay-credits btn btn-danger',
                                    'data-credit' => $model->getNeedCreditsPayCount()
                                ])
                            ) : RentsBooking::$translateStatus[$model->status]);
                    },
                    'contentOptions' => ['class' => 'professor-status']
                ],
                [
                    'format' => 'raw',
                    'value' => function ($model) {
                        $modelR = $model->getRatingModel(RentsRating::PERSON_TYPE_OWNER_RENT);
                        return $modelR ?
                            StarRating::widget([
                                'id' => 'show_reting_answer_' . $model->id,
                                'name' => 'show_reting_answer_' . $model->id,
                                'value' => $modelR->rating,
                                'disabled' => true,
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'min' => 0,
                                    'max' => 5
                                ],
                            ]) :
                            Html::button('Add rating tenant', ['class' => 'btn btn-primary js-add-rating-owner-rent']);
                    },
                    'visible' => $type == RentsBooking::SHOW_TYPE_ARCHIVE
                ],
            ],
        ]); ?>

    </div>
</section>
<script>
    $(function(){
        
        $('.my-bookings-block').on('click', '.js-pay-paypal', function(){
            var thisE = $(this);
            var id = thisE.closest('tr').data('key');
            thisE.attr('disabled', true);
            var money = thisE.data('money')
            swal({
                title: "Are you sure?",
                text: "you want payed  " + parseFloat(money) + '$',
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    location.href = '/payment-paypal/pay-for-rent-book?bookedId=' + id
                }
                thisE.attr('disabled', false);
            })
        });
        
        $('.my-bookings-block').on('click', '.js-pay-credits', function() {
            var thisE = $(this);
            var id = thisE.closest('tr').data('key');
            thisE.attr('disabled', true);
            var creditsNeed = thisE.data('credit');
            $.ajax({
                method: 'post',
                url: '/site/check-credits-count',
                dataType: 'json'
            }).done(function (response) {
                thisE.attr('disabled', false);
                if (parseFloat(creditsNeed) > parseFloat(response.amount)) {
                    swal({
                        title: "Not enough credits",
                        text: 'You have ' + parseFloat(response.amount) + ' credits, but you need to ' + parseFloat(creditsNeed),
                        html: true,
                        icon: "error",
                        confirmButtonText: "Ok",
                    });
                } else {
                    swal({
                        title: "Are you sure?",
                        text: "you want payed  " + parseFloat(creditsNeed) + ' credits',
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                        buttons: ["Cancel", "Yes, Payed"],
                    }).then((willPayed) => {
                        if (willPayed) {
                            $.ajax({
                                method: 'post',
                                url: '/services/rents/pay-for-booking/' + id,
                                dataType: 'json'
                            }).done(function (response) {
                                if (response.status) {
                                    swal("Booking payed")
                                        .then((value) => {
//                                            if () {
//                                                modalReload($modal.showAnswerModal);
//                                            }
                                        });
                                }
                            });
                        }
                    })
                }
            });
        });

        $('.my-bookings-block').on('click', '.js-add-rating-owner-rent', function() {
            var id = $(this).closest('tr').data('key')
            $.confirm({
                id: 'addRentRatingModal',
                title: 'Add rating',
                content: 'url:<?= Url::to(['/services/rents/add-rating']) ?>/' + id + '?type=<?= RentsRating::PERSON_TYPE_OWNER_RENT; ?>',
                columnClass: 'xl view-block modal-block '
            });
        });
        
    })
</script>