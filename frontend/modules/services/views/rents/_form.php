<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    common\models\services\RentsCategories,
    kartik\select2\Select2,
    yii\widgets\Pjax,
    common\models\services\Rents,
    frontend\widgets\UploadFiles;

/* @var $this yii\web\View */
/* @var $model common\models\services\Rents */
/* @var $form yii\widgets\ActiveForm */

?>
<?php Pjax::begin(['id' => 'pjax-form-create-rent', 'enablePushState' => false]); ?>

    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            if ($modal.createRentModal) {
                $modal.createRentModal.close();
            }
            location.reload()
//            $('#listVitrualClasses').yiiGridView("applyFilter");
        </script>
    <?php } ?>

<div class="rents-form" id="rents-form-block">

    <?php $form = ActiveForm::begin(['id' => 'form-create-rent', 'options' => ['data-pjax' => 'pjax-form-create-rent']]); ?> 
        <?= $form->field($model, 'lat')->hiddeninput()->label(false); ?>
        <?= $form->field($model, 'lng')->hiddeninput()->label(false); ?>

        <div class="row" style="margin-left: 16px;margin-right: 16px;">
            <div class="col-sm-3 col-xl-2 text-modal">
                <text>Title</text>
            </div>
            <div class="col-sm-9 col-xl-10">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false); ?>
            </div>
            <div class="col-sm-6">
                <div class="row">
                     <div class="col-sm-3 col-xl-2 text-modal">
                        <text>Image</text>
                    </div>
                    <div class="col-sm-6 col-xl-7">
                        <div>
                        <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
                        <img src="<?= $model->getImages(); ?>" class="js-preview-rents-image img-thumbnail"><br>
                        <a class="btn btn-sm btn-success js-add-rents-image" style="width:100%">Change image</a>
                    </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-12 text-modal">
                        <text>Content</text>
                    </div>
                    <div class="col-12">
                        <?= $form->field($model, 'content')->textarea(['rows' => 6, 'style' => 'height:auto !important'])->label(false); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 16px;margin-right: 16px; margin-top: 10px;">
            <div class="col-sm-6">
                <div class="row">

                    <div class="col-sm-3 col-xl-2 text-modal">
                        <text>Category_id</text>
                    </div>
                    <div class="col-sm-9 col-xl-10">
                         <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                            'data' => RentsCategories::getAllByGroupCategories(),
                            'options' => ['placeholder' => Yii::t('app', 'Select...')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ])->label(false); ?>

                    </div>
                    <div class="col-sm-3 col-xl-2 text-modal">
                        <text>Type Pay</text>
                    </div>
                    <div class="col-sm-9 col-xl-10">
                        <?= $form->field($model, 'type_pay')->dropDownList(Rents::$payTypesArray)->label(false); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-3 col-xl-2 text-modal">
                        <text>Credits</text>
                    </div>
                    <div class="col-sm-9 col-xl-10">
                        <?= $form->field($model, 'credits')->textInput()->label(false); ?>
                    </div>
                    <div class="col-sm-3 col-xl-2 text-modal">
                        <text>Money</text>
                    </div>
                    <div class="col-sm-9 col-xl-10">
                        <?= $form->field($model, 'money')->textInput()->label(false); ?>
                    </div>

                </div>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-3 col-xl-2 text-modal">
                        <text>Address</text>
                    </div>
                    <div class="col-sm-9 col-xl-10">
                        <?= $form->field($model, 'address')->textInput(['maxlength' => true])->label(false); ?>
                    </div>
                    <div class="col-sm-12">
                        <div id="map_n" class="map-block" style="width:100%;height:300px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <?= UploadFiles::widget([
            'folderType' => 'rents',
            'model' => 'Rents',
            'parentElementId' => 'rents-form-block'
        ]); ?>
        
    <?php ActiveForm::end(); ?>

</div>
<script>
    var autocomplete;
    var latInput = 'rents-lat';
    var lngInput = 'rents-lng';
    
    function initAutocomplete() {

        var mapOptions = {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13,
            minZoom: 8,
            maxZoom: 12,
        };

        var lat = document.getElementById(latInput).value;
        var lng = document.getElementById(lngInput).value;
        var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};

        if (lat && lng) {
            mapOptions.center.lat = parseFloat(lat);
            mapOptions.center.lng = parseFloat(lng);
        }

        var map = new google.maps.Map(document.getElementById('map_n'), mapOptions);
    //            var image = {
    //                url: '/images/icons/map_icon_2.png',
    //                size: new google.maps.Size(50, 32)
    //            };

        var marker = new google.maps.Marker({
            map: map,
    //                icon: image,
        });
        //var countryInput = $('#hostform-country');

        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('rents-address')),
            {types: ['geocode']});

    //            autocomplete.setComponentRestrictions(
    //                {'country': [countryInput.val()]});

        autocomplete.addListener('place_changed', fillInAddress);

    //            countryInput.on('change', function () {
    //                autocomplete.setComponentRestrictions({'country': [$(this).val()]})
    //            });

        setGeopos(latlng);

        function fillInAddress() {
            var place = autocomplete.getPlace();

            document.getElementById(latInput).value = place.geometry.location.lat();
            document.getElementById(lngInput).value = place.geometry.location.lng();

            setGeopos({
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            });
        }

        function setGeopos(latlng) {
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    if (results[1]) {
                        map.setZoom(11);
                        map.setCenter(latlng);
                        marker.setPosition(latlng);
                    } else {
                        console.log('No results found');
                    }
                }
            });
        }
    }
    

    $(function(){
        $dropzone.initializationDropzoneEleemnt('rents', '#rents-img_src', 'image/*');

        initAutocomplete();
    })
</script>
<?php Pjax::end(); ?>
