<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\services\RentsHouse */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rents Houses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rents-house-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'university_id',
            'type',
            'title',
            'content:ntext',
            'img_src',
            'house_type',
            'room_count',
            'min_rent',
            'adults',
            'type_pay',
            'credits',
            'money',
            'publish',
            'status',
            'address',
            'lat',
            'lng',
            'attr_wifi',
            'attr_parking',
            'attr_smoking',
            'attr_tv',
            'attr_animal',
            'attr_conditioner',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
