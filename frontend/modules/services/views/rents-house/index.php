<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\services\models\RentsHouseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rent a house');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rents-house-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Rents House'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'university_id',
            'type',
            'title',
            // 'content:ntext',
            // 'img_src',
            // 'house_type',
            // 'room_count',
            // 'min_rent',
            // 'adults',
            // 'type_pay',
            // 'credits',
            // 'money',
            // 'publish',
            // 'status',
            // 'address',
            // 'lat',
            // 'lng',
            // 'attr_wifi',
            // 'attr_parking',
            // 'attr_smoking',
            // 'attr_tv',
            // 'attr_animal',
            // 'attr_conditioner',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
