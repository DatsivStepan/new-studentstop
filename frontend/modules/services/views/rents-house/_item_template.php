<?php
use kartik\rating\StarRating,
    yii\helpers\Html,
    common\models\Favorites;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="classif-search-block js-item-content-block <?= $class; ?>" data-id="<?= $model->id; ?>">
    <div class="searc-block-my">
        <img class="image" src="<?= $model->getImages(); ?>" style="width:100%;">
        <div class="icon-block icon-star">
            <?php $fStatus = Favorites::getFavoriteStatus(Favorites::TYPE_RENTS_HOUSE, $model->id); ?>
            <?= Html::tag('i', '', [
                'class' => 'glyphicon glyphicon-star cm-favorite-star js-favorite-star-button ' . ($fStatus ? 'active' : ''),
                'data-type' => Favorites::TYPE_RENTS_HOUSE
            ]); ?>
        </div>
        <div class="price-block">
            <text> Price: <?= $model->money; ?></text>
            <text> Credits: <?= $model->credits; ?></text>
        </div>
    </div>
    <div class="searc-block-bottom">
        <h2><?= $model->title; ?></h2>
        <?= StarRating::widget([
            'name' => 'rating_rent_' . $model->id,
            'value' => $model->getRating(),
            'disabled' => true,
            'pluginOptions' => [
                'size' => 'xs',
                'min' => 0,
                'max' => 5
            ],
        ]); ?>
        <div class="row">
            <div class="col-6">
                <div class="row">
                    <div class="col-12 classif-text-block text-bl">
                        <p><?= $model->getAuthorName(); ?></p>
                    </div>
                    <div class="col-12 date text-bl data">
                        <p><?= $model->getDateCreate('d/m/Y'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-6 button-block">
                <?= Html::button($model->isOwner() ? 'View' :'Book', [
                    'class' => 'view-button btn btn-info js-show-rents-house',
                    'data-id' => $model->id,
                    'data-booking_from' => $booking_from,
                    'data-booking_to' => $booking_to
                ]); ?>
            </div>
        </div>
    </div>
</div>

