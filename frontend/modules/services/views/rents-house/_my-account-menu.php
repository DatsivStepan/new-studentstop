<?php

use yii\helpers\Html,
    yii\helpers\Url;


use common\components\AdminMenuUrlManager;
$modelMenuUrl = new AdminMenuUrlManager();

/* @var $this yii\web\View */
/* @var $model common\models\services\Rents */

?>
<section class="rents-menu">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link <?= $modelMenuUrl->checkUrl('services/rents-house/my-account'); ?>" href="<?= Url::to(['/services/rents-house/my-account']); ?>">Rents</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?= $modelMenuUrl->checkUrl('services/rents-house/my-requests'); ?>" href="<?= Url::to(['/services/rents-house/my-requests']); ?>">Request</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?= $modelMenuUrl->checkUrl('services/rents-house/my-booking-r'); ?>" href="<?= Url::to(['/services/rents-house/my-booking-r']); ?>">Boookings</a>
        </li>
        <li class="nav-item">
            <?= Html::button('Add rent', ['class' => 'btn btn-primary js-create-rent-house']); ?>
        </li>
    </ul>
</section>