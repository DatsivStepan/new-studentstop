<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    common\models\services\RentsCategories,
    kartik\select2\Select2,
    yii\widgets\Pjax,
    common\models\services\Rents,
    common\models\services\RentsBooking,
    kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\services\Rents */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'My booking');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="my-booking rents-house-my-booking">
    <nav class="navbar navbar-expand-lg">
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link <?= !$searchModel->showType ? 'active' : ''; ?>" href="?type=">Request</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $searchModel->showType == RentsBooking::SHOW_TYPE_RECENT ? 'active' : ''; ?>" href="?type=<?= RentsBooking::SHOW_TYPE_RECENT; ?>">Recent</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $searchModel->showType == RentsBooking::SHOW_TYPE_ARCHIVE ? 'active' : ''; ?>" href="?type=<?= RentsBooking::SHOW_TYPE_ARCHIVE; ?>">Archive</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="rents-form">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'responsive' => false,
            'striped' => false,
            'responsiveWrap' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'rent_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->rentModel ? $model->rentModel->title : '';
                    },
                    'contentOptions' => ['class' => 'professor-data']
                ],
                'reserve_from',
                'reserve_to',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->status;
                    },
                    'contentOptions' => ['class' => 'professor-status']
                ],
            ],
        ]); ?>

    </div>
</section>