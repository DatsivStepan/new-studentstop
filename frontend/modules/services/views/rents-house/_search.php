<?php

use yii\helpers\Html,
    yii\helpers\Url,
    yii\widgets\ActiveForm,
    kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model frontend\modules\services\models\RentsSearch */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile(Url::to(['/js/services/rents_a_house.js']));
?>
<section class="rents-search row">
    <div class="rent-search <?= !empty($col) ? $col : 'col-sm-12'; ?>">

        <?php $form = ActiveForm::begin([
            'method' => 'get',
            'action' => ['/services/rents-house/search']
        ]); ?>
            <?= $form->field($model, 'address', ['options' => ['class' => 'search-input']])->textInput(['placeholder' => 'Destination, city, adress'])->label(false) ?>

            <?= $form->field($model, 'booking_from', ['options' => ['class' => 'check-in']])->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Check in ...'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'startDate'=> date('Y-m-d H:i'),
                    'autoclose' => true
                ]
            ])->label(false); ?>

            <?= $form->field($model, 'booking_to', ['options' => ['class' => 'check-out']])->widget(DateTimePicker::classname(), [
                'options' => [
                    'placeholder' => 'Check out ...',
                ],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'startDate'=> date('Y-m-d H:i', strtotime(date('Y-m-d H:i') . ' +1 hour')),
                    'autoclose' => true
                ]
            ])->label(false); ?>

            <div class="form-group">
                <div class="button-search-block">
                <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-search']) ?>
                </div>
            </div>


        <?php ActiveForm::end(); ?>

    </div>
</section>