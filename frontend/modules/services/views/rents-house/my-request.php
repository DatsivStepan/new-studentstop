<?php

use yii\helpers\Html,
    kartik\grid\GridView,
    yii\widgets\Pjax,
    kartik\rating\StarRating,
    yii\helpers\Url,
    common\models\Favorites,
    common\models\services\RentsBooking;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\services\models\ClassifiedMarketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My requests');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .cm-favorite-star:hover{
        -ms-transform: scale(1.2, 1.2); /* IE 9 */
        -webkit-transform: scale(1.2, 1.2); /* Safari */
        transform: scale(1.2, 1.2);
        cursor:pointer;
    }
</style>
<div class="my-requests-block custom-my-requests">

    <?= $this->render('_search', [
        'model' => isset($searchModel) ? $searchModel : new \frontend\modules\services\models\RentsSearch(),
        'button' => true
    ]); ?>
    <?= $this->render('_my-account-menu'); ?>

    <div class="row search-block rents-house-my-requests-table">
        <?= GridView::widget([
            'dataProvider' => $dataRProvider,
            'responsive' => false,
            'striped' => false,
            'responsiveWrap' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'rent_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->rentModel ? $model->rentModel->title : '';
                    },
                    'contentOptions' => ['class' => 'professor-data']
                ],
                'reserve_from',
                'reserve_to',
                [
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->status == RentsBooking::STATUS_ON_CONFIRM ? 
                                Html::button('Confirm request', ['class' => 'btn btn-success js-confirm-request', 'data-status' => RentsBooking::STATUS_CONFIRM]) .
                                Html::button('Reject request', ['class' => 'btn btn-danger js-confirm-request', 'data-status' => RentsBooking::STATUS_CONFIRM])
                            : RentsBooking::$translateStatus[$model->status];
                    },
                ],
            ],
        ]); ?>
    </div>
</div>

<script>
    $(function(){
        $('.my-requests-block').on('click', '.js-confirm-request', function() {
            var thisE = $(this);
            swal({
                title: "Are you sure?",
                text: "you want request",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((sendRequest) => {
                if (sendRequest) {
                    var id = thisE.closest('tr').data('key');
                    var status = thisE.data('status');
                    $.ajax({
                        method: 'post',
                        url: '<?= Url::to(['/services/rents/change-booking-status']); ?>/' + id,
                        dataType: 'json',
                        data:{
                            status : status,
                        }
                    }).done(function (response) {
                        if (response.status) {
                            swal("Save!", {
                                icon: "success",
                            });
                            location.reload()
                        }
                    })
                }
            })
        });
    })
</script>