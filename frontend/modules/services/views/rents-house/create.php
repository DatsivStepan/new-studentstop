<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\services\RentsHouse */

$this->title = Yii::t('app', 'Create Rents House');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rents Houses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rents-house-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
