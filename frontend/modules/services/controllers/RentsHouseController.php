<?php

namespace frontend\modules\services\controllers;

use Yii,
    common\models\services\RentsHouse,
    frontend\modules\services\models\RentsHouseSearch,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    common\models\MenuItems,
    common\models\services\RentsBooking,
    frontend\modules\services\models\RentsBookingSearch,
    common\models\pay\PayRequests,
    common\models\services\RentsHouseImages,
    yii\web\Response;

/**
 * RentsHouseController implements the CRUD actions for RentsHouse model.
 */
class RentsHouseController extends Controller
{
    public $universityId;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function beforeAction($action) {
        if ($action == 'redirect') {
            $this->enableCsrfValidation = false;
        }
        $this->universityId = Yii::$app->user->identity->userInfoModel ? Yii::$app->user->identity->userInfoModel->university_id : null;
        if (!$this->universityId) {
            return $this->redirect(['/site/check-user-university']);
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all RentsHouse models.
     * @return mixed
     */
//    public function actionIndex()
//    {
//        $searchModel = new RentsHouseSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }

    /**
     * Lists all Rents models.
     * @return mixed
     */
    public function actionSearch()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_RENT_A_HOUSE;

        $searchModel = new RentsHouseSearch();
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Rents models.
     * @return mixed
     */
    public function actionMyAccount()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_RENT_A_HOUSE;

        $searchModel = new RentsHouseSearch();
        $dataProvider = $searchModel->mySearch();

        return $this->render('my-account-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Rents models.
     * @return mixed
     */
    public function actionFavorites()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_RENT_A_HOUSE;

        $searchModel = new RentsHouseSearch();
        $searchModel->favorites = 1;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('favorites', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Displays a single Rents model.
     * @param integer $id
     * @return mixed
     */
    public function actionShow($id)
    {
        $this->layout = '/modal';
        $model = $this->findModel($id);

        return $this->render('_view', [
            'model' => $model,
            'booking_from' => Yii::$app->request->get('booking_from'),
            'booking_to' => Yii::$app->request->get('booking_to'),
        ]);
    }

    /**
     * Creates a new Rents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateRentHouse($id = null)
    {
        $this->layout = '/modal';
        if (!$model = RentsHouse::findOne($id)) {
            $model = new RentsHouse();
            $model->user_id = Yii::$app->user->id;
            $model->university_id = $this->universityId;
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if ($images = json_decode($model->images)) {
                    foreach ($images as $image) {
                        if ($image) {
                            $rentImage = new RentsHouseImages();
                            $rentImage->rent_house_id = $model->id;
                            $rentImage->img_src = $image->src;
                            $rentImage->name = $image->name;
                            $rentImage->save();
                        }
                    }
                }
                Yii::$app->session->setFlash('successSave');
            }
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }
    
    
    /**
     * Displays a single Rents model.
     * @param string|null $type
     * @return mixed
     */
    public function actionBooking($type = null)
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_RENT_A_HOUSE;

        
        $searchModel = new RentsBookingSearch();
        $searchModel->showType = $type;
        $searchModel->rentBookingOwnerId = Yii::$app->user->id;

        switch ($searchModel->showType) {
            case RentsBooking::SHOW_TYPE_RECENT:
                $searchModel->payType = RentsHouse::PAY_TYPE_INSTANT;
                break;
            case RentsBooking::SHOW_TYPE_ARCHIVE:
                $searchModel->payType = RentsHouse::PAY_TYPE_INSTANT;
                break;
            default:
                $searchModel->payType = RentsHouse::PAY_TYPE_REQUEST;
                break;
        }
        
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search(RentsBooking::TYPE_HOUSE);

        return $this->render('my-booking', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Rents models.
     * @return mixed
     */
    public function actionMyRequests()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_RENT_A_HOUSE;

        $searchModel = new RentsBookingSearch();
        $searchModel->rentOwnerId = Yii::$app->user->id;
        $searchModel->payType = RentsHouse::PAY_TYPE_REQUEST;

        $dataProvider = $searchModel->search(RentsBooking::TYPE_HOUSE);

        return $this->render('my-request', [
            'searchRModel' => $searchModel,
            'dataRProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Rents models.
     * @return mixed
     */
    public function actionMyBookingR()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_RENTS;

        $searchModel = new RentsBookingSearch();
        $searchModel->rentOwnerId = Yii::$app->user->id;
        $searchModel->payType = RentsHouse::PAY_TYPE_INSTANT;

        $dataProvider = $searchModel->search(RentsBooking::TYPE_HOUSE);

        return $this->render('my-booking-r', [
            'searchRModel' => $searchModel,
            'dataRProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rents model.
     * @param integer $id
     * @return mixed
     */
    public function actionCreateBooking($id)
    {
        $this->layout = '/modal';
        $rent = $this->findModel($id);
        $model = new RentsBooking();
        $model->rent_id = $rent->id;
        $model->rent_type = RentsBooking::TYPE_HOUSE;
        $model->user_id = Yii::$app->user->id;
        $model->date_reserve = date("Y-m-d H:i:s");
        $model->reserve_from = Yii::$app->request->get('reserve_from');
        $model->reserve_to = Yii::$app->request->get('reserve_to');
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('successSave');
            }
        }

        return $this->render('_booking', [
            'rent' => $rent,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single RentsHouse model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * $param id
     * @return mixed
     */
    public function actionGetRentBookings($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $bookings = RentsBooking::find()->where(['rent_id' => $id, 'rent_type' => RentsBooking::TYPE_HOUSE])->all();

        $events = [];
        foreach ($bookings as $booking){
            $event = new \yii2fullcalendar\models\Event();
            $event->id = $booking->id;
            $event->title = $booking->user ? $booking->user->getNameAndSurname() : '';
            $event->start = $booking->reserve_from;
            $event->end = $booking->reserve_to;
            $events[] = $event;
        }

        return $events;
    }

    /**
     * $param id
     * @return mixed
     */
    public function actionPayForBooking($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = null;
        if ($id == null) {
            $model = new RentsBooking();
            $model->rent_id = Yii::$app->request->post('rent_id');
            $model->rent_type = RentsBooking::TYPE_HOUSE;
            $model->user_id = Yii::$app->user->id;
            $model->date_reserve = date("Y-m-d H:i:s");
            $model->reserve_from = Yii::$app->request->post('reserve_from');
            $model->reserve_to = Yii::$app->request->post('reserve_to');
            $model->status = RentsBooking::STATUS_NEW;
            if ($model->save()) {
                $id = $model->id;
            }
        }

        if ($id) {
            if ($rent = $model->rentHouseModel) {
                $model = $this->findBookingModel($id);
                $message = 'Payed credits for rent';
                if ($status = PayRequests::creditsPayRequest(Yii::$app->user->id, $rent->user_id, $model->getNeedCreditsPayCount(), $model->id, PayRequests::OPERATION_TYPE_RENT_HOUSE_BOOKING, PayRequests::MODULE_RENT_HOUSE_BOOKING, $message)) {
                    $model->status = RentsBooking::STATUS_PAYED;
                    $model->save();
                }
            }
        }

        return ['status' => $status];
    }

    
    /**
     * Finds the RentsBooking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RentsBooking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findBookingModel($id)
    {
        if (($model = RentsBooking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Creates a new RentsHouse model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RentsHouse();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RentsHouse model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RentsHouse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RentsHouse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RentsHouse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RentsHouse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
