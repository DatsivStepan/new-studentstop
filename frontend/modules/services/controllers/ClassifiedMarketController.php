<?php

namespace frontend\modules\services\controllers;

use Yii,
    common\models\services\ClassifiedMarket,
    frontend\modules\services\models\ClassifiedMarketSearch,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\AccessControl,
    common\models\MenuItems,
    common\models\services\ClassifiedMarketCategories;

/**
 * ClassifiedMarketController implements the CRUD actions for ClassifiedMarket model.
 */
class ClassifiedMarketController extends Controller
{
    public $universityId;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function beforeAction($action) {
        if ($action == 'redirect') {
            $this->enableCsrfValidation = false;
        }
        $this->universityId = Yii::$app->user->identity->userInfoModel ? Yii::$app->user->identity->userInfoModel->university_id : null;
        if (!$this->universityId) {
            return $this->redirect(['/site/check-user-university']);
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all ClassifiedMarket models.
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_VIRTUAL_CLASSIFIED_MARKET;

        return $this->render('index', [
            'searchModel' => new ClassifiedMarketSearch(Yii::$app->request->queryParams),
            'categories' => ClassifiedMarketCategories::getAllParentCategories(),
        ]);
    }

    /**
     * Lists all ClassifiedMarket models.
     * @return mixed
     */
    public function actionSearch($id = null)
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_VIRTUAL_CLASSIFIED_MARKET;

        $searchModel = new ClassifiedMarketSearch();
        $searchModel->category_id = $id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Rents models.
     * @return mixed
     */
    public function actionFavorites()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_VIRTUAL_CLASSIFIED_MARKET;

        $searchModel = new ClassifiedMarketSearch();
        $searchModel->favorites = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('favorites', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Lists all ClassifiedMarket models.
     * @return mixed
     */
    public function actionMyAccount($id = null)
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_VIRTUAL_CLASSIFIED_MARKET;

        $searchModel = new ClassifiedMarketSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('my-account', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ClassifiedMarket model.
     * @param integer $id
     * @return mixed
     */
    public function actionShow($id)
    {
        $this->layout = '/modal';

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * $param id
     * @return mixed
     */
    public function actionMoreCategoires($id = null)
    {
        $this->layout = '/modal';
        $model = $this->findCategorysModel($id);

        return $this->render('_more_categories', [
            'model' => $model,
        ]);
    }
    
    /**
     * Creates a new ClassifiedMarket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '/modal';
        $model = new ClassifiedMarket();
        $model->university_id = $this->universityId;
        $model->user_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ClassifiedMarket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ClassifiedMarket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClassifiedMarketCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClassifiedMarketCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCategorysModel($id)
    {
        if (($model = ClassifiedMarketCategories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the ClassifiedMarket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClassifiedMarket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClassifiedMarket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
