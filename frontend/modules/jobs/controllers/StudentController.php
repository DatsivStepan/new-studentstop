<?php

namespace frontend\modules\jobs\controllers;

use Yii,
    common\models\user\UserJobsResume,
    frontend\modules\jobs\models\UserJobsResumeSearch,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    common\models\User,
    common\models\user\UserInfo,
    yii\filters\AccessControl,
    common\models\MenuItems,
    frontend\modules\jobs\models\CompaniesJobsSearch,
    common\models\Post,
    common\models\PostComment,
    frontend\models\PostSearch,
    frontend\models\CompaniesEventsSearch,
    yii\web\Response,
    frontend\modules\jobs\models\CompaniesSearch;

/**
 * UserJobsResumeController implements the CRUD actions for UserJobsResume model.
 */
class StudentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function beforeAction($action) {
        if ((Yii::$app->user->identity->hasJobResume()) || ($action->id == 'create')) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_STUDENT;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        return parent::beforeAction($action);
    }

    /**
     * Lists all Companies models.
     * @return mixed
     */
    public function actionDashboard()
    {
        $model = $this->findModel(Yii::$app->user->id);
        $filtersCompaniesId = $model->getFavoriteCompaniesIds();

        $searchJModel = new CompaniesJobsSearch();
        $searchJModel->filterTabType = CompaniesJobsSearch::FILTER_TYPE_MATCHES;
        $searchJModel->favoriteUser = Yii::$app->user->id;
        $searchJModel->load(Yii::$app->request->post());
        $dataJProvider = $searchJModel->search();

        $searchCModel = new CompaniesSearch();
        $searchCModel->favoriteUser = Yii::$app->user->id;
        $searchCModel->load(Yii::$app->request->post());
        $dataCProvider = $searchCModel->search();

        $searchEModel = new CompaniesEventsSearch();
        $searchEModel->filterDate = date('Y-m-d');
        $searchEModel->filterCompanyId = $filtersCompaniesId ? $filtersCompaniesId : 999999999999999;
        $searchEModel->load(Yii::$app->request->queryParams);
        $dataEProvider = $searchEModel->search();

        $modelNews = new Post;
        $modelNewsComment= new PostComment;
        if (Yii::$app->request->isPost && $modelNews->load(Yii::$app->request->post())) {
            $modelNews->university_id = $model->id;
            $modelNews->type = Post::TYPE_JOBS;
            $modelNews->user_id = \Yii::$app->user->id;
            $modelNews->status = Post::STATUS_ACTIVE;
            if ($modelNews->save()) {
                $modelNews = new Post;
                $modelNewsComment= new PostComment;
            }
        }

        $searchNModel = new PostSearch();
        $searchNModel->filterCompanyId = $filtersCompaniesId ? $filtersCompaniesId : 999999999999999;
        if (Yii::$app->request->isPost) {
            $searchNModel->load(Yii::$app->request->post());
        }
        $searchNModel->type = Post::TYPE_JOBS;
        $dataNProvider = $searchNModel->search(Yii::$app->request->queryParams);
        
        return $this->render('dashboard', [
            'modelNews' => $modelNews,

            'searchJModel' => $searchJModel,
            'dataJProvider' => $dataJProvider,

            'searchCModel' => $searchCModel,
            'dataCProvider' => $dataCProvider,

            'searchEModel' => $searchEModel,
            'dataEProvider' => $dataEProvider,

            'searchNModel' => $searchNModel,
            'dataNProvider' => $dataNProvider,
            'modelNewsComment' => $modelNewsComment,
        ]);
    }

        /**
     * Lists all CompaniesJobs models.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = new CompaniesJobsSearch();
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserJobsResume model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionCreate($id = null)
    {
        $this->layout = '/modal';
        $user = $this->findUserModel(Yii::$app->user->id);
        if (!$userInfo = UserInfo::findOne(['id_user' => $user->id])) {
            $userInfo = new UserInfo();
            $userInfo->id_user = $user->id;
        }

        if (!$model = $user->jobsResumeModel) {
            $model = new UserJobsResume();
            $model->user_id = Yii::$app->user->id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $userInfo->load(Yii::$app->request->post());
            $userInfo->save();
            Yii::$app->session->setFlash('successSave');
        }

        return $this->render('_form', [
            'user' => $user,
            'userInfo' => $userInfo,
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing UserJobsResume model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserJobsResume model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserJobsResume the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserJobsResume::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUserModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
