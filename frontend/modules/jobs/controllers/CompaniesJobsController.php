<?php

namespace frontend\modules\jobs\controllers;

use Yii,
    common\models\jobs\CompaniesJobs,
    frontend\modules\jobs\models\CompaniesJobsSearch,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    common\models\MenuItems,
    frontend\modules\jobs\models\UserJobsResumeSearch;

/**
 * CompaniesJobsController implements the CRUD actions for CompaniesJobs model.
 */
class CompaniesJobsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompaniesJobs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompaniesJobsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CompaniesJobs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->identity->hasCompany()) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_COMPANY;
        } else if (Yii::$app->user->identity->hasJobResume()) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_STUDENT;
        }
        
        $searchRModel = new UserJobsResumeSearch();
        $searchRModel->filterTabType = UserJobsResumeSearch::FILTER_TYPE_APPLY;
        $searchRModel->company_job_id = $model->id;
        $searchRModel->load(Yii::$app->request->post());
//        var_dump($searchRModel);exit;
        $dataRProvider = $searchRModel->searchForJob();
        
        return $this->render('view', [
            'model' => $model,
            
            'searchRModel' => $searchRModel,
            'dataRProvider' => $dataRProvider,
        ]);
    }

    /**
     * Creates a new CompaniesJobs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompaniesJobs();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CompaniesJobs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CompaniesJobs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompaniesJobs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompaniesJobs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompaniesJobs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
