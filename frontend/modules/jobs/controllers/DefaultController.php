<?php

namespace frontend\modules\jobs\controllers;

use Yii,
    yii\web\Controller,
    yii\filters\AccessControl,
    common\models\MenuItems,
    common\models\jobs\Companies,
    common\models\jobs\CompaniesEvents,
    common\models\jobs\CompaniesJobsApply,
    yii\web\Response,
    yii\web\NotFoundHttpException,
    common\models\Post,
    common\models\PostComment,
    frontend\modules\jobs\models\UserJobsResumeSearch,
    frontend\models\PostSearch,
    common\models\user\UserJobsResume;

/**
 * Default controller for the `jobs` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->hasCompany() || Yii::$app->user->identity->hasJobResume()) {
            return $this->redirect('/jobs/default/dashboard');
        }

        return $this->render('index');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionDashboard()
    {
        if (Yii::$app->user->identity->hasCompany()) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_COMPANY;
            return $this->redirect('/jobs/companies/dashboard');
        } else if (Yii::$app->user->identity->hasJobResume()) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_STUDENT;
            return $this->redirect('/jobs/student/dashboard');
        }

        return $this->redirect('/jobs/default/index');
    }
    
    
    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionEventCreate($id = null)
    {
        $this->layout = '/modal';
        $company = $this->findModel(Yii::$app->user->id);

        $model = new CompaniesEvents;
        if ($id) {
            $model = $this->findEventsModel($id);
        }
        $model->company_id = $company->id;
        $model->user_id = Yii::$app->user->Id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }
        
        return $this->render('_events_create', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCompany($id)
    {
        $model = $this->findCompanyModel($id);
        if (Yii::$app->user->identity->hasCompany()) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_COMPANY;
        } else if (Yii::$app->user->identity->hasJobResume()) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_STUDENT;
        }

        $searchRModel = new UserJobsResumeSearch();
        $searchRModel->filterTabType = UserJobsResumeSearch::FILTER_TYPE_MATCHES;
        $searchRModel->company_id = $model->id;
        $searchRModel->company_owner_id = $model->user_id;
        $searchRModel->load(Yii::$app->request->post());
        $dataRProvider = $searchRModel->search();

        $modelNewsComment= new PostComment;
        $searchNModel = new PostSearch();
        if (Yii::$app->request->isPost) {
            $searchNModel->load(Yii::$app->request->post());
        }
        $searchNModel->university_id = $model->id;
        $searchNModel->type = Post::TYPE_JOBS;
        $dataNProvider = $searchNModel->search(Yii::$app->request->queryParams);


        return $this->render('company', [
            'model' => $model,
            
            'searchRModel' => $searchRModel,
            'dataRProvider' => $dataRProvider,

            'searchNModel' => $searchNModel,
            'dataNProvider' => $dataNProvider,
            'modelNewsComment' => $modelNewsComment,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionShowResume($id)
    {
        $this->layout = '/modal';
        $model = $this->findResumeModel($id);
        return $this->render('_show-resume', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionEventShow($id)
    {
        $this->layout = '/modal';
        $model = $this->findEventsModel($id);
        return $this->render('_events_show', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionApplyJob($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $status = $type = null;
        if ($model = CompaniesJobsApply::findOne(['company_job_id' => $id])) {
            $status = $model->delete();
            $type = 'delete';
        } else {
            $model = new CompaniesJobsApply();
            $model->user_id = Yii::$app->user->id;
            $model->company_job_id = $id;
            $status = $model->save();
            $type = 'new';
        }

        return ['status' => $status, 'type' => $type];
    }

    /**
     * Finds the CompaniesEvents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @return CompaniesEvents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEventsModel($id)
    {
        if (($model = CompaniesEvents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Finds the UserJobsResume model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @return UserJobsResume the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findResumeModel($id)
    {
        if (($model = UserJobsResume::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Finds the Companies model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @return Companies the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCompanyModel($id)
    {
        if (($model = Companies::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Finds the Companies model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @return Companies the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user_id)
    {
        if (($model = Companies::findOne(['user_id' => $user_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
