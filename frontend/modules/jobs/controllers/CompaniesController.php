<?php

namespace frontend\modules\jobs\controllers;

use Yii,
    common\models\jobs\Companies,
    frontend\modules\jobs\models\CompaniesSearch,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    frontend\modules\jobs\models\CompaniesJobsSearch,
    common\models\jobs\CompaniesJobs,
    common\models\MenuItems,
    frontend\models\CompaniesEventsSearch,
    frontend\modules\jobs\models\UserJobsResumeSearch,
    common\models\jobs\CompaniesJobsInvite,
    common\models\Post,
    common\models\PostComment,
    frontend\models\PostSearch;

/**
 * CompaniesController implements the CRUD actions for Companies model.
 */
class CompaniesController extends Controller
{
    public $companyId;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function beforeAction($action) {
        if (($company = Yii::$app->user->identity->hasCompany(true)) || ($action->id == 'create')) {
            $this->companyId = $company ? $company->id : null;
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_COMPANY;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        return parent::beforeAction($action);
    }

    /**
     * Lists all Companies models.
     * @return mixed
     */
    public function actionDashboard()
    {
        $model = $this->findModel(Yii::$app->user->id);

        $searchRModel = new UserJobsResumeSearch();
        $searchRModel->filterTabType = UserJobsResumeSearch::FILTER_TYPE_MATCHES;
        $searchRModel->company_id = $model->id;
        $searchRModel->company_owner_id = $model->user_id;
        $searchRModel->load(Yii::$app->request->post());
        $dataRProvider = $searchRModel->search();

        $searchEModel = new CompaniesEventsSearch();
        $searchEModel->company_id = $model->id;
        $searchEModel->filterDate = date('Y-m-d');
        $searchEModel->load(Yii::$app->request->queryParams);
        $dataEProvider = $searchEModel->search();

        $modelNews = new Post;
        $modelNewsComment= new PostComment;
        if (Yii::$app->request->isPost && $modelNews->load(Yii::$app->request->post())) {
            $modelNews->university_id = $model->id;
            $modelNews->type = Post::TYPE_JOBS;
            $modelNews->user_id = \Yii::$app->user->id;
            $modelNews->status = Post::STATUS_ACTIVE;
            if ($modelNews->save()) {
                $modelNews = new Post;
                $modelNewsComment= new PostComment;
            }
        }

        $searchNModel = new PostSearch();
        if (Yii::$app->request->isPost) {
            $searchNModel->load(Yii::$app->request->post());
        }
        $searchNModel->university_id = $model->id;
        $searchNModel->type = Post::TYPE_JOBS;
        $dataNProvider = $searchNModel->search(Yii::$app->request->queryParams);

        return $this->render('dashboard', [
            'modelNews' => $modelNews,

            'searchRModel' => $searchRModel,
            'dataRProvider' => $dataRProvider,

            'searchEModel' => $searchEModel,
            'dataEProvider' => $dataEProvider,

            'searchNModel' => $searchNModel,
            'dataNProvider' => $dataNProvider,
            'modelNewsComment' => $modelNewsComment,
        ]);
    }

    /**
     * Lists all UserJobsResume models.
     * @return mixed
     */
    public function actionFollowers()
    {
        $model = $this->findModel(Yii::$app->user->id);

        $searchModel = new UserJobsResumeSearch();
        $searchModel->company_id = $model->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('followers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all UserJobsResume models.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = new UserJobsResumeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Lists all Companies models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompaniesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Companies model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return mixed
     */
    public function actionJob($id)
    {
        if (Yii::$app->user->identity->hasCompany()) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_COMPANY;
        } else if (Yii::$app->user->identity->hasJobResume()) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_STUDENT;
        }
        $model = $this->findJobModel($id);

        return $this->render('job', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionJobs()
    {
        if (Yii::$app->user->identity->hasCompany()) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_COMPANY;
        } else if (Yii::$app->user->identity->hasJobResume()) {
            Yii::$app->params['menuType'] = MenuItems::TYPE_JOBS_STUDENT;
        }
        
        $model = $this->findModel(Yii::$app->user->id);

        $searchModel = new CompaniesJobsSearch();
        $searchModel->company_id = $model->id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('jobs', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionCreateInvite($id = null)
    {
        $this->layout = '/modal';

        $to_user_id = Yii::$app->request->get('to_user_id');
        if (!($modelC = Yii::$app->user->identity->hasCompany(true)) || !$to_user_id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (!$model = CompaniesJobsInvite::findOne($id)) {
            $model = new CompaniesJobsInvite();
            $model->to_user_id = $to_user_id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }
        return $this->render('_create_invite', [
            'model' => $model,
            'companiesJobs' => CompaniesJobs::getAllInArrayMap($modelC->id)
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionCreateJob($id = null)
    {
        $this->layout = '/modal';
        $company = $this->findModel(Yii::$app->user->id);

        if (!$model = CompaniesJobs::findOne($id)) {
            $model = new CompaniesJobs();
            $model->company_id = $company->id;
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }

        return $this->render('/companies-jobs/_form', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '/modal';
        if (!$model = Companies::findOne(['user_id' => Yii::$app->user->id])) {
            $model = new Companies();
            $model->user_id = Yii::$app->user->id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Companies model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Companies model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Companies model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @return Companies the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user_id)
    {
        if (($model = Companies::findOne(['user_id' => $user_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Companies model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @return Companies the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findJobModel($id)
    {
        if (($model = CompaniesJobs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
