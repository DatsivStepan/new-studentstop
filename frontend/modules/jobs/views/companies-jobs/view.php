<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\jobs\models\UserJobsResumeSearch;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\rating\StarRating;
use common\models\Favorites;

/* @var $this yii\web\View */
/* @var $model common\models\jobs\CompaniesJobs */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies Jobs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->identity->hasJobResume()) {
    echo $this->render('/student/_search');
} else {
    echo $this->render('/companies/_search');
}
?>
<div class="companies-jobs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-6">
                <ul class="nav nav-tabs js-tabs-filter-list" role="tablist">
                    <li class="nav-item">
                        <?= Html::a('Apply', null, [
                            'class' => 'nav-link active js-filter-tabs',
                            'data-toggle' => "tab",
                            'href' => "#tabF",
                            'role' => "tab",
                            'aria-selected' => "false",
                            'data-filter_type' => UserJobsResumeSearch::FILTER_TYPE_APPLY,
                        ])?>
                    </li>
                    <li class="nav-item">
                        <?= Html::a('Favorite', null, [
                            'class' => 'nav-link js-filter-tabs',
                            'data-toggle' => "tab",
                            'href' => "#tabF",
                            'role' => "tab",
                            'aria-selected' => "false",
                            'data-filter_type' => UserJobsResumeSearch::FILTER_TYPE_FAVORITE
                        ])?>
                    </li>
                </ul>
                <div class="tab-content my-scroll clearfix">
                    <div class="tab-pane my-scroll active" id="tabF" role="tabpanel" style="position:relative;">
                        <div class="loader-style js-loader-block">
                            <img src="/images/loader.gif" style="position: absolute;width:150px;margin: auto;left: 0;top: 0;bottom: 0;right: 0;">
                        </div>
                        <?php Pjax::begin(['id' => 'pjax-form-filter-tab', 'enablePushState' => false]); ?>

                            <?php $form = ActiveForm::begin(['id' => 'form-filter-tab', 'options' => ['data-pjax' => 'pjax-form-filter-tab']]); ?>
                                <?= $form->field($searchRModel, 'filterTabType')->hiddenInput()->label(false) ?>
                            <?php ActiveForm::end(); ?>

                            <?php if ($resumes = $dataRProvider->getModels()) { ?>
                                <?php foreach ($resumes as $resume) { ?>
                                    <?= $this->render('/companies/_resume-item-templ', [
                                        'model' => $resume,
                                        'showButton' => 'not_show'
                                    ]); ?>
                                <?php } ?>
                            <?php } else { ?>
                                <p>Not found</p>
                            <?php } ?>
                                <script>
                                    $('.js-loader-block').css('display', 'none');
                                </script>
                        <?php Pjax::end(); ?>
                    </div>
                </div>
        </div>
        <div class="col-sm-6">
            <?php if ($modelC = $model->companiesModel) { ?>
                <div class="company-container">
                    <img style="width:150px" src="<?= $modelC->getImages(); ?>">
                    <?= Html::a($modelC->getName(), $modelC->getLink()); ?>
                    Vacancies:<?= $modelC->getJobsCount(); ?>
                    
                    <?php if (Yii::$app->user->identity->hasJobResume()) { ?>
                        <?php $fStatus = Favorites::getFavoriteStatus(Favorites::TYPE_JOBS_COMPANY, $modelC->id); ?>
                        <?= Html::tag('i', '', [
                            'class' => 'glyphicon glyphicon-star cm-favorite-star js-favorite-star-button ' . ($fStatus ? 'active' : ''),
                            'data-type' => Favorites::TYPE_JOBS_COMPANY,
                            'data-id' => $modelC->id
                        ]); ?>
                    <?php } ?>
                    
                </div>
            <?php } ?>
            
            <div class="row item-jobs-block js-item-content-block" data-id="<?= $model->id; ?>">
                <div class="col-sm-6">
                    <img style="width:100%" src="<?= $model->getImages(); ?>">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <?php if (Yii::$app->user->identity->hasJobResume()) { ?>
                                <?php $applyStatus = $model->getApplyStatus(); ?>
                                <?= Html::button($applyStatus ? 'Unapply' : 'Apply', [
                                    'class' => 'btn btn-primary js-apply-job',
                                    'data-job_id' => $model->id,
                                ]); ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h4>
                        <?= $model->name; ?>
                        <?php if (Yii::$app->user->identity->hasJobResume()) { ?>
                            <?php $fStatus = Favorites::getFavoriteStatus(Favorites::TYPE_JOBS_RESUME, $model->id); ?>
                            <?= Html::tag('i', '', [
                                'class' => 'glyphicon glyphicon-star cm-favorite-star js-favorite-star-button ' . ($fStatus ? 'active' : ''),
                                'data-type' => Favorites::TYPE_JOBS_RESUME,
                                'data-id' => $modelC->id
                            ]); ?>
                        <?php } ?>
                    </h4>
                    <div class="row">
                        <table style="width:100%;">
                            <tr>
                                <td>salary</td>
                                <td><?= $model->salary; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    $('.js-filter-tabs').on('shown.bs.tab', function (e) {
        var filterType = $(this).data('filter_type');
        $('.js-loader-block').css('display', 'block');
        $('#userjobsresumesearch-filtertabtype').val(filterType);
        $('#form-filter-tab').submit();
    });
</script>