<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    frontend\modules\jobs\models\CompaniesJobsSearch;

/* @var $this yii\web\View */
/* @var $model frontend\modules\jobs\models\CompaniesJobsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jobs-search">
        <?php $form = ActiveForm::begin([
            'action' => ['search'],
            'method' => 'get',
        ]); ?>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'search_value')->textInput(['class' => "form-control"])->label(false) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'search_type')->dropDownList(CompaniesJobsSearch::$searchTypes, ['class' => "form-control"])->label(false) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

</div>
