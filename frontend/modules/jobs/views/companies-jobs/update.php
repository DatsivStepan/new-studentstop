<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\jobs\CompaniesJobs */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Companies Jobs',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies Jobs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="companies-jobs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
