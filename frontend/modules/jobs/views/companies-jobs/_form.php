<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    common\models\user\UserInfo,
    common\models\jobs\JobsFunctionality,
    yii\widgets\Pjax,
    kartik\select2\Select2,
    common\models\jobs\CompaniesJobs,
    common\models\University,
    common\models\UserRole;

/* @var $this yii\web\View */
/* @var $model common\models\jobs\CompaniesJobs */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(['id' => 'pjax-form-jobs-create', 'enablePushState' => false]); ?>
    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            if ($modal.studentResumeCreateModal) {
                $modal.studentResumeCreateModal.close();
            }
            if ($modal.studentResumeUpdateModal) {
                $modal.studentResumeUpdateModal.close();
            }
            
            location.reload()
        </script>
    <?php } ?>

    <div class="user-jobs-jobs-form">

        <?php $form = ActiveForm::begin(['id' => 'form-jobs', 'options' => ['data-pjax' => 'pjax-form-jobs-create']]); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-sm-3 col-xl-2 text-modal">
                    <text>Image</text>
                </div>
                <div class="col-sm-6 col-xl-7">
                    <div>
                        <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?> 
                        <img src="<?= $model->getImages(); ?>" class="js-preview-jobs-image img-thumbnail"><br>
                        <a class="btn btn-sm btn-success js-add-jobs-image" style="width:100%">Change image</a><br><br>
                    </div>
                </div>
            </div>
        
            <?= $form->field($model, 'gender')->dropDownList(array_merge([null => 'All gender'], UserInfo::$arrayGender), ['placeholder' => 'All gender']) ?>

            <?= $form->field($model, 'user_role_need')->widget(Select2::classname(), [
                    'data' => array_merge([null => 'All Academic status'], UserRole::getAllRoleInArrayMap()),
                    'options' => [
                        'placeholder' => Yii::t('app', 'Select...')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]); ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'show_in_university')->widget(Select2::classname(), [
                    'data' => array_merge([null => 'All university'], University::getAllUniversityInArrayMap()),
                    'options' => [
                        'placeholder' => Yii::t('app', 'Select...')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]); ?>

            <?= $form->field($model, 'jobs_function_id')
                ->widget(Select2::classname(), [
                    'data' => JobsFunctionality::getAllInArrayMap(),
                    'options' => [
                        'placeholder' => Yii::t('app', 'Select...')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]); ?>

            <?= $form->field($model, 'salary')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'salary_type')->dropDownList(CompaniesJobs::$arraySalaryTypes) ?>

            <?= $form->field($model, 'industry_level')->dropDownList(CompaniesJobs::$arrayIndustryLevel) ?>

            <?= $form->field($model, 'skills')->widget(Select2::classname(), [
                    'data' => [],
                    'options' => ['placeholder' => 'Type skills ...', 'multiple' => true],
                    'pluginOptions' => [
                        'tags' => true,
                        'tokenSeparators' => [',', ' '],
                        'maximumInputLength' => 10
                    ],
                ]); ?>

        <?php ActiveForm::end(); ?>

    </div>
    <script>
        $(function(){
            $dropzone.initializationDropzoneEleemnt('jobs', '#companiesjobs-img_src', 'image/*');
        })
    </script>
<?php Pjax::end(); ?>