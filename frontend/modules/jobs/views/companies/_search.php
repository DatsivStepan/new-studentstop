<?php

use yii\helpers\Html,
    yii\helpers\Url,
    yii\widgets\ActiveForm,
    frontend\modules\jobs\models\UserJobsResumeSearch;

/* @var $this yii\web\View */
/* @var $model frontend\modules\jobs\models\CompaniesSearch */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(Url::to(['/js/jobs/jobs.js']));

$model = empty($model) ? new \frontend\modules\jobs\models\UserJobsResumeSearch : $model; 
?>
<style>
    .cm-favorite-star.active {
        color:yellow;
    }
</style>
<div class="user-jobs-resume-search">
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/jobs/companies/search']),
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'search_value')->textInput(['class' => "form-control", 'placeholder' => 'Enter search text...'])->label(false) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'search_type')->dropDownList(UserJobsResumeSearch::$searchTypes, ['class' => "form-control"])->label(false) ?>
        </div>
        <div class="col-sm-2">
            <?= Html::submitButton('Filter', ['class' => 'btn btn-primary']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>