<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\jobs\models\UserJobsResumeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Jobs Resumes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-jobs-resume-index">

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?php if ($models = $dataProvider->getModels()) { ?>
        <div class="row js-resume-container">
            <?php foreach ($models as $model) { ?>
                <div class="col-sm-6">
                    <?= $this->render('_resume-item-templ', [
                        'model' => $model
                    ]); ?>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <p>Not found</p>
    <?php } ?>
</div>
