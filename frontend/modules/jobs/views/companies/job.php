<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\jobs\CompaniesJobs */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies Jobs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
if (Yii::$app->user->identity->hasCompany()) {
    echo $this->render('/companies/_search');
} else {
    echo $this->render('/student/_search');
}
?>
<div class="companies-jobs-view row">
    <div class="col-sm-6">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'company_id',
                'name',
                'gender',
                'user_role_need',
                'description:ntext',
                'show_in_university',
                'major_id',
                'jobs_function_id',
                'salary',
                'salary_type',
                'industry_level',
                'skills',
                'img_src',
                'type',
                'status',
                'created_at',
                'updated_at',
            ],
        ]) ?>
    </div>
    <div class="col-sm-6">
        
    </div>
</div>
