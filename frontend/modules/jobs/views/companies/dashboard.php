<?php
    use yii\helpers\Html,
        yii\helpers\Url,
        common\models\Favorites,
        yii\widgets\Pjax,
        yii\widgets\ActiveForm,
        frontend\modules\jobs\models\UserJobsResumeSearch;

?>
<?= $this->render('/companies/_search'); ?>
<style>
    .loader-style {
        display:none;position:absolute;width:100%;height:100%;background-color: grey;opacity:0.3;z-index:9999;
    }
</style>
<div class="row">
    <div class="col-sm-6 js-resume-container">
        <ul class="nav nav-tabs js-tabs-filter-list" role="tablist">
            <li class="nav-item">
                <?= Html::a('Matches', null, [
                    'class' => 'nav-link active js-filter-tabs',
                    'data-toggle' => "tab",
                    'href' => "#tabF",
                    'role' => "tab",
                    'aria-selected' => "false",
                    'data-filter_type' => UserJobsResumeSearch::FILTER_TYPE_MATCHES,
                ])?>
            </li>
            <li class="nav-item">
                <?= Html::a('Favorite', null, [
                    'class' => 'nav-link js-filter-tabs',
                    'data-toggle' => "tab",
                    'href' => "#tabF",
                    'role' => "tab",
                    'aria-selected' => "false",
                    'data-filter_type' => UserJobsResumeSearch::FILTER_TYPE_FAVORITE
                ])?>
            </li>
            <li class="nav-item">
                <?= Html::a('Offers', null, [
                    'class' => 'nav-link js-filter-tabs',
                    'data-toggle' => "tab",
                    'href' => "#tabF",
                    'role' => "tab",
                    'aria-selected' => "false",
                    'data-filter_type' => UserJobsResumeSearch::FILTER_TYPE_OFFERS
                ])?>
            </li>
            <li class="nav-item">
                <?= Html::a('Apply', null, [
                    'class' => 'nav-link js-filter-tabs',
                    'data-toggle' => "tab",
                    'href' => "#tabF",
                    'role' => "tab",
                    'aria-selected' => "false",
                    'data-filter_type' => UserJobsResumeSearch::FILTER_TYPE_APPLY
                ])?>
            </li>
            <li class="nav-item">
                <?= Html::a('News', null, [
                    'class' => 'nav-link',
                    'data-toggle' => "tab",
                    'href' => "#tabN",
                    'role' => "tab",
                    'aria-selected' => "false"
                ])?>
            </li>
        </ul>
        <div class="tab-content my-scroll clearfix">
            <div class="tab-pane my-scroll active" id="tabF" role="tabpanel" style="position:relative;">
                <div class="loader-style js-loader-block">
                    <img src="/images/loader.gif" style="position: absolute;width:150px;margin: auto;left: 0;top: 0;bottom: 0;right: 0;">
                </div>
                <?php Pjax::begin(['id' => 'pjax-form-filter-tab', 'enablePushState' => false]); ?>
                
                    <?php $form = ActiveForm::begin(['id' => 'form-filter-tab', 'options' => ['data-pjax' => 'pjax-form-filter-tab']]); ?>
                        <?= $form->field($searchRModel, 'filterTabType')->hiddenInput()->label(false) ?>
                    <?php ActiveForm::end(); ?>

                    <?php if ($resumes = $dataRProvider->getModels()) { ?>
                        <?php foreach ($resumes as $resume) { ?>
                            <?= $this->render('_resume-item-templ', [
                                'model' => $resume
                            ]); ?>
                        <?php } ?>
                    <?php } else { ?>
                        <p>Not found</p>
                    <?php } ?>
                        <script>
                            $('.js-loader-block').css('display', 'none');
                        </script>
                <?php Pjax::end(); ?>
            </div>
            <div class="tab-pane my-scroll" id="tabN" role="tabpanel">
                <?= $this->render('_news', [
                    'accessCreate' => true,
                    'modelNews' => $modelNews,
                    'modelNewsComment' => $modelNewsComment,
                    'searchNModel' => $searchNModel,
                    'dataNProvider' => $dataNProvider,
                ]); ?>
                News
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <?= $this->render('/default/_top'); ?>
        <?= $this->render('/default/_events', [
            'dataEProvider' => $dataEProvider,
            'searchEModel' => $searchEModel
        ]); ?>
    </div>
</div>

<script>
    $('.js-filter-tabs').on('shown.bs.tab', function (e) {
        var filterType = $(this).data('filter_type');
        $('.js-loader-block').css('display', 'block');
        $('#userjobsresumesearch-filtertabtype').val(filterType);
        $('#form-filter-tab').submit();
    });

</script>