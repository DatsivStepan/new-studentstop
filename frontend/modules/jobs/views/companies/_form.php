<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    yii\widgets\Pjax,
    kartik\select2\Select2,
    common\models\jobs\JobsIndustries,
    kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\jobs\Companies */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(['id' => 'pjax-form-company-create', 'enablePushState' => false]); ?>
    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            if ($modal.jobsCompanyCreateModal) {
                $modal.jobsCompanyCreateModal.close();
            }
            if ($modal.jobsCompanyUpdateModal) {
                $modal.jobsCompanyUpdateModal.close();
            }
            
            location.reload()
        </script>
    <?php } ?>
    <div class="companies-form">

        <?php $form = ActiveForm::begin(['id' => 'form-company', 'options' => ['data-pjax' => 'pjax-form-company-create']]); ?>

            <div class="row">
                <div class="col-sm-3 col-xl-2 text-modal">
                    <text>Image</text>
                </div>
                <div class="col-sm-6 col-xl-7">
                    <div>
                        <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?> 
                        <img src="<?= $model->getImages(); ?>" class="js-preview-company-image img-thumbnail"><br>
                        <a class="btn btn-sm btn-success js-add-company-image" style="width:100%">Change image</a><br><br>
                    </div>
                </div>
            </div>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'founded')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true
                ]
            ]); ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'social_link_facebook')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'social_link_twitter')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'social_link_linkedin')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'industry_id')
                    ->widget(Select2::classname(), [
                        'data' => JobsIndustries::getAllInArrayMap(),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Select...')
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]); ?>

        <?php ActiveForm::end(); ?>
    </div>
    <script>
        $(function(){
            $dropzone.initializationDropzoneEleemnt('company', '#companies-img_src', 'image/*');
        })
    </script>
<?php Pjax::end(); ?>
