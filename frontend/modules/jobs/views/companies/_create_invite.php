<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    yii\widgets\Pjax,
    kartik\select2\Select2,
    common\models\jobs\JobsIndustries,
    kartik\date\DatePicker,
    common\models\jobs\CompaniesJobs;

/* @var $this yii\web\View */
/* @var $model common\models\jobs\Companies */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="companies-form">
    <?php Pjax::begin(['id' => 'pjax-form-company-create', 'enablePushState' => false]); ?>
        <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
            <script>
                if ($modal.resumeInviteModal) {
                    $modal.resumeInviteModal.close();
                }
                location.reload()
            </script>
        <?php } ?>

        <?php $form = ActiveForm::begin(['id' => 'form-company', 'options' => ['data-pjax' => 'pjax-form-company-create']]); ?>

            <?= $form->field($model, 'salary')->input('number') ?>

            <?= $form->field($model, 'salary_type')->widget(Select2::classname(), [
                    'data' => CompaniesJobs::$arraySalaryTypes,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Select...')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]); ?>

            <?= $form->field($model, 'company_job_id')->widget(Select2::classname(), [
                    'data' => $companiesJobs,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Select...')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]); ?>

        <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
