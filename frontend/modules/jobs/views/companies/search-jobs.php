<?php

use yii\helpers\Html,
    kartik\grid\GridView,
    yii\widgets\Pjax,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\jobs\models\CompaniesJobsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Search vacancies');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Url::to(['/js/jobs/jobs.js']));
?>
<div class="companies-jobs-index">

    <?= $this->render('/companies-jobs/_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pjax' => true,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'label' => 'Company',
                'attribute' => 'company_id',
                'value' => function($model) {
                    return $model->companiesModel ? $model->companiesModel->getName() : '';
                }
            ],
            [
                'label' => 'Expected Salary',
                'attribute' => 'salary',
                'value' => function($model) {
                    return $model->getSalary();
                }
            ],
            [
                'label' => 'Data listed',
                'attribute' => 'created_at',
                'value' => function($model) {
                    return $model->getDateCreate();
                }
            ],
            [
                'label' => 'Favotites',
            ],
            [
                'label' => '',
                'value' => function($model) {
                    return Html::a('View', Url::toRoute([$model->getLink()]), [
                        'class' => 'btn btn-info',
                        'data-pjax' => 0
                    ]);
                },
                'format' => 'raw'
            ],
        ],
    ]); ?>
</div>
