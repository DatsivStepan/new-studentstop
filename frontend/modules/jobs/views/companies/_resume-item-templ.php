<?php
    use yii\helpers\Html,
        common\models\Favorites,
        kartik\rating\StarRating;
    
$showButton = !empty($showButton) ? ($showButton === 'not_show' ? false : true) : true;
?>
<div class="row item-jobs-block js-item-content-block" data-id="<?= $model->id; ?>">
    <div class="col-sm-6">
        <img style="width:100%" src="<?= $model->getAvatar(); ?>">
        <?php if ($showButton) { ?>
            <div class="row">
                <div class="col-sm-6 text-center" >
                    <?= Html::button('Invite', ['class' => 'btn btn-primary js-create-invite', 'data-user_id' => $model->userModel ? $model->userModel->id : 0]); ?>
                </div>
                <div class="col-sm-6 text-center">
                    <?= Html::button('Chat', ['class' => 'btn btn-primary js-mail-to-user', 'data-myid' => Yii::$app->user->id, 'data-user_id' => $model->user_id]); ?>
                </div>
                <div class="col-sm-12 text-center">
                    <?= Html::button('View', ['class' => 'btn btn-primary js-show-resume', 'data-id' => $model->id]); ?>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="col-sm-6">
        <h4>
            <?= $model->userModel ? $model->userModel->getNameAndSurname() : ''; ?>

            <?php if ($showButton) { ?>
                <?php $fStatus = Favorites::getFavoriteStatus(Favorites::TYPE_JOBS_JOB, $model->id); ?>
                <?= Html::tag('i', '', [
                    'class' => 'glyphicon glyphicon-star cm-favorite-star js-favorite-star-button ' . ($fStatus ? 'active' : ''),
                    'data-type' => Favorites::TYPE_JOBS_JOB
                ]); ?>
            <?php } ?>
        </h4>
        <div>
            <?= StarRating::widget([
                'id' => 'show_reting_resum_' . $model->id,
                'name' => 'show_reting_resum_' . $model->id,
                'value' => $model->userModel ? $model->userModel->getRating() : '',
                'disabled' => true,
                'pluginOptions' => [
                    'size' => 'xs',
                    'min' => 0,
                    'max' => 5
                ],
            ]); ?>
        </div>
        <div class="row">
            <table style="width:100%;">
                <tr>
                    <td>Address</td>
                    <td><?= $model->userInfoModel ? $model->userInfoModel->address : ''; ?></td>
                </tr>
                <tr>
                    <td>University</td>
                    <td><?= $model->userInfoModel ? $model->userInfoModel->getUniversityName() : ''; ?></td>
                </tr>
                <tr>
                    <td><?= $model->getAttributeLabel('current_gpa'); ?></td>
                    <td><?= $model->current_gpa; ?></td>
                </tr>
                <tr>
                    <td><?= $model->getAttributeLabel('overall_gpa'); ?></td>
                    <td><?= $model->overall_gpa; ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>