<?php

use yii\helpers\Html,
    yii\helpers\Url,
    kartik\rating\StarRating,
    common\models\jobs\Companies,
    yii\widgets\Pjax,
    yii\widgets\ActiveForm,
    common\models\Post;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(Url::to(['/js/plugins/moment.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepicker.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepaginator.js']));

$this->registerCssFile(Url::to(['/css/plugins/bootstrap-datepaginator.css']));
?>
<div id="js-post-block">
    <?php Pjax::begin(['id' => 'pjax-form', 'enablePushState' => false]); ?>
        <div class="post-view-container">
            <?php if ($accessCreate && ($modelNews != null)) { ?>
                <div class="row no-gutters new-post">
                    <?php $form = ActiveForm::begin(['id' => 'create-form-item', 'options' => ['data-pjax' => 'pjax-form']]); ?>
                        <div class="post-textarea">
                            <?= $form->field($modelNews, 'content')->textarea(['class' => 'w-100 my-scroll', 'placeholder' => 'Write your news'])->label(false) ?>
                            <?= Html::submitButton(null, ['class' => 'create-post']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            <?php } ?>
            <div>
                <div class="view-post-items">
                    <?php if ($posts = $dataNProvider->getModels()) { ?>
                        <?php foreach ($posts as $key => $post) { ?>
                            <div class="col-12 view-post-item" id="post-block-list-<?= $post->id; ?>">
                                <div class="row no-gutters justify-content-between">
                                    <div class="col-auto m-w-60">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto post-user-avatar">
                                                <div class="user-avatar" style="background: url(<?= $post->type == Post::TYPE_UNIVERSITY ? ($post->universityModel ? $post->universityModel->getImages() : 'univer') : $post->getAuthorAvatar(); ?>) center/cover;"></div>
                                            </div>
                                            <div class="col-auto post-data">
                                                <div class="post-user-name">
                                                    <?= $post->type == Post::TYPE_UNIVERSITY ? Html::a(($post->universityModel ? $post->universityModel->getName() : 'univer'), null) : Html::a($post->getAuthorName(), $post->getAuthorLink()) ; ?>
                                                </div>
                                                <div class="post-datetime">
                                                    <?= $post->getDateCreate(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto stats m-w-40">
                                        <div class="row no-gutters">
                                            <div class="col-auto likes icons-hover js-post-like-action <?= $post->getIfUserLikes() ? 'active' : ''; ?>" data-post_id="<?= $post->id; ?>" title="Like">
                                                <?= $post->getLikesCount(); ?>
                                            </div>
                                            <?php $count = $post->getCommentsCount(); ?>
                                            <div class="col-auto comments <?= $count ? 'active' : ''?>">
                                                <?= $count; ?>
                                            </div>
                                            <?php $countV = $post->getViewsCount(); ?>
                                            <div class="col-auto views <?= $countV ? 'active' : ''?>">
                                                <?= $countV; ?>
                                            </div>

                                            <?php if ($post->user_id == Yii::$app->user->id) { ?>
                                                <div class="col-auto js-update-post update" data-id="<?= $post->id; ?>"></div>
                                                <div class="col-auto js-delete-post delete" data-id="<?= $post->id; ?>"></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-gutters js-show-post-info" data-id="<?= $post->id; ?>" style="cursor:pointer;">
                                    <div class="col-12 post-content">
                                        <?= substr($post->getContent(), 0, 150).'...'; ?>
                                    </div>
                                    <div class="col-12">
                                        <div class="row post-files">
                                            <?php 
                                            $gridFiles = [
                                              1 => [0 => 12],
                                              2 => [0 => 6, 1 => 6],
                                              3 => [0 => 12, 1 => 6, 2 => 6],
                                              4 => [0 => 12, 1 => 4, 2 => 4, 3 => 4],
                                            ];

                                            if ($files = $post->getFilesWithLimit(4)) { 
                                              $countFiles = count($files);
                                            ?>
                                              <?php foreach ($files as $key => $file) { ?>
                                                  <div class="well col-<?= $gridFiles[$countFiles][$key]?>">
                                                        <div class="media-item">
                                                            <?php switch($file->type) {
                                                                case 'image': ?>
                                                                  <img src="/<?= $file->file_src; ?>" class="align-self-center"/>
                                                              <?php  break;
                                                                case 'video': ?>
                                                                  <video width="100%" controls class="align-self-center" controlsList="nodownload">
                                                                    <source src="/<?= $file->file_src; ?>" type="video/mp4">
                                                                    Your browser does not support the video tag.
                                                                  </video>
                                                              <?php break; ?>
                                                            <?php } ?>
                                                        </div>
                                                  </div>
                                              <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-gutters">
                                    <div class="col-12 comment-form">
                                        <?php $form = ActiveForm::begin(['options' => ['class' => 'create-form-comment']]); ?>
                                            <div class="update-data">
                                                <h3 class="inner"> 
                                                   <?= Html::img('/images/loader.gif', ['height' => 25]) ?>
                                                </h3>
                                            </div>
                                            <?= $form->field($modelNewsComment, 'post_id')->hiddenInput(['value' => $post->id])->label(false) ?>
                                            <?= $form->field($modelNewsComment, 'content')->textarea(['class' => 'w-100 my-scroll', 'placeholder' => 'Write your comment'])->label(false) ?>
                                            <?= Html::submitButton('', ['class' => 'create-post write-comment icons-hover js-send-comment', 'style' => 'cursor:pointer;']) ?>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                    <div class="col-12 comment-content">
                                        <?php if ($comment = $post->getLastComment()) { ?>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto comment-user-avatar">
                                                    <div class="comment-avatar" style="background: url(<?= $comment->getAuthorAvatar(); ?>) center/cover;"></div>
                                                </div>
                                                <div class="col-auto post-data">
                                                    <div class="post-user-name">
                                                        <?= Html::a($comment->getAuthorName(), $comment->getAuthorLink()) ; ?>
                                                    </div>
                                                </div>
                                                <div class="col-auto post-data ml-auto">
                                                    <div class="post-datetime">
                                                        <?= $comment->getDateCreate(); ?>
                                                    </div>
                                                </div>
                                                <div class="col-12 post-content">
                                                    <?= $comment->getContent(); ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="view-post-items">
                            <div class="col-12 view-post-item" id="post-block-list-12">
                                <p class="text-center">Not result</p>
                            </div>
                        </div>
                    <?php } ?>
                  </div>
            </div>
        </div>
    <?php Pjax::end(); ?>
</div>
<script>
    $(function(){
        $('#js-post-block').on('click', '.js-update-post', function () {
            var id = $(this).data('id')
            $.confirm({
                id: 'postUpdateModal',
                title: 'Update post',
                content: 'url:<?= Url::to(['/university/university/update-post']) ?>/' + id,
                columnClass: 'modal-block col-sm-12'
            });
        })
        
         $('#js-post-block').on('click', '.js-show-post-info', function () {
            $.dialog({
                id: 'showPostModal',
                content: 'url:<?= Url::to(['/university/university/show-post']) ?>/' + $(this).data('id'),
                columnClass: 'comments-popap-maine-container col-sm-12',
                 onContentReady: function () {
                    if($('#create-form-item')){
                        $('.comment-form-created').width($('#pjax-form-comment').width()- 37) ;
                        
                            var cometForm = ($('#pjax-form-comment').width()) - 37;
                            $('.comment-form-created').width(cometForm);
                      
                    }
                }
                
            });
        })
        
        $('#js-post-block').on('click', '.js-delete-post', function () {
            var id = $(this).data('id')
            swal({
                title: "Are you sure?",
                text: "delete post",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        method: 'post',
                        url: '<?= Url::to(['/university/university/delete-post']); ?>/' + id,
                        dataType: 'json'
                    }).done(function (response) {
                        if (response.status) {
                            $('#post-block-list-' + id).remove();
                            swal("Deleted!", {
                                icon: "success",
                            });
                        }
                    })
                }
            });
        })
        
        $('#js-post-block').on('click', ".js-send-comment", function(e) {
            e.preventDefault();
            var form = $(this).closest('form');
            var text = form.find('#postcomment-content').val();
            var id = form.find('#postcomment-post_id').val();
            $.ajax({
                method: 'post',
                url: '<?= Url::to(['/university/university/save-comment-post']); ?>/' + id,
                data: {text: text},
                dataType: 'json',
                beforeSend: function( xhr ) {
                    form.find('.update-data').show();
                }
            }).done(function (response) {
                if (response.status && response.data) {
                    form.find('#postcomment-content').val('')
                    form.find('.update-data').hide();
                    var comment = response.data;
                    form.closest('#post-block-list-'+id).find(' .comment-content').prepend(
                            '<div class="row no-gutters align-items-center">' +
                                '<div class="col-auto comment-user-avatar">' +
                                    '<div class="comment-avatar" style="background: url("' + comment.image + '") center/cover;"></div>' +
                                '</div>' +
                                '<div class="col-auto post-data">' +
                                    '<div class="post-user-name">' +
                                        comment.link + 
                                    '</div>' +
                                '</div>' +
                                '<div class="col-auto post-data ml-auto">' +
                                    '<div class="post-datetime">' + 
                                        comment.date + 
                                    '</div>' +
                                '</div>' +
                                '<div class="col-12 post-content">' +
                                        comment.content + 
                                '</div>' +
                            '</div>'
                    );
                }
            })
        });
        
        $('#js-post-block').on('click', '.js-post-like-action', function () {
            var thisElement = $(this)
            $.ajax({
                method: 'post',
                url: '<?= Url::to(['/university/university/like-post']); ?>/' + thisElement.data('post_id'),
                dataType: 'json'
            }).done(function (response) {
                if (response.status) {
                    thisElement.text(response.data)
                }
            })
        });
    })
</script>