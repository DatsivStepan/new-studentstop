<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\jobs\models\UserJobsResumeSearch;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\rating\StarRating;
use common\models\Favorites;

/* @var $this yii\web\View */
/* @var $model common\models\jobs\CompaniesJobs */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies Jobs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->identity->hasJobResume()) {
    echo $this->render('/student/_search');
} else {
    echo $this->render('/companies/_search');
}
?>
<div class="companies-jobs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="company-container">
            <img style="width:150px" src="<?= $model->getImages(); ?>">
            <?= Html::a($model->getName(), $model->getLink()); ?>
            Vacancies:<?= $model->getJobsCount(); ?>

            <?php if (Yii::$app->user->identity->hasJobResume()) { ?>
                <?php $fStatus = Favorites::getFavoriteStatus(Favorites::TYPE_JOBS_COMPANY, $model->id); ?>
                <?= Html::tag('i', '', [
                    'class' => 'glyphicon glyphicon-star cm-favorite-star js-favorite-star-button ' . ($fStatus ? 'active' : ''),
                    'data-type' => Favorites::TYPE_JOBS_COMPANY,
                    'data-id' => $model->id
                ]); ?>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs js-tabs-filter-list" role="tablist">
                <li class="nav-item">
                    <?= Html::a('Vacansies ', null, [
                        'class' => 'nav-link active',
                        'data-toggle' => "tab",
                        'href' => "#tabV",
                        'role' => "tab",
                        'aria-selected' => "false"
                    ])?>
                </li>
                <li class="nav-item">
                    <?= Html::a('News', null, [
                        'class' => 'nav-link',
                        'data-toggle' => "tab",
                        'href' => "#tabN",
                        'role' => "tab",
                        'aria-selected' => "false"
                    ])?>
                </li>
                <?php /*?>
                <li class="nav-item">
                    <?= Html::a('Friends', null, [
                        'class' => 'nav-link js-filter-tabs',
                        'data-toggle' => "tab",
                        'href' => "#tabF",
                        'role' => "tab",
                        'aria-selected' => "false",
                        'data-filter_type' => UserJobsResumeSearch::FILTER_TYPE_FRIENDS,
                    ])?>
                </li>
                <?php */ ?>
                <li class="nav-item">
                    <?= Html::a('Favorite', null, [
                        'class' => 'nav-link js-filter-tabs',
                        'data-toggle' => "tab",
                        'href' => "#tabF",
                        'role' => "tab",
                        'aria-selected' => "false",
                        'data-filter_type' => UserJobsResumeSearch::FILTER_TYPE_FAVORITE
                    ])?>
                </li>
                <li class="nav-item">
                    <?= Html::a('Offers', null, [
                        'class' => 'nav-link js-filter-tabs',
                        'data-toggle' => "tab",
                        'href' => "#tabF",
                        'role' => "tab",
                        'aria-selected' => "false",
                        'data-filter_type' => UserJobsResumeSearch::FILTER_TYPE_OFFERS
                    ])?>
                </li>
            </ul>
            <div class="tab-content my-scroll clearfix">
                <div class="tab-pane my-scroll active" id="tabF" role="tabpanel" style="position:relative;">
                    <div class="loader-style js-loader-block">
                        <img src="/images/loader.gif" style="position: absolute;width:150px;margin: auto;left: 0;top: 0;bottom: 0;right: 0;">
                    </div>
                    <?php Pjax::begin(['id' => 'pjax-form-filter-tab', 'enablePushState' => false]); ?>

                        <?php $form = ActiveForm::begin(['id' => 'form-filter-tab', 'options' => ['data-pjax' => 'pjax-form-filter-tab']]); ?>
                            <?= $form->field($searchRModel, 'filterTabType')->hiddenInput()->label(false) ?>
                        <?php ActiveForm::end(); ?>

                        <?php if ($resumes = $dataRProvider->getModels()) { ?>
                            <?php foreach ($resumes as $resume) { ?>
                                <?= $this->render('/companies/_resume-item-templ', [
                                    'model' => $resume,
                                    'showButton' => 'not_show'
                                ]); ?>
                            <?php } ?>
                        <?php } else { ?>
                            <p>Not found</p>
                        <?php } ?>
                            <script>
                                $('.js-loader-block').css('display', 'none');
                            </script>
                    <?php Pjax::end(); ?>
                </div>
                <div class="tab-pane my-scroll" id="tabN" role="tabpanel">
                    <?= $this->render('/companies/_news', [
                        'accessCreate' => true,
                        'modelNews' => null,
                        'modelNewsComment' => $modelNewsComment,
                        'searchNModel' => $searchNModel,
                        'dataNProvider' => $dataNProvider,
                    ]); ?>
                </div>
                <div class="tab-pane my-scroll" id="tabV" role="tabpanel">
                    Vacansies 
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    $('.js-filter-tabs').on('shown.bs.tab', function (e) {
        var filterType = $(this).data('filter_type');
        $('.js-loader-block').css('display', 'block');
        $('#userjobsresumesearch-filtertabtype').val(filterType);
        $('#form-filter-tab').submit();
    });
</script>