<?php

use yii\helpers\Html,
    yii\helpers\Url,
    kartik\rating\StarRating,
    common\models\jobs\Companies,
    yii\widgets\Pjax,
    yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(Url::to(['/js/plugins/moment.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepicker.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepaginator.js']));

$this->registerCssFile(Url::to(['/css/plugins/bootstrap-datepaginator.css']));
?>
<div class="row no-gutters calendar-widget show-btn-event" style="margin-bottom: 11px;" id="js-events-list-block">
    <div class="col-12 col-md-12">
        <div id="paginator" class="calendar-view"></div>
        <div class="calendar-custom js-event-create">
            <button class="custom-today">Today</button>
            <button class="custom-add-event js-add-event"></button>
        </div>
    </div>
    <div class="col-12 calendar-widget-content">
        <?php Pjax::begin(['id' => 'pjax-form-events-show', 'enablePushState' => false]); ?>
        
            <?php $form = ActiveForm::begin(['id' => 'events-search-form', 'options' => ['data-pjax' => 'pjax-form-events-show']]); ?>
                <?= $form->field($searchEModel, 'filterDate')->hiddenInput()->label(false) ?>
            <?php ActiveForm::end(); ?>

            <?php if ($events = $dataEProvider->getModels()) { ?>
                <?php foreach ($events as $key => $event) { ?>
                    <div class="row no-gutters event-block-under-calendar align-items-center">
                        <div class="col-12 col-md-5 date-create"><?= $event->getDateStart('Y M d H:i'); ?> - <?= $event->getDateEnd('Y M d H:i'); ?></div>
                        <div class="col-12 col-md-4 event-title"><a href="/"><?= $event->title; ?></a></div>
                        <div class="col-12 col-md-3 event-join-btn">
                            <a class="action-btn join-btn js-show-event" data-id="<?= $event->id; ?>">View</a>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="col-12 col-md-12 calendar-widget-content-courses">
                    Sorry But events not found
                </div>
            <?php } ?>
        <?php Pjax::end(); ?>
    </div>
</div>
<script>
    $(function() {

        $('#js-events-list-block').on('click', '.js-add-event', function () {
            $.confirm({
                id: 'eventCreateModal',
                title: 'Create event',
                content: 'url:<?= Url::to(['/jobs/default/event-create']) ?>',
            });
        });

        $('#js-events-list-block').on('click', '.js-show-event', function () {
            var id = $(this).data('id')
            $.dialog({
                id: 'eventShowModal',
                title: 'Show event',
                content: 'url:<?= Url::to(['/jobs/default/event-show']) ?>/'+id,
            });
        });
        
        function dateChange(date) {
            date = new Date(date);
            var year = date.getFullYear()
            var month = date.getMonth()+1;
            if(month < 10){
                month = '0'+month;
            }
            var day = date.getDate()
            var dayClick = year+'-'+month+'-'+day;

            $('#events-search-form').find('#companieseventssearch-filterdate').val(dayClick)
            $('#events-search-form').submit()
        }

        $('#paginator').datepaginator({
            showCalendar: false,
            textSelected: '<b>dddd</b><br/>Do, MMMM YYYY',
            text: '<b>ddd</b><br/>Do',
            itemWidth: 50,
            onSelectedDateChanged: function(event, date){
                dateChange(date);
            },
        });

        $('.calendar-custom').on('click', '.custom-today', function(e){
            e.preventDefault();
            var date = new Date();
            $('#paginator').datepaginator('setSelectedDate', `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`)
        });
    })
</script>
