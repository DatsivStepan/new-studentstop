<?php
    use yii\helpers\Html,
        yii\helpers\Url;

$this->registerJsFile(Url::to(['/js/jobs/jobs.js']));
?>
<style>
    .back_color_create_resume {
        text-align: center;
        float: left;
        background-image: url(/images/create_resume.jpg);
        height: 100%;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
        padding: 0px;
    }
    .back_color_create_bisiness {
        text-align: center;
        float: right;
        background-image: url(/images/create_bsiness.jpg);
        height: 100%;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
        padding: 0px;
    }
    .background-block{
        padding-left: 0px;
    }
    .back_color_create_resume a {
        top: 50%;
        position:absolute;
    }
    .back_color_create_bisiness a {
        top: 50%;
        position:absolute;
    }
</style>
<div>
    <div class="row" style="height:700px;  padding: 0px">
        <div class="back_color_create_resume col-sm-6">
            <?= Html::a('Create Resume', null, [
                'class' => 'btn btn-info js-student-resume-create'
            ]); ?>
        </div>
        <div class="back_color_create_bisiness col-sm-6">
            <?= Html::a('Create Business', null, [
                'class' => 'btn btn-info js-companies-create'
            ]); ?>
        </div>
    </div>
</div>