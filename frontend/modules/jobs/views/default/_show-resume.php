<?php
    use yii\helpers\Html,
        common\models\Favorites,
        kartik\rating\StarRating;
    
2?>
<div class="row item-jobs-block js-item-content-block" data-id="<?= $model->id; ?>">
    <div class="col-sm-6">
        <img style="width:100%" src="<?= $model->getAvatar(); ?>">
    </div>
    <div class="col-sm-6">
        <h4>
            <?= $model->userModel ? $model->userModel->getNameAndSurname() : ''; ?>
       </h4>
        <div>
            <?= StarRating::widget([
                'id' => 'show_resum_r_' . $model->id,
                'name' => 'show_resum_r_' . $model->id,
                'value' => $model->userModel ? $model->userModel->getRating() : '',
                'disabled' => true,
                'pluginOptions' => [
                    'size' => 'xs',
                    'min' => 0,
                    'max' => 5
                ],
            ]); ?>
        </div>
        <div class="row">
            <table style="width:100%;">
                <tr>
                    <td>Address</td>
                    <td><?= $model->userInfoModel ? $model->userInfoModel->address : ''; ?></td>
                </tr>
                <tr>
                    <td>University</td>
                    <td><?= $model->userInfoModel ? $model->userInfoModel->getUniversityName() : ''; ?></td>
                </tr>
                <tr>
                    <td><?= $model->getAttributeLabel('current_gpa'); ?></td>
                    <td><?= $model->current_gpa; ?></td>
                </tr>
                <tr>
                    <td><?= $model->getAttributeLabel('overall_gpa'); ?></td>
                    <td><?= $model->overall_gpa; ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>