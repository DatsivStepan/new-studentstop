<?php

use yii\helpers\Html,
    yii\helpers\Url,
    kartik\rating\StarRating,
    common\models\jobs\Companies;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */
/* @var $form yii\widgets\ActiveForm */

$filterArray = Companies::$filterTop;
$topList = Companies::getTopList(1);
?>

<div class="row no-gutters calendar-widget show-btn-event top-university-containter">
    <div class="col-sm-12">
        <ul class="nav nav-tabs js-tabs-top-list" role="tablist">
            <li class="nav-item"><a class="nav-link"><b>TOP 5:</b></a></li>
            <?php $k = 0; ?>
            <?php foreach ($filterArray as $filterId => $filterName) { ?>
                <?php $class = ''; ?>
                <li class="nav-item" data-id="<?= $filterId; ?>">
                    <?= Html::a($filterName, null, [
                        'class' => 'nav-link ' . ($filterId == 1 ? 'active' : ''),
                        'data-toggle' => "tab",
                        'href' => "#tabTop" . $filterId,
                        'role' => "tab",
                        'aria-selected' => "false"
                    ])?>
                </li>
                <?php $k++;?>
            <?php }?>
        </ul>
        <div class="tab-content my-scroll clearfix">
            <?php if ($topList) { ?>
                <?php foreach ($topList as $listId => $list) { ?>
                    <div class="tab-pane my-scroll <?= $listId == 1 ? 'active' : ''; ?>" id="tabTop<?= $listId; ?>" role="tabpanel">
                        <?php 
                            switch ($listId) {
                                case 1: ?>
                                    <?php if ($list) { ?>
                                        <table class="table">
                                            <?php foreach ($list as $user) { ?>
                                                <tr>
                                                    <td><img style="width:50px;" src="<?= $user->getAvatar(); ?>"></td>
                                                    <td><?= Html::a($user->getNameAndSurname(), $user->getProfileLink()); ?></td>
                                                    <td>
                                                        Rank:<?= $user->getRankCount(); ?> 
                                                        <?= StarRating::widget([
                                                            'name' => 'rating_u_'. $user->id,
                                                            'value' => ($listId == 1) ? $user->getRating() : $user->getProfessorRating(),
                                                            'disabled' => true,
                                                            'pluginOptions' => [
                                                                'size' => 'xs',
                                                                'min' => 0,
                                                                'max' => 5
                                                            ],
                                                        ]); ?>
                                                    </td>
                                                    <td>
                                                        <?= Html::button('Mail to user', ['class' => 'js-mail-to-user', 'data-myId' => Yii::$app->user->id, 'data-user_id' => $user->id])?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    <?php } else { ?>   
                                        <?php if ($listId == 1) { ?>
                                            <p class="text-center">NO students</p>
                                        <?php } else { ?>
                                            <p class="text-center">NO Professors</p>
                                        <?php } ?>
                                    <?php } ?>
                                <?php break;
                                case 2: 
                                case 3: ?>
                                    <?php if ($list) { ?>
                                        <table class="table">
                                            <?php foreach ($list as $l) { ?>
                                                <tr>
                                                    <td><img style="width:50px;" src="<?= $l->id; ?>"></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    <?php } else { ?>
                                        <p class="text-center">NO Class</p>
                                    <?php } ?>
                                <?php break;
                            }
                        ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
