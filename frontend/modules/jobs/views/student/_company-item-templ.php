<?php
    use yii\helpers\Html,
        yii\helpers\Url,
        common\models\Favorites,
        kartik\rating\StarRating;
?>
<div class="row item-jobs-company-block js-item-content-block" data-id="<?= $model->id; ?>">
    <div class="col-sm-6">
        <img style="width:100%" src="<?= $model->getImages(); ?>">
        <div class="row">
            <div class="col-sm-6 text-center">
                <?= Html::a('View', $model->getLink(), ['class' => 'btn btn-primary', 'data-pjax' => 0]); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <h4>
            <?= $model->name; ?>
            <?php $fStatus = Favorites::getFavoriteStatus(Favorites::TYPE_JOBS_COMPANY, $model->id); ?>
            <?= Html::tag('i', '', [
                'class' => 'glyphicon glyphicon-star cm-favorite-star js-favorite-star-button ' . ($fStatus ? 'active' : ''),
                'data-type' => Favorites::TYPE_JOBS_COMPANY
            ]); ?>
        </h4>
        <div class="row">
            <table style="width:100%;">
                <tr>
                    <td>Vacancies</td>
                    <td><?= $model->getJobsCount(); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>