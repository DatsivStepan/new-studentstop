<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    yii\widgets\Pjax,
    common\models\user\UserInfo,
    kartik\date\DatePicker,
    kartik\select2\Select2,
    common\models\jobs\JobsIndustries,
    common\models\UserRole;

/* @var $this yii\web\View */
/* @var $model common\models\user\UserJobsResume */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(['id' => 'pjax-form-resume-create', 'enablePushState' => false]); ?>
    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            if ($modal.studentResumeCreateModal) {
                $modal.studentResumeCreateModal.close();
            }
            if ($modal.studentResumeUpdateModal) {
                $modal.studentResumeUpdateModal.close();
            }
            
            location.reload()
        </script>
    <?php } ?>
    <div class="user-jobs-resume-form">

        <?php $form = ActiveForm::begin(['id' => 'form-resume', 'options' => ['data-pjax' => 'pjax-form-resume-create']]); ?>

            <div class="row">
                <div class="col-sm-3 col-xl-2 text-modal">
                    <text>Avatar</text>
                </div>
                <div class="col-sm-6 col-xl-7">
                    <div>
                        <?= $form->field($model, 'avatar')->hiddeninput()->label(false); ?> 
                        <img src="<?= $model->getAvatar(); ?>" class="js-preview-resume-image img-thumbnail"><br>
                        <a class="btn btn-sm btn-success js-add-resume-image" style="width:100%">Change image</a><br><br>
                    </div>
                </div>
            </div>

            <?= $form->field($userInfo, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($userInfo, 'surname')->textInput(['maxlength' => true]) ?>

            <?= $form->field($userInfo, 'birthday')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true
                ]
            ]); ?>

            <?= $form->field($userInfo, 'gender')->dropDownList(UserInfo::$arrayGender) ?>

            <?= $form->field($userInfo, 'date_begin_education')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true
                ]
            ]); ?>
        
            <?= $form->field($userInfo, 'date_end_education')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true
                ]
            ]); ?>
            <?= $form->field($userInfo, 'mobile')->textInput(['maxlength' => true]) ?>

            <?= $form->field($userInfo, 'address')->textInput(['maxlength' => true]) ?>

            <?= $form->field($userInfo, 'website')->textInput(['maxlength' => true]) ?>

            <?= $form->field($userInfo, 'facebook')->textInput(['maxlength' => true]) ?>

            <?= $form->field($userInfo, 'twitter')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'about')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'skills')->widget(Select2::classname(), [
                    'data' => [],
                    'options' => ['placeholder' => 'Select a color ...', 'multiple' => true],
                    'pluginOptions' => [
                        'tags' => true,
                        'tokenSeparators' => [',', ' '],
                        'maximumInputLength' => 10
                    ],
                ]); ?>

            <?= $form->field($userInfo, 'industry_id')
                ->widget(Select2::classname(), [
                    'data' => JobsIndustries::getAllInArrayMap(),
                    'options' => [
                        'placeholder' => Yii::t('app', 'Select...')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]); ?>

            <?= $form->field($model, 'current_gpa')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'overall_gpa')->textInput(['maxlength' => true]) ?>

        <?php ActiveForm::end(); ?>

    </div>
    <script>
        $(function(){
            $dropzone.initializationDropzoneEleemnt('resume', '#userjobsresume-avatar', 'image/*');
        })
    </script>
<?php Pjax::end(); ?>
