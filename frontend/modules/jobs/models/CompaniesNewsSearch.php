<?php

namespace frontend\modules\jobs\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\jobs\CompaniesNews;

/**
 * CompaniesJobsSearch represents the model behind the search form about `common\models\jobs\CompaniesJobs`.
 */
class CompaniesNewsSearch extends CompaniesNews
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'type', 'status'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'company_id' => $this->company_id,
        ]);

//        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
