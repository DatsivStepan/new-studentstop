<?php

namespace frontend\modules\jobs\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\jobs\CompaniesJobs;

/**
 * CompaniesJobsSearch represents the model behind the search form about `common\models\jobs\CompaniesJobs`.
 */
class CompaniesJobsSearch extends CompaniesJobs
{
    const FILTER_TYPE_MATCHES = 'matches';
    const FILTER_TYPE_FAVORITE = 'favorite';
//    const FILTER_TYPE_COMPANIES = 'companies';
    const FILTER_TYPE_OFFERS = 'offers';
    const FILTER_TYPE_APPLY = 'apply';
    
    public $search_value;
    public $search_type;
    public $filterTabType;
    public $favoriteUser;
    
    public static $searchTypes = [
        1 => 'Name',
        2 => 'Other'
    ];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id', 'company_id', 'gender', 'user_role_need', 'show_in_university', 'major_id', 'jobs_function_id',
                    'salary_type', 'industry_level', 'type', 'status', 'favoriteUser'
                ],
                'integer'
            ],
            [
                [
                    'name', 'description', 'skills', 'img_src', 'created_at', 'updated_at',
                    'search_type', 'search_value', 'filterTabType'
                ],
                'safe'
            ],
            [['salary'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()
                ->alias('cj')
                ->joinWith([
                    'favoritesModel favorites',
                    'companiesJobsInviteModel cji',
                    'companiesJobsApplyModel cja',
                ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

//        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
//            return $dataProvider;
//        }
        if ($this->filterTabType) {
            switch ($this->filterTabType) {
                case self::FILTER_TYPE_MATCHES:
                    break;
                case self::FILTER_TYPE_FAVORITE:
                    $query->andFilterWhere(['favorites.user_id' => $this->favoriteUser]);
                    break;
                case self::FILTER_TYPE_OFFERS:
                    $query->andFilterWhere(['cji.to_user_id' => $this->favoriteUser]);
                    break;
                case self::FILTER_TYPE_APPLY:
                    $query->andFilterWhere(['cja.user_id' => $this->favoriteUser]);
                    break;
            }
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cj.id' => $this->id,
            'cj.company_id' => $this->company_id,
            'cj.gender' => $this->gender,
            'cj.user_role_need' => $this->user_role_need,
            'cj.show_in_university' => $this->show_in_university,
            'cj.major_id' => $this->major_id,
            'cj.jobs_function_id' => $this->jobs_function_id,
            'cj.salary' => $this->salary,
            'cj.salary_type' => $this->salary_type,
            'cj.industry_level' => $this->industry_level,
            'cj.type' => $this->type,
            'cj.status' => $this->status,
            'cj.created_at' => $this->created_at,
            'cj.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'cj.name', $this->name])
            ->andFilterWhere(['like', 'cj.description', $this->description])
            ->andFilterWhere(['like', 'cj.skills', $this->skills])
            ->andFilterWhere(['like', 'cj.img_src', $this->img_src]);

        return $dataProvider;
    }
}
