<?php

namespace frontend\modules\jobs\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\jobs\Companies;

/**
 * CompaniesSearch represents the model behind the search form about `common\models\jobs\Companies`.
 */
class CompaniesSearch extends Companies
{
    public $favoriteUser;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'industry_id', 'type', 'status', 'favoriteUser'], 'integer'],
            [['name', 'address', 'founded', 'description', 'img_src', 'phone', 'email', 'fax', 'website', 'social_link_facebook', 'social_link_twitter', 'social_link_linkedin', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Companies::find()
                ->alias('c')
                ->joinWith(['favoritesJobModel favorites']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->favoriteUser) {
            $query->andWhere(['favorites.user_id' => $this->favoriteUser]);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'c.id' => $this->id,
            'c.user_id' => $this->user_id,
            'c.founded' => $this->founded,
            'c.industry_id' => $this->industry_id,
            'c.type' => $this->type,
            'c.status' => $this->status,
            'c.created_at' => $this->created_at,
            'c.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'c.name', $this->name])
            ->andFilterWhere(['like', 'c.address', $this->address])
            ->andFilterWhere(['like', 'c.description', $this->description])
            ->andFilterWhere(['like', 'c.img_src', $this->img_src])
            ->andFilterWhere(['like', 'c.phone', $this->phone])
            ->andFilterWhere(['like', 'c.email', $this->email])
            ->andFilterWhere(['like', 'c.fax', $this->fax])
            ->andFilterWhere(['like', 'c.website', $this->website])
            ->andFilterWhere(['like', 'c.social_link_facebook', $this->social_link_facebook])
            ->andFilterWhere(['like', 'c.social_link_twitter', $this->social_link_twitter])
            ->andFilterWhere(['like', 'c.social_link_linkedin', $this->social_link_linkedin]);

        return $dataProvider;
    }
}
