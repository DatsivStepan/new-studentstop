<?php

namespace frontend\modules\jobs\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\user\UserJobsResume;

/**
 * UserJobsResumeSearch represents the model behind the search form about `common\models\user\UserJobsResume`.
 */
class UserJobsResumeSearch extends UserJobsResume
{
    const FILTER_TYPE_MATCHES = 'matches';
    const FILTER_TYPE_FAVORITE = 'favorite';
    const FILTER_TYPE_OFFERS = 'offers';
    const FILTER_TYPE_APPLY = 'apply';
    const FILTER_TYPE_FRIENDS = 'friends';

    public $search_value;
    public $search_type;
    public $filterTabType;
    public $company_owner_id;
    public $company_id;
    public $company_job_id;

    public static $searchTypes = [
        1 => 'Name',
        2 => 'Other'
    ];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'search_type', 'company_owner_id', 'company_job_id', 'company_id'], 'integer'],
            [
                [
                    'avatar', 'about', 'skills', 'created_at', 'updated_at', 'search_value', 'filterTabType'
                ],
                'safe'
            ],
            [['current_gpa', 'overall_gpa'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()
                ->alias('ujr')
                ->joinWith([
                    'favoritesModel favorites',
                    'companiesJobsApplyModel.companiesJobsModel cja',
                    'companiesJobsInviteModel.companiesJobsModel cj',
                ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->filterTabType) {
            switch ($this->filterTabType) {
                case self::FILTER_TYPE_MATCHES:
                    break;
                case self::FILTER_TYPE_FAVORITE:
                    $query->andFilterWhere(['favorites.user_id' => $this->company_owner_id]);
                    break;
                case self::FILTER_TYPE_OFFERS:
                    $query->andFilterWhere(['cj.company_id' => $this->company_id]);
                    break;
                case self::FILTER_TYPE_APPLY:
                    $query->andFilterWhere(['cja.company_id' => $this->company_id]);
                    break;
            }
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'ujr.id' => $this->id,
            'ujr.user_id' => $this->user_id,
            'ujr.current_gpa' => $this->current_gpa,
            'ujr.overall_gpa' => $this->overall_gpa,
            'ujr.created_at' => $this->created_at,
            'ujr.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'ujr.avatar', $this->avatar])
            ->andFilterWhere(['like', 'ujr.about', $this->about])
            ->andFilterWhere(['like', 'ujr.skills', $this->skills]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchForJob()
    {
        $query = self::find()
                ->alias('ujr')
                ->joinWith([
                    'favoritesJobModel favorites',
                    'companiesJobsInviteModel.companiesJobsModel cj',
                    'companiesJobsApplyModel cja',
                ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->filterTabType) {
            switch ($this->filterTabType) {
                case self::FILTER_TYPE_FAVORITE:
                    $query->andFilterWhere(['favorites.object_id' => $this->company_job_id]);
                    break;
                case self::FILTER_TYPE_APPLY:
                    $query->andFilterWhere(['cja.company_job_id' => $this->company_job_id]);
                    break;
            }
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'ujr.id' => $this->id,
            'ujr.user_id' => $this->user_id,
            'ujr.current_gpa' => $this->current_gpa,
            'ujr.overall_gpa' => $this->overall_gpa,
            'ujr.created_at' => $this->created_at,
            'ujr.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'ujr.avatar', $this->avatar])
            ->andFilterWhere(['like', 'ujr.about', $this->about])
            ->andFilterWhere(['like', 'ujr.skills', $this->skills]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchFollowers()
    {
        $query = self::find()
                ->alias('ujr')
                ->joinWith([
                    'favoritesCompanyModel favorites',
                ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        
        $query->andWhere(['favorites.object_id' => $this->company_id]);
        
        // grid filtering conditions
        $query->andFilterWhere([
            'ujr.id' => $this->id,
            'ujr.user_id' => $this->user_id,
            'ujr.current_gpa' => $this->current_gpa,
            'ujr.overall_gpa' => $this->overall_gpa,
            'ujr.created_at' => $this->created_at,
            'ujr.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'ujr.avatar', $this->avatar])
            ->andFilterWhere(['like', 'ujr.about', $this->about])
            ->andFilterWhere(['like', 'ujr.skills', $this->skills]);

        return $dataProvider;
    }
}
