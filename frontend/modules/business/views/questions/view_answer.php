<?php

use yii\helpers\Html,
    yii\helpers\Url,
    frontend\components\helpers\FunctionHelper,
    common\models\business\QuestionsAnswers,
    kartik\rating\StarRating,
    yii\widgets\ActiveForm,
    yii\widgets\Pjax,
    common\models\pay\PayRequests;

/* @var $this yii\web\View */
?>
<div id="answer-payed-block" data-answer_id="<?= $model->id; ?>" data-credit="<?= (float)$model->credit; ?>" data-money="<?= (float)$model->money; ?>">
    <div class="text-center show-answer-modal-title"><?= $model->questionModel ? $model->questionModel->title : ''; ?></div>
    <div class="show-answer-text"><b>Short answer: </b><?= $model->show_answer; ?></div>
    <div class="show-answer-text">
        <?php if ($model->isPayed() && ($model->questionModel && $model->questionModel->isOwner())) { ?>
            <p><b>Full answer: </b><?= $model->answer; ?></p>
            <?php if ($model->status == QuestionsAnswers::STATUS_PAYED) { ?>
                <h5 class="text-center">You are given the opportunity to refuse an answer if for some reason it did not suit you</h5>
                <div class="col-sm-12">
                    <?= Html::textarea('reasonRegect', '', ['class' => 'form-control'])?>
                    <?= Html::button('Goot!', ['class' => 'btn btn-info js-confirm complete-pay-btn-got', 'data-status' => QuestionsAnswers::STATUS_CONFIRM]); ?>
                    <?= Html::button('Bat!', ['class' => 'btn btn-danger js-confirm complete-pay-btn-bat', 'data-status' => QuestionsAnswers::STATUS_ON_CONFIRM]); ?>
                </div>

            <?php } else if ($model->status == QuestionsAnswers::STATUS_ON_CONFIRM) { ?>
                <p>Your application for review by the admin</p>
            <?php } else if ($model->status == QuestionsAnswers::STATUS_REGECT) { ?>
                <p>The admin confirmed, and returned you the credits</p>
            <?php } else if ($model->status == QuestionsAnswers::STATUS_CONFIRM) { ?>
                <hr style="transform: translateX(-10px);width: calc(100% + 10px);">
                <div class="show-answer-rating">
                    <?php if ($modelRating = $model->answerRatingModel) { ?>
                            <?= StarRating::widget([
                                'options' => [
                                    'id' => 'rating' . uniqid()
                                ],
                                'name' => 'rating_' . uniqid(),
                                'value' => $modelRating->rating,
                                'disabled' => true,
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'min' => 0,
                                    'max' => 5
                                ],
                            ]); ?>
                    <?php } else { ?>
                            <?php $form = ActiveForm::begin(['id' => 'form-rating']); ?>
                                <div class="col-sm-12 col-xl-12">
                                    <?= $form->field($newRating, 'rating')->widget(StarRating::classname(), [
                                        'pluginOptions' => [
                                            'step' => '1',
                                            'size'=>'xs',
                                            'showCaption' => false,
                                        ],
                                    ]); ?>
                                </div>
                                <?= Html::button('Add rating', ['class' => 'btn btn-info js-add-rating add-rating-btn']); ?>

                            <?php ActiveForm::end(); ?>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } else { ?>
            <div class="complete-pay-modal">For a complete answer to pay</div>
            <div class="row" style="margin:0px;">
                <div class="col-sm-6 text-center">
                    <label> Pay via paypal:</label>
                    <?= Html::button(FunctionHelper::numberFormat($model->money) . '(P)', ['class' => 'btn btn-info  js-pay-for-paypal complete-pay-btn']); ?>
                </div>

<!--                <div class="col-sm-4 text-center">-->
<!--                    <label> Pay money:</label>-->
<!--                    --><?php // Html::button(FunctionHelper::numberFormat($model->money) . '(M)', ['class' => 'btn btn-info js-pay-for-money']); ?>
<!--                </div>-->

                <div class="col-sm-6 text-center">
                    <label> Pay via credites:</label>
                    <?= Html::button(FunctionHelper::numberFormat($model->credit) . '(C)', ['class' => 'btn btn-info js-pay-for-credits complete-pay-btn']); ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<script>
    $(function() {
        
        $('#answer-payed-block').on('click', '.js-confirm', function(){
            var thisE = $(this);
            var id = thisE.closest('#answer-payed-block').data('answer_id');
            thisE.attr('disabled', true);
            var status = thisE.data('status');
            var note = $('#answer-payed-block [name=reasonRegect]').val()
            
            $.ajax({
                method: 'get',
                url: '/business/questions/change-answer-status/' + id,
                dataType: 'json',
                data:{note:note, status:status},
            }).done(function (response) {
                if (response.status) {
                    modalReload($modal.showAnswerModal);
                }
            })
        });

        $('#answer-payed-block').on('click', '.js-pay-for-paypal', function(){
            var thisE = $(this);
            var id = thisE.closest('#answer-payed-block').data('answer_id');
            thisE.attr('disabled', true);
            var moneyAnswer = $(this).closest('#answer-payed-block').data('money');
            swal({
                title: "Are you sure?",
                text: "you want payed  " + parseFloat(moneyAnswer) + '$',
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    location.href = '/payment-paypal/pay-for-answer/?answerId=' + id
                }
                thisE.attr('disabled', false);
            })
        });

        $('#answer-payed-block').on('click', '.js-pay-for-money', function(){
            var thisE = $(this);
            var id = thisE.closest('#answer-payed-block').data('answer_id');
            thisE.attr('disabled', true);
            var creditsAnswer = $(this).closest('#answer-payed-block').data('money');
            $.ajax({
                method: 'post',
                url: '/site/check-money-count',
                dataType: 'json'
            }).done(function (response) {
                thisE.attr('disabled', false);
                if (parseFloat(creditsAnswer) > parseFloat(response.amount)) {
                    swal({
                        title: "Not enough credits",
                        text: 'You have ' + parseFloat(response.amount) + '$, but you need to ' + parseFloat(creditsAnswer),
                        html: true,
                        icon: "error",
                        confirmButtonText: "Ok",
                    });
                } else {
                    swal({
                        title: "Are you sure?",
                        text: "you want payed  " + parseFloat(creditsAnswer) + '$ ',
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                        buttons: ["Cancel", "Yes, Payed"],
                    }).then((willPayed) => {
                        if (willPayed) {
                            $.ajax({
                                method: 'post',
                                url: '/business/questions/pay-for-answer/' + id + '?type=<?= PayRequests::TYPE_PAY_MONEY; ?>',
                                dataType: 'json'
                            }).done(function (response) {
                                if (response.status) {
                                    swal("Answer payed")
                                        .then((value) => {
                                            modalReload($modal.showAnswerModal);
                                        });
                                }
                            });
                        }
                    })
                }
            });
        })

        $('#answer-payed-block').on('click', '.js-pay-for-credits', function(){
            var thisE = $(this);
            var id = thisE.closest('#answer-payed-block').data('answer_id');
            thisE.attr('disabled', true);
            var creditsAnswer = $(this).closest('#answer-payed-block').data('credit');
            $.ajax({
                method: 'post',
                url: '/site/check-credits-count',
                dataType: 'json'
            }).done(function (response) {
                thisE.attr('disabled', false);
                if (parseFloat(creditsAnswer) > parseFloat(response.amount)) {
                    swal({
                        title: "Not enough credits",
                        text: 'You have ' + parseFloat(response.amount) + ' credits, but you need to ' + parseFloat(creditsAnswer),
                        html: true,
                        icon: "error",
                        confirmButtonText: "Ok",
                    });
                } else {
                    swal({
                        title: "Are you sure?",
                        text: "you want payed  " + parseFloat(creditsAnswer) + ' credits',
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                        buttons: ["Cancel", "Yes, Payed"],
                    }).then((willPayed) => {
                        if (willPayed) {
                            $.ajax({
                                method: 'post',
                                url: '/business/questions/pay-for-answer/' + id + '?type=<?= PayRequests::TYPE_PAY_CREDIT; ?>',
                                dataType: 'json'
                            }).done(function (response) {
                                if (response.status) {
                                    swal("Answer payed")
                                        .then((value) => {
                                            modalReload($modal.showAnswerModal);
                                        });
                                }
                            });
                        }
                    })
                }
            });
        })

        $('#answer-payed-block').on('click', '.js-add-rating', function(){
            var thisEl = $(this)
            $.ajax({
                method: 'post',
                url: '<?= Url::to(['/business/questions/show-answer/', 'id' => $model->id]); ?>',
                data: thisEl.closest('form').serialize() + '&save=1',
                dataType: 'json',
                beforeSend: function () {
                    thisEl.prop({disabled: true});
                },
                success: function (responce) {
                    thisEl.prop({disabled:false});
                    if (responce.status) {
                        if ($modal.showAnswerModal) {
                            modalReload($modal.showAnswerModal);
                        }
                    }
                }
            });
        })
    })
</script>