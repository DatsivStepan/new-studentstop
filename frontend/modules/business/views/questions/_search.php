<?php

use yii\helpers\Html,
    yii\helpers\Url,
    yii\widgets\ActiveForm,
    kartik\select2\Select2,
    common\models\University,
    frontend\modules\business\models\QuestionsSearch,
    kartik\depdrop\DepDrop;

$this->registerJsFile(Url::to(['/js/business/questions.js']));

/* @var $this yii\web\View */
/* @var $model frontend\modules\services\models\RentsSearch */
/* @var $form yii\widgets\ActiveForm */
$model = $model ? $model : new QuestionsSearch;
?>
<div class="row business-search">

        <?php $form = ActiveForm::begin([
            'action' => ['search'],
            'method' => 'get',
        ]); ?>
            <div class="search">
                    <?= $form->field($model, 'title', ['options' => ['class' => 'search-input']])->textInput(['placeholder' => 'Question'])->label(false) ?>
            </div>
            <div class="country">
                    <?= $form->field($model, 'country')->widget(Select2::classname(), [
                        'data' => \common\models\location\Countries::getAllInMapArray(),
                        'options' => ['placeholder' => Yii::t('app', 'Select...')],
                        'pluginOptions' => [
                            'allowClear' => true
                            ]
                    ])->label(false); ?>
            </div>
            <div class="state">
                <?= $form->field($model, 'state')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => ['placeholder' => Yii::t('app', 'Select...')],
                    'select2Options' => [
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ],
                    'pluginOptions' => [
                        'initialize' => true,
                        'initDepends' => ['questionssearch-country'],
                        'depends' => ['questionssearch-country'],
                        'url' => Url::to(['/site/get-state'])
                    ],
                ])->label(false); ?>   
            </div>
            <div class="university">
                    <?= $form->field($model, 'university_id')->widget(Select2::classname(), [
                        'data' => University::getAllUniversityInArrayMap(),
                        'options' => ['placeholder' => Yii::t('app', 'Select...')],
                        'pluginOptions' => [
                            'allowClear' => true
                            ]
                    ])->label(false); ?>
            </div>
             <div class="button-search-block">
                 <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-search']) ?>
             </div>
        

        <?php ActiveForm::end(); ?>
</div>