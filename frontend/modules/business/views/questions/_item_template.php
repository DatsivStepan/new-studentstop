<?php

use yii\helpers\Html,
    yii\grid\GridView,
    yii\widgets\Pjax,
    yii\helpers\Url,
    common\models\Favorites;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\business\models\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
    <div class="col-sm-6 col-xl-4 qvection-search-block js-item-content-block" data-id="<?= $model->id; ?>">
        <div class="info-block-all">
            <?php if ($model->isOwner()) { ?>
            <div class="question-action-buttons text-right" style="margin-bottom:5px;">
                    <div class="col-auto js-update-question update" data-id="<?= $model->id; ?>"></div>
                    <div class="col-auto js-delete-question delete" data-id="<?= $model->id; ?>"></div>
                </div>
            <?php } ?>
            <div class="info-block row">
                    <div class="col-2">
                        <img src="<?= $model->getAuthorAvatar(); ?>">
                    </div>

                    <div class=" col-8 col-sm-3" style="overflow: hidden;padding-right: 0px;padding-top: 6px;">
                         <p><?= $model->getAuthorName(); ?></p>
                    </div>

                    <div class="col-6 col-sm-3">
                        <?= Html::button('View', [
                            'class' => 'view-button btn btn-info js-show-question',
                            'data-id' => $model->id
                        ]); ?>
                    </div>
                    <div class="col-6 col-sm-4" style="padding-top: 6px;padding-left: 0px">
                       <p> <?= $model->getPrice(); ?></p>

                    </div>
            </div>
            <div class="row" style="margin: 0;">
                <div class="col-12" style="margin-top: 10px">
                   <div class="question-info-title" style="float: left"> <?= $model->title; ?></div>
                    <div class="favorite-block" style="float: right;">
                        <div class="icon-block icon-star ">
                            <?php $fStatus = Favorites::getFavoriteStatus(Favorites::TYPE_QUESTION, $model->id); ?>
                            <?= Html::tag('i', '', [
                                'class' => 'glyphicon glyphicon-star cm-favorite-star js-favorite-star-button ' . ($fStatus ? 'active' : ''),
                                'data-type' => Favorites::TYPE_QUESTION
                            ]); ?>
                        </div>
                    </div>
                </div>
                
                <div class="col-12 classif-text-block text-bl question-info-description">
                    <text><?= $model->getContent(); ?></text>
                </div>
            </div>
        </div>
    </div>