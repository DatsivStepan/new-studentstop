<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\business\models\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invites');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <?= $this->render('_search', [
        'model' => null,
        'button' => false
    ]); ?>

    <div class="col-sm-12" style="margin-top:10px;">
        <div class=" classes-box">
            <div class="classes-block">
                <?= GridView::widget([
                    'id' => 'answersList',
                    'dataProvider' => $dataProvider,
                    'pjax' => true,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'label' => 'Question',
                            'attribute' => 'question_id',
                            'value' => function ($model) {
                                return $model->questionModel ? 
                                    Html::a($model->questionModel->title, null, ['class' => 'js-show-question questions-invite-title', 'data-id' => $model->question_id]) : '';
                            },
                            'format' => 'raw',
                        ],
                        [
                            'label' => 'Price',
                            'attribute' => 'price',
                            'value' => function ($model) {
                                return $model->questionModel ? $model->questionModel->getPrice() : '';
                            },
                            'format' => 'raw',
                        ],
                        [
                            'label' => 'Created',
                            'attribute' => 'created_at'
                        ],
                        [
                            'label' => 'Question',
                            'attribute' => 'question_id',
                            'value' => function ($model) {
                                return $model->questionModel ? Html::a("View", null, ['class' => 'js-show-question btn btn-primary questions-invite-view-btn', 'data-id' => $model->question_id]) : '';
                            },
                            'format' => 'raw',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>