<?php

use yii\helpers\Html,
    kartik\grid\GridView,
    yii\widgets\Pjax,
    yii\helpers\Url,
    kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\business\models\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invites');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="answer-index">

    <?= $this->render('_search', [
        'model' => null,
        'button' => false
    ]); ?>
    <div class="col-sm-12 my-ansvers-content" style="margin-top:10px;">
        <div class=" classes-box">
            <div class="classes-block">
                <?= GridView::widget([
                    'id' => 'answersList',
                    'dataProvider' => $dataProvider,
                    'pjax' => true,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'label' => 'Question',
                            'attribute' => 'question_id',
                            'value' => function ($model) {
                                return $model->questionModel ? 
                                    Html::a($model->questionModel->title, null, ['class' => 'js-show-question', 'data-id' => $model->question_id]) :
                                    '';
                            },
                            'format' => 'raw',
                        ],
                        'show_answer',
                        [
                            'label' => 'Created',
                            'attribute' => 'created_at'
                        ],
                        [
                            'label' => 'Price',
                            'value' => function ($model) {
                                return $model->getPrice();
                            }
                        ],
                        [
                            'value' => function ($model) {
                                return $model->isPayed() ? 
                                    Html::a('Payed', Url::to(['/profile/credits' . ($model->payRequestsModel ? '?operationId=' . $model->payRequestsModel->id : '?operationType=' . common\models\pay\PayRequests::OPERATION_TYPE_QUESTION_ANSWER)])) : (Html::button('U', ['class' => 'btn btn-xs btn-primary js-update-answer']) . ' ' .
                                    Html::button('D', ['class' => 'btn btn-xs btn-danger js-delete-answer']));
                            },
                            'format' => 'raw',
                        ],
                        [
                            'label' => 'Rating',
                            'value' => function ($model) {
                                return $model->answerRatingModel ? 
                                        StarRating::widget([
                                            'id' => 'show_reting_answer_' . $model->id,
                                            'name' => 'show_reting_answer_' . $model->id,
                                            'value' => $model->getRating(),
                                            'disabled' => true,
                                            'pluginOptions' => [
                                                'size' => 'xs',
                                                'min' => 0,
                                                'max' => 5
                                            ],
                                        ]) . 
                                        (!$model->questionsRatingModel ? Html::button('Add rating too', ['class' => 'btn btn-primary btn-xs js-add-question-rating']) : '')
                                    : '';
                            },
                            'format' => 'raw',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {

        $('.answer-index').on('click', '.js-add-question-rating', function () {
            var id = $(this).closest('tr').data('key')
            $.confirm({
                id: 'addQuestionRatingModal',
                title: 'Add rating',
                content: 'url:<?= Url::to(['/business/questions/add-rating']) ?>/' + id,
                columnClass: 'xl view-block modal-block '
            });
        });

        $('.answer-index').on('click', '.js-update-answer', function () {
            var id = $(this).closest('tr').data('key')
            $.confirm({
                id: 'updateAnswerModal',
                title: 'Update answer',
                content: 'url:<?= Url::to(['/business/questions/update-answer']) ?>/' + id,
                columnClass: 'xl view-block modal-block '
            });
        });

        $('.answer-index').on('click', '.js-delete-answer', function () {
            var id = $(this).closest('tr').data('key')
            swal({
                title: "Are you sure?",
                text: "delete answer",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        method: 'post',
                        url: '<?= Url::to(['/business/questions/delete-answer']); ?>/' + id,
                        dataType: 'json'
                    }).done(function (response) {
                        if (response.status) {
                            $('#answersList').yiiGridView('applyFilter')
                            swal("Deleted!", {
                                icon: "success",
                            });
                        }
                    })
                }
            });
        })
    })
</script>