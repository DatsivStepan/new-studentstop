<?php

use yii\helpers\Html,
    yii\widgets\DetailView,
    kartik\rating\StarRating,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\services\Rents */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="js-question-show-block view-modal-bl question-content">
    <div class="rents-view-modal row" style="background-color: #f3f3f3; padding:  10px 20px; margin: 0px;">

        <div class="col-12" style="padding-right: 0px;">
            <div class="row">
                <div class="col-12 custom-col-sm-1-5 image-block">
                    <img class="image" src="<?= $model->getAuthorAvatar(); ?>">
                </div>
                <div class="col-12 col-sm-4 classif-text-block text-bl question-user-name">
                    <p><?= $model->getAuthorName(); ?></p>
                </div>
                <div class="col-sm-4 button questions-search-apply-btn" style="padding-right: 0px;">
                    <?php if (!$model->isOwner()) { ?>
                        <?= Html::button('Apply', [
                            'class' => 'view-button btn btn-info js-apply-question',
                            'data-id' => $model->id
                        ]); ?>
                    <?php } ?>
                </div>
                <div class="col-12 custom-col-sm-2-5 text-bl question-modal-price" style="padding-top:0!important;">
                    <p><?= $model->getPrice(); ?></p>

                </div>
            </div>
        </div>
    </div>
    <div class="tab-menu">
        <ul class="nav nav-pills">
            <li class="nav-item first">
                <a class="nav-link active" data-toggle="pill" href="#description">Info</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#bids">Bids</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#rating">Rating</a>
            </li>
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane container active question-info-description" id="description">
            <p class="question-info-title"><?= $model->getTitle(); ?></p>
            <p><?= $model->getContent(); ?></p>
        </div>
        <div class="tab-pane container fade" id="bids">
            <table class="my-questions-modal-content" style="width:100%">
                <thead>
                <tr>
                    <th class="questions-bids-table-title">User</th>
                    <th class="questions-bids-table-title">Rating</th>
                    <th class="questions-bids-table-title">Price</th>
                </tr>
                </thead>
                <tbody>
                <?php if ($answers = $model->questionsAnswersModel) { ?>
                    <?php foreach ($answers as $answer) { ?>
                        <tr style="border-bottom: 1px solid #d0d2d5;">
                            <td class="classif-text-block text-bl" style="padding-bottom: 10px;">
                                <img class="questions-bids-img" src="<?= $answer->getAuthorAvatar(); ?>">
                                <span class="questions-author-name"><?= $answer->getAuthorName(); ?></span>
                            </td>
                            <td class="questions-bids-rating">
                                <?= StarRating::widget([
                                    'id' => 'show_reting_answer_' . $answer->id,
                                    'name' => 'show_reting_answer_' . $answer->id,
                                    'value' => $model->getRating(),
                                    'disabled' => true,
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'min' => 0,
                                        'max' => 5
                                    ],
                                ]); ?>
                            </td>
                            <td class="classif-text-block text-bl questions-bids-price">
                                <?= $answer->getPrice(); ?><br>
                            </td>
                            <td class="classif-text-block text-bl">
                                <?php if ($model->isOwner()) { ?>
                                    <?= Html::a("View", null, ['class' => 'js-show-answer btn btn-primary questions-admin-view', 'data-id' => $answer->id]); ?>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <p class="text-center">Bids not found</p>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane container fade" id="rating">
            <div class="row ">
                <div class="col-6 questions-bids-table-title">User</div>
                <div class="col-6 questions-bids-table-title">Rating</div>
            </div>
            <?php if ($dataRatings) { ?>
                <?php foreach ($dataRatings as $rating) { ?>
                    <div class="row rating-content-style">
                        <div class="col-6 classif-text-block text-bl">
                            <img class="questions-bids-img" src="<?= $rating->getAuthorAvatar(); ?>">
                            <span class="questions-author-name"><?= $rating->getAuthorName(); ?></span>
                        </div>
                        <div class="col-6 classif-text-block text-bl questions-bids-rating">
                            <?= StarRating::widget([
                                'id' => 'show_reting_q_' . $rating->id,
                                'name' => 'show_reting_q_' . $rating->id,
                                'value' => $rating->rating,
                                'disabled' => true,
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'min' => 0,
                                    'max' => 5
                                ],
                            ]); ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <p class="text-center">Ratings not found</p>
            <?php } ?>

        </div>
    </div>
</div>

<script>
    $(function(){
        $('.js-question-show-block').on('click', '.js-apply-question', function(){
            $.confirm({
                id: 'applyQuestionModal',
                title: 'Apply Question',
                content: 'url:<?= Url::to(['/business/questions/apply/'.$model->id]) ?>',
                columnClass: 'xl view-block modal-block ',
            });
        })
    })
</script>