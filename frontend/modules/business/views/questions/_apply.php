<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    common\models\business\QuestionsCategories,
    common\models\business\Questions,
    common\models\User,
    yii\widgets\Pjax,
    kartik\select2\Select2,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\business\Questions */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(['id' => 'pjax-form-apply-question', 'enablePushState' => false]); ?>
    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            if ($modal.applyQuestionModal) {
                $modal.applyQuestionModal.close();
            }

            if ($modal.updateAnswerModal) {
                $modal.updateAnswerModal.close();
            }

            if ($('#answersList').length) {
                $('#answersList').yiiGridView('applyFilter')
            }

            if ($modal.questionShowModal) {
                modalReload($modal.questionShowModal, '<?= Url::to(['/business/questions/show/'.$model->question_id]) ?>');
            }
//            location.reload()
        </script>
    <?php } ?>

    <div class="js-questions-form">

        <?php $form = ActiveForm::begin(['id' => 'form-market', 'options' => ['data-pjax' => 'pjax-form-apply-question']]); ?>
            <div class="row question-form" style="margin: 0px;margin-top: 10px;">
                <div class="col-sm-2 col-xl-2 text-modal">
                    <text>Show Answer</text>
                </div>
                <div class="col-sm-9 col-xl-10">
                    <?= $form->field($model, 'show_answer')->textInput(['maxlength' => true])->label(false); ?>
                </div>

                <div class="col-sm-2 col-xl-2 text-modal">
                    <text>Answer</text>
                </div>
                <div class="col-sm-9 col-xl-10">
                    <?= $form->field($model, 'answer')->textarea(['rows' => 2])->label(false); ?>
                </div>

                <div class="col-sm-2 col-xl-2 text-modal">
                    <text>Money</text>
                </div>
                <div class="col-sm-9 col-xl-10">
                   <?= $form->field($model, 'money')->input('number', ['min' => '0.00'])->label(false);  ?>
                </div>

                <div class="col-sm-2 col-xl-2 text-modal">
                    <text>Credit</text>
                </div>
                <div class="col-sm-9 col-xl-10">
                   <?= $form->field($model, 'credit')->input('number', ['min' => '0.00'])->label(false);  ?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>
    </div>

<?php Pjax::end(); ?>