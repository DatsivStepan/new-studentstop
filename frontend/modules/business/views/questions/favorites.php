<?php

use yii\helpers\Html,
    yii\grid\GridView,
    yii\widgets\Pjax,
    yii\helpers\Url,
    common\models\Favorites;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\business\models\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Questions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <?= $this->render('_search', [
        'model' => $searchModel,
        'button' => false
    ]); ?>
    <?php if ($models = $dataProvider->getModels()) { ?>
        <div class="row search-page">
            <?php foreach ($models as $model) { ?>
                <?= $this->render('_item_template', [
                    'model' => $model
                ])?>
            <?php } ?>
        </div>
    <?php } else { ?>
        <p>Not Found</p>
    <?php } ?>
</div>