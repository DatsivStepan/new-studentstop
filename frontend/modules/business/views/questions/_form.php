<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    common\models\business\QuestionsCategories,
    common\models\business\Questions,
    common\models\User,
    yii\widgets\Pjax,
    kartik\select2\Select2,
    yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\business\Questions */
/* @var $form yii\widgets\ActiveForm */

?>
<style>
    .question-form .form-control, .question-form .select2-container--krajee .select2-selection--multiple {
        height: auto !important;
    }
</style>
<?php Pjax::begin(['id' => 'pjax-form-question-create', 'enablePushState' => false]); ?>
    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            if ($modal.questionCreateModal) {
                $modal.questionCreateModal.close();
            }
            if ($modal.questionUpdateModal) {
                $modal.questionUpdateModal.close();
            }
            location.reload()
        </script>
    <?php } ?>

    <div class="js-questions-form question-form">
        <?php $form = ActiveForm::begin(['id' => 'form-market', 'options' => ['data-pjax' => 'pjax-form-question-create']]); ?>
            <div class="row" style="margin-left: 16px;margin-right: 16px; padding-top: 5px;">
                <div class="col-sm-6">
                     <div class="row">
                        <div class="col-sm-3 col-xl-2 text-modal">
                            <text>Title</text>
                        </div>
                        <div class="col-sm-9 col-xl-10">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false);  ?>
                        </div>

                        <div class="col-sm-3 col-xl-2 text-modal">
                           <text>Money</text>
                        </div>
                        <div class="col-sm-9 col-xl-10">
                            <?= $form->field($model, 'money')->input('number', ['min' => '0.00'])->label(false);  ?>
                        </div>

                        <div class="col-sm-3 col-xl-2 text-modal">
                            <text> Credits</text>
                        </div>
                        <div class="col-sm-9 col-xl-10">
                            <?= $form->field($model, 'credits')->input('number', ['min' => '0.00'])->label(false);  ?>
                        </div>

                        <div class="col-sm-3 col-xl-2 text-modal">
                            <text>Category</text>
                        </div>
                        <div class="col-sm-9 col-xl-10">
                            <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                                'data' => QuestionsCategories::getAllByGroupCategories(),
                                'options' => ['placeholder' => Yii::t('app', 'Select...')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ]
                            ])->label(false); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3 col-xl-2 text-modal">
                            <text> Description</text>
                        </div>
                        <div class="col-sm-9 col-xl-10">
                            <?= $form->field($model, 'description')->textarea(['rows' => 6])->label(false); ?>
                        </div>

                        <div class="col-sm-3 col-xl-2 text-modal">
                            <text>Privacy Status</text>
                        </div>
                        <div class="col-sm-9 col-xl-10">
                            <?= $form->field($model, 'privacy_status')->widget(Select2::classname(), [
                                    'data' => Questions::$privacyStatusArray,
                                    'options' => ['placeholder' => Yii::t('app', 'Select...')],
                                    'pluginOptions' => [
                                    ]
                                ])->label(false); ?>
                        </div>

                        <div class="js-invite-users-input-block row" style=" margin: 0px;width: 100%;display: <?= $model->privacy_status == Questions::PRIVACY_STATUS_INVITE ? 'flex' : 'none'; ?>">
                            <div class="col-sm-3 col-xl-2 text-modal">
                                <text>Invite Users</text>
                            </div>
                            <div class="col-sm-9 col-xl-10">
                                <?php $userDisabled = []; ?>
                                <?php $userShow = []; ?>
                                <?php if ($invites = $model->questionsInviteModel) {  ?>
                                    <?php foreach ($invites as $invite) { ?>
                                        <?php if ($user = $invite->userModel) { ?>
                                            <?php $userShow[] = $user->getNameAndSurname(); ?>
                                            <?php $userDisabled[$user->id] = ['disabled' => true]; ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php }  ?>

                                <div style="clear: both"></div>
                                <?= $form->field($model, 'inviteUsers')->widget(Select2::classname(), [
                                    'data' => User::getAllInArrayMap(),
                                    'options' => [
                                        'placeholder' => Yii::t('app', 'Select...'),
                                        'multiple' => true,
                                        'options' => $userDisabled
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ]
                                ])->label(false); ?>
                                <?php foreach ($invites as $invite) { ?>
                                    <span style="color: #555555;background: #f5f5f5;border: 1px solid #ccc;border-radius: 4px;cursor: default;margin: 5px 0 0 6px;padding: 0 6px;"><?= $user->getNameAndSurname(); ?></span>
                                <?php }  ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <script>
        $(function(){
            $('.js-questions-form').on('change', '#questions-privacy_status', function(){
                if ($(this).val() == '<?= Questions::PRIVACY_STATUS_INVITE; ?>') {
                    $('.js-invite-users-input-block').show();
                } else {
                    $('.js-invite-users-input-block').hide();
                }
            });
        })
    </script>
<?php Pjax::end(); ?>