<?php

use yii\helpers\Html,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\services\models\ClassifiedMarketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Question bank');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classified-market-index class-market">
            <?= $this->render('_search', [
                'model' => $searchModel,
                'button' => false
            ]); ?>
            
        <div class="row" style="margin: 0px;">
            <?php if ($categories) { ?>
                <?php foreach ($categories as $category) { ?>
                    <?php if ($category->getChildCategory(3)) { ?>
                        <div class="col-sm-3 classified-block">
                            <div class="classified-block-my">
                                <h2><?= $category->name; ?></h2>
                                <?php foreach ($category->getChildCategory(3) as $category) { ?>
                                    <div class="row another-cl">
                                        <div class="col-10 name-category">
                                             <?= Html::a($category->name, $category->getLink(), ['class' => '']); ?>
                                        </div>
                                       <div class="col-2 number-block">
                                           <div class="number"><?= $category->getQuestionCount(); ?></div>
                                       </div>
                                    </div>
                                <?php } ?>
                                <div class="button-cl">
                                    <a href="#">MORE</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>

</div>