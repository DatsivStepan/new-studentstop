<?php

namespace frontend\modules\business\controllers;

use Yii,
    common\models\business\Questions,
    frontend\modules\business\models\QuestionsSearch,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    common\models\MenuItems,
    yii\filters\AccessControl,
    common\models\business\QuestionsCategories,
    frontend\modules\business\models\QuestionsInviteSearch,
    frontend\modules\business\models\QuestionsRatingSearch,
    common\models\business\QuestionsAnswers,
    frontend\modules\business\models\AnswersSearch,
    yii\web\Response,
    common\models\pay\PayRequests,
    common\models\business\QuestionsRating;

/**
 * QuestionsController implements the CRUD actions for Questions model.
 */
class QuestionsController extends Controller
{
    public $universityId;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function beforeAction($action) {
        if ($action == 'redirect') {
            $this->enableCsrfValidation = false;
        }
        $this->universityId = Yii::$app->user->identity->userInfoModel ? Yii::$app->user->identity->userInfoModel->university_id : null;
        if (!$this->universityId) {
            return $this->redirect(['/site/check-user-university']);
        }
        return parent::beforeAction($action);
    }
    

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_QUESTION_BANK;
//        $searchModel = new QuestionsSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
            'searchModel' => new QuestionsSearch(),
            'categories' => QuestionsCategories::getAllParentCategories(),
        ]);
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionFavorites()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_QUESTION_BANK;

        $searchModel = new QuestionsSearch();
        $searchModel->favorites = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('favorites', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionSearch($id = null)
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_QUESTION_BANK;
        $searchModel = new QuestionsSearch();
        $searchModel->category_id = $id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionMyQuestions()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_QUESTION_BANK;
        $searchModel = new QuestionsSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('my-questions', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionMyAnswers()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_QUESTION_BANK;
        $searchModel = new AnswersSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('my-answers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionInvite()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_QUESTION_BANK;

        $searchModel = new QuestionsInviteSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('my-invites', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionAddRating($id)
    {
        $this->layout = '/modal';
        $model = $this->findAnswerModel($id);

        $newRating = new QuestionsRating;
        $newRating->user_id = $model->questionModel ? $model->questionModel->user_id : null;
        $newRating->answer_id = $model->id;
        $newRating->type = QuestionsRating::TYPE_QUESTION;
        if ($newRating->load(Yii::$app->request->post())) {
            if ($newRating->save()) {
                Yii::$app->session->setFlash('rSuccessSave');
            }
        }
        return $this->render('_add_rating', [
            'model' => $model,
            'newRating' => $newRating,
        ]);
    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     */
    public function actionShowAnswer($id)
    {
        $this->layout = '/modal';
        $model = $this->findAnswerModel($id);
        $newRating = new QuestionsRating;
        $newRating->user_id = $model->user_id;
        $newRating->answer_id = $model->id;

        if (Yii::$app->request->isPost && Yii::$app->request->post('save') !== null) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $status = false;
            if ($newRating->load(Yii::$app->request->post())) {
                $status = $newRating->save();
            }
            return ['status' => $status];
        }

        return $this->render('view_answer', [
            'model' => $model,
            'newRating' => $newRating,
        ]);
    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     */
    public function actionShow($id)
    {
        $this->layout = '/modal';
        $model = $this->findModel($id);
        
        $searchRatingModel = new QuestionsRatingSearch();
        $searchRatingModel->user_id = $model->user_id;

        return $this->render('view', [
            'model' => $model,
            'dataRatings' => $searchRatingModel->search()->getModels(),
        ]);
    }

    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdateAnswer($id)
    {
        $this->layout = '/modal';

        $model = $this->findAnswerModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }

        return $this->render('_apply', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionApply($id)
    {
        $this->layout = '/modal';
        
        $model = new QuestionsAnswers();
        $model->user_id = Yii::$app->user->id;
        $model->question_id = $id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }

        return $this->render('_apply', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @param $classId
     * @return mixed
     */
    public function actionCreate($id = null, $classId = null)
    {
        $this->layout = '/modal';

        if (!$model = Questions::findOne($id)) {
            $model = new Questions();
            $model->user_id = Yii::$app->user->id;
            $model->class_id = $classId;
            $model->university_id = $this->universityId;
            $model->privacy_status = Questions::PRIVACY_STATUS_ALL;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Questions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Questions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteAnswer($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findAnswerModel($id);
        $status = false;
        if ($model->isOwner()) {
            $status = $model->delete();
        }

        return ['status' => $status];
    }

    /**
     * @param integer $id
     * @return array
     */
    public function actionChangeAnswerStatus($id, $status, $note)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findAnswerModel($id);

        $resStatus = false;
        $model->status = (int)$status;
        if ($model->save()) {
            $resStatus = true;
//            $note add note to log
        }

        return ['status' => $resStatus];
    }
    /**
     * @param integer $id
     * @param integer $type
     * @return array
     */
    public function actionPayForAnswer($id, $type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findAnswerModel($id);
        $status = false;
        if ($type == PayRequests::TYPE_PAY_CREDIT) {
            $status = PayRequests::creditsPayRequest(
                Yii::$app->user->id,
                $model->user_id,
                $model->credit,
                $model->id,
                PayRequests::OPERATION_TYPE_QUESTION_ANSWER,
                PayRequests::MODULE_QUESTION_BANK,
                'Payed credits for answers'
            );
        } else if ($type == PayRequests::TYPE_PAY_MONEY) {
            $status = PayRequests::moneyPayRequest(
                Yii::$app->user->id,
                $model->user_id,
                $model->money,
                $model->id,
                PayRequests::OPERATION_TYPE_QUESTION_ANSWER,
                PayRequests::MODULE_QUESTION_BANK,
                'Payed money for answers'
            );
        }
        if ($status) {
            $model->status = QuestionsAnswers::STATUS_PAYED;
            $model->save();
        }

        return ['status' => $status];
    }

    /**
     * Deletes an existing Questions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $status = false;
        if ($model->isOwner()) {
            $status = $model->delete();
        }

        return ['status' => $status];
    }

    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the QuestionsAnswers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QuestionsAnswers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findAnswerModel($id)
    {
        if (($model = QuestionsAnswers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
