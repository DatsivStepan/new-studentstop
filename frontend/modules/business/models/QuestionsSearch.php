<?php

namespace frontend\modules\business\models;

use Yii,
    yii\base\Model,
    yii\data\ActiveDataProvider,
    common\models\business\Questions,
    common\models\business\QuestionsRating;

/**
 * QuestionsSearch represents the model behind the search form about `common\models\business\Questions`.
 */
class QuestionsSearch extends Questions
{
    public $country;
    public $state;
    public $dateShowFrom;
    public $favorites;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id', 'user_id', 'university_id', 'category_id', 'privacy_status', 'status', 'class_id',
                    'country', 'state', 'favorites'
                ],
                'integer'
            ],
            [['title', 'description', 'created_at', 'updated_at', 'dateShowFrom'], 'safe'],
            [['money', 'credits'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Questions::find()
            ->alias('q')
            ->select(['q.*', 'type' => 'favorites.type'])
            ->joinWith(['favoritesModel favorites']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->dateShowFrom) {
            $query->andFilterWhere([
                '>', 'q.created_at', $this->dateShowFrom
            ]);
        }

        if ($this->favorites) {
            $query->andFilterWhere([
                'favorites.user_id' => Yii::$app->user->id
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'q.id' => $this->id,
            'q.user_id' => $this->user_id,
            'q.university_id' => $this->university_id,
            'q.category_id' => $this->category_id,
            'q.privacy_status' => $this->privacy_status,
            'q.money' => $this->money,
            'q.credits' => $this->credits,
            'q.status' => $this->status,
            'q.class_id' => $this->class_id,
        ]);

        $query->andFilterWhere(['like', 'q.title', $this->title])
            ->andFilterWhere(['like', 'q.description', $this->description]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchRating()
    {
        $query = Questions::find()
            ->alias('q')
            ->joinWith(['questionsAnswersModel.questionsRatingModel r'])
            ->where(['not', ['r.id' => null]]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'q.id' => $this->id,
            'q.user_id' => $this->user_id,
            'q.university_id' => $this->university_id,
            'q.category_id' => $this->category_id,
            'q.privacy_status' => $this->privacy_status,
            'q.money' => $this->money,
            'q.credits' => $this->credits,
            'q.status' => $this->status,
            'q.class_id' => $this->class_id,
        ]);

        $query->andFilterWhere(['like', 'q.title', $this->title])
            ->andFilterWhere(['like', 'q.description', $this->description]);

        return $dataProvider;
    }
}
