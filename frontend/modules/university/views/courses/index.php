<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\university\models\SearchCourses */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row no-gutters courses-index">
    <div class="col-12 left-content">
        <div class="row no-gutters">
            <div class="col-3 mobile-100">
                <div class="majors-box">
                    <div class="majors-head position-relative">
                        <div class="btn-expand m-device-show open">
                            <i class="fas fa-chevron-circle-up"></i>
                            <i class="fas fa-chevron-circle-down"></i>
                        </div>
                        Majors
                    </div>
                    <div class="js-majors-block expand-block majors-block my-scroll">
                        <?php $k = 0; ?>
                        <?php foreach ($majors as $majorId => $majorName) { ?>
                        <?= Html::a($majorName, null, [
                            'class' => 'js-major-link js-major-id-'. $majorId. ' major-item ' . ($k == 0 ? 'active' : ''),
                            'data-id' => $majorId
                        ]); ?>
                        <?php $k ++; ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-3 m-p-10 mobile-100">
                <div class="courses-box">
                    <div class="courses-head position-relative">
                        <div class="btn-expand m-device-show open">
                            <i class="fas fa-chevron-circle-up"></i>
                            <i class="fas fa-chevron-circle-down"></i>
                        </div>
                        Courses
                    </div>
                    <div class="js-courses-block expand-block courses-block my-scroll">
                    </div>
                </div>
            </div>
            <div class="col-6 m-p-10 mobile-100">
                <div class="classes-box">
                    <div class="classes-head position-relative">
                        Classes
                    </div>
                    
                    <?php Pjax::begin(['id' => 'pjax-form-classes-search', 'enablePushState' => false]); ?>
                    <div style="display:none;">
                        <?php $form = ActiveForm::begin(['id' => 'form-search-classes', 'options' => ['data-pjax' => 'pjax-form-classes-search']]); ?>
                        <?= $form->field($searchModel, 'course_id')->textInput(); ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="js-classes-block classes-block my-scroll">
                        <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'responsive' => false,
                        'striped' => false,
                        'responsiveWrap' => false,
                        'columns' => [
    //                            'name',
                            [
                                'attribute' => 'professor',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    $professor = $model->professorModel;
                                    return $professor ? 
                                            Html::tag('img', '', [
                                                'src' => $professor->getAvatar(),
                                                'class' => 'professor-avatar',
                                            ]).
                                            Html::tag('div', $professor->getNameAndSurname(), [
                                                'class' => 'professor-name',
                                            ])
                                            : null;
                                },
                                'contentOptions' => ['class' => 'professor-data']
                            ],
                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model->getStatus();
                                },
                                'contentOptions' => ['class' => 'professor-status']
                            ],
                            [
                                'label' => 'Students',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model->getStudentsCount();
                                },
                                'contentOptions' => ['class' => 'professor-students']
                            ],
                            [
                                'label' => 'Rank',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model->getRank();
                                },
                                'contentOptions' => ['class' => 'professor-rank']
                            ],
                            [
                                'label' => 'Rating',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return '<div class="custom-stars">'.StarRating::widget([
                                            'id' => 'rating_'. uniqid() . $model->id,
                                            'name' => 'rating_' . $model->id,
                                            'value' => $model->getRating(),
                                            'disabled' => true,
                                            'pluginOptions' => [
                                                'size' => 'xs',
                                                'min' => 0,
                                                'max' => 5
                                            ],
                                        ])."</div>";
                                },
                                'contentOptions' => ['class' => 'professor-rating']
                            ],
                            [
                                'label' => '',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    $joinStatus = $model->getJoinClassText();

                                    return Html::a($joinStatus['text'], 
                                        $joinStatus['link'],
                                        [
                                            'class' => 'js-join-classes action-btn',
                                            'target' => '_blank',
                                            'data-id' => $model->id,
                                        ]);
                                },
                                'contentOptions' => ['class' => 'professor-btn']
                            ],
                        ],
                    ]); ?>
                    </div>
                    <?php Pjax::end();  ?>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    $(function () {
        
        $(".btn-expand").click(function(e){
            e.preventDefault();
            $(this).closest('.mobile-100').toggleClass('expanded');
            $(this).toggleClass('open');
        })
        // if($.fn.basictable){
        //     $('table.table-striped').basictable({
        //         breakpoint: 1400,
        //     });
        // }
        function getClass(courseId) {
            $('#form-search-classes').find('#searchclasses-course_id').val(courseId);
            $('#form-search-classes').submit();
        }

        function notMajor() {
            $('.js-majors-block').html('No results found')
            $('#form-search-classes').find('#searchclasses-course_id').val(0);
            $('#form-search-classes').submit();
        }

        function notCourse() {
            $('.js-courses-block').html('No results found')
            $('#form-search-classes').find('#searchclasses-course_id').val(0);
            $('#form-search-classes').submit();
        }

        function getCourse(majorId) {
            if (majorId) {
                $.ajax({
                    method: 'post',
                    url: 'get-course-by-major?id=' + majorId,
                    dataType: 'json'
                }).done(function (response) {
                    if (response.status) {
                        setCourseInBlock(response.data)
                    } else {
                        notCourse();
                    }
                })
            } else {
                notCourse();
            }
        }

        function setCourseInBlock(data) {
            if (data) {
                $('.js-courses-block').html('')
                $('.js-classes-block').html('')
                var k = 0;
                $.each(data, function (index, value) {
                    var clas = '';
                    if (k == 0) {
                        getClass(index)
                        clas = 'active';
                    }
                    $('.js-courses-block').append(
                        $('<a/>', {
                            class: 'js-course-link courses-item ' + clas,
                            'data-id': index,
                        }).text(value));
                    k++;
                });
            } else {
                notCourse();
            }
        }
        
        if ($('.js-major-link').first()) {
            getCourse($('.js-major-link').first().data('id'))
        } else {
            notMajor();
        }

        $(document).on('click', '.js-major-link', function () {
            var majorId = $(this).data('id');
            $('.js-major-link').removeClass('active');
            $(this).addClass('active')
            getCourse(majorId);
        })

        $(document).on('click', '.js-course-link', function () {
            var courseId = $(this).data('id')
            $('.js-course-link').removeClass('active');
            $(this).addClass('active')
            getClass(courseId);
        })

        if (<?= Yii::$app->request->get('majorId', 0); ?> > 0) {
            $('.js-major-id-<?= Yii::$app->request->get('majorId'); ?>').trigger('click')
        }
    })
</script>