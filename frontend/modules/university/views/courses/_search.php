<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
/* @var $this yii\web\View */
/* @var $model frontend\modules\university\models\SearchCourses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="courses-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'major_id')->widget(Select2::classname(), [
        'data' => $majors,
        'options' => ['placeholder' => Yii::t('app', 'Select...')],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]); ?>
    
    <?= $form->field($model, 'id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => $courses,
        'options' => ['id' => 'select-contragent', 'placeholder' => Yii::t('app', 'Select...')],
        'select2Options' => [
            'pluginOptions' => [
                'allowClear' => true
            ],
        ],
        'pluginOptions' => [
            'depends' => ['searchcourses-major_id'],
            'url' => Url::to(['get-courses-by-major'])
        ]
    ]); ?>

    <?= $form->field($model, 'name') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
