<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model common\models\university\VirtualClasses */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(['id' => 'pjax-form-bt', 'enablePushState' => false]); ?>

    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            
        </script>
    <?php } ?>

    <div class="become-a-tutor">

        <?php $form = ActiveForm::begin(['id' => 'form-become-tutor', 'options' => ['data-pjax' => 'pjax-form-bt']]); ?>
            <div class="row">
                <div class="col-2 col-form-label">
                    <label class="label-block"">Price</label>
                </div>
                <div class="col-10">
                     <?= $form->field($model, 'price')->input('number')->label(false); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-2 col-form-label label-block">
                    <label class="label-block">Description</label>
                </div>
                <div class="col-10">
                     <?= $form->field($model, 'description')->textarea()->label(false); ?>
                </div>
            </div>
           
            
        
            <?php foreach ($majors as $major) { ?>
                <?php if ($major->courses) { ?>
                    <h3><?= $major->name; ?></h3>
                    <div class="row"><?php foreach ($major->courses as $course) { ?>
                         
                            <div class="col-6">
                            <?= $form->field($model, 'courses[' . $course->id . ']', [
                                'template' => '{input} {label}',
                                'labelOptions' => ['class' => 'control-label'],
                            ])
                            ->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]])
                            ->label($course->name); ?></div>
                        
                    <?php } ?>
                            
                <?php } ?></div>
                
            <?php } ?>
        
        <?php ActiveForm::end(); ?>

    </div>
<?php Pjax::end(); ?>
