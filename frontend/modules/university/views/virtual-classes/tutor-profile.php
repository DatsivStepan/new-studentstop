<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\rating\StarRating;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\university\VirtualClasses */

$this->title = $modelUser->getNameAndSurname();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="virtual-classes-view tutor-profile">
    <div class="tutor-p">
        <div class="row">
            <div class="col-sm-4 user-image">
                <img src="<?= $modelUser->getAvatar(); ?>" style="width:100%;">
            </div>
            <div class="col-sm-8  user-block">
                <div class="row">
                    <div class="col-6">
                        <h3 class="name-user"><?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="col-6 button-block">
                        <?= $model->user_id == Yii::$app->user->id ? 
                        Html::a(Yii::t('app', ''), null, ['class' => 'btn btn-primary js-change-tutor-profile']) : ''; ?>
                        <?php /*= Html::a(Yii::t('app', ''), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) */ ?>
                    </div>
                </div>
                    <text>Rank: 3/5</text>
                    <?= StarRating::widget([
                        'name' => 'rating_user',
                        'value' => $modelUser->getRankCount(),
                        'disabled' => true,
                        'pluginOptions' => [
                            'size' => 'xs',
                            'min' => 0,
                            'max' => 5
                        ],
                    ]); ?>
                <table class="table user-table">
                    <tr>
                        <td class="category">Birthday:</td>
                        <td ><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->birthday : ''; ?></td>
                    
                        <td class="category">Mobile:</td>
                        <td><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->mobile : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="category">University:</td>
                        <td><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->getUniversityName() : ''; ?></td>
                        <td class="category">Address:</td>
                        <td><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->address : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="category">About:</td>
                        <td colspan="3"><?= $model->description; ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row button-block-2">
        <div class="col-sm-12">
            <?= Html::button('Add conference', ['class' => 'add-conference btn btn-primary js-add-virtual-class']); ?>
        </div>
    </div>
    <div class="table-block">
    <div class="row">
        <div class="col-sm-12">
            <div class="js-classes-block classes-block my-scroll">
                <?= GridView::widget([
                    'id' => 'listVitrualClasses',
                    'dataProvider' => $dataProvider,
                    'responsive' => false,
                    'striped' => false,
                    'responsiveWrap' => false,
                    'pjax' => true,
                    'columns' => [
                        'name',
                        [
                            'attribute' => 'date_start',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->date_start;
                            },
                            'contentOptions' => ['class' => 'professor-status']
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->status;
                            },
                            'contentOptions' => ['class' => 'professor-status']
                        ],
                        [
                            'label' => 'Students',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getStudentsCount();
                            },
                            'contentOptions' => ['class' => 'professor-students']
                        ],
                        [
                            'label' => 'Rank',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getRankCount();
                            },
                            'contentOptions' => ['class' => 'professor-rank']
                        ],
                        [
                            'label' => 'Rating',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return '<div class="custom-stars">'.StarRating::widget([
                                        'name' => 'rating_' . $model->id,
                                        'value' => $model->getRankCount(),
                                        'disabled' => true,
                                        'pluginOptions' => [
                                                'size' => 'xs',
                                                'min' => 0,
                                                'max' => 5
                                        ],
                                    ])."</div>";
                            },
                            'contentOptions' => ['class' => 'professor-rating']
                        ],
                        [
                            'label' => '',
                            'format' => 'raw',
                            'value' => function ($model) {
//                                $joinStatus = $model->getJoinClassText();

                                return Html::a('View', 
                                    'View',
                                    [
                                        'class' => 'js-join-classes action-btn',
                                        'target' => '_blank',
                                        'data-id' => $model->id,
                                   ]); 
                            },
                            'contentOptions' => ['class' => 'professor-btn']
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $(function(){
        $(document).on('click', '.js-change-tutor-profile', function(e) {
            e.preventDefault();
            $.confirm({
                id: 'becomeTutorModal',
                title: 'Become a tutor',
                content: 'url:<?= Url::to(['/university/virtual-classes/become-tutor']) ?>/<?= $model->id; ?>' ,
                columnClass: 'm'
            });
        });

        $(document).on('click', '.js-add-virtual-class', function(){
            $.confirm({
                id: 'addVirtualClassModal',
                title: 'Create Virtual Classes',
                content: 'url:<?= Url::to(['/university/virtual-classes/create']) ?>',
                columnClass: 'xl modal-block'
            });
        })
    })
</script>