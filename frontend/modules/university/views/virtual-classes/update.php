<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\university\VirtualClasses */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Virtual Classes',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Virtual Classes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="virtual-classes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
