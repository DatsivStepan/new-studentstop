<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\university\VirtualClasses */

$this->title = Yii::t('app', 'Create Virtual Classes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Virtual Classes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="virtual-classes-create">

    <?= $this->render('_form', [
        'model' => $model,
        'courses' => $courses,
    ]) ?>

</div>
