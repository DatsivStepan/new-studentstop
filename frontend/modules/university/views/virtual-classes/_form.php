<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;
use common\models\university\VirtualClasses;

/* @var $this yii\web\View */
/* @var $model common\models\university\VirtualClasses */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(['id' => 'pjax-form-bt', 'enablePushState' => false]); ?>

    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            $modal.addVirtualClassModal.close();
            $('#listVitrualClasses').yiiGridView("applyFilter");
        </script>
    <?php } ?>

<div class="virtual-classes-form ">

    <?php $form = ActiveForm::begin(['id' => 'form-become-tutor', 'options' => ['data-pjax' => 'pjax-form-bt']]); ?> 

        <div class="row" style="margin-left: 15px;margin-right: 15px;">
            <div class="col-sm-3 image-block">
                <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
                <img src="<?= $model->getImages(); ?>" class="previewMajorImage img-thumbnail"><br>
                <a class="btn btn-sm btn-success addMajorImage" style="width:100%">Change image</a><br><br>
            </div>
            <div class="col-sm-9  select-block">
                <form>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Course</label>
                        <div class="col-sm-10">
                            <?= $form->field($model, 'course_id')->widget(Select2::classname(), ['data' => $courses,
                    'options' => [
                    ],
                ])->label(false); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                           <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                           <?= $form->field($model, 'description')->textarea(['rows' => 2])->label(false); ?>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Date Start </label>
                        <div class="col-sm-10 date-class">
                           <?= $form->field($model, 'date_start')->widget(DateTimePicker::className(), [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd HH:ii:ss',
                        ],
                    ])->label(false);; ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Type </label>
                        <div class="col-sm-10">
                           <?= $form->field($model, 'type')->dropDownList(VirtualClasses::$typeArray)->label(false); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Price M</label>
                        <div class="col-sm-10">
                           <?= $form->field($model, 'price_m')->textInput()->label(false); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Price C</label>
                        <div class="col-sm-10">
                           <?= $form->field($model, 'price_c')->textInput()->label(false); ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    
    <?php ActiveForm::end(); ?>

</div>

<?php Pjax::end(); ?>