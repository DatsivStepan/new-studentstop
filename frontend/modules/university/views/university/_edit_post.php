<?php
use yii\helpers\Url,
    yii\widgets\Pjax,
    yii\helpers\Html,
    yii\widgets\ActiveForm,
    frontend\models\PostSearch,
    kartik\datetime\DateTimePicker,
    dosamigos\tinymce\TinyMce,
    common\models\PostFiles;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$imageString = [];
if ($files = $model->filesModel) {
    $count = count($files);
    foreach ($files as $key => $file) {
        $type = $file->type == PostFiles::TYPE_VIDEO ? PostFiles::TYPE_VIDEO . '/mp4' : PostFiles::TYPE_IMAGE . '/png';
        $imageString[] = [
            "file" => $file->file_src,
            "type" => $type
        ];
    }
}
$imageString = json_encode($imageString);
?>
<style>
    #js-preview-post-image-m .images-block {
        width: 50px;
        height: 50px;
        display: inline-block;
        margin: 2px;
        position: relative;
        background-size: cover;
    }
</style>
<div class="">
    <?php Pjax::begin(['id' => 'pjax-form-post', 'enablePushState' => false]); ?>

        <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
            <script>
                $modal.postUpdateModal.close();
                parent.location.reload();
            </script>
        <?php } ?>

        <div class="row" style="margin-left: 15px;margin-right: 15px;">
            <?php $form = ActiveForm::begin(['id' => 'create-form-item', 'options' => ['data-pjax' => 'pjax-form-post']]); ?>
                <div class="post-textarea">
                    <?= $form->field($model, 'images')->hiddenInput(['value' => $imageString, 'id' => 'post-images-input'])->label(false) ?>
                    <?= $form->field($model, 'content')->textarea(['class' => 'w-100 my-scroll', 'placeholder' => 'Write your news', 'style' => '    background: white;
                        border: 1px solid #d0d2d5;
                        height: 60px;
                        min-height: 60px;
                        border-radius: 5px;
                        resize: vertical;
                        outline: none;
                        padding: 15px 80px 15px 20px;
                        color: #231d2e;
                        font-size: 15px;
                        font-family: "Candara";'])->label(false) ?>
                    <i class="js-upload-file-button-m attach-btn" style="    width: 20px;
                        height: 11px;
                        background: url(/images/attach-file.png);
                        position: absolute;
                        top: 26px;
                        right: 70px;
                        cursor: pointer;"></i>
                </div>
                <div id="js-preview-post-image-m">
                    <?php if ($files = $model->filesModel) { ?>
                        <?php foreach ($files as $file) { ?>
                            <div class="images-block img-thumbnail" style="background-image:url(/<?= $file->file_src; ?>)">
                                <span class="glyphicon glyphicon-remove" style="position: absolute;right: -7px;top: -7px;cursor:pointer;"></span>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>

        <script>
            $(function(){
                var photosArray = jQuery.parseJSON('<?= $imageString; ?>');
                
                if ($('.js-upload-file-button-m').length) {
                    var previewNode = document.querySelector("#template-m");
                    previewNode.id = "";
                    var previewTemplate = previewNode.parentNode.innerHTML;
                    previewNode.parentNode.removeChild(previewNode);

                    var myDropzone = new Dropzone('#js-preview-post-image-m', {
                        url: "/site/upload-photo",
                        previewTemplate: previewTemplate,
                        previewsContainer: "#previews",
                        clickable: ".js-upload-file-button-m",
                        acceptedFiles: 'image/*,video/*',
                        uploadMultiple: false,
                        maxFiles: 1,
                    });

                    myDropzone.on("maxfilesexceeded", function (file) {
                        myDropzone.removeAllFiles();
                        myDropzone.addFile(file);
                    });

                    myDropzone.on("complete", function (response) {
                      if (response.status == 'success') {

                        photosArray.push({
                            file: response.xhr.response,
                            type: response.type
                        });

                        $('#post-images-input').val(JSON.stringify(photosArray));
                        $('#js-preview-post-image-m').append(
                          $('<div/>', {
                            class: 'images-block img-thumbnail',
                            style: 'background-image:url(/' + response.xhr.response + ')',
                          }).append(
                            $('<span/>', {
                              class: 'glyphicon glyphicon-remove',
                              style: 'position: absolute;right: -7px;top: -7px;cursor:pointer;'
                            })
                          )
                        )
                      }
                    });

                    myDropzone.on("removedfile", function (response) {
                      if (response.xhr != null) {
                        //deleteFile(response.xhr.response);
                      }
                    });

                }
            })
        </script>

        <div style="display:none;">
            <div class="row-table-style">
                <div class="table table-striped" class="files" id="previews-m">
                    <div id="template-m" class="file-row">

                    </div>
                </div>
            </div>
        </div>
    <?php Pjax::end();  ?>
</div>