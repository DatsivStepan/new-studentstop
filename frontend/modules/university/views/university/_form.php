<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use common\models\location\Countries;
use common\models\location\States;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Url;
use kartik\date\DatePicker;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\University */
/* @var $form yii\widgets\ActiveForm */
?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>
<div class="university-form" style="padding:15px;">
    <?php Pjax::begin(['id' => 'pjax-univesity-updated', 'enablePushState' => false]); ?>

        <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
            <script>
                swal("Updated!", {
                    icon: "success",
                });
            </script>
        <?php } ?>
        <?php $form = ActiveForm::begin(['id' => 'university-update-form', 'options' => ['data-pjax' => 'pjax-univesity-updated']]); ?>
            <?= $form->field($model, 'location_lat')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'location_lng')->hiddenInput()->label(false); ?>
            
            <?= $form->field($model, 'id_owner')->dropDownList(User::getAllInArrayMap()) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'history')->widget(TinyMce::className(), [
                    'options' => ['rows' => 6,'id'=>'eventsContent'],
                    'language' => 'en_GB',
                    'clientOptions' => [
                            'plugins' => [],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    ]
            ]); ?>

            <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'year_founded')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true
                ]
            ]); ?>

            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <div style="width:150px;">
                    <?= $form->field($model, 'image')->hiddeninput()->label(false); ?>
                    <img src="<?= $model->getImages(); ?>" class="js-preview-university-image img-thumbnail"><br>
                    <a class="btn btn-sm btn-success js-add-university-image" style="width:100%">Change image</a><br><br>
                </div>
            </div>
            
            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true
                ]
            ]); ?>

            <?= $form->field($model, 'telephone')->textInput(); ?>

            <?= $form->field($model, 'email')->textInput(); ?>

            <?= $form->field($model, 'website')->textInput(); ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>
</div>
<script>
    var autocomplete;
    var latInput = 'university-location_lat';
    var lngInput = 'university-location_lng';
    
    function initAutocomplete() {

        var mapOptions = {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13,
            minZoom: 8,
            maxZoom: 12,
        };

        var lat = document.getElementById(latInput).value;
        var lng = document.getElementById(lngInput).value;
        var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};

        if (lat && lng) {
            mapOptions.center.lat = parseFloat(lat);
            mapOptions.center.lng = parseFloat(lng);
        }

        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('university-location')),
            {types: ['geocode']});

        autocomplete.addListener('place_changed', fillInAddress);

        function fillInAddress() {
            var place = autocomplete.getPlace();

            document.getElementById(latInput).value = place.geometry.location.lat();
            document.getElementById(lngInput).value = place.geometry.location.lng();
        }
    }

    $(function(){
        initAutocomplete();

        $dropzone.initializationDropzoneEleemnt('university', '#university-image', 'image/*');
    })
</script>