<?php
use yii\helpers\Url,
    yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<?= Html::a('Edit University', Url::to(['/university/setting']), ['class' => 'btn btn-primary'])?>

<?= Html::a('Posts', Url::to(['/university/posts']), ['class' => 'btn btn-default'])?>
