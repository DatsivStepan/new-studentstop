<?php

use yii\helpers\Url,
    yii\widgets\Pjax,
    yii\helpers\Html,
    yii\widgets\ActiveForm,
    frontend\models\PostSearch;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->registerJsFile(Url::to(['/js/plugins/moment.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepicker.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepaginator.js']));

$this->registerCssFile(Url::to(['/css/plugins/bootstrap-datepaginator.css']));

$events = $dataProviderU->getModels();
?>
<script>
    $(function () {
        function dateChange(date) {
            date = new Date(date);
            var year = date.getFullYear()
            var month = date.getMonth()+1;
            if(month < 10){
                month = '0'+month;
            }
            var day = date.getDate()
            var dayClick = year+'-'+month+'-'+day;

            $('#events-search-form').find('#universityeventssearch-filterdate').val(dayClick)
            $('#events-search-form').submit()
        }

        $('#paginator').datepaginator({
            showCalendar: false,
            textSelected: '<b>dddd</b><br/>Do, MMMM YYYY',
            text: '<b>ddd</b><br/>Do',
            itemWidth: 50,
            onSelectedDateChanged: function(event, date){
                dateChange(date);
            },
        });
        $('.calendar-custom').on('click', '.custom-today', function (e) {
            e.preventDefault();
            var date = new Date();
            $('#paginator').datepaginator('setSelectedDate', `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`)
        });
    })
</script>
<div class="row no-gutters calendar-widget show-btn-event" id="js-events-list-block">
    <div class="col-12">
        <div id="paginator" class="calendar-view"></div>
        <div class="calendar-custom">
            <button class="custom-today">Today</button>
            <button class="custom-add-event js-event-create"></button>
        </div>
    </div>
    <div class="col-12 calendar-widget-content">
        <?php Pjax::begin(['id' => 'pjax-form-events-show', 'enablePushState' => false]); ?>
        
            <?php $form = ActiveForm::begin(['id' => 'events-search-form', 'options' => ['data-pjax' => 'pjax-form-events-show']]); ?>
                <?= $form->field($searchModelU, 'filterDate')->hiddenInput()->label(false) ?>
            <?php ActiveForm::end(); ?>

            <?php if ($dataProviderU->getModels()) { ?>
                <?php foreach ($events as $key => $event) { ?>
                    <div class="row no-gutters event-block-under-calendar align-items-center">
                        <div class="col-12 col-md-5 date-create"><?= $event->getDateStart('Y M d H:i'); ?> - <?= $event->getDateEnd('Y M d H:i'); ?></div>
                        <div class="col-12 col-md-4 event-title"><a href="/"><?= $event->title; ?></a></div>
                        <div class="col-12 col-md-3 event-join-btn">
                            <a class="action-btn join-btn js-show-event" data-id="<?= $event->id; ?>">View</a>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="col-12 col-md-12 calendar-widget-content-courses">
                    Sorry But events not found
                </div>
            <?php } ?>
        <?php Pjax::end(); ?>
    </div>
</div>
<script>
    $(function(){

        $('#js-events-list-block').on('click', '.js-show-event', function () {
            var id = $(this).data('id')
            $.dialog({
                id: 'eventShowModal',
                title: 'Show event',
                content: 'url:<?= Url::to(['/university/university/event-show']) ?>/'+id,
            });
        })
    })
</script>
    