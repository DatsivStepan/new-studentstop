<?php
use yii\helpers\Url,
    yii\widgets\Pjax,
    yii\helpers\Html,
    yii\widgets\ActiveForm,
    frontend\models\PostSearch;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="row no-gutters">
    <div class="col-12 col-sm-12 col-md-7 media-border">
        <div class="row no-gutters d-lg-none">
            <div class="col-auto mobile-post-user-img">
                <div class="user-avatar" style="background: url(<?= $model->getAuthorAvatar(); ?>) center/cover;"></div>
            </div>
            <div class="col-auto post-data">
                <div class="post-user-name">
                    <?= $model->getAuthorName(); ?>
                </div>
                <div class="post-datetime">
                    <?= $model->getDateCreate(); ?>
                </div>
            </div>
        </div>
        <p><?= $model->getContent(); ?></p>
        <div class="row no-gutters media-custom-style my-scroll justify-content-center">
            <?php if ($files = $model->getFilesWithLimit(4)) { ?>
                <?php foreach ($files as $key => $file) { ?>
                    <div class="media-item col-12 align-self-center">
                        <?php switch($file->type) {
                            case 'image': ?>
                              <img src="/<?= $file->file_src; ?>" class="align-self-center"/>
                          <?php  break;
                            case 'video': ?>
                              <video width="100%" controls class="align-self-center" controlsList="nodownload">
                                <source src="/<?= $file->file_src; ?>" type="video/mp4">
                                Your browser does not support the video tag.
                              </video>
                          <?php break; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>

    </div>
    <div class="col-12 col-sm-12 col-md-5 custom-style-coments">
        <?php Pjax::begin(['id' => 'pjax-form-comment', 'enablePushState' => false]); ?>
            <div class="row no-gutters">
                <div class="col-12 post-author-user d-none d-lg-flex">
                    <div class="row no-gutters ">
                        <div class="col-auto">
                            <div class="user-avatar" style="background: url(<?= $model->getAuthorAvatar(); ?>) center/cover;"></div>
                        </div>
                        <div class="col-auto post-data">
                            <div class="post-user-name">
                                <?= $model->getAuthorName(); ?>
                            </div>
                            <div class="post-datetime">
                                <?= $model->getDateCreate(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 comment-content custom-coment-conteiner-height my-scroll">
                    <?php if ($comments = $model->commentModel) { ?>
                        <?php foreach ($comments as $comment) { ?>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto comment-user-avatar">
                                    <div class="comment-avatar" style="background: url(<?= $comment->getAuthorAvatar(); ?>) center/cover;"></div>
                                </div>
                                <div class="col-auto post-data">
                                    <div class="post-user-name">
                                        <?= $comment->getAuthorName(); ?>
                                    </div>
                                </div>
                                <div class="col-auto post-data ml-auto">
                                    <div class="post-datetime">
                                        <?= $comment->getDateCreate(); ?>
                                    </div>
                                </div>
                                <div class="col-12 post-content">
                                    <?= $comment->getContent(); ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="col-12 comment-form comment-form-created">
                    <div class="comment-avatar" style="background: url(http://via.placeholder.com/150x150) center/cover;"></div>
                    <?php $form = ActiveForm::begin(['id' => 'create-form-item', 'options' => ['data-pjax' => 'pjax-form-comment']]); ?>
                        <?= $form->field($modelNewsComment, 'content')->textarea(['class' => 'w-100 my-scroll', 'placeholder' => 'Write your comment'])->label(false) ?>
                        <?= Html::submitButton('', ['class' => 'create-post write-comment icons-hover', 'style' => 'cursor:pointer;']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        <?php Pjax::end();  ?>
    </div>
</div>
<script>
    $(document).ready(function() {
     
    });
</script>