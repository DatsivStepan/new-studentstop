<?php
use yii\helpers\Url,
    yii\widgets\Pjax,
    yii\helpers\Html,
    yii\widgets\ActiveForm,
    frontend\models\PostSearch,
    kartik\checkbox\CheckboxX,
    common\models\PostFiles;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$posts = $dataProvider->getModels();
?>
<?= $this->render('_setting_tabs', [
    'model' => $model
]); ?>
<div class="row no-gutters post-view-container" id="university-setting-post_block">
    <?php Pjax::begin(['id' => 'pjax-univesity-setting-post', 'enablePushState' => false]); ?>

        <?php $form = ActiveForm::begin(['id' => 'university-univesity-setting-post', 'options' => ['data-pjax' => 'pjax-univesity-setting-post']]); ?>
        <?php ActiveForm::end(); ?>

        <div class="row">
            <div class="col-sm-6">
                <h2 class="text-center">Add post to slider</h2>
                <div class="view-post-items">
                    <?php if ($posts) { ?>
                        <?php foreach ($posts as $key => $post) { ?>
                            <div class="col-12 view-post-item" id="post-block-list-<?= $post->id; ?>">
                                <div class="row no-gutters justify-content-between">
                                    <?= CheckboxX::widget([
                                        'name' => 'allAccount',
                                        'value' => $post->inSlider,
                                        'options' => [
                                            'style' => 'margin-top:30px;',
                                            'class' => 'change-is-slider',
                                            'data-post_id' => $post->id
                                        ],
                                        'pluginOptions' => [
                                            'size' => 'lg',
                                            'threeState' => false,
                                        ],
                                    ]); ?>
                                    <div class="col-auto m-w-60">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto post-user-avatar">
                                                <div class="user-avatar" style="background: url(<?= $post->getAuthorAvatar(); ?>) center/cover;"></div>
                                            </div>
                                            <div class="col-auto post-data">
                                                <div class="post-user-name">
                                                    <?= Html::a($post->getAuthorName(), $post->getAuthorLink()) ; ?>
                                                </div>
                                                <div class="post-datetime">
                                                    <?= $post->getDateCreate(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto stats m-w-40">
                                        <div class="row no-gutters">
                                            <div class="col-auto likes icons-hover js-post-like-action <?= $post->getIfUserLikes() ? 'active' : ''; ?>" data-post_id="<?= $post->id; ?>" title="Like">
                                                <?= $post->getLikesCount(); ?>
                                            </div>
                                            <?php $count = $post->getCommentsCount(); ?>
                                            <div class="col-auto comments <?= $count ? 'active' : ''?>">
                                                <?= $count; ?>
                                            </div>
                                            <?php $countV = $post->getViewsCount(); ?>
                                            <div class="col-auto views <?= $countV ? 'active' : ''?>">
                                                <?= $countV; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-gutters js-show-post-info" data-id="<?= $post->id; ?>" style="cursor:pointer;">
                                    <div class="col-12 post-content">
                                        <?= $post->getContent(); ?>
                                    </div>
                                    <div class="col-12">
                                        <div class="row post-files">
                                            <?php 
                                            $gridFiles = [
                                              1 => [0 => 12],
                                              2 => [0 => 6, 1 => 6],
                                              3 => [0 => 12, 1 => 6, 2 => 6],
                                              4 => [0 => 12, 1 => 4, 2 => 4, 3 => 4],
                                            ];

                                            if ($files = $post->getFilesWithLimit(4)) { 
                                              $countFiles = count($files);
                                            ?>
                                              <?php foreach ($files as $key => $file) { ?>
                                                  <div class="well col-<?= $gridFiles[$countFiles][$key]?>">
                                                        <div class="media-item">
                                                            <?php switch($file->type) {
                                                                case 'image': ?>
                                                                  <img src="/<?= $file->file_src; ?>" class="align-self-center"/>
                                                              <?php  break;
                                                                case 'video': ?>
                                                                  <video width="100%" controls class="align-self-center" controlsList="nodownload">
                                                                    <source src="/<?= $file->file_src; ?>" type="video/mp4">
                                                                    Your browser does not support the video tag.
                                                                  </video>
                                                              <?php break; ?>
                                                            <?php } ?>
                                                        </div>
                                                  </div>
                                              <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="view-post-items">
                            <div class="col-12 view-post-item" id="post-block-list-12">
                                <p class="text-center">Not result</p>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
            <div class="col-sm-6">
                <h2 class="text-center">Here is your slider</h2>
                <div class="view-post-items">
                    <?php if ($files = PostFiles::getAllForSlider($model->id)) { ?>
                            <?php $countF = count($files); ?>
                            <div id="carouselExampleIndicators" class="carousel slide carousel-univer-classes" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <?php for ($i=0; $i < $countF; $i++) { ?>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i; ?>" class="<?= $i == 0 ? 'active' : ''; ?>"></li>
                                    <?php } ?>
                                </ol>
                                <div class="carousel-inner" style="height: 300px" style="">
                                    <?php foreach ($files as $key => $file) { ?>
                                        <div class="carousel-item <?= $key == 0 ? 'active' : ''; ?>" style="height: 100%;position:relative;">
                                            <img class="d-block w-100" src="/<?= $file->file_src; ?>" alt="Slide <?= $key + 1; ?>" style="object-fit: cover;height:100%;">
                                            <?php if ($text = $file->postModel->content) { ?>
                                                <div style="background-image: linear-gradient(white, #72638c, #4a2f77, #221538, black);position: absolute;opacity:0.4;left:0px;top:0px;width:100%;height:100%;"></div>
                                                <div style="position: absolute;top:70%;left:0px;width:100%;height:30%;color:white;padding:0 40px;"><?= substr($text, 0, 150).'...'; ?></div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php if ($countF > 1) { ?>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                      <span class="sr-only">Next</span>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div style="background: #f6f7fb;border-radius: 6px;padding: 0 10px 10px 10px;margin-top: 10px;">
                                <p class="text-center">Not result</p>
                            </div>
                        <?php } ?>
                </div>
            </div>
        </div>
      <?php Pjax::end(); ?>
    <!-- / view post -->
  </div>
<script>
    $(function() {
        $('#university-setting-post_block').on('change', '.change-is-slider', function(){
            $.ajax({
                method: 'post',
                url: '<?= Url::to(['/university/university/is-slider-posts']); ?>/?id='+$(this).data('post_id') + '&val='+$(this).val(),
                dataType: 'json'
            }).done(function (response) {
                if (response.status) {
                    $('#university-univesity-setting-post').submit();
                }
            });
        })
        
    })
</script>