<?php

use yii\helpers\Html,
    yii\helpers\Url,
    yii\widgets\Pjax,
    yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\profile\models\SearchModels */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Photos & Videos');
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::to(['/css/plugins/dropzone.css']));
$this->registerJsFile(Url::to(['/js/plugins/dropzone.js']));
$tabActive = Yii::$app->request->get('tab');
?>
<style>
    .removePhoto {
        z-index: 9999;
        position: absolute;
        right: -10px;
        top: -10px;
    }
    .js-block-image.active {
        opacity: 0.5;
    }
</style>
<div id="post-files-index">
    
    <?php Pjax::begin(['id' => 'pjax-form-files', 'enablePushState' => false]); ?>

        <div class="drag-and-drop user_photos" action="/site/upload-photo?type=profile" class="">
            <div class="dz-message needsclick">
                <h4>Drop files here or click to upload.</h4>
            </div>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'form', 'options' => ['data-pjax' => 'pjax-form-files']]); ?>
            <?= Html::hiddenInput('uploadsfiles', '', [])?>
            <?= Html::submitButton(Yii::t('profile', 'add'), ['name' => 'save-upload-files', 'class' => 'btn btn-info']) ?>
        <?php ActiveForm::end(); ?>

        <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
            <script>
                $modal.addPostPhotoModal.close();
                parent.location.reload();
            </script>
        <?php } ?>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link <?= $tabActive != 'video' ? 'active' : ''; ?>" data-toggle="tab" href="#image">Images</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= $tabActive == 'video' ? 'active' : ''; ?>" data-toggle="tab" href="#video">Videos</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane container <?= $tabActive != 'video' ? 'active' : 'fade'; ?>" id="image"  data-type="image">
                <?php if ($images) { ?>
                    <?php foreach ($images as $image) { ?>
                        <div style="display:inline-block;" class="js-block-image" data-id="<?= $image->id; ?>">
                            <img src="/<?= $image->file_src; ?>" class="js-add-select-file" style="width:150px;height:150px;">
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <p class="text-center">You dont have any photo</p>
                <?php } ?>
            </div>
            
            <div class="tab-pane container <?= $tabActive == 'video' ? 'active' : 'fade'; ?>" id="video">
                <?php if ($videos) { ?>
                    <?php foreach ($videos as $video) { ?>
                        <div style="display:inline-block;" class="js-block-image" data-id="<?= $image->id; ?>" data-type="video">
                            <video width="150" controls>
                                <source src="/<?= $video->file_src; ?>">
                            </video>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <p class="text-center"> You dont have any videos</p>
                <?php } ?>
            </div>
        </div>
    
        <?php $form = ActiveForm::begin(['id' => 'form-upload-post', 'options' => ['data-pjax' => 'pjax-form-files']]); ?>
            <?= Html::hiddenInput('uploadsfiles', '', [])?>
            <?= Html::a(Yii::t('profile', 'Save'), null, ['class' => 'btn btn-info js-create-post-files']) ?>
        <?php ActiveForm::end(); ?>

        
        <script>
            $(function(){
                var photosArray = {};
                if($('.user_photos').length){

                    var profDropzone = new Dropzone($('.user_photos')[0], { // Make the whole body a dropzone
                        //maxFilesize: 50,
                        dictDefaultMessage: 'message',
                        acceptedFiles:'image/*,video/*',
                    });

                    profDropzone.on("complete", function(response) {
                        if (response.status == 'success') {
                            var fileType = response.type;

                            if(typeof photosArray[response.xhr.response] == 'undefined'){
                                photosArray[response.xhr.response] = {};
                            }

                            photosArray[response.xhr.response] = fileType;
                            $(response.previewElement).prepend('<i class="fa fa-times removePhoto fa-2x" title="delete" data-name="'+response.name+'"></i>');
                            $('#form [name=uploadsfiles]').val(JSON.stringify(photosArray));
                        }
                    });
                    profDropzone.on("removedfile", function(response) {
                        var removeItem = response.xhr.response;
                        delete(photosArray[removeItem]);
                        $('#form [name=uploadsfiles]').val(JSON.stringify(photosArray));
                    });

                    $('#user-files-index').on('click','.removePhoto',function(){
                        var shos = $(this).parent();

                        var name_photo = $(this).data('name');
                        $.each( profDropzone.files, function( key, value ) {
                            if(value.name == name_photo){
                                profDropzone.removeFile(profDropzone.files[key]);
                            }
                        });
                    });
                }
            })
        </script>
        
        
    <?php Pjax::end(); ?>

</div>
<script>
    $(function() {
        var uploadsFiles = {'image': [], 'video' : []};
        $('#post-files-index').on('click', '.js-add-select-file', function() {
            var thisE = $(this);
            var elemBlock = $(this).closest('.js-block-image')
            var idFile = elemBlock.data('id')
            var typeFile = elemBlock.data('type')
            if (elemBlock.hasClass('active')) {
                elemBlock.removeClass('active');
                if (typeFile == 'video') {
                    uploadsFiles.video.splice(uploadsFiles.video.indexOf(idFile), 1)
                } else {
                    uploadsFiles.image.splice(uploadsFiles.image.indexOf(idFile), 1)
                }
                $('#form-upload-post [name=uploadsfiles]').val(JSON.stringify(uploadsFiles));
            } else {
                if (typeFile == 'video') {
                    uploadsFiles.video.push(idFile)
                } else {
                    uploadsFiles.image.push(idFile)
                }
                $('#form-upload-post [name=uploadsfiles]').val(JSON.stringify(uploadsFiles));
                elemBlock.addClass('active');
            }
        })
        $('#post-files-index').on('click', '.js-create-post-files', function(e) {
            e.preventDefault();
            if (uploadsFiles.image.length || uploadsFiles.video.length) {
                $('#form-upload-post').submit()
            } else {
                swal({
                    title: "Error",
                    text: "You did not select any file",
                    html: true,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  });
            }
        })
        
    })
</script>