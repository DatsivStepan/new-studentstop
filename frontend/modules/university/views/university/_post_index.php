<?php
use yii\helpers\Url,
    yii\widgets\Pjax,
    yii\helpers\Html,
    yii\widgets\ActiveForm,
    frontend\models\PostSearch,
    common\models\Post;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$posts = $dataProvider->getModels();
?>
  <?php Pjax::begin(['id' => 'pjax-form', 'enablePushState' => false]); ?>

  <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
  <script>
  </script>
  <?php } ?>
<div id="js-post-block">
  <div class="new-post">
    <div class="row justify-content-end no-gutters align-items-center panel-btns">
      <div class="col-auto post-btn"><span class="m-device-hide">Add</span> Post</div>
      <div class="col-auto photo-btn js-uploads-u-photo"><span class="m-device-hide">Add</span> Photo</div>
      <div class="col-auto video-btn js-uploads-u-video"><span class="m-device-hide">Add</span> Video</div>
      <div class="col-auto event-btn js-event-create"><span class="m-device-hide">Add</span> Event</div>
    </div>

    <div class="row no-gutters">
        <div class="col-auto post-user" style="position:relative;">
            <?php $myAvatar = Yii::$app->user->identity->getAvatar(); ?>
            <?php if ($university->id_owner == Yii::$app->user->id) { ?>
                <div class="post-sender-photo">
                    <img src="<?= $myAvatar; ?>" style="width:50px;cursor:pointer;" class="js-change-send-type rounded-circle">
                </div>
                <div style="position:absolute;left:-1px;top:-1px;background-color: #f6f7fb;border:1px solid #d0d2d5;" class="js-image-change-block hide">
                    <img src="<?= $myAvatar; ?>" data-href="<?= $myAvatar; ?>" data-type="<?= Post::TYPE_USER_UNIVERSITY; ?>" style="width:50px;cursor:pointer;" class="rounded-circle">
                    <img src="<?= $university->getImages(); ?>" data-href="<?= $university->getImages(); ?>" data-type="<?= Post::TYPE_UNIVERSITY; ?>" style="width:50px;cursor:pointer;" class="rounded-circle">
                </div>
            <?php } else { ?>
                <img src="<?= Yii::$app->user->identity->getAvatar(); ?>" style="width:50px;cursor:pointer;" class="rounded-circle">
            <?php } ?>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'create-form-item', 'options' => ['data-pjax' => 'pjax-form']]); ?>
          <div class="post-textarea">
            <?= $form->field($modelNews, 'type')->hiddenInput(['value' => Post::TYPE_USER_UNIVERSITY])->label(false) ?>
            <?= $form->field($modelNews, 'images')->hiddenInput()->label(false) ?>
            <?= $form->field($modelNews, 'content')->textarea(['class' => 'w-100 my-scroll', 'placeholder' => 'Write your news'])->label(false) ?>
            <i class="js-upload-file-button attach-btn"></i>
            <?= Html::submitButton(null, ['class' => 'create-post']) ?>
          </div>
          <div id="js-preview-post-image"></div>
        <?php ActiveForm::end(); ?>
    </div>

  </div>
  <div class="row no-gutters post-view-container">
    <div class="col-sm-12">
        <?php $form = ActiveForm::begin(['id' => 'form-filter', 'options' => ['data-pjax' => 'pjax-form']]); ?>
            <div class="d-none">
                <?= $form->field($searchModel, 'filter')->dropDownList(PostSearch::$filterArray); ?>
            </div>
        <?php ActiveForm::end(); ?>

        <ul class="nav nav-tabs js-tabs-list" role="tablist">
            <?php $k = 0; ?>
            <?php foreach (PostSearch::$filterArray as $filterId => $filterName) { ?>
                <?php $class = ''; 
                if (!$searchModel->filter && ($k == 0)) { 
                    $class = 'active';
                } else {  
                    $class = $searchModel->filter == $filterId ? 'active' : ''; 
                } ?>
                <li class="nav-item" data-id="<?= $filterId; ?>">
                    <?= Html::a($filterName, null, [
                        'class' => 'nav-link ' . $class,
                    ])?>
                </li>
                <?php $k++;?>
            <?php }?>
        </ul>

      <div class="view-post-items">
        <?php if ($posts) { ?>
            <?php foreach ($posts as $key => $post) { ?>
                <div class="col-12 view-post-item" id="post-block-list-<?= $post->id; ?>">
                    <div class="row no-gutters justify-content-between">
                        <div class="col-auto m-w-60">
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto post-user-avatar">
                                    <div class="user-avatar" style="background: url(<?= $post->type == Post::TYPE_UNIVERSITY ? ($post->universityModel ? $post->universityModel->getImages() : 'univer') : $post->getAuthorAvatar(); ?>) center/cover;"></div>
                                </div>
                                <div class="col-auto post-data">
                                    <div class="post-user-name">
                                        <?= $post->type == Post::TYPE_UNIVERSITY ? Html::a(($post->universityModel ? $post->universityModel->getName() : 'univer'), null) : Html::a($post->getAuthorName(), $post->getAuthorLink()) ; ?>
                                    </div>
                                    <div class="post-datetime">
                                        <?= $post->getDateCreate(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto stats m-w-40">
                            <div class="row no-gutters">
                                <div class="col-auto likes icons-hover js-post-like-action <?= $post->getIfUserLikes() ? 'active' : ''; ?>" data-post_id="<?= $post->id; ?>" title="Like">
                                    <?= $post->getLikesCount(); ?>
                                </div>
                                <?php $count = $post->getCommentsCount(); ?>
                                <div class="col-auto comments <?= $count ? 'active' : ''?>">
                                    <?= $count; ?>
                                </div>
                                <?php $countV = $post->getViewsCount(); ?>
                                <div class="col-auto views <?= $countV ? 'active' : ''?>">
                                    <?= $countV; ?>
                                </div>
                                
                                <?php if ($post->user_id == Yii::$app->user->id) { ?>
                                    <div class="col-auto js-update-post update" data-id="<?= $post->id; ?>"></div>
                                    <div class="col-auto js-delete-post delete" data-id="<?= $post->id; ?>"></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters js-show-post-info" data-id="<?= $post->id; ?>" style="cursor:pointer;">
                        <div class="col-12 post-content">
                            <?= substr($post->getContent(), 0, 150).'...'; ?>
                        </div>
                        <div class="col-12">
                            <div class="row post-files">
                                <?php 
                                $gridFiles = [
                                  1 => [0 => 12],
                                  2 => [0 => 6, 1 => 6],
                                  3 => [0 => 12, 1 => 6, 2 => 6],
                                  4 => [0 => 12, 1 => 4, 2 => 4, 3 => 4],
                                ];

                                if ($files = $post->getFilesWithLimit(4)) { 
                                  $countFiles = count($files);
                                ?>
                                  <?php foreach ($files as $key => $file) { ?>
                                      <div class="well col-<?= $gridFiles[$countFiles][$key]?>">
                                            <div class="media-item">
                                                <?php switch($file->type) {
                                                    case 'image': ?>
                                                      <img src="/<?= $file->file_src; ?>" class="align-self-center"/>
                                                  <?php  break;
                                                    case 'video': ?>
                                                      <video width="100%" controls class="align-self-center" controlsList="nodownload">
                                                        <source src="/<?= $file->file_src; ?>" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                      </video>
                                                  <?php break; ?>
                                                <?php } ?>
                                            </div>
                                      </div>
                                  <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-12 comment-form">
                            <?php $form = ActiveForm::begin(['options' => ['class' => 'create-form-comment']]); ?>
                                <div class="update-data">
                                    <h3 class="inner"> 
                                       <?= Html::img('/images/loader.gif', ['height' => 25]) ?>
                                    </h3>
                                </div>
                                <?= $form->field($modelNewsComment, 'post_id')->hiddenInput(['value' => $post->id])->label(false) ?>
                                <?= $form->field($modelNewsComment, 'content')->textarea(['class' => 'w-100 my-scroll', 'placeholder' => 'Write your comment'])->label(false) ?>
                                <?= Html::submitButton('', ['class' => 'create-post write-comment icons-hover js-send-comment', 'style' => 'cursor:pointer;']) ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <div class="col-12 comment-content">
                            <?php if ($comment = $post->getLastComment()) { ?>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto comment-user-avatar">
                                        <div class="comment-avatar" style="background: url(<?= $comment->getAuthorAvatar(); ?>) center/cover;"></div>
                                    </div>
                                    <div class="col-auto post-data">
                                        <div class="post-user-name">
                                            <?= Html::a($comment->getAuthorName(), $comment->getAuthorLink()) ; ?>
                                        </div>
                                    </div>
                                    <div class="col-auto post-data ml-auto">
                                        <div class="post-datetime">
                                            <?= $comment->getDateCreate(); ?>
                                        </div>
                                    </div>
                                    <div class="col-12 post-content">
                                        <?= $comment->getContent(); ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } else { ?>
            <div class="view-post-items">
                <div class="col-12 view-post-item" id="post-block-list-12">
                    <p class="text-center">Not result</p>
                </div>
            </div>
        <?php } ?>
      </div>

    </div>
    <!-- / view post -->
  </div>
</div>
    <div style="display:none;">
        <div class="row-table-style">
            <div class="table table-striped" class="files" id="previews">
                <div id="template" class="file-row">

                </div>
            </div>
        </div>
    </div>

  <script>
    $(function () {
        var photosArray = [];

        $('#js-post-block').on('click', '.js-change-send-type', function () {
            $('#js-post-block .js-image-change-block').toggleClass('hide');
        });

        $('#js-post-block').on('click', '.js-image-change-block img', function () {
            $('.js-change-send-type').attr('src', $(this).data('href'));
            $('#js-post-block #post-type').val($(this).data('type'));
            $('#js-post-block .js-image-change-block').addClass('hide');
        })

        $('#js-post-block').on('click', '.js-post-like-action', function () {
            var thisElement = $(this)
            $.ajax({
                method: 'post',
                url: '<?= Url::to(['/university/university/like-post']); ?>/' + thisElement.data('post_id'),
                dataType: 'json'
            }).done(function (response) {
                if (response.status) {
                    thisElement.text(response.data)
                }
            })
        });

//        $('#js-post-block').on('click', '.js-send-comment', function () {
//            var thisElement = $(this);
//            var form = thisElement.closest('form');
//            form.submit();
//            form.find('.update-data').show();
//            setTimeout(function () {
//                form.find('.update-data').hide();
//            }, 4000);
//            
//            $(this).closest('form').submit();
//        });

        $('#js-post-block').on('click', ".js-send-comment", function(e) {
            e.preventDefault();
            var form = $(this).closest('form');
            var text = form.find('#postcomment-content').val();
            var id = form.find('#postcomment-post_id').val();
            $.ajax({
                method: 'post',
                url: '<?= Url::to(['/university/university/save-comment-post']); ?>/' + id,
                data: {text: text},
                dataType: 'json',
                beforeSend: function( xhr ) {
                    form.find('.update-data').show();
                }
            }).done(function (response) {
                if (response.status && response.data) {
                    form.find('#postcomment-content').val('')
                    form.find('.update-data').hide();
                    var comment = response.data;
                    form.closest('#post-block-list-'+id).find(' .comment-content').prepend(
                            '<div class="row no-gutters align-items-center">' +
                                '<div class="col-auto comment-user-avatar">' +
                                    '<div class="comment-avatar" style="background: url("' + comment.image + '") center/cover;"></div>' +
                                '</div>' +
                                '<div class="col-auto post-data">' +
                                    '<div class="post-user-name">' +
                                        comment.link + 
                                    '</div>' +
                                '</div>' +
                                '<div class="col-auto post-data ml-auto">' +
                                    '<div class="post-datetime">' + 
                                        comment.date + 
                                    '</div>' +
                                '</div>' +
                                '<div class="col-12 post-content">' +
                                        comment.content + 
                                '</div>' +
                            '</div>'
                    );
                }
            })
        });
        
        $('#js-post-block').on('click', '.js-uploads-u-photo', function () {
            $.dialog({
                id: 'addPostPhotoModal',
                content: 'url:<?= Url::to(['/university/university/add-post-photo']) ?>',
                columnClass: 'comments-popap-maine-container col-sm-12'
            });
        })

        $('#js-post-block').on('click', '.js-delete-post', function () {
            var id = $(this).data('id')
            swal({
                title: "Are you sure?",
                text: "delete post",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        method: 'post',
                        url: '<?= Url::to(['/university/university/delete-post']); ?>/' + id,
                        dataType: 'json'
                    }).done(function (response) {
                        if (response.status) {
                            $('#post-block-list-' + id).remove();
                            swal("Deleted!", {
                                icon: "success",
                            });
                        }
                    })
                }
            });
        })

        $('#js-post-block').on('click', '.js-uploads-u-video', function () {
            $.dialog({
                id: 'addPostPhotoModal',
                content: 'url:<?= Url::to(['/university/university/add-post-photo']) ?>?tab=video',
                columnClass: 'comments-popap-maine-container col-sm-12'
            });
        })

        $('#js-post-block').on('click', '.js-show-post-info', function () {
            $.dialog({
                id: 'showPostModal',
                content: 'url:<?= Url::to(['/university/university/show-post']) ?>/' + $(this).data('id'),
                columnClass: 'comments-popap-maine-container col-sm-12',
                 onContentReady: function () {
                    if($('#create-form-item')){
                        $('.comment-form-created').width($('#pjax-form-comment').width()- 37) ;
                        
                            var cometForm = ($('#pjax-form-comment').width()) - 37;
                            $('.comment-form-created').width(cometForm);
                      
                    }
                }
                
            });
        })

        function showEventCreate(){
            $.confirm({
                id: 'eventCreateModal',
                title: 'Create event',
                content: 'url:<?= Url::to(['/university/university/event-create']) ?>?university=<?= $university->id;?>',
                columnClass: 'modal-block col-sm-12'
            });
        }

        $('#js-events-list-block').on('click', '.js-event-create', function () {
            showEventCreate();
        });

        $('#js-post-block').on('click', '.js-event-create', function () {
            showEventCreate();
        })

        $('#js-post-block').on('click', '.js-update-post', function () {
            var id = $(this).data('id')
            $.confirm({
                id: 'postUpdateModal',
                title: 'Update post',
                content: 'url:<?= Url::to(['/university/university/update-post']) ?>/' + id,
                columnClass: 'modal-block col-sm-12'
            });
        })

        $('#js-preview-post-image').on('click', '.js-remove-photo', function () {
            $(this).parent().remove();
            $.each(photosArray, function(keyF, valueA) {
                delete photosArray[keyF];
            });
            $('#post-images').val(JSON.stringify(photosArray));
        })

        $('#js-post-block').on('click', '.js-tabs-list li', function () {
            var filterId = $(this).data('id')
            $('#postsearch-filter option[value="' + filterId + '"]').prop('selected', true)
            $('#form-filter').submit()
        })

        if ($('.js-upload-file-button').length) {
          var previewNode = document.querySelector("#template");
          previewNode.id = "";
          var previewTemplate = previewNode.parentNode.innerHTML;
          previewNode.parentNode.removeChild(previewNode);

          var myDropzone = new Dropzone('#js-preview-post-image', {
            url: "/site/upload-photo",
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".js-upload-file-button",
            acceptedFiles: 'image/*,video/*',
            uploadMultiple: false,
            maxFiles: 1,
          });

          myDropzone.on("maxfilesexceeded", function (file) {
            myDropzone.removeAllFiles();
            myDropzone.addFile(file);
          });

          myDropzone.on("complete", function (response) {
            if (response.status == 'success') {

              photosArray.push({
                file: response.xhr.response,
                type: response.type
              });

              $('#post-images').val(JSON.stringify(photosArray));
              $('#js-preview-post-image').append(
                $('<div/>', {
                  class: 'images-block img-thumbnail',
                  style: 'background-image:url(/' + response.xhr.response + ')',
                }).append(
                  $('<span/>', {
                    class: 'glyphicon glyphicon-remove js-remove-photo',
                    style: 'position: absolute;right: -7px;top: -7px;cursor:pointer;',
                    'data-href': response.xhr.response
                  })
                )
              )
            }
          });

          myDropzone.on("removedfile", function (response) {
            if (response.xhr != null) {
              //deleteFile(response.xhr.response);
            }
          });

        }

    })
  </script>
  <?php Pjax::end();  ?>