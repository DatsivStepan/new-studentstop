<?php

use yii\helpers\Url,
    yii\widgets\Pjax,
    yii\helpers\Html,
    yii\widgets\ActiveForm,
    frontend\models\PostSearch,
    kartik\rating\StarRating,
    common\models\User;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$filterArray = common\models\University::$filterTop;
$topList = common\models\University::getTopList($university->id);
?>
<style>
    .top-university-containter .rating-container{
        display: inline-block !important;
    }
    .top-university-containter .rating-container .rating-stars .star > i {
        font-size:12px !important;
    }
</style>
<div class="row no-gutters calendar-widget show-btn-event top-university-containter" id="js-top-list-block">
    <div class="col-sm-12">
        <ul class="nav nav-tabs js-tabs-top-list" role="tablist">
            <li class="nav-item"><a class="nav-link"><b>TOP 8:</b></a></li>
            <?php $k = 0; ?>
            <?php foreach ($filterArray as $filterId => $filterName) { ?>
                <?php $class = ''; ?>
                <li class="nav-item" data-id="<?= $filterId; ?>">
                    <?= Html::a($filterName, null, [
                        'class' => 'nav-link ' . ($filterId == 1 ? 'active' : ''),
                        'data-toggle' => "tab",
                        'href' => "#tabTop" . $filterId,
                        'role' => "tab",
                        'aria-selected' => "false"
                    ])?>
                </li>
                <?php $k++;?>
            <?php }?>
        </ul>
        <div class="tab-content my-scroll clearfix">
            <?php if ($topList) { ?>
                <?php foreach ($topList as $listId => $list) { ?>
                    <div class="tab-pane my-scroll <?= $listId == 1 ? 'active' : ''; ?>" id="tabTop<?= $listId; ?>" role="tabpanel">
                        <?php 
                            switch ($listId) {
                                case 1: 
                                case 2: ?>
                                    <?php if ($list) { ?>
                                        <table class="table">
                                            <?php foreach ($list as $key => $user) { ?>
                                                <tr>
                                                    <td><img style="width:50px;" src="<?= $user->getAvatar(); ?>"></td>
                                                    <td><?= Html::a($user->getNameAndSurname(), $user->getProfileLink()); ?></td>
                                                    <td>
                                                        Rank: <?= ($key + 1) . $user->getRankCount(false); ?> 
                                                        <?= StarRating::widget([
                                                            'name' => 'rating_u_'. $user->id,
                                                            'value' => ($listId == 1) ? $user->getRating() : $user->getProfessorRating(),
                                                            'disabled' => true,
                                                            'pluginOptions' => [
                                                                'size' => 'xs',
                                                                'min' => 0,
                                                                'max' => 5
                                                            ],
                                                        ]); ?>
                                                    </td>
                                                    <td>
                                                        <?= Html::button('Mail to user', ['class' => 'js-mail-to-user', 'data-myId' => Yii::$app->user->id, 'data-user_id' => $user->id])?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    <?php } else { ?>   
                                        <?php if ($listId == 1) { ?>
                                            <p class="text-center">NO students</p>
                                        <?php } else { ?>
                                            <p class="text-center">NO Professors</p>
                                        <?php } ?>
                                    <?php } ?>
                                <?php break;
                                case 3: ?>
                                    <?php if ($list) { ?>
                                        <table class="table">
                                            <?php foreach ($list as $key => $major) { ?>
                                                <tr>
                                                    <td><img style="width:50px;" src="<?= $major->getImages(); ?>"></td>
                                                    <td><?= Html::a($major->getName(), $major->getLink()); ?></td>
                                                    <td>
                                                        Rank:<?= ($key + 1) . $major->getRank(); ?> 
                                                        <?= StarRating::widget([
                                                            'name' => 'rating_m_'. $major->id,
                                                            'value' => $major->getRating(),
                                                            'disabled' => true,
                                                            'pluginOptions' => [
                                                                'size' => 'xs',
                                                                'min' => 0,
                                                                'max' => 5
                                                            ],
                                                        ]); ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    <?php } else { ?>
                                        <p class="text-center">NO Major</p>
                                    <?php } ?>
                                <?php break;
                                case 4: ?>
                                    <?php if ($list) { ?>
                                        <table class="table">
                                            <?php foreach ($list as $key => $university) { ?>
                                                <tr>
                                                    <td><img style="width:50px;" src="<?= $university->getImages(); ?>"></td>
                                                    <td><?= Html::a($university->getName(), null); ?></td>
                                                    <td>
                                                        Rank:<?= ($key + 1) . $university->getRate(); ?> 
                                                        <?= StarRating::widget([
                                                            'name' => 'rating_m_'. $university->id,
                                                            'value' => $university->getRating(),
                                                            'disabled' => true,
                                                            'pluginOptions' => [
                                                                'size' => 'xs',
                                                                'min' => 0,
                                                                'max' => 5
                                                            ],
                                                        ]); ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    <?php } else { ?>
                                        <p class="text-center">NO University</p>
                                    <?php } ?>
                                <?php break;
                            }
                        ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
