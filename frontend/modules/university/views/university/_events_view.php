<?php
use yii\helpers\Url,
    yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row" style="margin-left: 15px;margin-right: 15px;">
    <div class="col-6">
        <img src="<?= $model->getImages(); ?>" class="js-preview-events-image img-thumbnail"><br>
    </div>
    <div class="col-6">
        <p class="text-center"><?= $model->getDateStart(); ?> - <?= $model->getDateEnd(); ?></p>
        <h2><?= $model->title; ?></h2>
        <p><?= $model->getContent(); ?></p>
    </div>
</div>