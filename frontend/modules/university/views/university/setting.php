<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\University */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Universities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_setting_tabs', [
    'model' => $model
]); ?>
<div class="row no-gutters">
    <div class="col-sm-12 col-md-12 col-lg-12" style="background: #f6f7fb;border-radius: 6px 6px 0 0;padding: 0;">
        <?= $this->render('_form', [
            'model' => $model
        ]); ?>
    </div>
</div>
      