<?php
use yii\helpers\Url,
    yii\helpers\Html,
    common\models\PostFiles;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php if ($files = PostFiles::getAllForSlider($university->id)) { ?>
    <?php $countF = count($files); ?>
    <div id="carouselExampleIndicators" class="carousel slide carousel-univer-classes" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php for ($i=0; $i < $countF; $i++) { ?>
                <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i; ?>" class="<?= $i == 0 ? 'active' : ''; ?>"></li>
            <?php } ?>
        </ol>
        <div class="carousel-inner" style="height: 300px" style="">
            <?php foreach ($files as $key => $file) { ?>
                <div class="carousel-item <?= $key == 0 ? 'active' : ''; ?>" style="height: 100%;position:relative;">
                    <div style="position: absolute;top:0px;left:0px;width:100%;color:white;padding:0 40px;">TRENDING</div>
                
                    <img class="d-block w-100" src="/<?= $file->file_src; ?>" alt="Slide <?= $key + 1; ?>" style="object-fit: cover;height:100%;">
                    <?php if ($text = $file->postModel->content) { ?>
                        <div style="background-image: linear-gradient(white, #72638c, #4a2f77, #221538, black);position: absolute;opacity:0.4;left:0px;top:0px;width:100%;height:100%;"></div>
                        <div style="position: absolute;top:70%;left:0px;width:100%;height:30%;color:white;padding:0 40px;"><?= substr($text, 0, 150).'...'; ?></div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <?php if ($countF > 1) { ?>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
        <?php } ?>
    </div>
<?php } else { ?>
    <div style="background: #f6f7fb;border-radius: 6px;padding: 0 10px 10px 10px;margin-top: 10px;">
        <p class="text-center">Not result</p>
    </div>
<?php } ?>