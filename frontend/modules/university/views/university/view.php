<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\University */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Universities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row no-gutters">
  <div class="col-6 left-content">
      <?= $this->render('_post_index', [
        'university' => $model,
        'modelNews' => $modelNews,
        'modelNewsComment' => $modelNewsComment,
        'searchModel' => $searchNewsModel,
        'dataProvider' => $dataProviderNews,
      ]); ?>
  </div>
  <div class="col-6 right-content home-carousel">
        <?= $this->render('_slider_block', [
            'university' => $model,
        ]) ?>
        <?= $this->render('_top_block', [
            'university' => $model,
        ]) ?>
      <div class="calendar-block">
        <?= $this->render('_calendar_block', [
            'university' => $model,
            'dataProviderU' => $dataProviderU,
            'searchModelU' => $searchModelU,
        ]) ?>
      </div>
  </div>
</div>
      