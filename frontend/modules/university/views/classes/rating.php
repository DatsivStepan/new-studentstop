<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm,
    kartik\select2\Select2,
    kartik\depdrop\DepDrop,
    yii\widgets\Pjax,
    yii\helpers\Url,
    kartik\rating\StarRating,
    common\models\Followers;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="row no-gutters courses-index left-content">
    <div class="col-12 col-md-6">
        <?php if ($modelUser = $model->professorModel) { ?>
            <div class="row no-gutters courses-personal-info-block">
                <div class="personal-info-left-block">
                    <img src="<?= $modelUser->getAvatar(); ?>" style="width:100%;">
                </div>
                <div class="personal-info-right-block">
                    <h5 class="personal-info-right-block-name"><?= $modelUser->getNameAndSurname(); ?></h5>
                    <div class="personal-info-right-block-rating">
                        <div class="personal-info-right-block-rating-title">
                            Rank: <?= $modelUser->getRankCount(); ?>
                        </div>
                        <div class="custom-stars">
                            <?= StarRating::widget([
                                'name' => 'rating_user',
                                'value' => $modelUser->getRating(),
                                'disabled' => true,
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'min' => 0,
                                    'max' => 5
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div class="personal-info-right-block-danye">
                        <div class="danye-block">
                            <p>Birthday:</p>
                            <span><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->birthday : ''; ?></span>
                        </div>
                        <div class="danye-block">
                            <p>Mobile:</p>
                            <span><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->mobile : ''; ?></span>
                        </div>
                        <div class="danye-block">
                            <p>University: </p>
                            <span><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->getUniversityName() : ''; ?></span>
                        </div>
                        <div class="danye-block">
                            <p>Address:</p>
                            <span><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->address : ''; ?></span>
                        </div>
                        <div class="danye-block">
                            <p>About</p>
                            <span><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->about : ''; ?></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="col-12 col-md-6 right-content home-carousel">
        <?= $this->render('_top', [
            'model' => $model
        ]); ?>
    </div>
</div>

<script>
    $(function(){

        function addRating (status) {
            $.confirm({
                id: 'addRatingModal',
                title: '',
                content: 'url:<?= Url::to(['/university/classes/add-rating']) ?>?id=<?= $model->id; ?>&status=' + status,
                columnClass: 'modal-block '
            });
        }

        $.confirm({
            id: 'choiseClassFollowModal',
            title: 'Your class is over!',
            content: '',
            closeIcon: false,
            backgroundDismiss: false,
            columnClass: 'xl view-block modal-block',
            buttons: {
                ok: {
                    text: 'Graduated',
                    btnClass: 'btn btn-sm btn-success',
                    action: function (saveButton) {
                        addRating('<?= Followers::STATUS_OLD; ?>');
                        return false;
                    }
                },
                ok2: {
                    text: 'Keep this class',
                    btnClass: 'btn btn-sm btn-success',
                    action: function (saveButton) {
                        addRating('<?= Followers::STATUS_ACTIVE; ?>');
                        return false;
                    }
                }
            },
        });
    })
</script>