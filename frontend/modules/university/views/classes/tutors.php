<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Classes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Url::to(['/js/plugins/moment.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepicker.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepaginator.js']));

$this->registerCssFile(Url::to(['/css/plugins/bootstrap-datepaginator.css']));
?>

<div class="row no-gutters courses-index left-content">
    <div class="col-12 col-md-6">
        <div class="row no-gutters calendar-widget show-btn-event" style="margin-bottom: 11px;">
            <?php if($tutorsC = $dataСProvider->getModels()) { ?>
                <table class="table">
                    <?php foreach ($tutorsC as $key => $tutorC) { ?>
                        <?php if ($user = $tutorC->userModel) { ?>
                                <tr style="border-bottom:1px solid black;">
                                    <td><img style="width:50px;" src="<?= $user->getAvatar(); ?>"></td>
                                    <td><?= Html::a($user->getNameAndSurname(), $user->getProfileLink()); ?></td>
                                    <td>
                                        $<?= $tutorC->price; ?>
                                    </td>
                                    <td>
                                        Rank:<?= $user->getRankCount(); ?> 
                                        <?= StarRating::widget([
                                            'name' => 'rating_u_'. $user->id,
                                            'value' => $user->getRating(),
                                            'disabled' => true,
                                            'pluginOptions' => [
                                                'size' => 'xs',
                                                'min' => 0,
                                                'max' => 5
                                            ],
                                        ]); ?>
                                    </td>
                                    <td>
                                        <?= Html::button('Mail to user', ['class' => 'js-mail-to-user', 'data-myId' => Yii::$app->user->id, 'data-user_id' => $user->id])?>
                                    </td>
                                </tr>
                        <?php } ?>
                    <?php } ?>
                </table>
            <?php } else { ?>
                <p class="text-center">This class does not have any tutors at this time. Please check back later.</p>
            <?php } ?>
        </div>
    </div>

    <div class="col-12 col-md-6 right-content home-carousel">
        <?= $this->render('_top', [
            'model' => $model
        ]); ?>
    </div>
</div>

<script>
    $(function(){
        $(document).on('click', '.js-show-question', function(){
            var id = $(this).data('id');

            $.dialog({
                id: 'questionShowModal',
                title: 'Question bank',
                content: 'url:<?= Url::to(['/business/questions/show']) ?>/' + id,
                columnClass: 'xl view-block modal-block '
            });
        })

        $(document).on('click', '.js-show-event', function () {
            var id = $(this).data('id');
            $.dialog({
                id: 'eventShowModal',
                title: 'Show event',
                content: 'url:<?= Url::to(['/university/classes/event-show']) ?>/'+id,
            })
        })
        
        $(document).on('click', '.js-event-create', function () {
            $.confirm({
                id: 'eventCreateModal',
                title: 'Create event',
                content: 'url:<?= Url::to(['/university/classes/event-create']) ?>?class=<?= $model->id;?>',
                columnClass: 'modal-block col-sm-12'
            });
        })
        
        $(document).on('click', '.js-question-create', function (e) {
            e.preventDefault();

            $.confirm({
                id: 'questionCreateModal',
                title: 'Create question',
                content: 'url:/business/questions/create?classId=<?= $model->id;?>',
                columnClass: 'modal-block xl'
            });
        })
        
        $(document).on('click', '.js-update-class', function(){
            $.confirm({
                id: 'classUpdateModal',
                title: 'Update class',
                content: 'url:<?= Url::to(['/university/classes/create']) ?>?id=<?= $model->id; ?>',
                columnClass: 'modal-block xl'
            });
        })
    })
</script>