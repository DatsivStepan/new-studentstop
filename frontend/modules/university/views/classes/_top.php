<?php

use yii\helpers\Html,
    yii\helpers\Url,
    kartik\rating\StarRating,
    common\models\university\Classes,
    common\models\university\ClassFiles;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */
/* @var $form yii\widgets\ActiveForm */

$filterArray = Classes::$filterTop;
$topList = Classes::getTopList($model->id);
?>
<?php if ($files = ClassFiles::getImagesForSider($model->id, 5, $model->getLastFinishedDate())) { ?>
    <?php $countF = count($files); ?>
    <div id="carouselExampleIndicators" class="carousel slide carousel-univer-classes" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php for ($i=0; $i < $countF; $i++) { ?>
                <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i; ?>" class="<?= $i == 0 ? 'active' : ''; ?>"></li>
            <?php } ?>
        </ol>
        <div class="carousel-inner" style="height: 300px" style="">
            <?php foreach ($files as $key => $file) { ?>
                <div class="carousel-item <?= $key == 0 ? 'active' : ''; ?>" style="height: 100%;position:relative;">
                    <div style="position: absolute;top:0px;left:0px;width:100%;color:white;padding:0 40px;">TRENDING</div>
                
                    <img class="d-block w-100" src="<?= $file->file_src; ?>" alt="Slide <?= $key + 1; ?>" style="object-fit: cover;height:100%;">
                    <?php if ($text = $file->description) { ?>
                        <div style="background-image: linear-gradient(white, #72638c, #4a2f77, #221538, black);position: absolute;opacity:0.4;left:0px;top:0px;width:100%;height:100%;"></div>
                        <div style="position: absolute;top:70%;left:0px;width:100%;height:30%;color:white;padding:0 40px;"><?= substr($text, 0, 150).'...'; ?></div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <?php if ($countF > 1) { ?>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
        <?php } ?>
    </div>
<?php } else { ?>
    <div style="background: #f6f7fb;border-radius: 6px;padding: 0 10px 10px 10px;margin-top: 10px;">
        <p class="text-center">This class does not have top post yet. Be first to add photo or video post.</p>
    </div>
<?php } ?>

<div class="row no-gutters calendar-widget show-btn-event top-university-containter">
    <div class="col-sm-12">
        <ul class="nav nav-tabs js-tabs-top-list" role="tablist">
            <li class="nav-item"><a class="nav-link name"><b>TOP 5:</b></a></li>
            <?php $k = 0; ?>
            <?php foreach ($filterArray as $filterId => $filterName) { ?>
                <?php $class = ''; ?>
                <li class="nav-item" data-id="<?= $filterId; ?>">
                    <?= Html::a($filterName, null, [
                        'class' => 'nav-link ' . ($filterId == 1 ? 'active' : ''),
                        'data-toggle' => "tab",
                        'href' => "#tabTop" . $filterId,
                        'role' => "tab",
                        'aria-selected' => "false"
                    ])?>
                </li>
                <?php $k++;?>
            <?php }?>
        </ul>
        <div class="tab-content my-scroll clearfix">
            <?php if ($topList) { ?>
                <?php foreach ($topList as $listId => $list) { ?>
                    <div class="tab-pane my-scroll <?= $listId == 1 ? 'active' : ''; ?>" id="tabTop<?= $listId; ?>" role="tabpanel">
                        <?php 
                            switch ($listId) {
                                case 1: 
                                case 2: ?>
                                    <?php if ($list) { ?>
                                        <table class="table">
                                            <?php foreach ($list as $user) { ?>
                                                <tr>
                                                    <td class="img-block"><img style="width:50px;" src="<?= $user->getAvatar(); ?>"></td>
                                                    <td class="avatar-name"><?= Html::a($user->getNameAndSurname(), $user->getProfileLink()); ?></td>
                                                    <td class="stars">
                                                        Rank:<?= $user->getRankCount(); ?> 
                                                        <?= StarRating::widget([
                                                            'name' => 'rating_u_'. $user->id,
                                                            'value' => ($listId == 1) ? $user->getRating() : $user->getProfessorRating(),
                                                            'disabled' => true,
                                                            'pluginOptions' => [
                                                                'size' => 'xs',
                                                                'min' => 0,
                                                                'max' => 5
                                                            ],
                                                        ]); ?>
                                                    </td>
                                                    <td class="button-block">
                                                        <?= Html::button('Mail to user', ['class' => 'js-mail-to-user', 'data-myId' => Yii::$app->user->id, 'data-user_id' => $user->id])?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    <?php } else { ?>   
                                        <?php if ($listId == 1) { ?>
                                            <p class="text-center">NO students</p>
                                        <?php } else { ?>
                                            <p class="text-center">NO Professors</p>
                                        <?php } ?>
                                    <?php } ?>
                                <?php break;
                                case 3: ?>
                                    <?php if ($list) { ?>
                                        <table class="table">
                                            <?php foreach ($list as $class) { ?>
                                                <tr>
                                                    <td class="img-block"><img style="width:50px;" src="<?= $class->getImages(); ?>"></td>
                                                    <td class="avatar-name"><?= Html::a($class->getName(), $class->getLink()); ?></td>
                                                    <td class="stars">
                                                        Rank:<?= $class->getRank(); ?> 
                                                        <?= StarRating::widget([
                                                            'name' => 'rating_m_'. $class->id,
                                                            'value' => $class->getRating(),
                                                            'disabled' => true,
                                                            'pluginOptions' => [
                                                                'size' => 'xs',
                                                                'min' => 0,
                                                                'max' => 5
                                                            ],
                                                        ]); ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    <?php } else { ?>
                                        <p class="text-center">NO Class</p>
                                    <?php } ?>
                                <?php break;
                            }
                        ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
