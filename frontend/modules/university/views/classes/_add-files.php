<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm,
    kartik\select2\Select2,
    kartik\depdrop\DepDrop,
    yii\widgets\Pjax,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */
/* @var $form yii\widgets\ActiveForm */

?>
<?php Pjax::begin(['id' => 'pjax-form-class-create', 'enablePushState' => false]); ?>
    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            if ($modal.addFileModal) {
                $modal.addFileModal.close();
            }
            
            location.reload()
        </script>
    <?php } ?>
    <div class="classes-form">

        <?php $form = ActiveForm::begin(['id' => 'form-search-classes', 'options' => ['data-pjax' => 'pjax-form-class-create']]); ?>
        <div class="row" style="margin-left: 16px;margin-right: 16px;">
            <div class="col-12">
                <?= $form->field($model, 'description')->textarea(['rows' => 2]); ?>
            </div>

            <div class="col-12 text-center">
                <?= $form->field($model, 'name')->hiddeninput()->label(false); ?>
                <?= $form->field($model, 'mimeType')->hiddeninput()->label(false); ?>
                <?= $form->field($model, 'file_src')->hiddeninput()->label(false); ?>
                <a class="btn btn-info newFiles">Add new Files</a>
                <div class="row user_class_new_file"></div>
            </div>
        </div>
        
        <?php ActiveForm::end(); ?>

    </div>

    <div style="display:none">
        <div class="table table-striped previewTemplateFiles" >
            <div id="template" class="file-row row files-block">
                <!-- This is used as the file preview template -->
                <div class="new-photo col-sm-5">
                    <span class="preview"><img data-dz-thumbnail /></span>
                    <div class="photo-name">
                        <p class="name" data-dz-name></p>
                        <strong class="error text-danger" data-dz-errormessage></strong>
                        <p class="size" data-dz-size></p>
                    </div>
                </div>
                <div class="progress progress-striped active col-sm-4" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
                <div class="col-sm-2">
                <button data-dz-remove class="btn delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                </div>
            </div>
        </div>
    </div>
        
    <script>
        $(function(){
            if($('.user_class_new_file').length){
                var previewNode = document.querySelector(".previewTemplateFiles");
                previewNode.id = "";
                var previewTemplate = previewNode.parentNode.innerHTML;
                previewNode.parentNode.removeChild(previewNode);

                var myDropzoneA = new Dropzone($('.user_class_new_file')[0], {
                    maxFiles:1,
                    uploadMultiple:false,
                    url: "/site/upload-files?type=classes",
                    previewTemplate:previewTemplate,
                    clickable: ".newFiles",
                    previewsContainer: ".user_class_new_file",
                });

//                myDropzoneA.on("maxfilesexceeded", function(file) {
//                    myDropzoneA.removeAllFiles();
//                    myDropzoneA.addFile(file);
//                });

                myDropzoneA.on("complete", function(response) {
                    if (response.status == 'success') {
                        $('#classfiles-file_src').val(response.xhr.response);
                        $('#classfiles-mimetype').val(response.type);
                        $('#classfiles-name').val(response.name);
        //                    $('.eventButton').show();
                    }
                });

                myDropzoneA.on("removedfile", function(response) {
                    if(response.xhr != null){
                        $('#classfiles-file_src').val('');
                        $('#classfiles-mimetype').val('');
                        $('#classfiles-name').val('');
                    }
                });
            }
        })
    </script>
<?php Pjax::end(); ?>