<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm,
    kartik\select2\Select2,
    kartik\depdrop\DepDrop,
    yii\widgets\Pjax,
    yii\helpers\Url,
    kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */
/* @var $form yii\widgets\ActiveForm */

?>
<?php Pjax::begin(['id' => 'pjax-form-class-create', 'enablePushState' => false]); ?>
    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            if ($modal.addRatingModal) {
                $modal.addRatingModal.close();
            }

            location.reload()
        </script>
    <?php } ?>
    
    <div class="classes-form">

        <?php $form = ActiveForm::begin(['id' => 'form-search-classes', 'options' => ['data-pjax' => 'pjax-form-class-create']]); ?>
            <div class="row" style="margin-left: 16px;margin-right: 16px;">
                <h3>Rating Do's and Don'ts</h3>
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Do</h4>
                        <p>Double check your comments before posting. Course codes must be accurate, and it doesn’t hurt to check grammar.</p>
                    </div>
                    <div class="col-sm-4">
                        <h4>Do</h4>
                        <p>Discuss the professor’s professional abilities including teaching style and ability to convey the material clearly.</p>
                    </div>
                    <div class="col-sm-4">
                        <h4>Do</h4>
                        <p>Use profanity, name-calling, derogatory terms, definitive language, (e.g., "always","never","etc."). And, don’t claim that the professor shows bias or favoritism for or against students.</p>
                    </div>
                </div>

                <h3>It's your turn to grade Professor <?= $modelC->professorModel ? $modelC->professorModel->getNameAndSurname() : '';  ?>.</h3>
                <div class="col-12">
                    <?= $form->field($model, 'rate_professor')->widget(StarRating::classname(), [
                            'pluginOptions' => [
                                'step' => '1',
                                'size'=>'xs',
                                'showCaption' => false,
                            ],
                        ]); ?>

                    <?= $form->field($model, 'prof_again')->radioList([1 => 'Yeah', 2 => 'Um, no']); ?>

                    <?= $form->field($model, 'take_for_credit')->radioList([1 => 'Yeah', 2 => 'Um, no']); ?>

                    <?= $form->field($model, 'textbook_use')->radioList([1 => 'Yeah', 2 => 'Um, no']); ?>

                    <?= $form->field($model, 'attendance')->radioList([1 => 'Yeah', 2 => 'Um, no']); ?>

                    <?= $form->field($model, 'hotness')->radioList([1 => 'Yeah', 2 => 'Um, no']); ?>

                    <?= $form->field($model, 'more_specific')->textarea(); ?>

                    <?= $form->field($model, 'level_of_difficulty')->textarea(); ?>

                </div>
            </div>
        <?php ActiveForm::end(); ?>

    </div>

<?php Pjax::end(); ?>