<?php
use yii\helpers\Html,
    kartik\grid\GridView,
    kartik\rating\StarRating;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'responsive' => false,
    'striped' => false,
    'responsiveWrap' => false,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Class',
            'attribute' => 'name',
            'value' => function ($model) {
                return Html::a($model->name, $model->getLink());
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'professor',
            'format' => 'raw',
            'value' => function ($model) {
                $professor = $model->professorModel;
                return $professor ? 
                        Html::tag('img', '', [
                            'src' => $professor->getAvatar(),
                            'class' => 'professor-avatar',
                        ]).
                        Html::tag('div', $professor->getNameAndSurname(), [
                            'class' => 'professor-name',
                        ])
                        : null;
            },
            'contentOptions' => ['class' => 'professor-data']
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->getStatus();
            },
            'contentOptions' => ['class' => 'professor-status']
        ],
        [
            'label' => 'Students',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->getStudentsCount();
            },
            'contentOptions' => ['class' => 'professor-students']
        ],
        [
            'label' => 'Rank',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->getRank();
            },
            'contentOptions' => ['class' => 'professor-rank']
        ],
        [
            'label' => 'Rating',
            'format' => 'raw',
            'value' => function ($model) {
                return '<div class="custom-stars">'.StarRating::widget([
                        'name' => 'rating_' . $model->id,
                        'value' => $model->getRating(),
                        'disabled' => true,
                        'pluginOptions' => [
                                'size' => 'xs',
                                'min' => 0,
                                'max' => 5
                        ],
                    ])."</div>";
            },
            'contentOptions' => ['class' => 'professor-rating']
        ],
        [
            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                $joinStatus = $model->getJoinClassText();

                return Html::a($joinStatus['text'],
                    $joinStatus['link'],
                    [
                        'class' => $joinStatus['class'].' action-btn join-btn',
                        'data-id' => $model->id,
                        'data-pjax' => 0,
                        'data-view' => 1
                    ]);
            },
            'contentOptions' => ['class' => 'professor-btn']
        ],
    ],
]); ?>
