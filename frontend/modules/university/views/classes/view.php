<?php

use yii\helpers\Url,
    yii\helpers\Html,
    yii\widgets\DetailView,
    kartik\rating\StarRating,
    yii\widgets\Pjax,
    yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Classes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Url::to(['/js/plugins/moment.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepicker.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepaginator.js']));

$this->registerCssFile(Url::to(['/css/plugins/bootstrap-datepaginator.css']));
?>

<script>
    $(function() {
        function dateChange(date) {
            date = new Date(date);
            var year = date.getFullYear()
            var month = date.getMonth()+1;
            if(month < 10){
                month = '0'+month;
            }
            var day = date.getDate()
            var dayClick = year+'-'+month+'-'+day;

            $('#events-search-form').find('#classeseventssearch-filterdate').val(dayClick)
            $('#events-search-form').submit()
        }

        $('#paginator').datepaginator({
            showCalendar: false,
            textSelected: '<b>dddd</b><br/>Do, MMMM YYYY',
            text: '<b>ddd</b><br/>Do',
            itemWidth: 50,
            onSelectedDateChanged: function(event, date){
                dateChange(date);
            },
        });

        $('.calendar-custom').on('click', '.custom-today', function(e){
            e.preventDefault();
            var date = new Date();
            $('#paginator').datepaginator('setSelectedDate', `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`)
        });
    })
</script>


<div class="row no-gutters courses-index left-content">
    <div class="col-12 col-md-6">
        <?php if ($modelUser = $model->professorModel) { ?>
            <div class="row no-gutters courses-personal-info-block">
                <div class="personal-info-left-block">
                    <img src="<?= $modelUser->getAvatar(); ?>" style="width:100%;">
                </div>
                <div class="personal-info-right-block">
                    <h5 class="personal-info-right-block-name"><?= $modelUser->getNameAndSurname(); ?></h5>
                    <div class="personal-info-right-block-rating">
                        <div class="personal-info-right-block-rating-title">
                            Rank: <?= $modelUser->getRankCount(true); ?>
                        </div>
                        <div class="custom-stars">
                            <?= StarRating::widget([
                                'name' => 'rating_user',
                                'value' => $modelUser->getProfessorRating(),
                                'disabled' => true,
                                'pluginOptions' => [
                                    'size' => 'xs',
                                    'min' => 0,
                                    'max' => 5
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div class="personal-info-right-block-danye row">
                        <div class="col-12 danye-block">
                            <p>Birthday:</p>
                            <span><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->birthday : ''; ?></span>
                        </div>
                        <div class="col-12 danye-block">
                            <p>Mobile:</p>
                            <span><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->mobile : ''; ?></span>
                        </div>
                        <div class="col-12 danye-block">
                            <p>University: </p>
                            <span><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->getUniversityName() : ''; ?></span>
                        </div>
                        <div class="col-12 danye-block">
                            <p>Address:</p>
                            <span><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->address : ''; ?></span>
                        </div>
                        <div class="col-12 danye-block">
                            <p>About</p>
                            <span><?= $modelUser->userInfoModel ? $modelUser->userInfoModel->about : ''; ?></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="row no-gutters courses-join-block">
            <div class="col-12 col-md-7 col-xl-9">
                <h3 class="courses-join-block-name">
                    Course: <?= Html::encode($this->title) ?><br>
                    <?= $model->courseModel && $model->courseModel->majorModel ? 'Major: ' . Html::encode($model->courseModel->majorModel->getName()) : ''; ?>
                </h3>
                <div class="personal-info-right-block-rating-title">
                    Rank: <?= $model->getRank(); ?>
                </div>
                <div class="custom-stars">
                    <?= StarRating::widget([
                        'name' => 'rating_class',
                        'value' => $model->getRating(),
                        'disabled' => true,
                        'pluginOptions' => [
                            'size' => 'xs',
                            'min' => 0,
                            'max' => 5
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-12 col-md-5 col-xl-3 text-right align-self-center">
                <?php
                if (!$model->isOwner()) {
                    $joinStatus = $model->getJoinClassText(Yii::$app->user->id, false);

                    echo Html::a($joinStatus['text'],
                        $joinStatus['link'],
                        [
                            'class' => $joinStatus['class'].' action-btn join-btn',
                            'data-id' => $model->id,
                            'data-view' => 0,
                        ]);
                } else {
                    echo Html::button('Update',
                        [
                            'class' => 'js-update-class action-btn join-btn',
                            'target' => '_blank',
                        ]);
                }
                ?>
            </div>
        </div>
        <div class="row no-gutters calendar-widget show-btn-event calendar-block">
            <div class="block-tabs-courses">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#chat" role="tab">Chat</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#question-bank" role="tab">Question bank</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#students" role="tab">students</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#class-info" role="tab">Class info</a>
                    </li>
                </ul>
                <div class="tab-content my-scroll clearfix">
                    <div class="tab-pane active" id="chat" role="tabpanel">Chat</div>
                    <div class="tab-pane my-scroll" id="question-bank" role="tabpanel">
                        <?php if ($model->checkStudentCreatedStatus()) { ?>
                            <div class="text-center">
                                <button class="custom-add-event-courses js-question-create"></button>
                                <span class="text-add-event">Question bank</span>
                            </div>
                        <?php } ?>
                            <?php if($questions = $dataQProvider->getModels()) { ?>
                                <?php foreach ($questions as $key => $question) { ?>
                                        <div class="info-block row">
                                            <div class="col-12">
                                                <?= $question->title; ?>
                                            </div>

                                            <div class="col-2">
                                                <img style="width:100px;" src="<?= $question->getAuthorAvatar(); ?>">
                                            </div>

                                            <div class=" col-8 col-sm-3" style="overflow: hidden;padding-right: 0px;padding-top: 6px;">
                                                 <p><?= $question->getAuthorName(); ?></p>
                                            </div>

                                            <div class="col-6 col-sm-3">
                                               <p> <?= $question->getPrice(); ?></p>
                                            </div>
                                            <div class="col-6 col-sm-4" style="padding-top: 6px;padding-left: 0px">
                                                <?= Html::button('View', [
                                                    'class' => 'view-button btn btn-info js-show-question',
                                                    'data-id' => $question->id
                                                ]); ?>
                                            </div>
                                        </div>
                                <?php } ?>
                            <?php } else { ?>
                                <p class="center">Sorry But questions not found</p>
                            <?php } ?>
                    </div>
                    <div class="tab-pane my-scroll" id="students" role="tabpanel">
                        <?php if ($models = $model->classesFollowModel) { ?>
                            <table class="table">
                                <?php foreach ($models as $follower) { ?>
                                    <?php if ($user = $follower->userModel) { ?>
                                            <tr>
                                                <td class="img-b"><img class="img-tab-calendar" src="<?= $user->getAvatar(); ?>"></td>
                                                <td class="name"><?= Html::a($user->getNameAndSurname(), $user->getProfileLink()); ?></td>
                                                <td class="stars">
                                                    Rank:<?= $user->getRankCount(); ?> 
                                                    <?= StarRating::widget([
                                                        'name' => 'rating_u_'. $user->id,
                                                        'value' => $user->getRating(),
                                                        'disabled' => true,
                                                        'pluginOptions' => [
                                                            'size' => 'xs',
                                                            'min' => 0,
                                                            'max' => 5
                                                        ],
                                                    ]); ?>
                                                </td>
                                                <td class="button-block">
                                                    <?= Html::button('Mail to user', ['class' => 'js-mail-to-user', 'data-myId' => Yii::$app->user->id, 'data-user_id' => $user->id])?>
                                                </td>
                                            </tr>
                                    <?php } ?>
                                <?php } ?>
                            </table>
                        <?php } else { ?>
                            <p class="text-center">NO STUDENTS</p>
                        <?php } ?>
                    </div>
                    <div class="tab-pane my-scroll" id="class-info" role="tabpanel">
                        <?= $model->description; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters calendar-widget show-btn-event" style="margin-bottom: 11px;">
            <div class="col-12 col-md-12">
                <div id="paginator" class="calendar-view"></div>
                <div class="calendar-custom">
                    <button class="custom-today">Today</button>
                    <?php if ($model->checkStudentCreatedStatus()) { ?>
                        <button class="custom-add-event js-event-create"></button>
                    <?php } ?>
                </div>
            </div>
            <div class="col-12 col-md-12 calendar-widget-content-courses">
                <?php Pjax::begin(['id' => 'pjax-form-events-show', 'enablePushState' => false]); ?>

                    <?php $form = ActiveForm::begin(['id' => 'events-search-form', 'options' => ['data-pjax' => 'pjax-form-events-show']]); ?>
                        <?= $form->field($searchEModel, 'filterDate')->hiddenInput()->label(false) ?>
                    <?php ActiveForm::end(); ?>

                    <?php if ($events = $dataEProvider->getModels()) { ?>
                        <?php foreach ($events as $key => $event) { ?>
                            <div class="row no-gutters event-block-under-calendar align-items-center">
                                <div class="col-12 col-md-2 text-center">
                                    <div class="event-img" style="background-image: url(<?= $event->getImages(); ?>);"></div>
                                    <div class="event-date-start-end"><?= $event->getDateStart(); ?>-<?= $event->getDateEnd(); ?></div>
                                </div>
                                <div class="col-12 col-md-3 event-title"><a href="/"><?= $event->title; ?></a></div>
                                <div class="col-12 col-md-2 date-create"><?= $event->getDateCreate(); ?></div>
                                <div class="col-12 col-md-3 event-short-content"><?= $event->short_content; ?></div>
                                <div class="col-12 col-md-2 event-join-btn">
                                    <a class="action-btn join-btn js-show-event" data-id="<?= $event->id; ?>">View</a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="col-12 col-md-12 calendar-widget-content-courses">
                            Sorry But events not found
                        </div>
                    <?php } ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6 right-content home-carousel">
        <?= $this->render('_top', [
            'model' => $model
        ]); ?>
    </div>
</div>

<script>
    $(function(){
        $(document).on('click', '.js-show-question', function(){
            var id = $(this).data('id');

            $.dialog({
                id: 'questionShowModal',
                title: 'Question bank',
                content: 'url:<?= Url::to(['/business/questions/show']) ?>/' + id,
                columnClass: 'xl view-block modal-block '
            });
        })

        $(document).on('click', '.js-show-event', function () {
            var id = $(this).data('id');
            $.dialog({
                id: 'eventShowModal',
                title: 'Show event',
                content: 'url:<?= Url::to(['/university/classes/event-show']) ?>/'+id,
            })
        })
        
        $(document).on('click', '.js-event-create', function () {
            $.confirm({
                id: 'eventCreateModal',
                title: 'Create event',
                content: 'url:<?= Url::to(['/university/classes/event-create']) ?>?class=<?= $model->id;?>',
                columnClass: 'modal-block col-sm-12'
            });
        })
        
        $(document).on('click', '.js-question-create', function (e) {
            e.preventDefault();

            $.confirm({
                id: 'questionCreateModal',
                title: 'Create question',
                content: 'url:/business/questions/create?classId=<?= $model->id;?>',
                columnClass: 'modal-block xl'
            });
        })
        
        $(document).on('click', '.js-update-class', function(){
            $.confirm({
                id: 'classUpdateModal',
                title: 'Update class',
                content: 'url:<?= Url::to(['/university/classes/create']) ?>?id=<?= $model->id; ?>',
                columnClass: 'modal-block xl'
            });
        })
    })
</script>