<?php

use yii\helpers\Html,
    kartik\grid\GridView,
    yii\widgets\Pjax,
    yii\widgets\ActiveForm,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\university\models\SearchCourses */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row no-gutters courses-index">
        <div class="col-12 left-content">
            <?= Html::a('New Class', null, [
                'class' => 'btn-new-class btn btn-success js-create-new-class'
            ])?>
            <div class="classes-box">
                <div class="classes-head">My Classes</div>
                <?php /*Pjax::begin(['id' => 'pjax-form', 'enablePushState' => false]); ?>
                    <div style="display:none;">
                        <?php $form = ActiveForm::begin(['id' => 'form-search-classes', 'options' => ['data-pjax' => 'pjax-form']]); ?>
                            <?= $form->field($searchModel, 'course_id')->textInput(); ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                <?php Pjax::end(); */ ?>
                <?php if ($dataProvider->getModels()) { ?>
                    <div class="js-classes-block classes-block my-scroll">
                        <?= $this->render('_classes_grid',[
                            'dataProvider' => $dataProvider
                        ]); ?>
                    </div>
                <?php } else { ?>
                    <p>YOU DON'T ATTEND ANY CLASSES. PLEASE START BUILDING YOUR COURSE</p>
                <?php } ?>
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $(document).on('click', '.js-create-new-class', function(){
                $.confirm({
                    id: 'classCreateModal',
                    title: 'Create class',
                    content: 'url:<?= Url::to(['/university/classes/create']) ?>',
                    columnClass: 'modal-block xl'
                });
            })
        })
    </script>