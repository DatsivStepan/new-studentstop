<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Classes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Url::to(['/js/plugins/moment.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepicker.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepaginator.js']));

$this->registerCssFile(Url::to(['/css/plugins/bootstrap-datepaginator.css']));
?>

<div class="row no-gutters courses-index left-content">
    <div class="col-12 col-md-6">
        <div class="row no-gutters calendar-widget show-btn-event files-page" style="overflow: -webkit-paged-x; padding-top: 15px; margin-bottom: 11px;">
            <?php if ($model->checkStudentCreatedStatus()) { ?>
                <div class="text-center col-sm-12">
                    <button class="custom-add-event-courses js-add-file"></button>
                    <span class="text-add-event">Add Files</span>
                </div>
            <?php } ?>
            <?php if($files = $dataFProvider->getModels()) { ?>
                <table class="table">
                    <?php foreach ($files as $key => $file) { ?>
                        <?php if ($user = $file->userModel) { ?>
                            <tr style="border-bottom:1px solid black;">
                                <td><img src="<?= $user->getAvatar(); ?>"></td>
                                <td class="professor-name"><?= Html::a($user->getNameAndSurname(), $user->getProfileLink()); ?></td>
                                <td>
                                    <?= Html::a($file->name, Url::to([$file->file_src]), ['target' => '_blank', 'download' => true]); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </table>
            <?php } else { ?>
                <p class="center">Sorry But files not found</p>
            <?php } ?>
        </div>
    </div>

    <div class="col-12 col-md-6 right-content home-carousel">
        <?= $this->render('_top', [
            'model' => $model
        ]); ?>
    </div>
</div>

<script>
    $(function(){
        $(document).on('click', '.js-add-file', function(){
            $.confirm({
                id: 'addFileModal',
                title: 'Add file to class',
                content: 'url:<?= Url::to(['/university/classes/add-files']) ?>?id=<?= $model->id; ?>',
                columnClass: 'xl view-block modal-block '
            });
        })

    })
</script>