<?php
use yii\helpers\Url,
    yii\widgets\Pjax,
    yii\helpers\Html,
    yii\widgets\ActiveForm,
    frontend\models\PostSearch,
    kartik\datetime\DateTimePicker,
    dosamigos\tinymce\TinyMce;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
  <?php Pjax::begin(['id' => 'pjax-form-event', 'enablePushState' => false]); ?>

    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
      <script>
          $modal.eventCreateModal.close();
          location.reload();
      </script>
    <?php } ?>

    <?php $form = ActiveForm::begin(['id' => 'create-form-item', 'options' => ['data-pjax' => 'pjax-form-event']]); ?>
    <div class="row" style="margin-left: 15px;margin-right: 15px;">
        <div class="col-6 events-create-modal">
            <div class="row">
                <div class="col-3 col-xl-2 text-modal">
                    <text>Image</text>
                </div>
                <div class="col-6 col-xl-7">
                    <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
                    <img src="<?= $model->getImages(); ?>" class="js-preview-events-image img-thumbnail"><br>
                    <a class="btn btn-sm btn-success js-add-events-image" style="width:100%">Change image</a><br><br>
                </div>
                
            </div>
            <div class="row">
                <div class="col-3 col-xl-2 text-modal">
                    <text> Date Start</text>
                </div>
                <div class="col-9 col-xl-10">
                    <?= $form->field($model, 'date_start')->widget(DateTimePicker::classname(), [
                        'options' => ['placeholder' => 'Enter event time ...'],
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                ])->label(false); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-3 col-xl-2 text-modal">
                    <text>Date End</text>
                </div>
                <div class="col-9 col-xl-10">
                    <?= $form->field($model, 'date_end')->widget(DateTimePicker::classname(), [
                        'options' => ['placeholder' => 'Enter event time ...'],
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                ])->label(false); ?>
                </div>
            </div>
        </div>
        <div class="col-6 events-create-modal">
            <div class="row">
                <div class="col-5 text-modal">
                    <text> Title</text>
                </div>
                <div class="col-12">
                    <?= $form->field($model, 'title')->textInput(['placeholder' => 'Write short'])->label(false); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-5 text-modal">
                    <text>Short Content</text>
                </div>
                <div class="col-12">
                    <?= $form->field($model, 'short_content')->textarea(['placeholder' => 'Write short'])->label(false); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-5 text-modal">
                    <text>Content</text>
                </div>
                <div class="col-12">
                    <?php $idT = 'contentId' . uniqid(); ?>
                    <?= $form->field($model, 'content')->widget(TinyMce::className(), [
                        'options' => ['rows' => 6, 'id' => $idT],
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ])->label(false);?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
      
<script>
    $(function(){
        $dropzone.initializationDropzoneEleemnt('events', '#classesevents-img_src', 'image/*');
    })
</script>
  <?php Pjax::end();  ?>