<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm,
    kartik\select2\Select2,
    kartik\depdrop\DepDrop,
    yii\widgets\Pjax,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */
/* @var $form yii\widgets\ActiveForm */

?>
<?php Pjax::begin(['id' => 'pjax-form-class-create', 'enablePushState' => false]); ?>
    <?php if (Yii::$app->session->hasFlash('successSave')) { ?>
        <script>
            if ($modal.classCreateModal) {
                $modal.classCreateModal.close();
            }
            if ($modal.classUpdateModal) {
                $modal.classUpdateModal.close();
            }
            
            location.reload()
        </script>
    <?php } ?>
    <div class="classes-form">
        <?php $form = ActiveForm::begin(['id' => 'form-search-classes', 'options' => ['data-pjax' => 'pjax-form-class-create']]); ?>
            <div class="row" style="margin-left: 16px;margin-right: 16px;">
                <div class="col-6 events-create-modal create-modal">
                    <div class="row">
                        <div class="col-3 col-xl-2 text-modal">
                            <text>Name</text>
                        </div>
                        <div class="col-9 col-xl-10">
                            <?= $form->field($model, 'name')->textInput([
                            'maxlength' => true,
                            'readonly' => true
                        ])->label(false); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 col-xl-2 text-modal">
                            <text>Major Id</text>
                        </div>
                        <div class="col-9 col-xl-10">
                            <?= $form->field($model, 'major_id')->widget(Select2::classname(), [
                                'data' => $majors,
                                'options' => ['placeholder' => Yii::t('app', 'Select...')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ]
                            ])->label(false); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 col-lg-2 text-modal">
                            <text>Course</text>
                        </div>
                        <div class="col-9 col-lg-10">
                            <?= $form->field($model, 'course_id')->widget(DepDrop::classname(), [
                                'type' => DepDrop::TYPE_SELECT2,
                                'data' => $courses,
                                'options' => ['id' => 'select-course', 'placeholder' => Yii::t('app', 'Select...')],
                                'select2Options' => [
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ],
                                'pluginOptions' => [
                                    //'initialize' => true,
                                    //'initDepends' => ['classes-major_id'],
                                    'depends' => ['classes-major_id'],
                                    'url' => Url::to(['/university/courses/get-courses-by-major'])
                                ],
                                'pluginEvents' => [
                                    "change" => "function() {
                                        if($(this).val()) {
                                            $('#classes-name').val($('#select-course option[value='+$(this).val()+']').text());
                                        } else {
                                            $('#classes-name').val('');
                                        }
                                    }"
                                ],
                            ])->label(false); ?>    
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 col-lg-2 text-modal">
                            <text>Professor</text>
                        </div>
                        <div class="col-9 col-lg-10">
                            <?= $form->field($model, 'professor')->widget(Select2::classname(), [
                                'data' => $professor,
                                'options' => ['placeholder' => Yii::t('app', 'Select...')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ]
                            ])->label(false); ?>
                        </div>
                    </div>
                </div>
                <div class="col-6 events-create-modal create-modal">
                     <div class="row">
                        <div class="col-5 text-modal">
                            <text>Description</text>
                        </div>
                        <div class="col-12">
                            <?= $form->field($model, 'description')->textarea(['rows' => 3])->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>

    </div>
    <script>
        $(function(){
            $dropzone.initializationDropzoneEleemnt('classes', '#classes-img_src', 'image/*');
        })
    </script>
<?php Pjax::end(); ?>