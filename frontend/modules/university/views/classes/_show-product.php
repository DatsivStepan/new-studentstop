<?php
use yii\helpers\Url,
    yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row js-show-product-block" style="margin-left: 15px;margin-right: 15px;">
    <div class="col-12">
        <h3><?= $model->title; ?></h3>
        <p><?= $model->description; ?></p>
    </div>
    <div class="col-12 text-center">
    <?php if ($file = $model->classFilesModel) { ?>
        <?php if($model->isPayed() || $model->isOwner()) { ?>
            <?= Html::a($file->name, Url::to([$file->file_src], true), ['target' => '_blank']); ?>
        <?php } else { ?>
            <?= Html::a('Pay with Paypal ' . $model->getMoney(), null, ['class' => 'paypal-btn js-pay-paypal btn btn-info'])?>

            <?= Html::a('Pay credits ' . $model->getCredits(), null, [
                'class' => 'creditbtn js-pay-credits btn btn-success',
                'data-credit' => $model->price_c
            ])?>
        <?php } ?>
    <?php } else { ?>
        Error
    <?php } ?>
    </div>
</div>
<script>
    $(function(){
        
        $('.js-show-product-block').on('click', '.js-pay-paypal', function(){
            var thisE = $(this);
            var id = '<?= $model->id; ?>';
            thisE.attr('disabled', true);
            swal({
                title: "Are you sure?",
                text: "you want payed  " + parseFloat('<?= $model->getMoney(); ?>') + '$',
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    location.href = '/payment-paypal/pay-for-class-product/?productId=' + id
                }
                thisE.attr('disabled', false);
            })
        });

        $('.js-show-product-block').on('click', '.js-pay-credits', function() {
            var thisE = $(this);
            var id = '<?= $model->id; ?>';
            thisE.attr('disabled', true);
            var creditsNeed = thisE.data('credit');
            $.ajax({
                method: 'post',
                url: '<?= Url::to(['/site/check-credits-count']); ?>',
                dataType: 'json'
            }).done(function (response) {
                thisE.attr('disabled', false);
                if (parseFloat(creditsNeed) > parseFloat(response.amount)) {
                    swal({
                        title: "Not enough credits",
                        text: 'You have ' + parseFloat(response.amount) + ' credits, but you need to ' + parseFloat(creditsNeed),
                        html: true,
                        icon: "error",
                        confirmButtonText: "Ok",
                    });
                } else {
                    swal({
                        title: "Are you sure?",
                        text: "you want payed  " + parseFloat(creditsNeed) + ' credits',
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                        buttons: ["Cancel", "Yes, Payed"],
                    }).then((willPayed) => {
                        if (willPayed) {
                            $.ajax({
                                method: 'post',
                                url: '<?= Url::to(['/university/classes/pay-for-product']); ?>?id=' + id,
                                dataType: 'json'
                            }).done(function (response) {
                                if (response.status) {
                                    modalReload($modal.showProductModal);
                                    swal({
                                        title: "Products payed",
                                        html: true,
                                        icon: "success",
                                        confirmButtonText: "Ok",
                                    });
                                }
                            });
                        }
                    })
                }
            });
        });
    })
</script>