<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model common\models\university\Classes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Classes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Url::to(['/js/plugins/moment.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepicker.js']));
$this->registerJsFile(Url::to(['/js/plugins/bootstrap-datepaginator.js']));

$this->registerCssFile(Url::to(['/css/plugins/bootstrap-datepaginator.css']));
?>

<div class="row no-gutters courses-index left-content">
    <div class="col-12 col-md-6">
        <div class="row no-gutters calendar-widget show-btn-event product-page" style="overflow: -webkit-paged-x; margin-bottom: 11px;padding-top: 15px;">
            <?php if ($model->checkStudentCreatedStatus()) { ?>
                <div class="text-center col-sm-12">
                    <button class="custom-add-event-courses js-add-product"></button>
                    <span class="text-add-event">Add Products</span>
                </div>
            <?php } ?>
            <?php if($products = $dataPProvider->getModels()) { ?>
                <table class="table">
                    <?php foreach ($products as $key => $product) { ?>
                        <?php if ($product->classFilesModel && ($user = $product->classFilesModel->userModel)) { ?>
                            <tr style="border-bottom:1px solid black;">
                                <td><img src="<?= $user->getAvatar(); ?>"></td>
                                <td class="professor-name"><?= Html::a($user->getNameAndSurname(), $user->getProfileLink()); ?></td>
                                <td>
                                    <?= $product->title; ?>
                                </td>
                                <td>
                                    <?= $product->getPrice(); ?>
                                </td>
                                <td class="button-block">
                                     <?= Html::button('Show more', [
                                        'class' => 'view-button btn btn-info js-show-product',
                                        'data-product_id' => $product->id
                                    ]); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </table>
            <?php } else { ?>
                <p class="center">Sorry But files not found</p>
            <?php } ?>
        </div>
    </div>

    <div class="col-12 col-md-6 right-content home-carousel">
        <?= $this->render('_top', [
            'model' => $model
        ]); ?>
    </div>
</div>

<script>
    $(function(){
        $(document).on('click', '.js-add-product', function(){
            $.confirm({
                id: 'addProductModal',
                title: 'Add product to class',
                content: 'url:<?= Url::to(['/university/classes/add-product']) ?>?id=<?= $model->id; ?>',
                columnClass: 'xl view-block modal-block '
            });
        })

        $(document).on('click', '.js-show-product', function(){
        
            $.dialog({
                id: 'showProductModal',
                title: 'Show product',
                content: 'url:<?= Url::to(['/university/classes/show-product']) ?>?id=' + $(this).data('product_id'),
                columnClass: 'xl view-block modal-block '
            });
        })

    })
</script>