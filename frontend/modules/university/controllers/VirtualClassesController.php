<?php

namespace frontend\modules\university\controllers;

use Yii,
    yii\helpers\Url,
    yii\filters\AccessControl,
    common\models\university\VirtualClasses,
    frontend\modules\university\models\SearchVirtualClasses,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    common\models\CoursesTutors,
    common\models\User,
    common\models\MenuItems,
    common\models\university\Majors,
    common\models\university\Courses;

/**
 * VirtualClassesController implements the CRUD actions for VirtualClasses model.
 */
class VirtualClassesController extends Controller
{
    public $universityId;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function beforeAction($action) {
        Yii::$app->params['menuType'] = !Yii::$app->user->identity->tutorModel ? MenuItems::TYPE_VIRTUAL_CLASSES : MenuItems::TYPE_VIRTUAL_CLASSES_ACTIVE;

        $this->universityId = Yii::$app->user->identity->userInfoModel ? Yii::$app->user->identity->userInfoModel->university_id : null;
        if (!$this->universityId) {
            return $this->redirect(['/site/check-user-university']);
        }
        return parent::beforeAction($action);
    }
    
    /**
     * Lists all Courses models.
     * @return mixed
     */
    public function actionMyVirtualClasses()
    {
        Yii::$app->params['menuType'] = !Yii::$app->user->identity->tutorModel ? MenuItems::TYPE_VIRTUAL_CLASSES : MenuItems::TYPE_VIRTUAL_CLASSES_ACTIVE;
        $searchModel = new SearchVirtualClasses();
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchMyClasses();

        return $this->render('my-classes', [
            'majors' => Majors::getAllMajorsInArrayMap($this->universityId),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Lists all VirtualClasses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchVirtualClasses();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionTutorProfile($id = null)
    {
        if (!$id) {
            $id = Yii::$app->user->id;
        }
        $model = $this->findTutorModel($id);
        $modelUser = $this->findUserModel($model->user_id);
        
        $searchModel = new SearchVirtualClasses();
        $searchModel->tutors_id = $model->id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('tutor-profile', [
            'model' => $model,
            'modelUser' => $modelUser,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionBecomeTutor($id = null)
    {
        $this->layout = '/modal';
//        if (Yii::$app->user->identity->tutorModel) {
//            return $this->redirect('/');
//        }
        if ($id) {
            $model = $this->findTutorModel($id);
        } else {
            $model = new CoursesTutors;
            $model->user_id = Yii::$app->user->id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('tutorSaved');
            return $this->redirect(Url::to(['/university/virtual-classes/tutor-profile', 'id' => $model->user_id]));
        }
        return $this->render('become-tutor', [
            'model' => $model,
            'majors' => \common\models\university\Majors::getAllMajors($this->universityId)
        ]);
    }

    /**
     * Displays a single VirtualClasses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VirtualClasses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '/modal';
        $model = new VirtualClasses();
        $model->tutors_id = Yii::$app->user->identity->tutorModel->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }

        return $this->render('create', [
            'model' => $model,
            'courses' => Courses::getAllInArrayMapWithMajor($this->universityId)
        ]);
    }

    /**
     * Updates an existing VirtualClasses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VirtualClasses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoursesTutors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoursesTutors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findTutorModel($id)
    {
        if (($model = CoursesTutors::findOne(['user_id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUserModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the VirtualClasses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VirtualClasses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VirtualClasses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
