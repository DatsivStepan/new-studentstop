<?php

namespace frontend\modules\university\controllers;

use Yii,
    common\models\University,
    frontend\modules\university\models\SearchUniversity,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\AccessControl,
    common\models\Post,
    common\models\PostComment,
    common\models\PostFiles,
    common\models\university\UniversityEvents,
    yii\web\Response,
    common\models\user\UserFiles,
    common\models\User,
    frontend\models\PostSearch;

/**
 * UniversityController implements the CRUD actions for University model.
 */
class UniversityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionEventShow($id)
    {
        $this->layout = '/modal';
        $model = $this->findEventsModel($id);
        return $this->render('_events_view', [
            'model' => $model,
        ]);
    }

    public function actionIsSliderPosts($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findPostModel($id);
        $model->inSlider = Yii::$app->request->get('val');

        return ['status' => $model->save()];
    }
    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPosts($id = null)
    {
        if (!Yii::$app->user->identity->userInfoModel) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $model = $this->findModel(Yii::$app->user->identity->userInfoModel->university_id);
        if (Yii::$app->user->id != $model->id_owner) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $searchModel = new PostSearch();
        if (Yii::$app->request->isPost) {
            $searchModel->load(Yii::$app->request->post());
        }
        $searchModel->filterImage = 1;
        $searchModel->university_id = $model->id;
        $searchModel->type = [Post::TYPE_UNIVERSITY, Post::TYPE_USER_UNIVERSITY];

        return $this->render('setting_posts', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $searchModel->search(Yii::$app->request->queryParams),
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSetting($id = null)
    {
        if (!Yii::$app->user->identity->userInfoModel) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $model = $this->findModel(Yii::$app->user->identity->userInfoModel->university_id);
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('successSave');
            }
        }
        if (Yii::$app->user->id != $model->id_owner) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        return $this->render('setting', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionEventCreate($id = null)
    {
        $this->layout = '/modal';
        $model = new UniversityEvents;
        if ($id) {
            $model = $this->findEventsModel($id);
        }
        $model->university_id = Yii::$app->request->get('university');
        $model->user_id = Yii::$app->user->Id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }
        
        return $this->render('_events_create', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionLikePost($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findPostModel($id);

        return ['status' => true, 'data' => $model->likePost()];
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSaveCommentPost($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $modelP = $this->findPostModel($id);

        $model = new PostComment;
        $model->post_id = $id;
        $model->user_id = Yii::$app->user->id;
        $model->content = Yii::$app->request->post('text');
        $status = false;
        $data = [];
        if ($model->save()) {
            $status = true;
            $data = [
                'image' => $model->getAuthorAvatar(),
                'link' => \yii\helpers\Html::a($model->getAuthorName(), $model->getAuthorLink()),
                'date' => $model->getDateCreate(),
                'content' => $model->getContent()
            ];
            
        }
        return ['status' => $status, 'data' => $data];
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeletePost($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findPostModel($id);
        $status = false;
        if (Yii::$app->user->id == $model->user->id) {
            $status = $model->delete();
        }

        return ['status' => $status];
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdatePost($id)
    {
        $this->layout = '/modal';
        $model = $this->findPostModel($id);
        if ($model->user_id != Yii::$app->user->id) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                PostFiles::deleteAll(['post_id' => $model->id]);
                if ($images = json_decode($model->images)) {
                    foreach ($images as $image) {
                        $postFiles = new PostFiles();
                        $postFiles->post_id = $model->id;
                        $postFiles->file_src = $image->file;
                        $postFiles->type = stristr($image->type, '/', true);
                        $postFiles->save();
                    }
                }
                Yii::$app->session->setFlash('successSave');
            }
        }

        return $this->render('_edit_post', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionShowPost($id)
    {
        $this->layout = '/modal';
        $model = $this->findPostModel($id);
        $model->showPost();
        $modelNewsComment = new PostComment;
        $modelNewsComment->post_id = $id;
        $modelNewsComment->user_id = Yii::$app->user->Id;
        if ($modelNewsComment->load(Yii::$app->request->post())) {
            if ($modelNewsComment->content && $modelNewsComment->save()) {
                Yii::$app->session->setFlash('successSave');
                $modelNewsComment = new PostComment;
            }
        }
        return $this->render('_show_post', [
            'model' => $model,
            'modelNewsComment' => $modelNewsComment,
        ]);
    }

    /**
     * Displays a single University model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelNews = new Post;
        $modelNewsComment= new PostComment;
        
        if (Yii::$app->request->isPost && $modelNews->load(Yii::$app->request->post())) {
            $modelNews->university_id = $model->id;
            $modelNews->user_id = \Yii::$app->user->id;
//            $modelNews->type = Post::TYPE_UNIVERSITY;
            $modelNews->status = Post::STATUS_ACTIVE;
            if ($modelNews->save()) {
                if ($images = json_decode($modelNews->images)) {
                    foreach ($images as $image) {
                        if ($image) {
                            $postFiles = new PostFiles();
                            $postFiles->post_id = $modelNews->id;
                            $postFiles->file_src = $image->file;
                            $postFiles->type = stristr($image->type, '/', true);
                            $postFiles->save();
                        }
                    }
                }
            }
            $modelNews = new Post;
            $modelNewsComment= new PostComment;
        }

        $searchModelU = new \frontend\models\UniversityEventsSearch();
        $searchModelU->filterDate = date('Y-m-d');
        if (Yii::$app->request->isPost) {
            $searchModelU->load(Yii::$app->request->post());
        }
        $searchModelU->university_id = $model->id;
        $dataProviderU = $searchModelU->search(Yii::$app->request->queryParams);

        $searchModel = new PostSearch();
        if (Yii::$app->request->isPost) {
            $searchModel->load(Yii::$app->request->post());
        }
        $searchModel->university_id = $model->id;
        $searchModel->type = [Post::TYPE_UNIVERSITY, Post::TYPE_USER_UNIVERSITY];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isPost && $modelNewsComment->load(Yii::$app->request->post())) {
            $modelNewsComment->user_id = \Yii::$app->user->id;
            $modelNewsComment->save();
            $modelNewsComment= new PostComment;
        }

        return $this->render('view', [
            'model' => $model,
            'modelNews' => $modelNews,
            'modelNewsComment' => $modelNewsComment,
            'searchNewsModel' => $searchModel,
            'dataProviderNews' => $dataProvider,
            'searchModelU' => $searchModelU,
            'dataProviderU' => $dataProviderU,
        ]);
    }

    /**
     * Lists all University models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $searchModel = new SearchUniversity();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
    }

    /**
     * Creates a new University model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new University();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    

    public function actionAddPostPhoto()
    {
        $this->layout = '/modal';
        $userId = Yii::$app->user->id;
        $universityId = Yii::$app->user->identity->userInfoModel->university_id;
        if (!$userId || !$universityId) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        if (Yii::$app->request->isPost) {
            if (Yii::$app->request->post('save-upload-files') !== null) {
                if ($files = json_decode(Yii::$app->request->post('uploadsfiles'))) {
                    foreach ($files as $src => $mType) {
                        $modelF = new UserFiles();
                        $modelF->user_id = $userId;
                        $modelF->file_src = $src;
                        $type = null;
                        if (strpos($mType, 'video') !== false) {
                            $type = 'video';
                        }
                        if (strpos($mType, 'image') !== false) {
                            $type = 'image';
                        }
                        if ($type != null) {
                            $modelF->type = $type;
                            $modelF->save();
                        }
                    }
                }
            } else {
                $modelNews = new Post;
                $modelNews->university_id = $universityId;
                $modelNews->user_id = $userId;
                $modelNews->type = Post::TYPE_UNIVERSITY;
                $modelNews->status = Post::STATUS_ACTIVE;
                if ($modelNews->save()) {
                    $files = json_decode(Yii::$app->request->post('uploadsfiles'));
                    if ($images = $files->image) {
                        foreach ($images as $image) {
                            if ($file = UserFiles::findOne($image)) {
                                $postFiles = new PostFiles();
                                $postFiles->post_id = $modelNews->id;
                                $postFiles->file_src = $file->file_src;
                                $postFiles->type = PostFiles::TYPE_IMAGE;
                                $postFiles->save();
                            }
                        }
                    }

                    if ($videos = $files->video) {
                        foreach ($videos as $video) {
                            if ($file = UserFiles::findOne($video)) {
                                $postFiles = new PostFiles();
                                $postFiles->post_id = $modelNews->id;
                                $postFiles->file_src = $file->file_src;
                                $postFiles->type = PostFiles::TYPE_VIDEO;
                                $postFiles->save();
                            }
                        }
                    }
                    Yii::$app->session->setFlash('successSave');
                }
            }
        }

        return $this->render('_photos-videos', [
            'images' => UserFiles::find()->where(['user_id' => $userId, 'type' => 'image'])->orderBy(['id' => SORT_DESC])->all(),
            'videos' => UserFiles::find()->where(['user_id' => $userId, 'type' => 'video'])->orderBy(['id' => SORT_DESC])->all()
        ]);
    }

    /**
     * Updates an existing University model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing University model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UniversityEvents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UniversityEvents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEventsModel($id)
    {
        if (($model = UniversityEvents::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPostModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Finds the University model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return University the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = University::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
