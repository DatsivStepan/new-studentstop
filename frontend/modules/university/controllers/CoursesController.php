<?php

namespace frontend\modules\university\controllers;

use Yii,
    common\models\university\Courses,
    frontend\modules\university\models\SearchClasses,
    frontend\modules\university\models\SearchVirtualClasses,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\AccessControl,
    yii\web\Response,
    yii\helpers\ArrayHelper,
    common\models\university\Majors,
    yii\data\ActiveDataProvider,
    common\models\MenuItems;

/**
 * CoursesController implements the CRUD actions for Courses model.
 */
class CoursesController extends Controller
{
    public $universityId;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function beforeAction($action) {
        if (!\Yii::$app->user->isGuest) {
            if ($action == 'redirect') {
                $this->enableCsrfValidation = false;
            }
            $this->universityId = Yii::$app->user->identity->userInfoModel ? Yii::$app->user->identity->userInfoModel->university_id : null;
            if (!$this->universityId) {
                return $this->redirect(['/site/check-user-university']);
            }
            return parent::beforeAction($action);
        } else {
            return $this->redirect('/site/login');
        }
    }
    
    public function actionGetCourseByMajor($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = ['status' => false, 'data' => null];
        $courses = Courses::getArrayMapCoursesByMajorId($id);
        if ($courses) {
            $response = ['status' => true, 'data' => $courses];
        }
        return $response;
    }

    public function actionClassesSearch()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_CLASSES;
        $searchModel = new SearchClasses();
        $searchModel->course_id = 0;
        if (Yii::$app->request->isPost && $searchModel->load(Yii::$app->request->post())) {
            $searchModel->load(Yii::$app->request->queryParams);
        }
        $dataProvider = $searchModel->search();

        return $this->render('index', [
            'majors' => Majors::getAllMajorsInArrayMap($this->universityId),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionVirtualClassesSearch()
    {
        Yii::$app->params['menuType'] = !Yii::$app->user->identity->tutorModel ? MenuItems::TYPE_VIRTUAL_CLASSES : MenuItems::TYPE_VIRTUAL_CLASSES_ACTIVE;
        $searchModel = new SearchVirtualClasses();
        $searchModel->course_id = 0;
        if (Yii::$app->request->isPost && $searchModel->load(Yii::$app->request->post())) {
            $searchModel->load(Yii::$app->request->queryParams);
        }
        $dataProvider = $searchModel->search();

        return $this->render('index-v', [
            'majors' => Majors::getAllMajorsInArrayMap($this->universityId),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     /**
     * @param string $select
     * @return mixed
     */
    public function actionGetCoursesByMajor($select = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $modelCourses = Courses::find()->where(['major_id' => Yii::$app->request->post('depdrop_parents')])->all();
        $res = ArrayHelper::getColumn($modelCourses, function($element){
            return ['id' => $element->id, 'name' => $element->name];
        });

        return ['output' => $res, 'selected' => $select];
    }

    /**
     * Displays a single Courses model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Courses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Courses();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Courses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Courses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Courses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Courses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Courses::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
