<?php

namespace frontend\modules\university\controllers;

use Yii,
    common\models\university\Classes,
    frontend\modules\university\models\SearchClasses,
    yii\web\Controller,
    yii\web\Response,
    yii\web\NotFoundHttpException,
    yii\filters\AccessControl,
    common\models\university\Majors,
    common\models\university\Courses,
    common\models\User,
    common\models\MenuItems,
    frontend\modules\university\models\SearchFollowers,
    common\models\Followers,
    frontend\models\ClassesEventsSearch,
    common\models\university\ClassesEvents,
    frontend\modules\business\models\QuestionsSearch,
    frontend\modules\university\models\SearchCoursesTutors,
    frontend\modules\university\models\SearchClassFiles,
    common\models\university\ClassFiles,
    common\models\university\ClassProducts,
    frontend\modules\university\models\SearchClassProduct,
    common\models\Configuration,
    common\models\university\ClassesFinishedHistory,
    common\models\university\ClassesRating,
    common\models\UserRole,
    common\models\pay\PayRequests;

/**
 * ClassesController implements the CRUD actions for Classes model.
 */
class ClassesController extends Controller
{
    public $universityId;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function beforeAction($action) {
        if (!\Yii::$app->user->isGuest) {
            if ($action == 'redirect') {
                $this->enableCsrfValidation = false;
            }
            $this->universityId = Yii::$app->user->identity->userInfoModel ? Yii::$app->user->identity->userInfoModel->university_id : null;
            if (!$this->universityId) {
                return $this->redirect(['/site/check-user-university']);
            }
            return parent::beforeAction($action);
        } else {
            return $this->redirect('/site/login');
        }
    }

    /**
     * Lists all Courses models.
     * @return mixed
     */
    public function actionMyClasses()
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_CLASSES;
        $searchModel = new SearchClasses();
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchMyClasses();

        return $this->render('my-classes', [
            'majors' => Majors::getAllMajorsInArrayMap($this->universityId),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Classes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchClasses();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $name
     * @return mixed
     */
    public function actionResetClasses()
    {
        $dates = Configuration::getClassFihishedDates();
        $dateNow = date('Y-m-d');
        if (in_array($dateNow, $dates)) {
            if (!ClassesFinishedHistory::findOne(['date_finished' => $dateNow])) {
                if ($classes = Classes::find()->all()) {
                    foreach ($classes as $class) {
                        if ($class->classesFollowModel) {
                            foreach ($class->classesFollowModel as $follow) {
                                if ($user = $follow->userModel) {
                                    $follow->statusF = Followers::STATUS_DEACTIVE;
                                    if ($follow->save()) {
    //                                    $user    notificate user
                                    }
                                } else {
                                    $follow->delete();
                                }
                            }
                        }
                    }
                }

                ClassesFinishedHistory::addFinishedDate($dateNow);
            }
        }
    }

    /**
     * @param integer $name
     * @return mixed
     */
    public function actionRating($name)
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_CLASS_ONE;
        $modelC = $this->findModelByName($name);
        $follower = $modelC->isFollower();

        if (!$modelC->isOwner() && !$follower) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        } else if ($follower == Followers::STATUS_DEACTIVE) {
            return $this->render('rating', [
                'model' => $modelC
            ]);
        } else {
            return $this->redirect('/university/classes/' . $modelC->url_name . '/view');
        }
    }
    
    /**
     * @param integer $name
     * @return mixed
     */
    public function actionAddRating($id, $status)
    {
        $this->layout = '/modal';
        $modelC = $this->findModel($id);
        $userId = Yii::$app->user->id;
        $follower = $modelC->isFollower($userId);
        if (!$modelC->isOwner($userId) && !$follower) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        } else if ($follower == Followers::STATUS_DEACTIVE) {

            $model = new ClassesRating();
            $model->class_id = $id;
            $model->user_id = $userId;
            $model->professor_id = $modelC->professor;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if ($followerM = Followers::findOne([
                    'user_id' => $userId,
                    'object_id' => $modelC->id,
                    'type' => Followers::TYPE_CLASSES
                ])) {
                    $followerM->statusF = $status;
                    $followerM->save();
                }
                
                Yii::$app->session->setFlash('successSave');
            }

            return $this->render('_add-rating', [
                'model' => $model,
                'modelC' => $modelC,
            ]);

        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * @param integer $name
     * @return mixed
     */
    public function actionTutors($name)
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_CLASS_ONE;
        $model = $this->findModelByName($name);
        $follower = $model->isFollower();
        if (!$model->isOwner() && !$follower) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        } else if ($follower == Followers::STATUS_DEACTIVE) {
            return $this->redirect('/university/classes/' . $name . '/rating');
        }

        $searchCModel = new SearchCoursesTutors();
        $searchCModel->classId = $model->course_id;
        $searchCModel->load(Yii::$app->request->queryParams);

        return $this->render('tutors', [
            'model' => $model,
            'dataСProvider' => $searchCModel->search(),
        ]);
    }

    /**
     * @param integer $name
     * @return mixed
     */
    public function actionAddFiles($id)
    {
        $this->layout = '/modal';
        $modelC = $this->findModel($id);
        $follower = $modelC->isFollower();
        if (!$modelC->isOwner() && !$follower) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        } else if ($follower == Followers::STATUS_DEACTIVE) {
            return $this->redirect('/university/classes/' . $name . '/rating');
        }

        $model = new ClassFiles();
        $model->class_id = $id;
        $model->user_id = Yii::$app->user->id;
        $model->typeClass = ClassFiles::TYPE_CLASS;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }

        return $this->render('_add-files', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionShowProduct($id)
    {
        $this->layout = '/modal';
        $modelP = $this->findProduct($id);

        return $this->render('_show-product', [
            'model' => $modelP,
        ]);
    }

    /**
     * @param integer $name
     * @return mixed
     */
    public function actionPayForProduct($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = false;
        $product = $this->findProduct($id);
        $class = $this->findModel($product->class_id);

        $message = 'Payed credits for product';
        $status = PayRequests::creditsPayRequest(Yii::$app->user->id, $class->professor, $product->price_c, $product->id, PayRequests::OPERATION_TYPE_PAY_PRODUCT, PayRequests::MODULE_COURSE_CLASSES, $message);

        return ['status' => $status];
    }

    /**
     * @param integer $name
     * @return mixed
     */
    public function actionAddProduct($id)
    {
        $this->layout = '/modal';
        $modelC = $this->findModel($id);
        $follower = $modelC->isFollower();
        if (!$modelC->isOwner() && !$follower) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        } else if ($follower == Followers::STATUS_DEACTIVE) {
            return $this->redirect('/university/classes/' . $name . '/rating');
        }
        $model = new ClassProducts();
        $model->class_id = $modelC->id;
        $modelF = new ClassFiles();
        $modelF->class_id = $id;
        $modelF->user_id = Yii::$app->user->id;
        $modelF->typeClass = ClassFiles::TYPE_CLASS;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->file_id) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('successSave');
                }
            } else if ($modelF->load(Yii::$app->request->post()) && Yii::$app->request->post('file_src')) {
                $modelF->file_src = Yii::$app->request->post('file_src');
                if ($modelF->save()) {
                    $model->file_id = $modelF->id;
                    if ($model->save()) {
                        Yii::$app->session->setFlash('successSave');
                    }
                }
            } else {
                Yii::$app->session->setFlash('errorSave');
            }
        }

        return $this->render('_add-products', [
            'model' => $model,
            'modelF' => $modelF,
            'myFiles' => ClassFiles::getMyFileInArrayMap(),
        ]);
    }

    /**
     * @param integer $name
     * @return mixed
     */
    public function actionProducts($name)
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_CLASS_ONE;
        $modelС = $this->findModelByName($name);
        $follower = $modelС->isFollower();
        if (!$modelС->isOwner() && !$follower) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        } else if ($follower == Followers::STATUS_DEACTIVE) {
            return $this->redirect('/university/classes/' . $name . '/rating');
        }
        $searchPModel = new SearchClassProduct();
        $searchPModel->class_id = $modelС->id;
        $searchPModel->dateShowFrom = $modelС->getLastFinishedDate();
        $searchPModel->load(Yii::$app->request->queryParams);

        return $this->render('products', [
            'model' => $modelС,
            'dataPProvider' => $searchPModel->search(),
        ]);
    }

    /**
     * @param integer $name
     * @return mixed
     */
    public function actionFiles($name)
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_CLASS_ONE;
        $modelС = $this->findModelByName($name);
        $follower = $modelС->isFollower();
        if (!$modelС->isOwner() && !$follower) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        } else if ($follower == Followers::STATUS_DEACTIVE) {
            return $this->redirect('/university/classes/' . $name . '/rating');
        }

        $searchFModel = new SearchClassFiles();
        $searchFModel->dateShowFrom = $modelС->getLastFinishedDate();
        $searchFModel->class_id = $modelС->id;
        $searchFModel->load(Yii::$app->request->queryParams);

        return $this->render('files', [
            'model' => $modelС,
            'dataFProvider' => $searchFModel->search(),
        ]);
    }

    /**
     * Displays a single Classes model.
     * @param integer $name
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($name)
    {
        Yii::$app->params['menuType'] = MenuItems::TYPE_CLASS_ONE;
        $model = $this->findModelByName($name);
        $follower = $model->isFollower();
        if (!$model->isOwner() && !$follower) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        } else if ($follower == Followers::STATUS_DEACTIVE) {
            return $this->redirect('/university/classes/' . $name . '/rating');
        }
        
        $searchEModel = new ClassesEventsSearch();
        $searchEModel->class_id = $model->id;
        $searchEModel->dateShowFrom = $model->getLastFinishedDate();
        $searchEModel->filterDate = date('Y-m-d');
        if (Yii::$app->request->isPost) {
            $searchEModel->load(Yii::$app->request->post());
        }

        $dataEProvider = $searchEModel->search();

        $searchQModel = new QuestionsSearch();
        $searchQModel->class_id = $model->id;
        $searchQModel->dateShowFrom = $model->getLastFinishedDate();
        $searchQModel->load(Yii::$app->request->queryParams);
        $dataQProvider = $searchQModel->search();

        return $this->render('view', [
            'model' => $model,
            'searchEModel' => $searchEModel,
            'dataEProvider' => $dataEProvider,

            'searchQModel' => $searchQModel,
            'dataQProvider' => $dataQProvider
        ]);
    }

    /**
     * Creates a new Classes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id=null)
    {
        $this->layout = '/modal';
        $courses = [];
        if ($id) {
            $model = $this->findModel($id);
            $model->major_id = $model->courseModel ? $model->courseModel->major_id : 0;
            $courses = $model->major_id ? Courses::getAllInArrayMap(null, $model->major_id) : [];
        } else {
            $model = new Classes();
            $model->creator = Yii::$app->user->id;
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNew = $model->isNewRecord;
            if ($model->save()) {
                if ($isNew && ($model->creator != $model->professor)) {
                    Followers::createNew(Followers::TYPE_CLASSES, $model->id, $model->creator);
                }

                Yii::$app->session->setFlash('successSave');
            }
        }

        return $this->render('_form', [
            'model' => $model,
            'courses' => $courses,
            'professor' => User::getAllInArrayMap($this->universityId, UserRole::ROLE_PROFFESOR),
            'majors' => Majors::getAllMajorsInArrayMap($this->universityId),
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionEventCreate($id = null)
    {
        $this->layout = '/modal';
        $model = new ClassesEvents;
        if ($id) {
            $model = $this->findEventsModel($id);
        }
        $model->class_id = Yii::$app->request->get('class');
        $model->user_id = Yii::$app->user->Id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successSave');
        }
        
        return $this->render('_events_create', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionEventShow($id)
    {
        $this->layout = '/modal';
        $model = $this->findEventsModel($id);

        return $this->render('_events_view', [
            'model' => $model,
        ]);
    }
    
    /**
     * Updates an existing Classes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Classes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClassesEventsSearch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClassesEventsSearch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEventsModel($id)
    {
        if (($model = ClassesEvents::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    /**
     * Finds the ClassProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClassProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findProduct($id)
    {
        if (($model = ClassProducts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    /**
     * Finds the Classes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Classes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Classes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModelByName($name)
    {
        if (($model = Classes::findOne(['url_name' => $name])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
