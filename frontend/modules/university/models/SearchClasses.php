<?php

namespace frontend\modules\university\models;

use Yii,
    yii\base\Model,
    yii\data\ActiveDataProvider,
    common\models\university\Classes,
    common\models\university\ClassesRating;

/**
 * SearchClasses represents the model behind the search form of `common\models\university\Classes`.
 */
class SearchClasses extends Classes
{
    public $userId;
    public $rating;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'status', 'creator', 'professor', 'university_id', 'userId', 'rating'], 'integer'],
            [['name', 'description', 'created_at', 'updated_at', 'url_name', 'img_src', 'date_active'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Classes::find();

        if ($this->course_id === 0) {
            $query->where(['course_id' => 0]);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status' => $this->status,
            'creator' => $this->creator,
            'professor' => $this->professor,
            'university_id' => $this->university_id,
            'date_active' => $this->date_active,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'url_name', $this->url_name])
            ->andFilterWhere(['like', 'img_src', $this->img_src]);

        return $dataProvider;
    }
    
    public function searchMyClasses($userId = null)
    {
        $query = Classes::find()
                ->alias('c')
                ->joinWith(['classesFollowModel cf']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->userId = $userId ? $userId : Yii::$app->user->id;
        // grid filtering conditions
        $query->andFilterWhere([
            'OR',
            ['cf.user_id' => $this->userId],
            ['c.professor' => $this->userId]
        ]);

//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'url_name', $this->url_name])
//            ->andFilterWhere(['like', 'img_src', $this->img_src]);

        return $dataProvider;
    }
    
    public function searchClassesRating()
    {
        $query = Classes::find()
                ->alias('c')
                ->select([
                    'c.*',
                    'rating' => '(SELECT SUM(cr.rate_professor) / COUNT(cr.id) FROM ' . ClassesRating::tableName() . ' cr WHERE cr.class_id = c.id)'
                ])
                ->orderBy(['rating' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
