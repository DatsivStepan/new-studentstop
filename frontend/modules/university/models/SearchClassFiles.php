<?php

namespace frontend\modules\university\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\university\Classes;
use common\models\university\ClassFiles;

/**
 * SearchClasses represents the model behind the search form of `common\models\university\Classes`.
 */
class SearchClassFiles extends ClassFiles
{
    public $dateShowFrom;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'class_id', 'typeClass', 'status'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'dateShowFrom'], 'safe'],
            [['file_src', 'name', 'mimeType'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($type = null)
    {
        $type = $type ? $type : ClassFiles::TYPE_CLASS;
        $query = self::find()
                ->alias('cf')
                ->joinWith(['classProductModel cp'])
                ->where(['cp.id' => null]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if ($this->dateShowFrom) {
            $query->andFilterWhere([
                '>', 'cf.created_at', $this->dateShowFrom
            ]);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'cf.id' => $this->id,
            'cf.class_id' => $this->class_id,
            'cf.typeClass' => $type
        ]);

//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'url_name', $this->url_name])
//            ->andFilterWhere(['like', 'img_src', $this->img_src]);

        return $dataProvider;
    }
}
