<?php

namespace frontend\modules\university\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\university\Classes;
use common\models\university\ClassProducts;

/**
 * SearchClasses represents the model behind the search form of `common\models\university\Classes`.
 */
class SearchClassProduct extends ClassProducts
{
    public $class_id;
    public $dateShowFrom;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_id', 'status', 'class_id'], 'integer'],
            [['description'], 'string'],
            [['price_m', 'price_c'], 'number'],
            [['created_at', 'updated_at', 'dateShowFrom'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()
            ->alias('cp')
            ->select(['cp.*'])
            ->joinWith(['classFilesModel cf']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->dateShowFrom) {
            $query->andFilterWhere([
                '>=', 'cp.created_at', $this->dateShowFrom
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
//            'cp.id' => $this->id,
            'cp.class_id' => $this->class_id,
        ]);

//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'url_name', $this->url_name])
//            ->andFilterWhere(['like', 'img_src', $this->img_src]);

        return $dataProvider;
    }
}
