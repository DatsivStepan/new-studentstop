<?php

namespace frontend\modules\university\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\university\VirtualClasses;

/**
 * SearchVirtualClasses represents the model behind the search form about `common\models\university\VirtualClasses`.
 */
class SearchVirtualClasses extends VirtualClasses
{
    public $userId;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'userId', 'tutors_id', 'status', 'price_type', 'price_m', 'price_c'], 'integer'],
            [['name', 'img_src', 'description', 'date_start', 'slug', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = VirtualClasses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'course_id' => $this->course_id,
            'tutors_id' => $this->tutors_id,
            'date_start' => $this->date_start,
            'status' => $this->status,
            'price_type' => $this->price_type,
            'price_m' => $this->price_m,
            'price_c' => $this->price_c,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'img_src', $this->img_src])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
    
    public function searchMyClasses()
    {
        $query = self::find()
                ->alias('c')
                ->joinWith(['classesFollowModel cf']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $userId = Yii::$app->user->id;
        if ($this->userId) {
            $userId = $this->userId;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'cf.user_id' => $userId,
        ]);
//
//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'url_name', $this->url_name])
//            ->andFilterWhere(['like', 'img_src', $this->img_src]);

        return $dataProvider;
    }
}
