<?php

namespace frontend\modules\university\models;

use Yii,
    yii\base\Model,
    yii\data\ActiveDataProvider,
    common\models\University,
    common\models\university\ClassesRating;

/**
 * SearchUniversity represents the model behind the search form of `common\models\University`.
 */
class SearchUniversity extends University
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_owner', 'status', 'country_id', 'region_id'], 'integer'],
            [['name', 'history', 'slug', 'date', 'image', 'location', 'year_founded', 'facebook_link', 'linkedin_link', 'twitter_link', 'telephone', 'email', 'website', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = University::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_owner' => $this->id_owner,
            'status' => $this->status,
            'date' => $this->date,
            'country_id' => $this->country_id,
            'region_id' => $this->region_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'history', $this->history])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'year_founded', $this->year_founded])
            ->andFilterWhere(['like', 'facebook_link', $this->facebook_link])
            ->andFilterWhere(['like', 'linkedin_link', $this->linkedin_link])
            ->andFilterWhere(['like', 'twitter_link', $this->twitter_link])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'website', $this->website]);

        return $dataProvider;
    }

    public function searchByRating()
    {
        $query = self::find()
            ->alias('u')
            ->select([
                'u.*',
                'rating' => '('. ClassesRating::find()
                    ->alias('cr')
                    ->select('SUM(cr.rate_professor) / COUNT(cr.id)')
                    ->joinWith(['classesModel.courseModel.majorModel m'])
                    ->where('m.university_id = u.id')
                    ->createCommand()
                    ->sql . ')'
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
