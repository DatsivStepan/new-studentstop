<?php

namespace frontend\modules\university\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\university\Courses;
use common\models\University;

/**
 * SearchCourses represents the model behind the search form of `common\models\university\Courses`.
 */
class SearchCourses extends Courses
{
    public $profesor;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'major_id', 'status', 'creator', 'profesor'], 'integer'],
            [['name', 'description', 'created_at', 'updated_at', 'url_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $universityId)
    {
        $query = Courses::find()
                ->alias('courses')
                ->joinWith([
                    'majorModel major',
                    'classesModel classes',
                ])
                ->leftJoin(University::tableName() . 'university ', 'university.id = major.university_id')
                ->where(['major.university_id' => $universityId]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'courses.major_id' => $this->major_id,
            'courses.id' => $this->id,
        ])
            ->andFilterWhere(['like', 'courses.name', $this->name]);

        return $dataProvider;
    }
}
