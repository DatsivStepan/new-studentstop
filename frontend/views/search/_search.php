<?php

use yii\helpers\Html,
    yii\helpers\Url,
    yii\widgets\ActiveForm,
    kartik\datetime\DateTimePicker,
    common\models\University,
    common\models\university\Majors,
    common\models\university\Courses,
    kartik\select2\Select2,
    common\models\location\Countries,
    common\models\UserRole,
    kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model frontend\modules\services\models\RentsSearch */
/* @var $form yii\widgets\ActiveForm */

?>

<section class="rents-search">
    <div class="rent-search row">
        
        <?php $form = ActiveForm::begin([
            'method' => 'get',
            'action' => ['/search'],
            'options' => [
                'style' => 'width:100%;'
            ]
        ]); ?>

            <div class="row js-members-search-block">
                <div class="col-sm-4">
                    <?= $form->field($model, 'filterValueM', ['options' => ['class' => 'search-input', 'style' => 'width:100%;']])->textInput(['placeholder' => 'Username'])->label(false) ?>
                </div>

                <div class="col-sm-3">
                    <?= $form->field($model, 'filterUniversityM')->widget(Select2::classname(), [
                        'data' => University::getAllUniversityInArrayMap(),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Select univerisity'),
                            'id' => 'filterUniversityMId'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])->label(false); ?>
                </div>

                <div class="col-sm-3">
                    <?= $form->field($model, 'filterUserStatusM')->widget(Select2::classname(), [
                        'data' => UserRole::getAllRoleInArrayMap(),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Select user status'),
                            'id' => 'filterUserStatusM'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])->label(false); ?>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <div class="button-search-block">
                            <?= Html::submitButton(Yii::t('app', 'Search'), [
                                'class' => 'btn btn-primary btn-search',
                                'style' => 'width:100%;',
                                'name' => 'searchMembers'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row js-university-search-block hide">
                <div class="col-sm-4">
                    <?= $form->field($model, 'filterValueU', ['options' => ['class' => 'search-input', 'style' => 'width:100%;']])->textInput(['placeholder' => 'University name'])->label(false) ?>
                </div>

                <div class="col-sm-3">
                    <?= $form->field($model, 'filterMajorsU')->widget(Select2::classname(), [
                        'data' => Majors::getAllMajorsInArrayMap(),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Select majors'),
                            'id' => 'filterMajorsId'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])->label(false); ?>
                </div>

                <div class="col-sm-3">
                    <?= $form->field($model, 'filterCourseU')->widget(DepDrop::classname(), [
                        'type' => DepDrop::TYPE_SELECT2,
                        'options' => ['id' => 'select-contragent', 'placeholder' => Yii::t('app', 'Select course')],
                        'select2Options' => [
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ],
                        'pluginOptions' => [
                            'initialize' => true,
                            'initDepends' => ['filterMajorsId'],
                            'depends' => ['filterMajorsId'],
                            'url' => Url::to(['university/courses/get-courses-by-major', 'select' => $model->filterCourseU])
                        ]
                    ])->label(false); ?>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <div class="button-search-block">
                        <?= Html::submitButton(Yii::t('app', 'Search'), [
                            'class' => 'btn btn-primary btn-search',
                            'style' => 'width:100%;',
                            'name' => 'searchUniversity'
                        ]) ?>
                        </div>
                    </div>
                </div>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</section>