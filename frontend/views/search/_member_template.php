<?php
use kartik\rating\StarRating,
    yii\helpers\Html,
    common\models\Favorites;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="classif-search-block js-item-content-block" data-id="<?= $model->id; ?>">
    <div class="searc-block-my">
        <img class="image" src="<?= $model->getAvatar(); ?>" style="width:100%;">
        <div class="price-block">
        </div>
    </div>
    <div class="searc-block-bottom">
        <h2><?= $model->userInfoModel->getNameAndSurname(); ?></h2>
        <div class="student-card-status"> <span  style="display: inline-block;
    min-width: 60px;
    color: #989898;
    font-size: 13px;
    font-family: 'Candara';
    font-weight: 400;">Status: </span>
            <span> <?= $model->getRole(); ?></span>
        </div>
        <div class="student-card-rating"> 
            <div class="rating-title" style="display: inline-block;
    min-width: 60px;
    color: #989898;
    font-size: 13px;
    font-family: 'Candara';
    font-weight: 400;">
                Rank: <?= $model->getRankCount(); ?>
            </div>
            <div class="custom-stars">
                <?= StarRating::widget([
                    'name' => 'rating_rent_' . $model->id,
                    'value' => $model->getRating(),
                    'disabled' => true,
                    'pluginOptions' => [
                        'size' => 'xs',
                        'min' => 0,
                        'max' => 5
                    ],
                ]); ?>
            </div>
        </div>
        <div class="row">
            <?= Html::a('Profile', $model->getProfileLink(),
                [
                    'class' => 'view-button btn btn-info js-show-rents-house',
                    'data-id' => $model->id,
                    'style' => 'width:100%;',
                    'target' => '_blank',
                    'data-pjax' => 0,
                ]); ?>

            <?= ($model->id != Yii::$app->user->id) ? Html::button($model->checkFriendRequestType(), [
                'class' => 'view-button btn btn-info  js-friend-request-button',
                'data-user_id' => Yii::$app->user->id,
                'data-friend_id' => $model->id,
                'style' => 'margin-top:10px;width:100%;'
            ]) : ''; ?>
        </div>
    </div>
</div>

