<?php
use kartik\rating\StarRating,
    yii\helpers\Html,
    common\models\Favorites,
    common\models\University;

/* 
 * $model common\models\University
 */

$uDataArray = [
    'id' => $model->id,
    'name' => $model->getName(),
    'address' => $model->getAddress(),
    'telephone' => $model->telephone ? $model->telephone : '',
    'email' => $model->email ? $model->email : '',
    'website' => $model->website ? $model->website : '',
    'friendCount' => $model->getMyFriendCount()
];
?>
<div class="classif-search-block js-item-content-block" data-id="<?= $model->id; ?>" data-u_data='<?= json_encode($uDataArray); ?>'>
    <?= Html::hiddenInput('lat', $model->location_lat, ['class' => 'js-lat']); ?>
    <?= Html::hiddenInput('lng', $model->location_lng, ['class' => 'js-lng']); ?>
    <div class="searc-block-my">
        <img class="image" src="<?= $model->getImages(); ?>" style="width:100%;">
        <div class="price-block">
        </div>
    </div>
    <div class="searc-block-bottom">
        <h2><?= $model->getName(); ?></h2>
        <?= StarRating::widget([
            'name' => 'rating_rent_' . $model->id,
            'value' => $model->getRating(),
            'disabled' => true,
            'pluginOptions' => [
                'size' => 'xs',
                'min' => 0,
                'max' => 5
            ],
        ]); ?>
        <div class="row">
            <div>
                <i><?= $model->getAddress(); ?></i>
            </div>
            <hr style="margin:10px 0px;">
            <div>
                <?= $model->getHistory(72); ?>
            </div>
        </div>
        <div class="row">
            <?= Html::button('Show in Map',
                [
                    'class' => 'view-button btn btn-info js-show-university-on-map',
                    'style' => 'width:100%;',
                    'target' => '_blank',
                    'data-pjax' => 0,
                ]); ?>
        </div>
    </div>
</div>
