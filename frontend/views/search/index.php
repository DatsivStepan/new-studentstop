<?php

use yii\helpers\Html,
    yii\grid\GridView,
    yii\widgets\Pjax,
    kartik\rating\StarRating,
    yii\helpers\Url,
    common\models\Favorites,
    yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\services\models\ClassifiedMarketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyDoSL8IN0GJJOwChniCpdNoPvWQa0c7ZnI&libraries=places', [
//    'async' => true,
//    'defer' => true,
//]);

$this->title = Yii::t('app', 'Search');
$this->params['breadcrumbs'][] = $this->title;

$formSearchMembers = Yii::$app->request->get('searchMembers') !== null ? 1 : 0;
$formSearchUniversity = Yii::$app->request->get('searchUniversity') !== null ? 1 : 0;
?>
<style>
    .cm-favorite-star:hover{
        -ms-transform: scale(1.2, 1.2); /* IE 9 */
        -webkit-transform: scale(1.2, 1.2); /* Safari */
        transform: scale(1.2, 1.2);
        cursor:pointer;
    }
    .rents-right-block{
        width: 322px;
        background: white;
        float: right;
        margin-top: 0;
        transform: translateX(220px);
        transition: transform 0.6s ease-in-out;
        z-index: 10;
        box-shadow: 0 0 20px rgba(28, 43, 66, 0.25);position: fixed;
        top: -10px;
        z-index: 10;
        min-height: 100vh;
        height: 100%;
        right: 209px;
        overflow: auto; padding:0px;
    } 
</style>
<div id="searchBlockContainer">
    <div class="classified-market-index custom-r-h-search">
       <?= $this->render('_search', [
            'model' => $searchModel,
            'button' => true
        ]); ?>
        <div class="classified-market-index custom-my-requests desktop-display-none">
            <section class="rents-menu">
                <ul class="nav nav-tabs">
                    <li class="nav-item" style="width:50%;text-align:center;">
                        <a class="nav-link active">MAP</a>
                    </li>
                    <li class="nav-item" style="width:50%; text-align:center;">
                        <a class="nav-link">INFO</a>
                    </li>
                </ul>
            </section>
        </div>
        <div class="row search-block">
            <div id="map" style="height:100vh;width:100%;">

            </div>
        </div>
    </div>

    <div class="js-tabs-block rents-right-block rents-right-block-c-s">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link text-center js-menu-tab active js-tab-link-members" data-toggle="tab" href="#s_members" role="tab">Members</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-center js-menu-tab js-tab-link-university" data-toggle="tab" href="#s_university" role="tab">University</a>
            </li>
        </ul>

        <div class="tab-content my-scroll clearfix">
            <div class="tab-pane active" id="s_members" role="tabpanel" style="padding:15px;">
                <?php Pjax::begin(['id' => 'pjax-form-members-search', 'enablePushState' => false]); ?>

                    <?php $form = ActiveForm::begin(['id' => 'form-search-members', 'options' => ['data-pjax' => 'pjax-form-members-search']]); ?>
                        <?= $form->field($searchModel, 'filterUniversityMap')->hiddenInput(); ?>
                    <?php ActiveForm::end(); ?>

                    <?php if ($members = $dataMembersProvider->getModels()) { ?>
                        <?php foreach ($members as $member) { ?>
                            <?= $this->render('_member_template', ['model' => $member])?>
                            <hr>
                        <?php } ?>
                    <?php } else { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>

                <?php Pjax::end(); ?>
            </div>

            <div class="tab-pane my-scroll" id="s_university" role="tabpanel" style="padding:15px;">
                <?php if ($universities = $dataUniversityProvider->getModels()) { ?>
                    <?php foreach ($universities as $university) { ?>
                        <?= $this->render('_university_template', ['model' => $university])?>
                        <hr>
                    <?php } ?>
                <?php } else { ?>
                    <p class="text-center">Not found</p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script>
    var map;
    var search = {
        initAutocomplete: function() {
            var mapOptions = {
                center: {lat: 37.2755783, lng: -104.6571311},
                zoom: 6,
            };
            map = new google.maps.Map(document.getElementById('map'), mapOptions);
        },
        showUniversityOnMap: function(lat, lng) {
            var latLng = {lat:parseFloat(lat),lng:parseFloat(lng)};
            map.setCenter(latLng);
            map.setZoom(12);
            $('html, body').animate({ scrollTop: 0 }, 'slow', function () { });
        },
        loadUniversityMarkers: function() {
            $("#s_university .js-item-content-block").each(function (index) {
                var lat = $(this).find('.js-lat').val()
                var lng = $(this).find('.js-lng').val()
                var universityData = $(this).data('u_data');
                var userCount = universityData.friendCount;

                if (lat && lng) {
                    var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
                    var image = {
                        url: '/images/icon/university_map_icon.png',
                        scaledSize: new google.maps.Size(32,34)
//                        labelOrigin: new google.maps.Point(20, 12)
                    };

                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        labelClass: "marker-labels",
                        zIndex:999+parseInt(userCount),
                        icon: image,
                        zIndex: 1
                    });
                    var phone = '';
                    if (universityData.telephone) {
                        phone = '<p>Telefone - ' + universityData.telephone + '</p>';
                    }
                    var website = '';
                    if (universityData.website) {
                        website = '<p>Website - ' + universityData.website + '</p>';
                    }
                    var email = '';
                    if (universityData.email) {
                        email = '<p>Email - ' + universityData.email + '</p>';
                    }
                    var infowindow = new google.maps.InfoWindow({
                        content: '<div>'+
                                    '<h3>' + universityData.name + '</h3>'+
                                    '<p><i>' + universityData.address + '</i></p>'+
                                    website +
                                    phone +
                                    email +
                                    '<button class="btn btn-xs btn-primary js-show-university-friends" style="width:100%;" data-university_id=' + universityData.id + '>Show friends</button>'+
                                '</div>'
                    });

                    
                    var zoom = map.getZoom();
                    marker.setVisible(zoom >= 7);

                    var icon2 = {
                        url:'/images/icon/default_university_count2.png',
                        scaledSize: new google.maps.Size(18,18)
                    };
                    var marker2 = '';
                    marker2 = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        zIndex:999+parseInt(userCount)+1,
                        label:{
                            text:userCount,
                            color:'white',
                        },
                        icon:icon2,
                    });
                    marker2.addListener('click', function() {
                        infowindow.open(map, marker);
                    });

                    var zoom = map.getZoom();
                    marker2.setVisible(zoom >= 7);
                    
                    
                    google.maps.event.addListener(map, 'zoom_changed', function() {
                        var zoom = map.getZoom();
                        marker2.setVisible(zoom >= 7);
                        marker.setVisible(zoom >= 7);
                    });

                    var icon3 = {
                        url:'/images/icon/default_university_count2.png',
                        scaledSize: new google.maps.Size(32,34)
                    };
                    var marker3 = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        label: {
                            text: userCount ,
                            color: 'white',
                            fontSize: '16px',
//                            fontWeight: "bold"
                        },
                        zIndex:999+parseInt(userCount),
                        icon:icon3
                    });
                    google.maps.event.addListener(map, 'zoom_changed', function() {
                        var zoom = map.getZoom();
                        marker3.setVisible(zoom < 7);
                    });
                }
                map.setZoom(12);
                if (latlng) {
                    map.setCenter(latlng);
                }
            })  
        },
        filterFriendMembers: function(uId) {
            $('#mainsearch-filteruniversitymap').val(uId);
            $('#form-search-members').submit();
            $('.js-tab-link-members').trigger('click')
        },
        checkHash: function() {
            var hash = window.location.hash;
            if (hash == '#s_members') {
                $('.js-university-search-block').addClass('hide')
                $('.js-members-search-block').removeClass('hide')
            } else if (hash == '#s_university') {
                $('.js-university-search-block').removeClass('hide')
                $('.js-members-search-block').addClass('hide')
            } else if ('<?= $formSearchMembers; ?>' == 1) {
                hash = '#s_members';
                $('.js-university-search-block').addClass('hide')
                $('.js-members-search-block').removeClass('hide')
            } else if ('<?= $formSearchUniversity; ?>' == 1) {
                hash = '#s_university';
                $('.js-university-search-block').removeClass('hide')
                $('.js-members-search-block').addClass('hide')
            }
            
            hash && $('ul.nav a.js-menu-tab[href="' + hash + '"]').tab('show');
        },
    };

    $(function () {
        search.initAutocomplete();
        search.loadUniversityMarkers();
        
        
        search.checkHash();
        $(window).on('hashchange',function(){ 
            search.checkHash();
        });

        $('ul.nav a.js-menu-tab').click(function (e) {
            $(this).tab('show');
            window.location.hash = this.hash;
            console.log(this.hash);
            if (this.hash == '#s_members') {
                $('.js-university-search-block').addClass('hide')
                $('.js-members-search-block').removeClass('hide')
            } else {
                $('.js-university-search-block').removeClass('hide')
                $('.js-members-search-block').addClass('hide')
            }
            $('html,body').scrollTop(0);
            $('.js-tabs-block').scrollTop(0);
        });

        $('#searchBlockContainer').on('click', '.js-show-university-friends', function(){
            var uId = $(this).data('university_id')
            search.filterFriendMembers(uId);
        });

        $(document).on('click', '.js-show-university-on-map', function(){
            var parentBlock = $(this).closest('.js-item-content-block')
            var lat = parentBlock.find('.js-lat').val();
            var lng = parentBlock.find('.js-lng').val();

            search.showUniversityOnMap(lat, lng);
        })
    })
</script>