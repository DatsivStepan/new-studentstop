<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\MobileAsset;
use common\widgets\Alert;
use frontend\widgets\WLang;

AppAsset::register($this);

if(\Yii::$app->devicedetect->isMobile() && !(\Yii::$app->devicedetect->isTablet())){
    MobileAsset::register($this);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

  <main class="container-fluid auth-bg clearfix">
    <div class="container">
      <div class="row justify-content-between head-box">
        <div class="col-auto align-self-center">
            <a href="/" ><img style="height: 35px;" src="/images/logo-white.png" alt="login"></a>
        </div>
        <div class="col-auto align-self-center">
            <?php if (Yii::$app->user->isGuest) { ?>
                <button class="auth-cnange-btn">
                    <span class="d-none active">Sign up</span>
                    <span class="d-none">Sign in</span>
                </button>
            <?php } else { ?>
                <?= Html::beginForm(['/site/logout'], 'post'); ?>
                    <?= Html::submitButton(
                        '<span class="d-none active">Logout</span>',
                        ['class' => 'auth-cnange-btn']
                    ); ?>
                <?= Html::endForm() ?>
            <?php } ?>
        </div>
      </div>
        <?= $content ?>
    </div>
  </main>
</div>

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
