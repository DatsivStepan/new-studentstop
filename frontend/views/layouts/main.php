<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\helpers\Url;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\MobileAsset;
use frontend\widgets\WLang;

AppAsset::register($this);
if(\Yii::$app->devicedetect->isMobile() && !(\Yii::$app->devicedetect->isTablet())){
  MobileAsset::register($this);
}
$modelUser = Yii::$app->user->identity;
$menuType = array_key_exists('menuType', Yii::$app->params) ? Yii::$app->params['menuType'] : null;
?>
  <?php $this->beginPage() ?>
  <!DOCTYPE html>
  <html lang="<?= Yii::$app->language ?>">

  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?php 
    $viewWidth = '1140';
    if(\Yii::$app->devicedetect->isMobile() && !(\Yii::$app->devicedetect->isTablet())){ 
      $viewWidth = 'device-width';
    }?>
    <meta name="viewport" content="width=<?=$viewWidth?>, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
      <title>
        <?= Html::encode($this->title) ?>
      </title>
      <?php $this->head() ?>
  </head>

  <body>
    <?php $this->beginBody() ?>
    <div style="display:none;">
      <?php NavBar::begin(); NavBar::end(); ?>
    </div>
    <div class="wrap fixed-sidebar fixed-header <?= $menuType ? 'fixed-right-menu': ''; ?>">
      <?= $this->render('//site/_header', ['user' => $modelUser]); ?>
        <div class="wrap-limiter">
            <?= $this->render('//site/_left_sidebar', ['userInfo' => $modelUser && $modelUser->userInfoModel ? $modelUser->userInfoModel : null]); ?>
            <main class="main-content">
              <?= $content ?>
            </main>
            <?php if ($menuType) { ?>
                <?= $this->render('//site/_right_menu', ['menuType' => $menuType]); ?>
            <?php } ?>
        </div>
    </div>
    <?php $this->endBody() ?>

    <?php if (Yii::$app->session->hasFlash('userActivated')) { ?>
        <script>
            $(function () {
                swal({
                    title: "Congratulations! Your registration is confirmed!",
                    text: "You’ve successfully completed registration for The Student Stop",
                    html: true,
                    icon: "success",
                    confirmButtonText: "Start Browsing",
    //                button: "Start Browsing",
                  });
            })
        </script>
    <?php } ?>

  </body>

  </html>
  <?php $this->endPage() ?>