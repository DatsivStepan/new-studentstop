<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\helpers\Url;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\MobileAsset;
use frontend\widgets\WLang;

AppAsset::register($this);

if(\Yii::$app->devicedetect->isMobile() && !(\Yii::$app->devicedetect->isTablet())){
  MobileAsset::register($this);
}
$modelUser = Yii::$app->user->identity;
$menuType = array_key_exists('menuType', Yii::$app->params) ? Yii::$app->params['menuType'] : null;
?>
  <?php $this->beginPage() ?>
  <!DOCTYPE html>
  <html lang="<?= Yii::$app->language ?>">

  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?php 
    $viewWidth = '1140';
    if(\Yii::$app->devicedetect->isMobile()){ 
      $viewWidth = 'device-width';
    }?>
    <meta name="viewport" content="width=<?=$viewWidth?>, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
      <title>
        <?= Html::encode($this->title) ?>
      </title>
      <?php $this->head() ?>
  </head>

  <body>
    <?php $this->beginBody() ?>
      <div style="display:none;">
          <?php NavBar::begin(); NavBar::end(); ?>
      </div>

      <?= $content ?>
    <?php $this->endBody() ?>

  </body>

  </html>
  <?php $this->endPage() ?>