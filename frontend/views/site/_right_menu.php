<?php
use yii\helpers\Html,
    yii\helpers\Url,
    common\models\MenuItems;

$menuItems = MenuItems::getAllItemByType($menuType);

$this->registerJsFile(Url::to(['/js/js_for_menu.js']));
?>
<aside class="right-sidebar">
    <div class="open-panel-btn m-device-show"></div>
    <div class="my-scroll">
        <?php foreach ($menuItems as $item) {  ?>
            
            <?php 
                $url = $item->url;
                switch ($menuType) {
                    case MenuItems::TYPE_PROFILE:
                        if (Yii::$app->request->get('id')){
                            $url .= '/' . Yii::$app->request->get('id');
                        }
                        break;
                    case MenuItems::TYPE_CLASS_ONE:
                        $url = str_replace("#name", Yii::$app->request->get('name'), $url);;
                        break;

                    default:
                        break;
                }
            ?>
            <?php $link = HTML::a(
                '<img src="' . $item->getImages() . '"/>' . $item->name, 
                Url::to([$url]), 
                [
                    'class' => $item->class_attr,
                    'data-id' => $menuType == MenuItems::TYPE_PROFILE ? Yii::$app->request->get('id') : null,
                    'data-user_id' => $item->class_attr && !Yii::$app->user->isGuest ? Yii::$app->user->id : null
                ]);
            
                    switch ($menuType) {
                       case MenuItems::TYPE_PROFILE:
                            if (Yii::$app->request->get('id') && (Yii::$app->request->get('id') != Yii::$app->user->id) && (in_array($item->name, MenuItems::$arrayProfilePageNotShow))) {
                                $link = '';
                            }
                            break;
                    }
                ?>
                <?= $link; ?>
        <?php } ?>
    </div>
</aside>
<script>
    $(function(){
        $('.open-panel-btn').click(function(e){
            e.preventDefault();
            $('aside.right-sidebar').toggleClass('open');
        });
        
        function showProfileAbout(id, user_id) {
            if (id && (id != user_id)) {
                $.dialog({
                    id: 'userAboutModal',
                    content: 'url:/profile/profile/about/' + id,
                    columnClass: 'm',
                });
            } else {
                $.confirm({
                    id: 'userAboutModal',
                    content: 'url:/profile/profile/about',
                    columnClass: 'm',
                    buttons: {
                        close: {
                            btnClass: 'hide',
                        },
                        ok: {
                            text: "Save"
                        }
                    }
                });
            }
        }
        if ($('.js-open-about-profile').length) {
            if (<?= Yii::$app->request->get('openPopup', 0); ?>) {
                showProfileAbout($('.js-open-about-profile').data('id'), $('.js-open-about-profile').data('user_id'));
            }
        
            $(document).on('click', '.js-open-about-profile', function(e) {
                e.preventDefault();
                showProfileAbout($(this).data('id'), $(this).data('user_id'));
            });
        }
    });
</script>