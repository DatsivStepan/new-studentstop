<?php
use yii\helpers\Html,
    kartik\rating\StarRating;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$university = $userInfo && $userInfo->universityModel ? $userInfo->universityModel : null;
if ($university) { 
?>
    <style>
            .rating-container .caption .label {
                    display: none !important;
            }

            .rating-container .clear-rating {
                    display: none !important;
            }
    </style>
    <aside class="left-sidebar">
        <div class="row no-gutters academy-data">
            <div class="col-12">
                <img src="<?= $university->getImages(); ?>" class="academy-logo">
                <div class="academy-name">
                    <?= $university->getName(); ?>
                </div>
                <div class="academy-rating">
                    <div class="academy-rating-title">
                        Rank: <?= $university->getRate(); ?>
                    </div>
                    <div class="custom-stars">
                        <?= StarRating::widget([
                            'name' => 'rating_20',
                            'value' => $university->getRating(),
                            'disabled' => true,
                            'pluginOptions' => [
                                'size' => 'xs',
                                'min' => 0,
                                'max' => 5
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-12 academy-search">
                <input type="text" placeholder="Search" class="search_friends"/>
            </div>
            <div class="col-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link text-center active" style="font-size:12px;padding: 0 7px !important;" data-toggle="tab" href="#friends" role="tab">Friends</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-center" style="font-size:12px;padding: 0 7px !important;" data-toggle="tab" href="#class" role="tab">Classmates</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-center" style="font-size:12px;padding: 0 7px !important;" data-toggle="tab" href="#alumni" role="tab">Alumni</a>
                    </li>
                </ul>
                <div class="tab-content my-scroll clearfix">
                    <div class="tab-pane active" id="friends" role="tabpanel">
                        <p class="text-center">NO FRIENDS</p>
                    </div>
                    <div class="tab-pane my-scroll" id="class" role="tabpanel">
                        <p class="text-center">NO CLASSMATES</p>
                    </div>
                    <div class="tab-pane my-scroll" id="alumni" role="tabpanel">
                        <p class="text-center">NO ALUMNI</p>
                    </div>
                </div>
            </div>
        </div>
    </aside>
    <script>
        $(function() {
            function searchFriends(search_value) {
                $.ajax({
                    method: 'post',
                    url: '/profile/profile/get-my-friends',
                    dataType: 'json',
                    data: {value:search_value}
                }).done(function (response) {
                    if (response.status) {
                        var friends = response.data;
                        $('#friends').html('');
                        $.each(friends, function( key, friend ) {
                            $('#friends').append("<div class='student-card-avatar' style='background: url(" + friend.avatar + ") center/cover no-repeat;width:20px;height:20px;'></div>" +
                                    "<a href='" + friend.link + "'>" + friend.name + "</a><hr>");
                        })
                    } else {
                        $('#friends').html('<p class="text-center">NO FRIENDS</p>');
                    }
                });
            }

            $(document).on('keyup', '.search_friends', function(){
                searchFriends($(this).val())
            })

            searchFriends('')
        })
    </script>
<?php } ?>