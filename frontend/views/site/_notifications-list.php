<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\business\models\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<style>
    #notificationsList tr th {
        font-size:11px !important;
        padding:5px !important;
    }

    #notificationsList tr td {
        font-size:11px !important;
        padding:5px !important;
    }
</style>
<div class="notifications-list">
    <?= GridView::widget([
        'id' => 'notificationsList',
        'dataProvider' => $dataProvider,
        'pjax' => true,
        'panel' => [],
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'module',
                'value' => function ($model) {
                    return $model->notificationsModel->module;
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Price',
                'attribute' => 'text',
                'value' => function ($model) {
                    return $model->notificationsModel->text;
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Created',
                'attribute' => 'created_at'
            ],
        ],
    ]); ?>
</div>