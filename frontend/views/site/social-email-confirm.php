<?php

use yii\bootstrap\Html,
    yii\bootstrap\ActiveForm,
    yii\helpers\Url,
    yii\bootstrap\Alert;

$this->title = 'Welcome ' . ($userInfoModel ? $userInfoModel->getNameAndSurname() : $model->email . '!');
?>
<style>
    .block-welcome {
        background-color: white;
        border-radius: 10px;
        padding:15px;
        margin-top: 30px;
    }

    .block-welcome .header-text-sub{
        text-align: center;
        color: rgba(22,19,56, 0.8);
        font-size: 16px;
        font-family: 'Candara Bold';
        font-weight: 400;
        letter-spacing: 0.6px;
    }
    
    .block-welcome .header-text {
        text-align: center;
        color:#161338;
        font-size: 26px;
        font-weight: 700;
    }
    
    .block-welcome .header-text-sub {
        color: rgba(22,19,56, 0.8);
        font-size: 16px;
        font-family: 'Candara Bold';
    }
    .block-welcome label {
        font-family: 'Candara Bold';
    }
    .block-welcome .select2-selection__clear {
        display:none;
    }
</style>
<div class="row">
    <div class="block-welcome col-sm-8 offset-sm-2">
        <?php if (Yii::$app->session->hasFlash('emailSended')) { ?>
            <?= Alert::widget([
                'options' => ['class' => 'alert-success'],
                'body' => 'Email sended again!',
            ]) ?>
        <?php } ?>
        <h6 class="text-center">Please confirm your .edu email to finish registration</h6>

        <?php $form = ActiveForm::begin(); ?>
            <div class="text-center" style="margin-top:30px;">
                <?php $model->social_email = preg_match('/.com$/', $model->social_email) ? $model->social_email : ''; ?>
                <?= $form->field($model, 'social_email')->textInput([
                  'type' => 'email',
                  'class' => 'form-control ',
                  'placeholder' => 'Email...'
                ])->label(false) ?>
                <br>
                <?= Html::submitButton('Resend Email', [
                    'name' => 'send-confirm-to-email',
                    'class' => 'btn btn-info pull-right',
                    'style' => "background-color:#31e7c5;border-radius:50px;border:0px solid transparent;padding:15px 40px;font-weight: bold;text-transform: uppercase;font-family: 'Candara Bold';",
                ]); ?>
            </div>
        <?php $form = ActiveForm::end(); ?>

    </div>
</div>