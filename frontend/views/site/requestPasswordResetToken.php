<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Forgot Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .block-welcome {
        background-color: white;
        border-radius: 10px;
        padding:15px;
        margin-top: 30px;
    }

    .block-welcome .header-text-sub{
        text-align: center;
        color: rgba(22,19,56, 0.8);
        font-size: 16px;
        font-family: 'Candara Bold';
        font-weight: 400;
        letter-spacing: 0.6px;
    }
    
    .block-welcome .header-text {
        text-align: center;
        color:#161338;
        font-size: 26px;
        font-weight: 700;
    }
    
    .block-welcome .header-text-sub {
        color: rgba(22,19,56, 0.8);
        font-size: 16px;
        font-family: 'Candara Bold';
    }
    .block-welcome label {
        font-family: 'Candara Bold';
    }
    .block-welcome .select2-selection__clear {
        display:none;
    }
</style>
<div class="row">
    <div class="block-welcome col-sm-8 offset-sm-2">
        <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

        <p class="text-center">Enter your email and we’ll send you a link to reset your password.</p>

        <div class="row">
            <div class="col-sm-12">
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                    <?= $form->field($model, 'email')->textInput([
                        'autofocus' => true,
                        'placeholder' => 'Email',
                    ])->label(false) ?>

                    <div class="text-center" style="margin-top:30px;">
                        <?= Html::submitButton('Reset my password', [
                            'name' => 'send-confirm-to-email',
                            'class' => 'btn btn-info pull-right',
                            'style' => "background-color:#31e7c5;border-radius:50px;border:0px solid transparent;padding:15px 40px;font-weight: bold;text-transform: uppercase;font-family: 'Candara Bold';",
                        ]); ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
