<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\UserRole;
use common\models\University;
use kartik\select2\Select2;
use yii\bootstrap\Alert;

$this->title = 'Welcome ' . $model->getNameAndSurname() . '!';
?>
<style>
    .block-welcome {
        background-color: white;
        border-radius: 10px;
        padding:15px;
        margin-top: 30px;
    }

    .block-welcome .header-text-sub{
        text-align: center;
        color: rgba(22,19,56, 0.8);
        font-size: 16px;
        font-family: 'Candara Bold';
        font-weight: 400;
        letter-spacing: 0.6px;
    }
    
    .block-welcome .header-text {
        text-align: center;
        color:#161338;
        font-size: 26px;
        font-weight: 700;
    }
    
    .block-welcome .header-text-sub {
        color: rgba(22,19,56, 0.8);
        font-size: 16px;
        font-family: 'Candara Bold';
    }
    .block-welcome label {
        font-family: 'Candara Bold';
    }
    .block-welcome .select2-selection__clear {
        display:none;
    }
</style>
<div class="row">
    <div class="block-welcome col-sm-8 offset-sm-2">
        <?php if (Yii::$app->session->hasFlash('userActive')) { ?>
            <?= Alert::widget([
                'options' => ['class' => 'alert-success'],
                'body' => 'User activated!',
            ]) ?>
        <?php } ?>
        <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-sm-10 offset-sm-1">
                    <h1 class="header-text text-center" style="margin-top: 30px"><?= $this->title; ?></h1>
                    <p class="header-text-sub" style="margin-bottom: 0px">You are going to use StudentStop platform.</p>
                    <p class="header-text-sub">Pleace read terms and condition and confirm you promary network University</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 offset-sm-1">
                    <div class="row">
                        <div class="col col-md-6">
                            <?= $form->field($model, 'university_id')->widget(Select2::classname(), [
                                'data' => University::getAllUniversityInArrayMap(),
                                'options' => ['placeholder' => Yii::t('app', 'Select...')],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'allowClear' => true
                                ]
                            ]) ?>
                        </div>
                        <div class="col col-md-6">
                            <?= $form->field($model, 'user_role_id')->widget(Select2::classname(), [
                                'data' => UserRole::getAllRoleInArrayMap(),
                                'options' => ['placeholder' => Yii::t('app', 'Select...')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ]
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <iframe width="60%" style="margin:0 auto;" height="360" src="https://www.youtube.com/embed/xFa2_PVMeDQ" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="row" style="margin-top:20px;">
                <div class="col col-sm-12 text-center text-terms" >
                    <input type="checkbox" name="option2" required="required" value="a2" style="color:#161338;"> Terms and conditions of Student Stop LLC
                </div>
                <div class="col col-sm-12 text-center" style="padding-top: 10px">
                    <?= Html::submitButton('Accept & Continue', [
                        'class' => 'btn btn-info pull-right',
                        'style' => "background-color:#31e7c5;border-radius:50px;border:0px solid transparent;padding:15px 40px;font-weight: bold;text-transform: uppercase;font-family: 'Candara Bold';",
                    ]); ?>
                </div>
            </div>

        <?php $form = ActiveForm::end(); ?>
    </div>
</div>