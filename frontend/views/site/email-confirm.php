<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\UserRole;
use common\models\University;
use kartik\select2\Select2;
use yii\bootstrap\Alert;

$this->title = 'Welcome ' . ($userInfoModel ? $userInfoModel->getNameAndSurname() : $model->email . '!');
?>
<style>
    .block-welcome {
        background-color: white;
        border-radius: 10px;
        padding:15px;
        margin-top: 30px;
    }

    .block-welcome .header-text-sub{
        text-align: center;
        color: rgba(22,19,56, 0.8);
        font-size: 16px;
        font-family: 'Candara Bold';
        font-weight: 400;
        letter-spacing: 0.6px;
    }
    
    .block-welcome .header-text {
        text-align: center;
        color:#161338;
        font-size: 26px;
        font-weight: 700;
    }
    
    .block-welcome .header-text-sub {
        color: rgba(22,19,56, 0.8);
        font-size: 16px;
        font-family: 'Candara Bold';
    }
    .block-welcome label {
        font-family: 'Candara Bold';
    }
    .block-welcome .select2-selection__clear {
        display:none;
    }
</style>
<div class="row">
    <div class="block-welcome col-sm-8 offset-sm-2">
        <?php if (Yii::$app->session->hasFlash('emailSended')) { ?>
            <?= Alert::widget([
                'options' => ['class' => 'alert-success'],
                'body' => 'Email sended again!',
            ]) ?>
        <?php } ?>
        <h5 class="text-center">Thank You!</h5>
        <h6 class="text-center">Please check your email to activate your account</h6>

        <?php $form = ActiveForm::begin(); ?>
            <div class="text-center" style="margin-top:30px;">
                <?= Html::submitButton('Resend Email', [
                    'name' => 'send-confirm-to-email',
                    'class' => 'btn btn-info pull-right',
                    'style' => "background-color:#31e7c5;border-radius:50px;border:0px solid transparent;padding:15px 40px;font-weight: bold;text-transform: uppercase;font-family: 'Candara Bold';",
                ]); ?>
            </div>
        <?php $form = ActiveForm::end(); ?>

    </div>
</div>