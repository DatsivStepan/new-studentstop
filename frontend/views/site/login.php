<?php

use yii\helpers\Html,
    yii\bootstrap\ActiveForm,
    yii\helpers\Url,
    common\models\University,
    common\models\UserRole,
    nodge\eauth\Widget,
    yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .form-group.has-danger, .form-group.has-success{
        position:relative;
    }
    .form-group p.text-danger{
        display:none;
    }
    .form-group.has-danger:before {
        content:'';
        position:absolute;
        width:20px;
        height:20px;
        right: 30px;
        top: 24px;
        background-image: url('/images/icon/form_danger_icon.png');
        background-size: contain;
        opacity: 0.8;
    }
    .form-group.has-success:before {
        content:'';
        position:absolute;
        width:20px;
        height:20px;
        right: 30px;
        top: 24px;
        background-image: url('/images/icon/form_success_icon.png');
        background-size: contain;
        opacity: 0.8;
    }
</style>
      <div class="row auth-box align-items-center">
        <div class="col-md-4 offset-md-2 col-sm-5 offset-sm-1 d-none d-sm-block">
          <img src="/images/phones-img.png" alt="App" class="app-preview">
          <div class="row justify-content-center apps-btn">
            <div class="col-auto"><img src="/images/google-store.png" alt="Play Market"></div>
            <div class="col-auto"><img src="/images/apple-store.png" alt="App Store"></div>
          </div>
        </div>
        <div class="col-md-4 offset-md-1 col-sm-5 offset-sm-1 col-10 offset-1 auth-form d-none active">
            <?php if (Yii::$app->session->hasFlash('userActive')) { ?>
                <?= Alert::widget([
                    'options' => ['class' => 'alert-success'],
                    'body' => 'User activated!',
                ]) ?>
            <?php } ?>
          <div class="auth-form-head text-center">
            You are student?
            <p>Studentstop created for you 
            </p>
          </div>
          <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'fieldConfig' => [
                'template' => "{label}{input}{error}{hint}"
            ],
          ]); ?>
            <div class="field-group">
                <?= $form->field($model, 'email')->textInput([
                  'type' => 'email',
                  'class' => 'form-control ',
                  'placeholder' => 'test@domain.com'
                ])->label(false) ?>

              <?= $form->field($model, 'password')->passwordInput([
                'class' => 'form-control ',
                'placeholder' => '********'
              ])->label(false) ?>
            </div>
            <div class="text-right">
                <?= Html::a('Forgot password?', ['site/request-password-reset'], ['style' => 'color:white;']) ?>
            </div>
            <button type="submit" class="btn sign-up-btn js-login-btn">Login <span class="fa fa-spinner fa-spin hide js-spin-block"></span></button>
            <!-- <div class="form-buttons clearfix" style="margin-top: 0px;display:none;height:0px;">
              <label class="pull-left" style="margin-top: 0px">
                <?= $form->field($model, 'rememberMe')->checkbox()->label(false) ?>
                  <span>Remember me</span>
              </label>
            </div> -->
          <?php ActiveForm::end() ?>
          <div class="or-socials">
            or
            <?= Widget::widget(['action' => 'site/login']); ?>
          </div>       
        </div>
        <div class="col-md-4 offset-md-1 col-sm-5 offset-sm-1 col-10 offset-1 auth-form d-none sign-up">
          <div class="auth-form-head text-center">
            You are student?
            <p>Studentstop created for you</p>
          </div> 
          <?php $form = ActiveForm::begin([
            'fieldConfig' => [
              'template' => "{label}{input}{error}{hint}",
            ],
            // 'enableClientValidation' => true,
            'enableAjaxValidation' => true,
            'action' => Url::to('/site/signup', true)
          ]); ?>
          <div class="field-group">
            <?= $form->field($modelSignUp, 'name')->textInput([
              'type' => 'text',
              'class' => 'form-control',
              'placeholder' => 'First name'
            ])->label(false) ?>

            <?= $form->field($modelSignUp, 'surname')->textInput([
              'type' => 'text',
              'class' => 'form-control',
              'placeholder' => 'Last name'
            ])->label(false) ?>

            <?= $form->field($modelSignUp, 'email', [
              'validateOnChange' => true
            ])->textInput([
              'type' => 'email',
              'class' => 'form-control',
              'placeholder' => 'E-mail'
            ])->label(false) ?>

            <?= $form->field($modelSignUp, 'password')->passwordInput([
              'class' => 'form-control',
              'placeholder' => 'Password'
            ])->label(false) ?>
              
            <?= $form->field($modelSignUp, 'password_repeat')->passwordInput([
              'class' => 'form-control',
              'placeholder' => 'Confirm password'
            ])->label(false) ?>


            <?= $form->field($modelSignUp, 'user_role_id')->dropDownList(UserRole::getAllRoleInArrayMap(), [
              'class' => 'form-control'
            ])->label(false) ?>

            <?= $form->field($modelSignUp, 'university_id')->dropDownList(University::getAllUniversityInArrayMap(), [
              'class' => 'form-control'
            ])->label(false) ?>
          </div>

          <button type="submit" class="btn sign-up-btn js-signup-btn">Sign Up <span class="fa fa-spinner fa-spin hide js-spin-block"></span></button>
          <!-- <a class="btn btn-white pull-left show-pane-login" style="width:100%;font-size:14px;">Back to login</a> -->

          <?php ActiveForm::end() ?>
          <div class="or-socials">
            or
            <?= Widget::widget(['action' => 'site/login']); ?>
          </div>       
        </div>
      </div>
    

  <script>
    $(document).ready(function () {
        $(document).on('click', '.auth-cnange-btn', function (e) {
            $(this).children('span').toggleClass('active');
            $(".auth-form").toggleClass('active');
        })

        $(document).on('click', '.js-signup-btn, js-login-btn', function () {
            var thisElement = $(this);
            thisElement.prop({disabled: true});
            thisElement.find('.js-spin-block').removeClass('hide');
            thisElement.closest('form').submit();
            setTimeout(function () {
                thisElement.prop({disabled: false});
                thisElement.find('.js-spin-block').addClass('hide');
            }, 4000);
        })
    })
  </script>