<?php
use yii\helpers\Html,
    yii\helpers\Url,
    common\models\MenuItems;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$userInfo = $user->userInfoModel;
?>
<header class="bg-violet site-header">
    <div class="wrap-limiter" style="overflow: initial;">
        <nav class="navbar">
            <div class="menu-toggler m-device-show">
                <div class="line"></div>
            </div>
            <a href="<?= Url::home(); ?>" class="navbar-brand">
                <img style="height: 35px;" src="<?= Url::to('images/logo-white.png',true) ?>" class="mr-nav-first-menu-brand">
            </a>
            <?php if($user) { ?>
                <div class="main-menu-profile js-header-menu-profile">
                    <ul class="navbar-nav">
                        <li class="nav-item notification-icon js-notification-header">
                            <a class="nav-link" href="#">
                                <span class="badge js-notification-count">0</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle profile-avatar" src="<?= $userInfo ? $userInfo->getAvatar() : ''; ?>">
                                <span class="m-device-hide"><?= $user->getNameAndSurname(); ?></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <?php foreach (MenuItems::getAllItemByType(MenuItems::TYPE_PROFILE_HEADER) as $item) { ?>
                                    <a class="dropdown-item" href="<?= $item->url; ?>">
                                            <?= $item->name; ?>
                                    </a>
                                    <?php } ?>
                                    <a class="dropdown-item" href="/profile/credits">Credits (<?= $user->getCreditsAmount(); ?>)</a>
                                    <a class="dropdown-item" href="/profile/money">Money (<?= $user->getMoneyAmount(); ?>)</a>
                                    <a class="dropdown-item" href="">Level</a>
                                    <div class="dropdown-divider"></div>
                                    <?= Html::beginForm(['/site/logout'], 'post'); ?>
                                        <?= Html::submitButton(
                                            'Logout',
                                            ['class' => 'auth-cnange-btn dropdown-item']
                                    ); ?>
                                    <?= Html::endForm() ?>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="main-menu">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <div class="close-menu m-device-show"></div>
                        </li>
                        <?php foreach (MenuItems::getAllItemByType(MenuItems::TYPE_HOME) as $item) { ?>
                            <li class="nav-item dropdown <?= $item->url; ?>-icon">
                                <a class="nav-link hover dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?= $item->url == 'university' && $userInfo ? $userInfo->getUniversityName() : $item->name; ?>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <?php if ($item->getChildItem()) { ?>
                                        <?php foreach ($item->getChildItem() as $childItem) { ?>
                                            <?= Html::a($childItem->name, Url::to(['/' . $childItem->url]), [
                                                'class' => "dropdown-item"
                                            ])?>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if (($item->url == 'university') && $userInfo && $userInfo->isUniversityManager()) { ?>
                                        <?= Html::a('Setting', Url::to(['/university/setting']), [
                                            'class' => "dropdown-item"
                                        ])?>
                                    <?php } ?>
                                </div>
                            </li>
                        <?php } ?>
                        <li class="nav-item message-icon">
                            <a class="nav-link hover" href="#">Message</a>
                        </li>
                    </ul>
                </div>
            <?php } ?>
        </nav>
    </div>
</header>
<?php if (Yii::$app->session->hasFlash('returnPaymentResultSuccess')) { ?>
    <script>
        swal({
            title: "Success!",
            text: "Payment has passed successfully!",
            icon: "success",
        })
    </script>
<?php } ?>

<script>
    $(function(){
        $('.menu-toggler').click(function(){
            document.body.classList.add('open-menu');
            $('.main-menu').toggleClass('show');
        });
        $('.close-menu').click(function(){
            document.body.classList.remove('open-menu');
            $('.main-menu').toggleClass('show');
        });
    });
</script>