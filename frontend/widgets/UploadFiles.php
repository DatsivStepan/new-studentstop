<?php

namespace frontend\widgets;

use common\models\Post;
use common\models\User;
use yii\base\InvalidConfigException;
use yii\base\Widget;

class UploadFiles extends Widget
{
    public $folderType = null;
    public $filesArray = null;
    public $model = null;
    public $parentElementId = null;
//    public $limit = 10;

    public function init()
    {
    }

    public function run()
    {
        return $this->render('upload-files', [
            'folderType' => $this->folderType,
            'model' => $this->model,
            'parentElementId' => $this->parentElementId,
            'filesArray' => json_encode($this->filesArray ? $this->filesArray : [])
        ]);
    }
}