<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $posts common\models\Post[] */

?>
    <style>
        html, body {
        height: 100%;
        }
        #actions {
        margin: 2em 0;
        }
        /* Mimic table appearance */
        div.table {
        display: table;
        }
        div.table .file-row {
        display: table-row;
        }
        div.table .file-row > div {
        display: table-cell;
        vertical-align: top;
        border-top: 1px solid #ddd;
        padding: 8px;
        }
        div.table .file-row:nth-child(odd) {
        background: #f9f9f9;
        }
        /* The total progress gets shown by event listeners */
        #total-progress {
        opacity: 0;
        transition: opacity 0.3s linear;
        }
        /* Hide the progress bar when finished */
        #previews .file-row.dz-success .progress {
        opacity: 0;
        transition: opacity 0.3s linear;
        }
        /* Hide the delete button initially */
        #previews .file-row .delete {
        display: none;
        }
        /* Hide the start and cancel buttons and show the delete button */
        #previews .file-row.dz-success .start,
        #previews .file-row.dz-success .cancel {
        display: none;
        }
        #previews .file-row.dz-success .delete {
        display: block;
        }
    </style>

    <div class="container">
        <?= Html::hiddenInput($model.'[images]', $filesArray, ['id' => $model.'_images'])?>
        <div id="actions" class="row">

            <div class="col-lg-12 text-center">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Upload images...</span>
                </span>
            </div>

            <div class="" style="display:none;">
                <!-- The global file processing state -->
                <span class="fileupload-process">
                  <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                      <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                  </div>
                </span>
            </div>

        </div>

        <div class="table table-striped files" id="previews">

            <div id="template" class="file-row">
                <!-- This is used as the file preview template -->
                <div>
                    <span class="preview"><img data-dz-thumbnail /></span>
                </div>
                <div>
                    <p class="name" data-dz-name></p>
                    <strong class="error text-danger" data-dz-errormessage></strong>
                </div>
                <div>
                    <p class="size" data-dz-size></p>
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                        <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                    </div>
                </div>
                <div>
                    <a class="btn btn-primary start">
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Start</span>
                    </a>
                    <a data-dz-remove class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancel</span>
                    </a>
                    <a data-dz-remove class="btn btn-danger delete">
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Delete</span>
                    </a>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(function(){
            var filesArray = jQuery.parseJSON('<?= $filesArray; ?>');
            // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
            var previewNode = $('#<?= $parentElementId; ?>').find("#template");
            previewNode.id = "";
            var previewTemplate = previewNode.parent().html();
            previewNode.parent().find("#template").html('');

            var myDropzone = new Dropzone($('#<?= $parentElementId; ?>')[0], { // Make the whole body a dropzone
                url: "/site/upload-photo?type=<?= $folderType; ?>", // Set the url
                thumbnailWidth: 80,
                thumbnailHeight: 80,
                parallelUploads: 20,
                previewTemplate: previewTemplate,
                //autoQueue: false, // Make sure the files aren't queued until manually added
                previewsContainer: "#previews", // Define the container to display the previews
                clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
            });

            myDropzone.on("addedfile", function(file) {
                // Hookup the start button
                file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
            });

            // Update the total progress bar
            myDropzone.on("totaluploadprogress", function(progress) {
                $('#<?= $parentElementId; ?>').find("#total-progress .progress-bar").css('width', progress + "%");
            });
             myDropzone.on("complete", function(response) {
                if (response.status == 'success') {
                    filesArray.push({src:response.xhr.response, name:response.name});
                    
                    $('#<?= $parentElementId; ?>').find('#<?= $model; ?>_images').val(JSON.stringify(filesArray))
                }
            })
            myDropzone.on("sending", function(file) {
                // Show the total progress bar when upload starts
                $('#<?= $parentElementId; ?>').find("#total-progress").css('opacity', '1');
                // And disable the start button
                file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
            });

            // Hide the total progress bar when nothing's uploading anymore
            myDropzone.on("queuecomplete", function(progress) {
                $('#<?= $parentElementId; ?>').find("#total-progress").css('opacity', '0');
            });

            myDropzone.on("removedfile", function(response) {
                if(response.xhr != null){
                    filesArray = filesArray.filter(function(element, index){
                        return element.src != response.xhr.response;
                    }) 
                    $('#<?= $parentElementId; ?>').find('#<?= $model; ?>_images').val(JSON.stringify(filesArray))
                }
            });

            // Setup the buttons for all transfers
            // The "add files" button doesn't need to be setup because the config
            // `clickable` has already been specified.
            $('#<?= $parentElementId; ?>').on('click', '#actions .start', function() {
                myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
            });

            $('#<?= $parentElementId; ?>').on('click', '#actions .cancel', function() {
                myDropzone.removeAllFiles(true);
            });
        
        })
    </script>
