<?php
namespace frontend\controllers;

use Yii,
    yii\base\InvalidParamException,
    yii\web\BadRequestHttpException,
    yii\web\Controller,
    yii\filters\VerbFilter,
    yii\filters\AccessControl,
    common\models\LoginForm,
    frontend\models\PasswordResetRequestForm,
    frontend\models\ResetPasswordForm,
    frontend\models\SignupForm,
    common\models\User,
    yii\web\Response,
    yii\widgets\ActiveForm,
    common\models\University,
    common\models\user\UserInfo,
    yii\web\NotFoundHttpException,
    common\models\EmailSender,
    common\models\user\UserFriend,
    common\models\university\Classes,
    common\models\Followers,
    common\models\Favorites,
    common\models\user\UserSocial,
    common\models\location\States,
    yii\helpers\ArrayHelper,
    common\models\Notifications,
    frontend\models\NotificationsListSearch;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['upload-photo', 'logout', 'signup', 'email-confirm-panel', 'activate-user-email'],
                'rules' => [
                    [
                        'actions' => ['upload-photo', 'signup', 'email-confirm-panel', 'activate-user-email'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['upload-photo', 'logout', 'email-confirm-panel', 'activate-user-email', 'check-credits-count', 'get-state'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        } else {
            return $this->redirect(['check-user-university']);
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
        $this->layout = 'without-header-footer';

        $serviceName = \Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {

                    $profile = $eauth->getAttributes();
                    $identity = User::findByEAuth($eauth);

                    $model = User::find()
                        ->alias('u')
                        ->joinWith(['userSocialModel us'])
                        ->where(['like', 'us.social_id',  $profile['id']])
                        ->one();
                    
                    if (!$model) {
                        $model = new User();
                        $model->username = (isset($profile['name'])) ? $profile['name'] : $profile['id'];
                        $model->status = User::STATUS_ACTIVE;
                        $model->email = $profile['id'] . '@studentstop.com';
                        $model->social_id = (string)$profile['id'];
                        $model->password = \Yii::$app->security->generateRandomString(8);
                        $model->setPassword($model->password);
                        $model->generateAuthKey();
                        if ($model->save()) {
                            $modelSoc = new UserSocial();
                            $modelSoc->user_id = $model->id;
                            $modelSoc->social_id = $profile['id'];
                            $modelSoc->save();
                            if (Yii::$app->getUser()->login($model)) {
                                $eauth->redirect();
                            }
                        }
                    } else if (Yii::$app->getUser()->login($model)) {
                        $eauth->redirect();
                    }

                }else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            } catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }
        
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['check-user-university']);
        }

        $model = new LoginForm();
        $modelSignUp = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['check-user-university']);
        }

        return $this->render('login', [
            'model' => $model,
            'modelSignUp' => $modelSignUp,
        ]);
        
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionEmailConfirmPanel($id)
    {
        if (($model = User::findOne($id)) && !$model->active_status) {
            $model->active_status = 1;
            if ($model->save() ) {
                Yii::$app->session->setFlash('userActive');
                return $this->redirect(['index']);
            }
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {

                EmailSender::sendEmail(
                    [$user->email => $user->userInfoModel ? $user->userInfoModel->getNameAndSurname() : $user->email],
                    'Register User',
                    'activated-user',
                    [
                        'user' => $user
                    ]
                );

                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(['check-user-university']);
                }
            }
        }
    }

    public function actionActivateSocialUser($id, $social)
    {
        $this->layout = 'without-header-footer';
        $user = User::find()
                ->alias('u')
                ->joinWith(['userSocialModel us'])
                ->where(['us.social_id' => $social])
                ->andWhere([
                    'OR',
                    ['us.status' => UserSocial::STATUS_NOT_ACTIVE],
                    ['us.status' => null]
                ])
                ->andWhere(['u.id' => $id])
                ->one();

        $userSocial = UserSocial::find()
            ->where([
                'user_id' => $id,
                'social_id' => $social
            ])->one();

        if (!$user || !$userSocial) {
            throw new NotFoundHttpException('Not Found.');
        }
        if ($oldUser = User::find()->where(['email' => $user->social_email])->one()) {
            $userSocial->user_id = $oldUser->id;
            $userSocial->save();
            Yii::$app->user->logout();
            $user->delete();
            Yii::$app->getUser()->login($oldUser);
            if (EmailSender::sendEmail(
                    [$oldUser->email => $oldUser->userInfoModel ? $oldUser->userInfoModel->getNameAndSurname() : $oldUser->email],
                    'Account Activated',
                    'register-user',
                    [
                        'user' => $oldUser
                    ]
                )) {
                    Yii::$app->session->setFlash('userActivated');
                }

                return $this->redirect(['check-user-university']);
        } else {
            $user->email = $user->social_email ? $user->social_email : $user->email;
            $user->active_status = User::STATUS_EMAIL_ACTIVE;
            if ($user->save()) {
                if (EmailSender::sendEmail(
                    [$user->email => $user->userInfoModel ? $user->userInfoModel->getNameAndSurname() : $user->email],
                    'Account Activated',
                    'register-user',
                    [
                        'user' => $user
                    ]
                )) {
                    Yii::$app->session->setFlash('userActivated');
                }

                return $this->redirect(['check-user-university']);
            }
        }
        throw new NotFoundHttpException('Not Found.');
    }

    public function actionActivateUserEmail($id, $key)
    {
        $this->layout = 'without-header-footer';
        $user = User::find()->where([
            'id' => $id, 'auth_key' => $key
        ])->andWhere([
            'OR',
            ['active_status' => User::STATUS_EMAIL_NOT_ACTIVE],
            ['active_status' => null],
        ])->one();

        if (!$user) {
            throw new NotFoundHttpException('Not Found.');
        }
        
        $user->active_status = User::STATUS_EMAIL_ACTIVE;
        if ($user->save()) {
            if (EmailSender::sendEmail(
                [$user->email => $user->userInfoModel ? $user->userInfoModel->getNameAndSurname() : $user->email],
                'Account Activated',
                'register-user',
                [
                    'user' => $user
                ]
            )) {
                Yii::$app->session->setFlash('userActivated');
            }

            return $this->redirect(['check-user-university']);
        } else {
            throw new NotFoundHttpException('Not Found.');
        }
    }

    public function actionCheckUserUniversity()
    {
        $this->layout = 'without-header-footer';
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $user = Yii::$app->user->identity;
        $userInfoModel = $user->userInfoModel;

        if ($user->active_status) {
            
            if (!$userInfoModel) {
                $userInfoModel = new UserInfo;
                $userInfoModel->id_user = Yii::$app->user->identity->id;
            }

            if ($userInfoModel->load(Yii::$app->request->post())) {
                $userInfoModel->save();
            }

            if (University::findOne($userInfoModel->university_id)) {
                return $this->redirect(['/university/' . $userInfoModel->university_id]);
            }

            return $this->render('check-user-university', [
                'model' => $userInfoModel,
            ]);
        } else if ($user->social_id) {
            if (Yii::$app->request->isPost && (Yii::$app->request->post('send-confirm-to-email') !== null)) {
                if ($user->load(Yii::$app->request->post()) && $user->save()) {
//                        if ($oldUser = User::find()->where(['email' => $user->social_email])->one()) {
                    if (EmailSender::sendEmail(
                        [$user->social_email => $user->username],
                        'Register User SOCIAL',
                        'register-user-social',
                        [
                            'user' => $user
                        ]
                    )) {
                        Yii::$app->session->setFlash('emailSended');
                    }
//                        }
                } else {
                    Yii::$app->session->setFlash('emailNotSended');
                }
            }
            
            return $this->render('social-email-confirm', [
                'model' => $user,
                'userInfoModel' => $userInfoModel,
            ]);
        } else {
            if (Yii::$app->request->isPost && (Yii::$app->request->post('send-confirm-to-email') !== null)) {
                if (EmailSender::sendEmail(
                    [$user->email => $user->userInfoModel ? $user->userInfoModel->getNameAndSurname() : $user->email],
                    'Register User',
                    'activated-user',
                    [
                        'user' => $user
                    ]
                )) {
                    Yii::$app->session->setFlash('emailSended');
                }

            }

            return $this->render('email-confirm', [
                'model' => $user,
                'userInfoModel' => $userInfoModel,
            ]);
        }

    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'without-header-footer';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionUploadFiles($type = 'other')
    {
        $ds = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot') . '/files/' . $type . '/';   //2

        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }

            $endFile = explode('.', $_FILES['file']['name']);
            $endFile = end($endFile);

            $tempFile = $_FILES['file']['tmp_name'];   

            $targetPath =  $storeFolder . $ds.Yii::$app->user->id . $ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber . '.' . $endFile;  //5
            move_uploaded_file($tempFile, $targetFile); //6

            return '/files/' . $type . '/' . \Yii::$app->user->id . '/' . $for_name . '_' . $randNumber . '.' . $endFile;
        }
    }

    public function actionUploadPhoto($type = 'other')
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot') . '/images/' . $type . '/';   //2
        // mkdir($storeFolder);

        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }

            $endFile = explode('.', $_FILES['file']['name']);
            $endFile = end($endFile);

            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber . '.' . $endFile;  //5
            move_uploaded_file($tempFile, $targetFile); //6
            return 'images/' . $type . '/' . \Yii::$app->user->id . '/' . $for_name . '_' . $randNumber . '.' . $endFile; //5
        }
    }
    
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'without-header-footer';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     */
    public function actionNotificationsList()
    {
        $this->layout = 'modal';
        $searchModel = new NotificationsListSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search();

        return $this->render('_notifications-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array
     */
    public function actionGetNotificationCount()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['count' => Notifications::getNotReadCount()];
    }

    /**
     * @param integer $id
     * @return array
     */
    public function actionFavoritesRequest($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $type = Yii::$app->request->post('type');
        $userId = Yii::$app->request->post('user_id') ? Yii::$app->request->post('user_id') : Yii::$app->user->id;
        if ($model = Favorites::findOne(['user_id' => $userId, 'type' => $type, 'object_id' => $id])) {
            if ($model->delete()) {
                $res = ['status' => true, 'type' => 'delete'];
            }
        } else {
            if (Favorites::addNew($type, $id, $userId)) {
                $res = ['status' => true, 'type' => 'new'];
            }
        }
        return $res; 
    }
    
    /**
     * @param integer $classId
     * @param bool $view
     * @return array
     */
    public function actionClassJoinRequest($classId, $view = true)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = ['status' => false, 'text' => Classes::BUTTON_JOIN_TEXT, 'link' => null, 'class' => ' js-join-classes '];
        $model = Classes::findOne($classId);
        if ($model) {
            $modelFollow = Followers::findOne(['user_id' => Yii::$app->user->id, 'object_id' => $model->id, 'type' => Followers::TYPE_CLASSES]);
            if ($modelFollow && $modelFollow->delete()) {
                return ['status' => true, 'text' => Classes::BUTTON_JOIN_TEXT, 'link' => null, 'class' => ' js-join-classes '];
            } else {
                if (Followers::createNew(Followers::TYPE_CLASSES, $model->id, Yii::$app->user->id)) {
                    if ($view) {
                        return ['status' => true, 'text' => Classes::BUTTON_VIEW_TEXT, 'link' => $model->getLink(), 'class' => null];
                    } else {
                        return ['status' => true, 'text' => Classes::BUTTON_UNJOIN_TEXT, 'link' => null, 'class' => ' js-join-classes '];
                    }
                }
                
            }
        }
        
        return $response;
    }

    /**
     * @param integer $userId
     * @param integer $friendId
     * @return array
     */
    public function actionFriendRequest($userId, $friendId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = ['status' => false, 'text' => null];

        switch (UserFriend::checkFriendRequestType($userId, $friendId)) {
            case UserFriend::STATUS_FOLLOW:
                $response = [
                    'status' => UserFriend::saveFollow($userId, $friendId),
                    'text' => UserFriend::$arrayStatus[UserFriend::STATUS_UNFOLLOW]
                ];
                break;
            case UserFriend::STATUS_CONFIRM:
                $response = [
                    'status' => UserFriend::saveFollow($userId, $friendId),
                    'text' => UserFriend::$arrayStatus[UserFriend::STATUS_UNFOLLOW]
                ];
                break;
            case UserFriend::STATUS_UNFOLLOW:
                $response = [
                    'status' => UserFriend::saveUnFollow($userId, $friendId),
                    'text' => UserFriend::$arrayStatus[UserFriend::STATUS_FOLLOW]
                ];
                break;
        }
        
        return $response;
    }

    /**
     * @param integer $userId
     * @return array
     */
    public function actionCheckCreditsCount($userId = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return ['amount' => $this->findUserModel($userId ? $userId : Yii::$app->user->id)->getCreditsAmount()];
    }

    /**
     * @param integer $userId
     * @return array
     */
    public function actionCheckMoneyCount($userId = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return ['amount' => $this->findUserModel($userId ? $userId : Yii::$app->user->id)->getMoneyAmount()];
    }
    
    /**
     * @return array
     */
    public function actionGetState()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $modelCourses = States::getAll(Yii::$app->request->post('depdrop_parents'));
        $res = ArrayHelper::getColumn($modelCourses, function($element){
            return ['id' => $element->id, 'name' => $element->name];
        });

        return ['output' => $res, 'selected' => ''];
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUserModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

