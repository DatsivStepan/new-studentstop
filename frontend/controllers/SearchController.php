<?php

namespace frontend\controllers;

use Yii,
    common\models\User,
    frontend\models\UserSearch,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    frontend\models\MainSearch;

class SearchController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new MainSearch();
        $searchModel->load(Yii::$app->request->queryParams);
        if (Yii::$app->request->isPost) {
            $searchModel->load(Yii::$app->request->post());
        }

        $dataMembersProvider = $searchModel->searchMembers();
        $dataUniversityProvider = $searchModel->searchUniveristy();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataMembersProvider' => $dataMembersProvider,
            'dataUniversityProvider' => $dataUniversityProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
