<?php

namespace frontend\controllers;

use Yii,
    yii\helpers\Url,
    yii\web\Controller,
    frontend\components\Paypal,
    PayPal\Api\Item,
    PayPal\Api\Payer,
    PayPal\Api\Amount,
    PayPal\Api\Payment,
    PayPal\Api\Details,
    PayPal\Api\ItemList,
    PayPal\Rest\ApiContext,
    yii\base\UserException,
    PayPal\Api\Transaction,
    PayPal\Api\RedirectUrls,
    PayPal\Api\PaymentExecution,
    PayPal\Auth\OAuthTokenCredential,
    PayPal\Exception\PayPalConnectionException,
    yii\web\NotFoundHttpException,
    common\models\business\QuestionsAnswers,
    common\models\university\ClassProducts,
    common\models\pay\PayRequests,
    common\models\services\RentsBooking,
    common\models\services\Rents;

/**
 * Class PaymentPaypalController
 * @package app\controllers
 */
class PaymentPaypalController extends Controller
{
    
     /**
     * @param $action
     * @return bool
     */
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @param int $bookedId
     * @param int $rentId
     * @param string $bookingFrom
     * @param string $bookingTo
     * @return response data
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPayForRentBook($bookedId = null, $rentId = null, $bookingFrom = null, $bookingTo = null)
    {
        $money = 0;
        $rentId = $rentId ? $rentId : 0;
        if ($bookedId) {
            $modelBooking = $this->findRentsBookingModel($bookedId);
            $money = $modelBooking->getNeedMoneyPayCount();
            $rentId = $modelBooking->rent_id;
        } else if ($rent = $this->findRentsModel($rentId)) {
            $interval = date_diff(date_create($bookingFrom), date_create($bookingTo));
            $hours = ($interval->days * 24) + $interval->h;
            $money = $hours * $rent->money;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $rent = $this->findRentsModel($rentId);
                
        $dPrice = (float)$money;

        $pay = new Paypal();
        $pay = $pay->api;

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item2 = new Item();
        $item2->setName('Payment answer')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku(time()) // Similar to `item_number` in Classic API
            ->setPrice($dPrice);
        $itemList = new ItemList();
        $itemList->setItems(array($item2));


        $details = new Details();
        $details->setShipping(0.00)
            ->setTax(0.00)
            ->setSubtotal($dPrice);


        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($dPrice+0.00)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Paymend answer ')
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $urlRequest = 'payment-paypal/return-result?approved=true&module=' . PayRequests::MODULE_RENT_BOOKING . '&operation=' . PayRequests::OPERATION_TYPE_RENT_BOOKING . '&bookedId=' . $bookedId . 
                '&bookingFrom=' . urlencode($bookingFrom) . '&bookingTo=' . urlencode($bookingTo) . '&objectId=' . $rentId;

        $redirectUrls->setReturnUrl(Url::home(true) . 'payment-paypal/return-result?approved=true' . $urlRequest)
            ->setCancelUrl(Url::home(true) . 'payment-paypal/return-result?approved=false' . $urlRequest);

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $payment->create($pay);

        PayRequests::preparePaypalPayRequest(
            Yii::$app->user->id,
            $rent->user_id,
            $money,
            $bookedId ? $bookedId : $rentId,
            PayRequests::OPERATION_TYPE_RENT_BOOKING,
            PayRequests::MODULE_RENT_BOOKING,
            'Payed paypal for rent booking',
            $payment->getId()
        );

        Yii::$app->getResponse()->redirect($payment->getApprovalLink());
    }

    /**
     * @param int $answerId
     * @return response data
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPayForAnswer($answerId)
    {
        $modelAnswer = $this->findQuestionsAnswersModel($answerId);
        $dPrice = (float)$modelAnswer->money;
        
        $pay = new Paypal();
        $pay = $pay->api;

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item2 = new Item();
        $item2->setName('Payment answer')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku(time()) // Similar to `item_number` in Classic API
            ->setPrice($dPrice);
        $itemList = new ItemList();
        $itemList->setItems(array($item2));


        $details = new Details();
        $details->setShipping(0.00)
            ->setTax(0.00)
            ->setSubtotal($dPrice);


        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($dPrice+0.00)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Paymend answer ')
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(Url::home(true) . 'payment-paypal/return-result?module=' . PayRequests::MODULE_QUESTION_BANK . '&operation=' . PayRequests::OPERATION_TYPE_QUESTION_ANSWER . '&approved=true&objectId=' . $modelAnswer->id)
            ->setCancelUrl(Url::home(true) . 'payment-paypal/return-result?module=' . PayRequests::MODULE_QUESTION_BANK . '&operation=' . PayRequests::OPERATION_TYPE_QUESTION_ANSWER . '&approved=false&objectId=' . $modelAnswer->id);

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $payment->create($pay);

        PayRequests::preparePaypalPayRequest(
            Yii::$app->user->id,
            $modelAnswer->user_id,
            $modelAnswer->money,
            $modelAnswer->id,
            PayRequests::OPERATION_TYPE_QUESTION_ANSWER,
            PayRequests::MODULE_QUESTION_BANK,
            'Payed paypal for answers',
            $payment->getId()
        );

        Yii::$app->getResponse()->redirect($payment->getApprovalLink());
    }

    /**
     * @param int $productId
     * @return response data
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPayForClassProduct($productId)
    {
        $modelProduct = $this->findClassProductsModel($productId);
        if (!$file = $modelProduct->classFilesModel) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $dPrice = (float)$modelProduct->money;

        $pay = new Paypal();
        $pay = $pay->api;

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item2 = new Item();
        $item2->setName('Payment answer')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku(time()) // Similar to `item_number` in Classic API
            ->setPrice($dPrice);
        $itemList = new ItemList();
        $itemList->setItems(array($item2));


        $details = new Details();
        $details->setShipping(0.00)
            ->setTax(0.00)
            ->setSubtotal($dPrice);


        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($dPrice+0.00)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Paymend answer ')
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(Url::home(true) . 'payment-paypal/return-result?module=' . PayRequests::MODULE_COURSE_CLASSES . '&operation=' . PayRequests::OPERATION_TYPE_PAY_PRODUCT . '&approved=true&objectId=' . $modelProduct->id)
            ->setCancelUrl(Url::home(true) . 'payment-paypal/return-result?module=' . PayRequests::MODULE_COURSE_CLASSES . '&operation=' . PayRequests::OPERATION_TYPE_PAY_PRODUCT . '&approved=false&objectId=' . $modelProduct->id);

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $payment->create($pay);

        PayRequests::preparePaypalPayRequest(
            Yii::$app->user->id,
            $file->user_id,
            $modelProduct->money,
            $modelProduct->id,
            PayRequests::OPERATION_TYPE_PAY_PRODUCT,
            PayRequests::MODULE_COURSE_CLASSES,
            'Payed paypal for class product',
            $payment->getId()
        );

        Yii::$app->getResponse()->redirect($payment->getApprovalLink());
    }

    
    /**
     * @return response data
     */
    public function actionReturnResult($module, $operation, $approved, $objectId)
    {
        $pay = New Paypal();
        $pay = $pay->api;
        $payedId = $_GET['PayerID'];
        $paymentId = Yii::$app->request->get('paymentId');
        try {
            $payment = Payment::get($paymentId, $pay);
            $execution = new PaymentExecution();
            $execution->setPayerId($payedId);
            $payment->execute($execution, $pay);
        } catch (PayPalConnectionException $e) {
            throw new UserException($e->getMessage());
            Yii::$app->session->setFlash('returnPaymentError');
        }

        if ($payRequests = PayRequests::findOne(['paypal_id' => $paymentId])) {
            
            switch ($module) {
                case PayRequests::MODULE_QUESTION_BANK:
                    if ($operation == PayRequests::OPERATION_TYPE_QUESTION_ANSWER) {
                        $modelAnswer = $this->findQuestionsAnswersModel($objectId);
                        if ($approved == 'true') {
                            $payRequests->pay_status = PayRequests::PAY_STATUS_APPROVED;
                            Yii::$app->session->setFlash('returnPaymentResultSuccess');
                        } else {
                            $payRequests->pay_status = PayRequests::PAY_STATUS_NO_APPROVED;
                            Yii::$app->session->setFlash('returnPaymentError');
                        }
                        if ($payRequests->save()) {
                            //підвердження тільки тоді коли немає написаного webhook
                                if ($payRequests->confirmPaypalPay()) {
                                    $modelAnswer->status = QuestionsAnswers::STATUS_PAYED;
                                    $modelAnswer->save();
                                }
                            //
                        } else {
                            Yii::$app->session->setFlash('returnPaymentError');
                        }

                        return $this->redirect('/business/questions/my-questions?question_id=' . $modelAnswer->question_id);
                    }
                    break;
                case PayRequests::MODULE_COURSE_CLASSES:
                    if ($operation == PayRequests::OPERATION_TYPE_PAY_PRODUCT) {
                        $modelProduct = $this->findClassProductsModel($objectId);
                        if ($approved == 'true') {
                            $payRequests->pay_status = PayRequests::PAY_STATUS_APPROVED;
                            Yii::$app->session->setFlash('returnPaymentResultSuccess');
                        } else {
                            $payRequests->pay_status = PayRequests::PAY_STATUS_NO_APPROVED;
                            Yii::$app->session->setFlash('returnPaymentError');
                        }
                        if ($payRequests->save()) {
                            //підвердження тимчасове, тільки тоді коли немає написаного webhook
                                $payRequests->confirmPaypalPay();
                            //
                        } else {
                            Yii::$app->session->setFlash('returnPaymentError');
                        }

                        return $modelProduct->classesModel ? $this->redirect($modelProduct->classesModel->getLink('products') . '/products?product_id=' . $modelProduct->id) :  $this->redirect('/site/index');
                    }
                    break;
                case PayRequests::MODULE_RENT_BOOKING:
                    if ($operation == PayRequests::OPERATION_TYPE_RENT_BOOKING) {

                        $bookedId = Yii::$app->request->get('bookedId');
                        $bookingFrom = Yii::$app->request->get('bookingFrom');
                        $bookingTo = Yii::$app->request->get('bookingTo');

                        if ($approved == 'true') {
                            $payRequests->pay_status = PayRequests::PAY_STATUS_APPROVED;
                        } else {
                            $payRequests->pay_status = PayRequests::PAY_STATUS_NO_APPROVED;
                            Yii::$app->session->setFlash('returnPaymentError');
                        }
                        
                        $modelBooking = null;
                        $rent = $this->findRentsModel($objectId);
                        if ($bookedId) {
                            $modelBooking = $this->findRentsBookingModel($bookedId);
                            $modelBooking->save();
                        } else if ($rent && $bookingFrom && $bookingTo) {
                            $modelBooking = new RentsBooking();
                            $modelBooking->rent_id = $rent->id;
                            $modelBooking->rent_type = RentsBooking::TYPE_RENT;
                            $modelBooking->user_id = Yii::$app->user->id;
                            $modelBooking->date_reserve = date("Y-m-d H:i:s");
                            $modelBooking->reserve_from = $bookingFrom;
                            $modelBooking->reserve_to = $bookingTo;
                            $modelBooking->status = RentsBooking::STATUS_NEW;
                            if ($modelBooking->save()) {
                                $payRequests->objectId = $modelBooking->id;
                                $payRequests->save();
                            }
                        } else {
                            throw new NotFoundHttpException('The requested page does not exist.');
                        }

                        //підвердження тимчасове, тільки тоді коли немає написаного webhook
                            if ($payRequests->confirmPaypalPay()) {
                                $modelBooking = $this->findRentsBookingModel($payRequests->objectId);
                                $modelBooking->status = RentsBooking::STATUS_PAYED;
                                $modelBooking->save();
                            }
                        //

                        return $this->redirect($rent->type_pay == Rents::PAY_TYPE_INSTANT ? '/services/rents/my-booking?type=1'  : '/services/rents/my-booking');
                    }

                    break;
            }
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /**
     * @param int $profileId
     * @return response data
     */
    public function actionForWebHook()
    {
        $requestBody = Yii::$app->request->getRawBody();
        if ($requestBody->event_type == PayRequests::PAYPAL_STATUS_COMPLETE) {
            $paymentId = $requestBody->resource->parent_payment;

            if ($payRequests = PayRequests::findOne(['paypal_id' => $paymentId])) {
                if ($payRequests->confirmPaypalPay()) {
                    
                    //якщо потрібно виконати ще якісь функції окрім простого добавлення грошей до акаунту користувача
                    switch ($payRequests->module) {
                        case PayRequests::MODULE_QUESTION_BANK:
                            if ($payRequests->operation == PayRequests::OPERATION_TYPE_QUESTION_ANSWER) {
                                $modelAnswer = $this->findQuestionsAnswersModel($payRequests->objectId);
                                $modelAnswer->status = QuestionsAnswers::STATUS_PAYED;
                                $modelAnswer->save();
                            }
                        break;
                        case PayRequests::MODULE_RENT_BOOKING:
                            if ($operation == PayRequests::OPERATION_TYPE_RENT_BOOKING) {
                                $modelBooking = $this->findRentsBookingModel($payRequests->objectId);
                                $modelBooking->status = RentsBooking::STATUS_PAYED;
                                $modelBooking->save();
                            }
                        break;
                    }
                    //
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return QuestionsAnswers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findQuestionsAnswersModel($id)
    {
        if (($model = QuestionsAnswers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return ClassProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findClassProductsModel($id)
    {
        if (($model = ClassProducts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return RentsBooking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findRentsBookingModel($id)
    {
        if (($model = RentsBooking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return Rents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findRentsModel($id)
    {
        if (($model = Rents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
