<?php

namespace frontend\components\helpers;

use Yii,
    yii\helpers\Html,
    yii\helpers\ArrayHelper;

/**
 * Class FunctionHelper
 * @package frontend\components\helpers
 */

class FunctionHelper
{
    /**
     * @param $format
     * @param $date
     * @param bool $hmi
     * @return array|mixed
     */
    private static function dateFor($format, $date, $hmi = false)
    {
        if (!is_array($date)) {
            $date = [$date];
        }
        $dates = [];
        foreach ($date as $d) {
            $dates[] = !empty($d) ? date($format . ($hmi ? " H:i:s" : ""), strtotime($d)) : null;
        }
        return count($dates) > 1 ? $dates : $dates[0];
    }

    /**
     * @param array|string $date
     * @param bool $hmi
     * @return array|mixed
     */
    public static function dateForMysql($date, $hmi = false)
    {
        return empty($date) ? null : self::dateFor("Y-m-d", $date, $hmi);
    }

    /**
     * @param array|string $date
     * @param bool $hmi
     * @return array|mixed
     */
    public static function dateForForm($date, $hmi = false)
    {
        return ($date == '0000-00-00' || empty($date)) ? null : self::dateFor("d.m.Y", $date, $hmi);
    }

    /**
     * @param array|string $date
     * @param bool $hmi
     * @return array|mixed
     */
    public static function dateForTemplate($date, $hmi = false)
    {
        return ($date == '0000-00-00' || empty($date)) ? null : self::dateFor("Y/m/d", $date, $hmi);
    }

    /**
     * @param array|string $date
     * @return array|mixed
     */
    public static function numberFormat($price, $abs = false, $decimals = 2, $dec_point = '.')
    {
        return number_format($abs ? abs((float)$price) : (float)$price, $decimals, $dec_point, ' ');
    }
}