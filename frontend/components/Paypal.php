<?php
/**
 * Created by PhpStorm.
 * User: Listat
 * Date: 26.07.2016
 * Time: 12:41
 */

namespace frontend\components;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class Paypal
{
    public $api;

    function __construct() {
        $this->api = new ApiContext(
            new OAuthTokenCredential(
                'ATaYOL6z1cHpdlYbNYF9Aj-9yNurZwtrCysEf179jQpnAIeKCJelEjEsTL_RSBd-bV6qS6gP6IVXCpB4',
                'EEAWdG1UWHkU_2PLISNJv5gt1HyIL0UGI-RauS53AD2v-MK1YHVXrHl6yMIkGelnMkuhozlvWAQ9BkMg'
            )
        );

        $this->api->setConfig([
            'mode' => 'sandbox',
            'http.ConnectionTimeOut'=>30,
            'log.LogEnabled'=> 0,
            'log.FileName'=> '',
            'log.LogLevel' => 'FINE',
            'validation.level'=>'log'
        ]);
    }
}