var $dropzone = {
    initializationDropzoneEleemnt: function (element, formElemenVal, acceptedFiles = '') {

        if($('.js-add-' + element + '-image').length){

            $(document).find('body').append('<div style="display:none;">'+
                    '<div class="row-table-style">'+
                        '<div class="table table-striped" class="files" id="previews' + element +'">'+
                            '<div id="template' + element + '" class="file-row">'+

                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>')

            var previewNode = document.querySelector("#template" + element);
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzone = new Dropzone($(formElemenVal)[0], {
                url: "/site/upload-photo?type=" + element,
                previewTemplate: previewTemplate,
                previewsContainer: "#previews" + element,
                clickable: ".js-add-" + element + "-image",
                acceptedFiles:acceptedFiles,
                uploadMultiple:false,
                maxFiles:1,
            });

            myDropzone.on("maxfilesexceeded", function(file) {
                    myDropzone.removeAllFiles();
                    myDropzone.addFile(file);
            });

            myDropzone.on("complete", function(response) {
                if (response.status == 'success') {
                    $('.js-preview-' + element + '-image').attr('src', '/'+response.xhr.response)
                    $(formElemenVal).val(response.xhr.response);
                }
            });

            myDropzone.on("removedfile", function(response) {
                if(response.xhr != null){
                   //deleteFile(response.xhr.response);
                }
            });

        }
    },
};
var $main = {
    checkNotoficationNotReadCount: function () {
        $.ajax({
            url:'/site/get-notification-count',
            dataType:'json',
        }).done(function(response) {
            if (response.count) {
                $('.js-header-menu-profile .js-notification-header .js-notification-count').text(response.count);
            }
        });
    }
};


$(document).ready(function() {

    $(document).on('click', '.js-friend-request-button', function(e) {
        e.preventDefault();
        var thisElement = $(this);
        thisElement.prop('disabled', true)
        var userId = thisElement.data('user_id')
        var friendId = thisElement.data('friend_id')
        $.ajax({
            type: 'POST',
            url:'/site/friend-request?userId=' + userId + '&friendId=' + friendId,
            dataType:'json',
            method:'post'
        }).done(function(response) {
            thisElement.prop('disabled', false)
            if (response.status) {
                thisElement.text(response.text)
            }
        })
    })

    $(document).on('click', '.js-join-classes', function(e) {
        var classId = $(this).data('id');
        var thisElement = $(this);
        var view = $(this).data('view');
        thisElement.prop('disabled', true)
        $.ajax({
            type: 'POST',
            url:'/site/class-join-request?classId=' + classId + '&view=' + view,
            dataType:'json',
            method:'post'
        }).done(function(response) {
            thisElement.prop('disabled', false)
            if (response.status) {
                thisElement.text(response.text)
                if (response.class) {
                    thisElement.addClass(response.class);
                } else {
                    thisElement.removeClass('js-join-classes');
                }
                if (response.link) {
                    thisElement.prop('href', response.link)
                } else {
                    thisElement.prop('href', '#')
                }
            }
        })
    })
    
    if ($('.js-header-menu-profile').length) {
        $main.checkNotoficationNotReadCount()
        setInterval(function(){
            $main.checkNotoficationNotReadCount()
        }, 3000);

        $(document).on('click', '.js-notification-header', function(e) {
            e.preventDefault();
            $.dialog({
                id: 'notificationModal',
                content: 'url:/site/notifications-list',
                columnClass: 'xl view-block modal-block '
            });
        });
        
    }
})
