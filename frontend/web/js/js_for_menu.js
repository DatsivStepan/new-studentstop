$(document).ready(function() {
    
    if ($('.js-create-new-question').length) {
        $(document).on('click', '.js-create-new-question', function(e) {
            e.preventDefault();

            $.confirm({
                id: 'questionCreateModal',
                title: 'Create question',
                content: 'url:/business/questions/create',
                columnClass: 'modal-block xl'
            });
        });
    }

    if ($('.js-add-classified-market').length) {
        $(document).on('click', '.js-add-classified-market', function(e) {
            e.preventDefault();
            $.confirm({
                id: 'classifieldMarketCreateModal',
                title: 'Create',
                content: 'url:/services/classified-market/create/',
                columnClass: 'modal-block xl'
            });
        });
    }
    
    if ($('.js-jobs-show-my-company').length) {
        $(document).on('click', '.js-jobs-show-my-company', function(e) {
            e.preventDefault();
            $.confirm({
                id: 'jobsCompanyUpdateModal',
                title: 'Update',
                content: 'url:/jobs/companies/create',
                columnClass: 'xl modal-block'
            });
        })
    }

    
})
