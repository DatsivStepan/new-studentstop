$(document).ready(function() {

    $(document).on('click', '.js-companies-create', function() {
        $.confirm({
            id: 'jobsCompanyCreateModal',
            title: 'Create',
            content: 'url:/jobs/companies/create',
            columnClass: 'xl modal-block'
        });
    })

    $(document).on('click', '.js-student-resume-create', function() {
        $.confirm({
            id: 'studentResumeCreateModal',
            title: 'Create',
            content: 'url:/jobs/student/create',
            columnClass: 'xl modal-block'
        });
    })

    $(document).on('click', '.js-jobs-show-my-resume', function(e) {
        e.preventDefault()
        $.confirm({
            id: 'studentResumeUpdateModal',
            title: 'Create',
            content: 'url:/jobs/student/create',
            columnClass: 'xl modal-block'
        });
    })
    
    $(document).on('click', '.js-add-job', function() {
        $.confirm({
            id: 'companyCreateJobModal',
            title: 'Create',
            content: 'url:/jobs/companies/create-job',
            columnClass: 'xl modal-block'
        });
    });
    
    $(document).on('click', '.js-favorite-star-button', function(){
        var thisElement = $(this);
        var id = $(this).data('id')
        if (typeof id == 'undefined') {
            id = $(this).closest('.js-item-content-block').data('id')
        }
        var type = $(this).data('type');
        $.ajax({
            method: 'post',
            url: '/site/favorites-request/?id=' + id,
            data:{type:type},
            dataType: 'json'
        }).done(function (response) {
            if (response.status) {
                if (response.type == 'new') {
                    thisElement.addClass('active')
                } else if (response.type == 'delete') {
                    thisElement.removeClass('active')
                }
            }
        })
    })

    $('.js-resume-container').on('click', '.js-show-resume', function(){
        var id = $(this).data('id');
        $.dialog({
            id: 'showResumeModal',
            title: 'Resume View',
            content: 'url:/jobs/default/show-resume?id=' +id,
            columnClass: 'm'
        });
    })

    $('.js-resume-container').on('click', '.js-create-invite', function(){
        var to_user_id = $(this).data('user_id');
        $.confirm({
            id: 'resumeInviteModal',
            title: 'Create',
            content: 'url:/jobs/companies/create-invite?to_user_id=' + to_user_id,
            columnClass: 'm'
        });
    })

    $('.js-jobs-container, .companies-jobs-view').on('click', '.js-apply-job', function(){
        var job_id = $(this).data('job_id');
        var thisButton = $(this);
        swal({
            title: "Are you sure?",
            text: "apply job",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    method: 'post',
                    url: '/jobs/default/apply-job?id=' + job_id,
                    dataType: 'json'
                }).done(function (response) {
                    if (response.status) {
                        if (response.type == 'new') {
                            thisButton.text('Unapply');
                        } else {
                            thisButton.text('Apply');
                        }
                    }
                })
            }
        })
    })
})
