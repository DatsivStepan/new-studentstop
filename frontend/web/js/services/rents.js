$(document).ready(function() {
    $(document).on('click', '.js-favorite-star-button', function(){
        var thisElement = $(this);
        var id = $(this).closest('.js-item-content-block').data('id')
        var type = $(this).data('type')
        $.ajax({
            method: 'post',
            url: '/site/favorites-request/?id=' + id,
            data:{type:type},
            dataType: 'json'
        }).done(function (response) {
            if (response.status) {
                if (response.type == 'new') {
                    thisElement.addClass('active')
                } else if (response.type == 'delete') {
                    thisElement.removeClass('active')
                }
            }
        })
    })

    $(document).on('click', '.js-show-rents', function(){
        var id = $(this).data('id');
        var booking_from = $(this).data('booking_from');
        if (typeof booking_from == 'undefined') {
            booking_from = ''
        }
        var booking_to = $(this).data('booking_to');
        if (typeof booking_to == 'undefined') {
            booking_to = ''
        }
        
        $.dialog({
            id: 'rentsShowModal',
            title: 'Rent',
            content: 'url:/services/rents/show/' + id + '?booking_from=' + booking_from + '&booking_to=' + booking_to,
            columnClass: 'xl modal-block'
        });
    })
    

    $(document).on('click', '.js-create-rent', function(){
        $.confirm({
            id: 'createRentModal',
            title: 'Add Rent',
            content: 'url:/services/rents/create-rent',
            columnClass: 'xl modal-block',
        });
    })
})
