$(document).ready(function() {
    $(document).on('click', '.js-favorite-star-button', function(){
        var thisElement = $(this);
        var id = $(this).closest('.js-item-content-block').data('id')
        var type = $(this).data('type')
        $.ajax({
            method: 'post',
            url: '/site/favorites-request/?id=' + id,
            data:{type:type},
            dataType: 'json'
        }).done(function (response) {
            if (response.status) {
                if (response.type == 'new') {
                    thisElement.addClass('active')
                } else if (response.type == 'delete') {
                    thisElement.removeClass('active')
                }
            }
        })
    })

    $(document).on('click', '.js-show-classified-market', function() {
        var id = $(this).data('key')
        var title = $(this).data('title')
        $.dialog({
            id: 'classifieldMarketShowModal',
            title: title,
            content: 'url:/services/classified-market/show/' + id,
            columnClass: 'm'
        });
    })
})
