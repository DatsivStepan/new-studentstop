$(document).ready(function() {
    $(document).on('click', '.js-favorite-star-button', function(){
        var thisElement = $(this);
        var id = $(this).closest('.js-item-content-block').data('id')
        var type = $(this).data('type')
        $.ajax({
            method: 'post',
            url: '/site/favorites-request/?id=' + id,
            data:{type:type},
            dataType: 'json'
        }).done(function (response) {
            if (response.status) {
                if (response.type == 'new') {
                    thisElement.addClass('active')
                } else if (response.type == 'delete') {
                    thisElement.removeClass('active')
                }
            }
        })
    });

    $(document).on('click', '.js-show-question', function(){
        var id = $(this).data('id')

        $.dialog({
            id: 'questionShowModal',
            title: 'Question view',
            content: 'url:/business/questions/show/' + id,
            columnClass: 'xl view-block modal-block question-modal-container'
        });
    });

    $(document).on('click', '.js-update-question', function () {
        var id = $(this).closest('.js-item-content-block').data('id')
        $.confirm({
            id: 'questionUpdateModal',
            title: 'Update question',
            content: 'url:/business/questions/create/' + id,
            columnClass: 'modal-block xl'
        });
    })

    $(document).on('click', '.js-delete-question', function () {
        var id = $(this).closest('.js-item-content-block').data('id')
        swal({
            title: "Are you sure?",
            text: "delete question",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    method: 'post',
                    url: '/business/questions/delete/' + id,
                    dataType: 'json'
                }).done(function (response) {
                    if (response.status) {
                        location.reload()
                    }
                })
            }
        });
    })

    $(document).on('click', '.js-show-answer', function(){
        var id = $(this).data('id');
        $.dialog({
            id: 'showAnswerModal',
            title: '',
            content: 'url:/business/questions/show-answer/' + id,
            columnClass: 'xl view-block modal-block '
        });
    });
})
