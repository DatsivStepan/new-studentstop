<?php
namespace frontend\models;

use yii;
use yii\base\Model;
use common\models\User;
use common\models\user\UserInfo;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $name;
    public $surname;
    public $email;
    public $password;
    public $password_repeat;
    public $user_role_id;
    public $university_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            ['username', 'trim'],
//            ['username', 'required'],
//            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
//            ['username', 'string', 'min' => 2, 'max' => 255],

            [['university_id', 'user_role_id'], 'integer'],
            [['name', 'surname'], 'required'],
            [['name', 'surname'], 'string'],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'match', 'pattern' => '/.com$/', 'message' => 'email must be org.'],
            ['email', 'unique', 
                'targetClass' => '\common\models\User',
                'message' => 'This email address has already been taken.',
            ],

            ['password', 'required'],
            [['password', 'password_repeat'], 'string', 'min' => 6],
            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->status = User::STATUS_ACTIVE;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $res = null;
        if ($user->save()) {
            $modelUserInfo = new UserInfo();
            $modelUserInfo->id_user = $user->id;
            $modelUserInfo->name = $this->name;
            $modelUserInfo->surname = $this->surname;
            $modelUserInfo->university_id = $this->university_id;
            $modelUserInfo->user_role_id = $this->user_role_id;
            if ($modelUserInfo->save()) {
                $res = true;
            }
        }

        return $user->save() ? $user : null;
    }
}
