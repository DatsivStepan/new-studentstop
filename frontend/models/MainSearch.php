<?php

namespace frontend\models;

use yii\base\Model,
    yii\data\ActiveDataProvider,
    common\models\User,
    common\models\University;

class MainSearch extends User
{
    public $filterValueM;
    public $filterUniversityM;
    public $filterUserStatusM;

    public $filterValueU;
    public $filterMajorsU;
    public $filterCourseU;
    
    public $filterUniversityMap;

    public function rules()
    {
        return [
            [
                [
                    'filterUniversityMap', 'filterUniversityM', 'filterUserStatusM',
                    'filterMajorsU', 'filterCourseU'
                ],
                'integer'
            ],
            [
                [
                    'username'
                ], 
                'safe'
            ],
            [
                [
                    'filterValueM', 'filterValueU'
                ], 
                'string'
            ],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchMembers()
    {
        $query = User::find()
            ->alias('u')
            ->joinWith([
                'toFriedsModels f',
                'userInfoModel uf'
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ]
        ]);

        if ($this->filterUniversityMap) {
            $query->andFilterWhere(['f.user_id' => $this->filterUniversityMap]);
        } else {
            $query
                ->andFilterWhere(['like', 'CONCAT(uf.name, " ",  uf.surname)', $this->filterValueM])
                ->andFilterWhere(['uf.university_id' => $this->filterUniversityM])
                ->andFilterWhere(['uf.user_role_id' => $this->filterUserStatusM]);
        }

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'u.username', $this->username]);

        return $dataProvider;
    }
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchUniveristy()
    {
        $query = University::find()
            ->alias('un')
            ->joinWith([
                'majorsModel m',
                'majorsModel.courses c',
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ]
        ]);

        $query
            ->andFilterWhere(['like', 'un.name', $this->filterValueU])
            ->andFilterWhere(['m.id' => $this->filterMajorsU])
            ->andFilterWhere(['c.id' => $this->filterCourseU]);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

//        $query->andFilterWhere(['like', 'username', $this->username]);

        return $dataProvider;
    }
}
