<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Post;

class PostSearch extends Post
{
    public $filter;
    public $filterImage;
    public $filterCompanyId;
    
    public static $filterArray = [
        1 => 'All',
        2 => 'My',
        3 => 'Friends',
        4 => 'University',
    ];

    public function rules()
    {
        return [
            [['title', 'filterCompanyId'], 'safe'],
            [['university_id', 'filter', 'filterImage', 'type'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchSlider($params)
    {
        $query = Post::find()
                ->where(['inSlider' => 1])
                ->andWhere(['university_id' => $this->university_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find()
                ->alias('p')
                ->joinWith(['universityModel u', 'filesModel f'])
                ->orderBy(['p.created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);
        
//        if (!$this->validate()) {
//            return $dataProvider;
//        }

        if ($this->filterImage) {
            $query->andWhere(['not', ['f.id' => null]]);
        }
        if ($this->filter == 2) {
            $query->andWhere(['p.user_id' => \Yii::$app->user->id]);
        }

        if ($this->filter == 3) {
            $query->andWhere(['p.user_id' => \common\models\user\UserFriend::getFriendsIds()]);
        }

        if (($this->filter == 4) && ($un = \common\models\University::findOne($this->university_id))) {
            $query->andWhere([
                'p.type' => Post::TYPE_UNIVERSITY
            ]);
        }

        if ($this->filterCompanyId) {
            $query->andFilterWhere([
                'p.university_id' => $this->filterCompanyId
            ]);
        }
        
        $query->andFilterWhere([
            'p.university_id' => $this->university_id,
            'p.type' => $this->filter != 4 ? $this->type : null
        ])
        ->andFilterWhere(['like', 'p.description', $this->title]);

        return $dataProvider;
    }
}
