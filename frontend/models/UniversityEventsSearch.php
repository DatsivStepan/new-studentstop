<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\university\UniversityEvents;

class UniversityEventsSearch extends UniversityEvents
{
    public $filterDate;

    public function rules()
    {
        return [
            [['university_id', 'user_id', 'status'], 'integer'],
            [['date_start', 'date_end', 'created_at', 'updated_at', 'filterDate'], 'safe'],
            [['short_content', 'content'], 'string'],
            [['title', 'img_src'], 'string', 'max' => 255],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UniversityEvents::find()->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->filterDate) {
            $query->andFilterWhere(['<=', 'date_start', $this->filterDate.' 23:59']);
            $query->andFilterWhere(['>=', 'date_end', $this->filterDate.' 00:00']);
        }

        $query->andFilterWhere([
            'university_id' => $this->university_id
        ])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
