<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\university\ClassesEvents;

class ClassesEventsSearch extends ClassesEvents
{
    public $dateShowFrom;
    public $filterDate;

    public function rules()
    {
        return [
            [['class_id', 'user_id', 'status'], 'integer'],
            [['date_start', 'date_end', 'created_at', 'updated_at', 'dateShowFrom', 'filterDate'], 'safe'],
            [['short_content', 'content'], 'string'],
            [['title', 'img_src'], 'string', 'max' => 255],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        if ($this->dateShowFrom) {
            $query->andWhere([
                '>', 'created_at', $this->dateShowFrom
            ]);
        }

        if ($this->filterDate) {
            $query->andFilterWhere(['<=', 'date_start', $this->filterDate.' 23:59']);
            $query->andFilterWhere(['>=', 'date_end', $this->filterDate.' 00:00']);
        }

        $query->andFilterWhere([
            'class_id' => $this->class_id,
        ])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
