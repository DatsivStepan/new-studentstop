<?php

namespace frontend\models;

use yii\base\Model,
    yii\data\ActiveDataProvider,
    common\models\NotificationsUsers;

class NotificationsListSearch extends NotificationsUsers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_id', 'user_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()
            ->alias('nu')
            ->joinWith(['notificationsModel n'])
            ->where(['not', ['n.id' => null]])
            ->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'nu.user_id' => $this->user_id,
        ]);
        
        return $dataProvider;
    }
}
