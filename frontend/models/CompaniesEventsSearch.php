<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\jobs\CompaniesEvents;

class CompaniesEventsSearch extends CompaniesEvents
{
    public $filterDate;
    public $filterCompanyId;

    public function rules()
    {
        return [
            [['company_id', 'user_id', 'status'], 'integer'],
            [['date_start', 'date_end', 'created_at', 'updated_at', 'filterDate', 'filterCompanyId'], 'safe'],
            [['short_content', 'content'], 'string'],
            [['title', 'img_src'], 'string', 'max' => 255],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        if ($this->filterDate) {
            $query->andFilterWhere(['<=', 'date_start', $this->filterDate.' 23:59']);
            $query->andFilterWhere(['>=', 'date_end', $this->filterDate.' 00:00']);
        }

        if ($this->filterCompanyId) {
            $query->andWhere([
                'company_id' => $this->filterCompanyId,
            ]);
        }
        
        $query->andFilterWhere([
            'company_id' => $this->company_id,
        ])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
