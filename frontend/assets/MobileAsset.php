<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;
/**
 * Main frontend application asset bundle.
 */
class MobileAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      'build/scss/mobile-media.css',
      'build/scss/mobile.css',
    ];
    public $js = [
    ];
    public $jsOptions = array(
        'position' => View::POS_HEAD
    );
}
