<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;
/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        'css/plugins/glyphicon.css',
        'css/plugins/jquery-confirm.min.css',
        'css/plugins/sweetalert.css',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css',
        // 'css/basictable.css',
        'css/mycss.css',
        'css/mycss-media.css',
        'css/site.css',
        'css/css-site.css',
        'css/media.css',
        'build/scss/styles.css',
        'build/scss/theme.css',
        'build/scss/media.css',
    ];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyDoSL8IN0GJJOwChniCpdNoPvWQa0c7ZnI&libraries=places',
        'https://use.fontawesome.com/releases/v5.0.7/js/all.js',
        'js/plugins/dropzone.js',
        'js/plugins/jquery-confirm.min.js',
        'js/jquery-confirm.settings.js',
        'https://unpkg.com/sweetalert/dist/sweetalert.min.js',
        // s'js/jquery.basictable.min.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = array(
        'position' => View::POS_HEAD
    );
}
