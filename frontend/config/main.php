<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'business' => [
            'class' => 'frontend\modules\business\Module',
        ],
        'university' => [
            'class' => 'frontend\modules\university\Module',
        ],
        'profile' => [
            'class' => 'frontend\modules\profile\Module',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'services' => [
            'class' => 'frontend\modules\services\Module',
        ],
        'jobs' => [
            'class' => 'frontend\modules\jobs\Module',
        ],
        'trending' => [
            'class' => 'frontend\modules\trending\Module',
        ],
    ],
    'components' => [
        'language'=>'en-EN',
        'i18n' => [
            'class'=> Zelenin\yii\modules\I18n\components\I18N::className(),
            'languages' => ['ru-RU', 'en-EN'],
            'translations' => [
                'yii' => [
                    'class' => yii\i18n\DbMessageSource::className()
                ],
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ]
        ],
        'eauth' => [
                'class' => 'nodge\eauth\EAuth',
                'popup' => true, // Use the popup window instead of redirecting.
                'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
                'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
                'httpClient' => [
                        // uncomment this to use streams in safe_mode
                        //'useStreamsFallback' => true,
                ],
                'services' => [ // You can change the providers and their classes.
                    'facebook' => [
                        // register your app here: https://developers.facebook.com/apps/
                        'class' => 'nodge\eauth\services\FacebookOAuth2Service',
                        'clientId' => '2021503741439928',
//                        'clientId' => '2021503741439928',//server
                        'clientSecret' => '3fee97833c0d298f483adc3e56aa9620',
//                        'clientSecret' => '3fee97833c0d298f483adc3e56aa9620',
                        'title' => '',
                    ],
                    'linkedin_oauth2' => [
                        // register your app here: https://www.linkedin.com/secure/developer
                        'class' => 'nodge\eauth\services\LinkedinOAuth2Service',
                        'clientId' => '77dlzb9en5w61w',
                        'clientSecret' => 'NuHsf04j8SYTrzO9',
                        'title' => '',
                    ],
                ],
        ],
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
            'class' => 'frontend\components\LangRequest',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/eauth.log',
                    'categories' => ['nodge\eauth\*'],
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
       'assetManager' => [
           'bundles' => require(__DIR__ . '/assets.php'),
           'converter'=> [
                'class'=>'nizsheanez\assetConverter\Converter',
                'destinationDir' => 'build', //at which folder of @webroot put compiled files
                'force'=> true,
                'parsers' => [
                    'scss' => [ // file extension to parse
                        'class' => 'nizsheanez\assetConverter\Sass',
                        'output' => 'css', // parsed output file type
                        'options' => [] // optional options
                    ]
                ]
           ]
        ],
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',

                'university/<id:\d+>/' => 'university/university/view',
                'university/setting' => 'university/university/setting',
                'university/posts' => 'university/university/posts',
                'profile/<_a:[\w-]+>' => 'profile/profile/<_a>',
                'profile/<_a:[\w-]+>/<id:\d+>' => 'profile/profile/<_a>',

                'university/classes/<name:[\w-]+>/<_a>' => 'university/classes/<_a>',

                '<_c:[\w-]+>' => '<_c>/index',
                '<_c:[\w-]+>/<id:\d+>' => '<_c>/view',
                '<_c:[\w-]+>/<id:\d+>/<_a:[\w-]+>' => '<_c>/<_a>',
                '<_c:[\w-]+>/<_a:[\w-]+>/<id:\d+>' => '<_c>/<_a>',
                '<_m:[\w-]+>/<_c:[\w-]+>/<_a:[\w-]+>/<id:\d+>' => '<_m>/<_c>/<_a>',


                'login/<service:linkedin_oauth2|facebook|etc>' => 'site/login',
            ],
        ],
    ],
    'params' => $params,
];
