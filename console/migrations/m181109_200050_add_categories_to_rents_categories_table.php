<?php

use yii\db\Migration;

/**
 * Class m181109_200050_add_categories_to_rents_categories_table
 */
class m181109_200050_add_categories_to_rents_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `rents_categories` (`id`, `name`, `img_src`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
            (5, 'Community', NULL, NULL, 0, NULL, NULL),
            (6, 'Activities', NULL, 5, 0, NULL, NULL),
            (7, 'Artists', NULL, 5, 0, NULL, NULL),
            (8, 'Childcare', NULL, 5, 0, NULL, NULL),
            (9, 'housing', NULL, NULL, 0, NULL, NULL),
            (10, 'Roomates', NULL, 9, 0, NULL, NULL),
            (11, 'Rent a Hous (Long term)', NULL, 9, 0, NULL, NULL),
            (12, 'Rent an Apartment (Long term)', NULL, 9, 0, NULL, NULL),
            (13, 'jobs', NULL, NULL, 0, NULL, NULL),
            (14, 'Accounting+finance', NULL, 13, 0, NULL, NULL),
            (15, 'Admin/office', NULL, 13, 0, NULL, NULL),
            (16, 'Arch/Engineering', NULL, 13, 0, NULL, NULL),
            (17, 'Personals', NULL, NULL, 0, NULL, NULL),
            (18, 'Strictly plantonic', NULL, 17, 0, NULL, NULL),
            (19, 'Rants and raves', NULL, 17, 0, NULL, NULL),
            (20, 'Missed connections', NULL, 17, 0, NULL, NULL),
            (21, 'For sale', NULL, NULL, 0, NULL, NULL),
            (22, 'Antiques', NULL, 21, 0, NULL, NULL),
            (23, 'Appliances', NULL, 21, 0, NULL, NULL),
            (24, 'Art+Crafts', NULL, 21, 0, NULL, NULL),
            (25, 'Services', NULL, NULL, 0, NULL, NULL),
            (26, 'Automotive', NULL, 25, 0, NULL, NULL),
            (27, 'Legal', NULL, 25, 0, NULL, NULL),
            (28, 'Beauty', NULL, 25, 0, NULL, NULL),
            (29, 'Gigs', NULL, NULL, 0, NULL, NULL),
            (30, 'Computer', NULL, 29, 0, NULL, NULL),
            (31, 'Event', NULL, 29, 0, NULL, NULL),
            (32, 'Creative', NULL, 29, 0, NULL, NULL),
            (33, 'Discussion', NULL, NULL, 0, NULL, NULL),
            (34, 'Apple', NULL, 33, 0, NULL, NULL),
            (35, 'Photo', NULL, 33, 0, NULL, NULL),
            (36, 'Arts', NULL, 33, 0, NULL, NULL),
            (37, 'dd', NULL, 5, 0, NULL, NULL);
            ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181109_200050_add_categories_to_rents_categories_table cannot be reverted.\n";

        return false;
    }
}
