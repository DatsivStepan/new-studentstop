<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rents_images`.
 */
class m180724_195609_create_rents_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rents_images', [
            'id' => $this->primaryKey(),
            'rent_id' => $this->integer()->notNull(),
            'name' => $this->string()->null(),
            'img_src' => $this->string()->notNull(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rents_images');
    }
}
