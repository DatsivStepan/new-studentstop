<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m181115_185803_change_url_menu_item_in_rent_menu
 */
class m181115_185803_change_url_menu_item_in_rent_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($model = MenuItems::find()->where(['like', 'name', 'My account'])->andWhere(['type' => MenuItems::TYPE_RENTS])->one()) {
            $model->url = '/services/rents/my-rents';
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181115_185803_change_url_menu_item_in_rent_menu cannot be reverted.\n";

        return false;
    }
}
