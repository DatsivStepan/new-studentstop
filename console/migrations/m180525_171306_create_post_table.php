<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m180525_171306_create_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null(),
            'university_id' => $this->integer()->null(),
            'title' => $this->string()->null(),
            'content' => $this->text()->null(),
            'img_src' => $this->string()->null(),
            'type' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post');
    }
}
