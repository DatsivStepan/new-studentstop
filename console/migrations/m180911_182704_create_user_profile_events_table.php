<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_profile_events`.
 */
class m180911_182704_create_user_profile_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_profile_events', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'events_id' => $this->integer()->notNull(),
            'events_type' => $this->integer()->notNull(),
            'note' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_profile_events');
    }
}
