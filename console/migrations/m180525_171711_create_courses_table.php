<?php

use yii\db\Migration;

/**
 * Handles the creation of table `courses`.
 */
class m180525_171711_create_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('courses', [
            'id' => $this->primaryKey(),
            'major_id' => $this->integer()->null(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->null(),
            'status' => $this->integer()->null(),
            'creator' => $this->integer()->null(),
            'url_name' => $this->string()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
        
        $this->execute("
        INSERT INTO `courses` (`id`, `major_id`, `name`, `description`, `created_at`, `updated_at`, `status`, `creator`, `url_name`) VALUES
        (13, 1, 'ARCH 100. Architectural Foundations I', '', '2017-03-20 22:14:16', '2017-03-20 22:14:16', 1, 1, 'ARCH100'),
        (14, 1, 'ARCH 101. Architectural Foundations II', '', '2017-03-20 22:14:33', '2017-03-20 22:14:33', 1, 1, 'ARCH101'),
        (15, 1, 'ARCH 103. Introduction to Architecture', '', '2017-03-21 07:53:37', '2017-03-21 07:53:37', 1, 1, 'ARCH103'),
        (16, 1, 'ARCH 104. Principles of Modern Architecture', '', '2017-03-21 07:53:55', '2017-03-21 07:53:55', 1, 1, 'ARCH104'),
        (17, 1, 'ARCH 105. B.A. Architectural Studies Seminar.', '', '2017-03-21 07:54:21', '2017-03-21 07:54:21', 1, 1, 'ARCH105'),
        (18, 1, 'ARCH 106. B.A. Architectural Studies Seminar II', '', '2017-03-21 07:54:36', '2017-03-21 07:54:36', 1, 1, 'ARCH106'),
        (19, 2, 'ACCT 200. Fundamentals of Financial Accounting', '', '2017-03-21 07:55:13', '2017-03-21 07:55:13', 1, 1, 'ACCT200'),
        (20, 2, 'ACCT 201. Managerial Accounting I', '', '2017-03-21 07:55:27', '2017-03-21 07:55:27', 1, 1, 'ACCT201'),
        (21, 2, 'ACCT 205. Survey of Accounting', '', '2017-03-21 07:55:38', '2017-03-21 07:55:38', 1, 1, 'ACCT205'),
        (22, 2, 'ACCT 300. Special Topics in Accounting', '', '2017-03-21 07:55:52', '2017-03-21 07:55:52', 1, 1, 'ACCT300'),
        (23, 2, 'ACCT 303. Introduction to the Accounting Profession', '', '2017-03-21 07:56:09', '2017-03-21 07:56:09', 1, 1, 'ACCT303'),
        (24, 3, 'C&T 100. Introduction to the Education Profession', '', '2017-03-21 07:56:58', '2017-03-21 07:56:58', 1, 1, 'CT100'),
        (25, 3, 'C&T 177. First Year Seminar', '', '2017-03-21 07:57:16', '2017-03-21 07:57:16', 1, 1, 'CT177'),
        (26, 3, 'C&T 235. Multicultural Education', '', '2017-03-21 07:57:30', '2017-03-21 07:57:30', 1, 1, 'CT255'),
        (27, 4, 'AE 211. Computing for Engineers', '', '2017-03-21 07:58:10', '2017-03-21 07:58:10', 1, 1, 'AE211'),
        (28, 4, 'AE 221. Introduction to Global History of Aerospace Technologies', '', '2017-03-21 07:58:31', '2017-03-21 07:58:31', 1, 1, 'AE221'),
        (29, 4, 'AE 241. Private Flight Course', '', '2017-03-21 07:58:42', '2017-03-21 07:58:42', 1, 1, 'AE241'),
        (30, 4, 'AE 242. Private Flight Aeronautics', '', '2017-03-21 07:58:57', '2017-03-21 07:58:57', 1, 1, 'AE242'),
        (31, 5, 'ANTM 380. Special Topics In Anatomy', '', '2017-03-21 07:59:27', '2017-03-21 07:59:27', 1, 1, 'ANTM380'),
        (32, 5, 'CLS 520. Phlebotomy', '', '2017-03-21 07:59:44', '2017-03-21 07:59:44', 1, 1, 'CLS520'),
        (33, 5, 'CLS 530. Clinical Chemistry I', '', '2017-03-21 07:59:56', '2017-03-21 07:59:56', 1, 1, 'CLS530'),
        (34, 5, 'CLS 532. Clinical Microbiology I', '', '2017-03-21 08:00:14', '2017-03-21 08:00:14', 1, 1, 'CLS532'),
        (35, 6, 'JOUR 2. Grammar and Usage', '', '2017-03-21 08:00:49', '2017-03-21 08:00:49', 1, 1, 'JOUR2'),
        (36, 6, 'JOUR 101. Media and Society', '', '2017-03-21 08:05:12', '2017-03-21 08:05:13', 1, 1, 'JOUR101'),
        (37, 6, 'JOUR 150. Stand and Deliver', '', '2017-03-21 08:05:23', '2017-03-21 08:05:23', 1, 1, 'JOUR150'),
        (38, 6, 'JOUR 177. First Year Seminar', '', '2017-03-21 08:06:12', '2017-03-21 08:06:12', 1, 1, 'JOUR177'),
        (39, 6, 'LAW 804. Civil Procedure', '', '2017-03-21 08:07:13', '2017-03-21 08:07:13', 1, 1, 'LAW804'),
        (40, 6, 'LAW 809. Contracts', '', '2017-03-21 08:07:48', '2017-03-21 08:07:48', 1, 1, 'LAW809'),
        (41, 6, 'LAW 814. Criminal Law', '', '2017-03-21 08:09:07', '2017-03-21 08:09:07', 1, 1, 'LAW814'),
        (42, 7, 'ACMP 527. Accompanying', '', '2017-03-21 08:10:59', '2017-03-21 08:10:59', 1, 1, 'ACMP527'),
        (43, 7, 'ACMP 727. Accompanying', '', '2017-03-21 08:11:16', '2017-03-21 08:11:16', 1, 1, 'ACMP727'),
        (44, 7, 'BAND 202. Wind Ensemble', '', '2017-03-21 08:11:30', '2017-03-21 08:11:30', 1, 1, 'BAND202'),
        (45, 7, 'NRSG 520. Medical Spanish I', '', '2017-03-21 08:20:33', '2017-03-21 08:20:33', 1, 1, 'NRSG520'),
        (46, 7, 'NRSG 521. Medical Spanish II', '', '2017-03-21 08:20:45', '2017-03-21 08:20:45', 1, 1, 'NRSG521'),
        (47, 7, 'NRSG 550. Research Coordinator', '', '2017-03-21 08:20:56', '2017-03-21 08:20:56', 1, 1, 'NRSG550'),
        (48, 8, 'MDCM 601. Medicinal Biochemistry I', '', '2017-03-21 08:21:31', '2017-03-21 08:21:31', 1, 1, 'MDCM601'),
        (49, 8, 'MDCM 603. Medicinal Biochemistry II', '', '2017-03-21 08:21:41', '2017-03-21 08:21:41', 1, 1, 'MDCM603'),
        (50, 8, 'MDCM 606. Phytomedicinal Agents', '', '2017-03-21 08:21:51', '2017-03-21 08:21:51', 1, 1, 'MDCM606');
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('courses');
    }
}
