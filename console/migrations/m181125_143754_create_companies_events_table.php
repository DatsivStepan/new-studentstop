<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies_events`.
 */
class m181125_143754_create_companies_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies_events', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'date_start' => $this->dateTime()->notNull(),
            'date_end' => $this->dateTime()->notNull(),
            'title' => $this->string()->notNull(),
            'short_content' => $this->text()->null(),
            'content' => $this->text()->notNull(),
            'img_src' => $this->string()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('companies_events');
    }
}
