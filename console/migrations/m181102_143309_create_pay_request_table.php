<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pay_request`.
 */
class m181102_143309_create_pay_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pay_requests', [
            'id' => $this->primaryKey(),
            'typePay' => $this->integer()->notNull(),
            'module' => $this->integer()->notNull(),
            'amount' => $this->decimal(10, 2)->null(),
            'from_user_id' => $this->integer()->null(),
            'to_user_id' => $this->integer()->null(),
            'operationType' => $this->string()->null(),
            'note' => $this->text()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pay_request');
    }
}
