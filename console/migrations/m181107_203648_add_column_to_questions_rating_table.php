<?php

use yii\db\Migration,
    common\models\business\QuestionsRating;

/**
 * Class m181107_203648_add_column_to_questions_rating_table
 */
class m181107_203648_add_column_to_questions_rating_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(QuestionsRating::tableName(), 'type', $this->integer()->defaultValue(0)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181107_203648_add_column_to_questions_rating_table cannot be reverted.\n";

        return false;
    }
}
