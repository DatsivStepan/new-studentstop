<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m181027_135328_add_item_to_menu_for_class
 */
class m181027_135328_add_item_to_menu_for_class extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $arrayItem = [
            'Dashboard' => [
                'url' => '/university/classes/#name/view',
            ],
            'Tutors' => [
                'url' => '/university/classes/#name/tutors',
            ],
            'Products' => [
                'url' => '/university/classes/#name/products',
            ],
            'Files' => [
                'url' => '/university/classes/#name/files',
            ],
        ];

        foreach ($arrayItem as $name => $item) {
            $model = new MenuItems();
            $model->name = $name;
            $model->type = (string)MenuItems::TYPE_CLASS_ONE;
            $model->url = $item['url'];
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181027_135328_add_item_to_menu_for_class cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181027_135328_add_item_to_menu_for_class cannot be reverted.\n";

        return false;
    }
    */
}
