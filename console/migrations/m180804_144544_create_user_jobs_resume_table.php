<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_jobs_resume`.
 */
class m180804_144544_create_user_jobs_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_jobs_resume', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'avatar' => $this->string()->null(),
            'about' => $this->string()->null(),
            'skills' => $this->string()->null(),
            'current_gpa' => $this->decimal(10,2)->null(),
            'overall_gpa' => $this->decimal(10,2)->null(),

            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_jobs_resume');
    }
}
