<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_friend`.
 */
class m180525_171221_create_user_friend_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_friend', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null(),
            'friend_id' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_friend');
    }
}
