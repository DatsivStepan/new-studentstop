<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m181030_100832_some_fix_for_menu_classes
 */
class m181030_100832_some_fix_for_menu_classes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($model = MenuItems::findOne(['url' => '/university/classes/my-classes', 'name' => 'Dashboard', 'type' => MenuItems::TYPE_CLASSES])) {
            $model->name = 'My Classes';
            $model->save();
        }

        $model = new MenuItems();
        $model->name = 'My Classes';
        $model->type = (string)MenuItems::TYPE_CLASS_ONE;
        $model->url = '/university/classes/my-classes';
        $model->sort = 0;
        $model->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181030_100832_some_fix_for_menu_classes cannot be reverted.\n";

        return false;
    }
}
