<?php

use yii\db\Migration,
    common\models\Configuration;

/**
 * Class m181108_155511_add_configuration_item_end_class
 */
class m181108_155511_add_configuration_item_end_class extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $modelC1 = new Configuration;
        $modelC1->name = 'Class fihished date 1 (month-day)';
        $modelC1->key = 'fihishedDate1';
        $modelC1->value = '01-01';
        $modelC1->save();

        $modelC1 = new Configuration;
        $modelC1->name = 'Class fihished date 2 (month-day)';
        $modelC1->key = 'fihishedDate2';
        $modelC1->value = '01-07';
        $modelC1->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181108_155511_add_configuration_item_end_class cannot be reverted.\n";

        return false;
    }
}
