<?php

use yii\db\Migration;

/**
 * Class m181007_163550_add_column_to_posts_table
 */
class m181007_163550_add_column_to_posts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(common\models\Post::tableName(), 'inSlider', $this->smallInteger()->null()->after('status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181007_163550_add_column_to_posts_table cannot be reverted.\n";

        return false;
    }

}
