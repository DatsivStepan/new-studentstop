<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m180609_080842_add_item_for_menu
 */
class m180609_080842_add_item_for_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = MenuItems::findOne(19);
        $model->url = 'services/classified-market/index';
        $model->save();

        $arrayItem = [
            'Dashboard' => [
                'url' => '/services/classified-market/index',
                'class_attr' => null,
            ],
            'My favorites' => [
                'url' => '/services/classified-market/favorites',
                'class_attr' => null,
            ],
            'My account' => [
                'url' => '/services/classified-market/my-account',
                'class_attr' => null,
            ],
            'Add post' => [
                'url' => null,
                'class_attr' => 'js-add-classified-market',
            ],
        ];

        foreach ($arrayItem as $name => $item) {
            $model = new MenuItems();
            $model->name = $name;
            $model->type = (string)MenuItems::TYPE_VIRTUAL_CLASSIFIED_MARKET;
            $model->url = $item['url'];
            $model->class_attr = $item['class_attr'];
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180609_080842_add_item_for_menu cannot be reverted.\n";

        return false;
    }
}
