<?php

use yii\db\Migration,
    common\models\pay\PayRequests;

/**
 * Class m190124_205828_add_column_to_pay_request_table
 */
class m190124_205828_add_column_to_pay_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(PayRequests::tableName(), 'pay_status', $this->integer()->defaultValue(0)->after('note'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190124_205828_add_column_to_pay_request_table cannot be reverted.\n";

        return false;
    }
}
