<?php

use yii\db\Migration;

/**
 * Class m181108_132014_add_url_to_header_menu_item
 */
class m181108_132014_add_url_to_header_menu_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($model = \common\models\MenuItems::find()->where(['like', 'name', 'Ranking'])->andWhere(['type' => '', 'url' => ''])->one()) {
            $model->url = '/trending/rating/universities';
            $model->save();
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181108_132014_add_url_to_header_menu_item cannot be reverted.\n";

        return false;
    }
}
