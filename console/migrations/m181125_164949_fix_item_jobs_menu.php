<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m181125_164949_fix_item_jobs_menu
 */
class m181125_164949_fix_item_jobs_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($model = MenuItems::findOne(['name' => 'Search', 'type' => MenuItems::TYPE_JOBS_COMPANY])) {
            $model->url = '/jobs/companies/search';
            $model->save();
        }
        if ($model = MenuItems::findOne(['name' => 'Search', 'type' => MenuItems::TYPE_JOBS_STUDENT])) {
            $model->url = '/jobs/student/search';
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181125_164949_fix_item_jobs_menu cannot be reverted.\n";

        return false;
    }

}
