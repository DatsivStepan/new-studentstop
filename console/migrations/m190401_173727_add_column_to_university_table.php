<?php

use yii\db\Migration,
    common\models\University;

/**
 * Class m190401_173727_add_column_to_university_table
 */
class m190401_173727_add_column_to_university_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(University::tableName(), 'location_lat', $this->string()->null()->after('location'));
        $this->addColumn(University::tableName(), 'location_lng', $this->string()->null()->after('location'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190401_173727_add_column_to_university_table cannot be reverted.\n";

        return false;
    }
}
