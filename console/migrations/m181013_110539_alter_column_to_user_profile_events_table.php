<?php

use yii\db\Migration,
    common\models\user\UserProfileEvents;

/**
 * Class m181013_110539_alter_column_to_user_profile_events_table
 */
class m181013_110539_alter_column_to_user_profile_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(UserProfileEvents::tableName(), 'note', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181013_110539_alter_column_to_user_profile_events_table cannot be reverted.\n";

        return false;
    }

}
