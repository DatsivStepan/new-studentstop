<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m181002_180657_fix_profile_item_menu
 */
class m181002_180657_fix_profile_item_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($model = MenuItems::findOne(['url' => 'profile/about', 'name' => 'About', 'type' => MenuItems::TYPE_PROFILE])) {
            $model->url = null;
            $model->class_attr = 'js-open-about-profile';
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181002_180657_fix_profile_item_menu cannot be reverted.\n";

        return false;
    }

}
