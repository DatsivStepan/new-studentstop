<?php

use yii\db\Migration;

/**
 * Handles the creation of table `questions_answers`.
 */
class m180722_084523_create_questions_answers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('questions_answers', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null(),
            'question_id' => $this->integer()->null(),

            'answer' => $this->string()->notNull(),
            'show_answer' => $this->string()->null(),
            
            'money' => $this->string()->null(),
            'credit' => $this->string()->null(),
            
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('questions_answers');
    }
}
