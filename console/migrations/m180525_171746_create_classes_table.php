<?php

use yii\db\Migration;

/**
 * Handles the creation of table `classes`.
 */
class m180525_171746_create_classes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('classes', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->null(),
            'university_id' => $this->integer()->null(),
            'name' => $this->string()->notNull(),
            'description' => $this->string()->null(),
            'status' => $this->integer()->defaultValue(0)->notNull(),
            'creator' => $this->integer()->null(),
            'professor' => $this->integer()->null(),
            'url_name' => $this->string()->null(),
            'img_src' => $this->string()->null(),
            'date_active' => $this->string()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('classes');
    }
}
