<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies_jobs_invite`.
 */
class m181126_085950_create_companies_jobs_invite_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies_jobs_invite', [
            'id' => $this->primaryKey(),
            'company_job_id' => $this->integer()->notNull(),
            'to_user_id' => $this->integer()->notNull(),
            'salary' => $this->decimal(10,2)->null(),
            'salary_type' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('companies_jobs_invite');
    }
}
