<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m181115_184911_rename_menu_item_in_rent_menu
 */
class m181115_184911_rename_menu_item_in_rent_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($model = MenuItems::find()->where(['like', 'name', 'Add booking'])->andWhere(['type' => MenuItems::TYPE_RENTS])->one()) {
            $model->name = 'My booking';
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181115_184911_rename_menu_item_in_rent_menu cannot be reverted.\n";

        return false;
    }
}
