<?php

use yii\db\Migration;

/**
 * Class m180721_101923_add_rents_house_table
 */
class m180721_101923_add_rents_house_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rents_house', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'university_id' => $this->integer()->notNull(),
            'type' => $this->integer()->null(),

            'title' => $this->string()->null(),
            'content' => $this->text()->null(),
            'img_src' => $this->string()->null(),

            'house_type' => $this->integer()->null(),
            'room_count' => $this->integer()->null(),
            'min_rent' => $this->integer()->null(),
            'adults' => $this->integer()->null(),

            'type_pay' => $this->integer()->null(),
            'credits' => $this->float()->null(),
            'money' => $this->float()->null(),
            
            'publish' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            
            'address' => $this->string()->null(),
            'lat' => $this->string()->null(),
            'lng' => $this->string()->null(),

            'attr_wifi' => $this->smallInteger(1)->null(),
            'attr_parking' => $this->smallInteger(1)->null(),
            'attr_smoking' => $this->smallInteger(1)->null(),
            'attr_tv' => $this->smallInteger(1)->null(),
            'attr_animal' => $this->smallInteger(1)->null(),
            'attr_conditioner' => $this->smallInteger(1)->null(),

            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rents_house');
    }

}
