<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tutors`.
 */
class m180525_171724_create_tutors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('courses_tutors', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'description' => $this->text()->null(),
            'price' => $this->float()->null(),
            'courses' => $this->text()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tutors');
    }
}
