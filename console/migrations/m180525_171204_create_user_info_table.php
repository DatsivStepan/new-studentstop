<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_info`.
 */
class m180525_171204_create_user_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_info', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'name' => $this->string()->null(),
            'surname' => $this->string()->null(),
            'university_id' => $this->integer()->null(),
            'major_id' => $this->integer()->null(),
            'user_role_id' => $this->integer()->null(),
            'birthday' => $this->date()->null(),
            'mobile' => $this->string(50)->null(),
            'address' => $this->string()->null(),
            'website' => $this->string()->null(),
            'facebook' => $this->string()->null(),
            'twitter' => $this->string()->null(),
            'linkedin' => $this->string()->null(),
            'gender' => $this->smallInteger(1)->null(),
            'avatar' => $this->string()->null(),
            'avatar_wrapper' => $this->string()->null(),
            'position_lat' => $this->string()->null(),
            'position_lng' => $this->string()->null(),
            'about' => $this->text()->null(),
            'date_end_education' => $this->date()->null(),
            'date_begin_education' => $this->date()->null(),
            'industry_id' => $this->integer()->null(),
            'jobs_function_id' => $this->integer()->null(),
            'country_id' => $this->integer()->null(),
            'states_id' => $this->integer()->null(),
            'city_id' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_info');
    }
}
