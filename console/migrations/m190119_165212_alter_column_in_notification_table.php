<?php

use yii\db\Migration,
    common\models\Notifications;

/**
 * Class m190119_165212_alter
 */
class m190119_165212_alter_column_in_notification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(Notifications::tableName(), 'text', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190119_165212_alter cannot be reverted.\n";

        return false;
    }
}
