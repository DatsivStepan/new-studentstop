<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m180925_172642_fix_for_menu_items
 */
class m180925_172642_fix_for_menu_items extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($model = MenuItems::findOne(['url' => 'profile', 'name' => 'Dashboard', 'type' => MenuItems::TYPE_PROFILE])) {
            $model->url = 'profile/index';
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180925_172642_fix_for_menu_items cannot be reverted.\n";

        return false;
    }

}
