<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rents_booking`.
 */
class m180714_082414_create_rents_booking_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rents_booking', [
            'id' => $this->primaryKey(),
            'rent_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),

            'rent_type' => $this->string()->null(),
            'date_reserve' => $this->dateTime()->notNull(),

            'reserve_from' => $this->dateTime()->notNull(),
            'reserve_to' => $this->dateTime()->notNull(),

            'rating_count' => $this->dateTime()->null(),
            'rating_comment' => $this->dateTime()->null(),

            'status' => $this->integer()->null(),

            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rents_booking');
    }
}
