<?php

use yii\db\Migration;

/**
 * Handles the creation of table `university`.
 */
class m180525_171236_create_university_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('university', [
            'id' => $this->primaryKey(),
            'id_owner' => $this->integer()->null(),
            'name' => $this->string()->notNull(),
            'history' => $this->text()->null(),
            'status' => $this->integer()->null(),
            'slug' => $this->string()->null(),
            'date' => $this->dateTime()->null(),
            'image' => $this->string()->null(),
            'location' => $this->string()->null(),
            'year_founded' => $this->dateTime()->null(),
            'facebook_link' => $this->string()->null(),
            'linkedin_link' => $this->string()->null(),
            'twitter_link' => $this->string()->null(),
            'country_id' => $this->integer()->null(),
            'region_id' => $this->integer()->null(),
            'telephone' => $this->string(100)->null(),
            'email' => $this->string()->null(),
            'website' => $this->string()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
        $this->batchInsert('{{%university}}', ['id_owner', 'name', 'history', 'country_id', 'region_id', 'created_at', 'updated_at'], 
            [['1', 'The University of Kansas', 'History The University of Kansas', 231, '4140', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")]]);        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('university');
    }
}
