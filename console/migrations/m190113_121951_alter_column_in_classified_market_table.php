<?php

use yii\db\Migration,
    common\models\services\ClassifiedMarket;

/**
 * Class m190113_121951_alter_column_in_classified_market_table
 */
class m190113_121951_alter_column_in_classified_market_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(ClassifiedMarket::tableName(), 'image_src', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190113_121951_alter_column_in_classified_market_table cannot be reverted.\n";

        return false;
    }
}
