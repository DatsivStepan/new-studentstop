<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m180722_093017_add_items_to_menu_for_question_bank
 */
class m180722_093017_add_items_to_menu_for_question_bank extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = MenuItems::find()->where(['like', 'name', 'Question bank'])->one();
        $model->url = '/business/questions/index';
        $model->save();

        $arrayItem = [
            'Search' => [
                'url' => '/business/questions/search',
                'class_attr' => null,
            ],
            'My favorites' => [
                'url' => '/business/questions/favorites',
                'class_attr' => null,
            ],
            'Post question' => [
                'url' => null,
                'class_attr' => 'js-create-new-question',
            ],
            'My question' => [
                'url' => '/business/questions/my-questions',
                'class_attr' => null,
            ],
            'My answer' => [
                'url' => '/business/questions/my-answers',
                'class_attr' => null,
            ],
            'Invite' => [
                'url' => '/business/questions/invite',
                'class_attr' => null,
            ],
        ];

        foreach ($arrayItem as $name => $item) {
            $model = new MenuItems();
            $model->name = $name;
            $model->type = (string)MenuItems::TYPE_QUESTION_BANK;
            $model->url = $item['url'];
            $model->class_attr = $item['class_attr'];
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180722_093017_add_items_to_menu_for_question_bank cannot be reverted.\n";

        return false;
    }

}
