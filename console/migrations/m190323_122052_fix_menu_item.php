<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m190323_122052_fix_menu_item
 */
class m190323_122052_fix_menu_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($model = MenuItems::findOne(['name' => 'Files & History'])) {
            $model->url = '/trending/files-history/files';
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190323_122052_fix_menu_item cannot be reverted.\n";

        return false;
    }
}
