<?php

use yii\db\Migration;

/**
 * Handles the creation of table `classes_events`.
 */
class m180602_111354_create_classes_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('classes_events', [
            'id' => $this->primaryKey(),
            'class_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'date_start' => $this->dateTime()->notNull(),
            'date_end' => $this->dateTime()->notNull(),
            'title' => $this->string()->notNull(),
            'short_content' => $this->text()->null(),
            'content' => $this->text()->notNull(),
            'img_src' => $this->string()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('classes_events');
    }
}
