<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_files`.
 */
class m180910_205046_create_user_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_files', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'file_src' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_files');
    }
}
