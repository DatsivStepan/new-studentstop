<?php

use yii\db\Migration;

/**
 * Handles the creation of table `virtual_classes`.
 */
class m180525_200535_create_virtual_classes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('virtual_classes', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->notNull(),
            'tutors_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'date_start' => $this->dateTime()->notNull(),
            'img_src' => $this->string()->null(),
            'description' => $this->text()->null(),
            'slug' => $this->string()->null(),
            'status' => $this->integer()->null(),
            'type' => $this->integer()->null(),
            'price_type' => $this->integer()->null(),
            'price_m' => $this->integer()->null(),
            'price_c' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('virtual_classes');
    }
}
