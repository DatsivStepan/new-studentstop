<?php

use yii\db\Migration,
    common\models\pay\PayRequests;

/**
 * Class m190127_113651_addColumn_pay_requests_table
 */
class m190127_113651_addColumn_pay_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(PayRequests::tableName(), 'paypal_id', $this->string()->null()->after('note'));
        $this->addColumn(PayRequests::tableName(), 'paypal_hash', $this->string()->null()->after('paypal_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190127_113651_addColumn_pay_requests_table cannot be reverted.\n";

        return false;
    }
}
