<?php

use yii\db\Migration;

/**
 * Handles the creation of table `jobs_industries`.
 */
class m180805_095921_create_jobs_industries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jobs_industries', [
            'id' => $this->primaryKey(),
            'name' => $this->integer()->notNull(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('jobs_industries');
    }
}
