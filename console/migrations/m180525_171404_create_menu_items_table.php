<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu_items`.
 */
class m180525_171404_create_menu_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('menu_items', [
            'id' => $this->primaryKey(),
            'logo' => $this->string()->null(),
            'name' => $this->string()->null(),
            'type' => $this->string(100)->null(),
            'url' => $this->string()->null(),
            'class_attr' => $this->string()->null(),
            'parent_id' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            'sort' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);

        $this->execute("
           INSERT INTO `menu_items` (`id`, `logo`, `name`, `type`, `url`, `parent_id`, `status`, `class_attr`, `sort`, `created_at`, `updated_at`) VALUES
           (11, '', 'University', '3', 'university', NULL, 1, NULL, 1, '2018-02-24 16:51:21', '2018-02-24 17:15:56'),
           (2, '', 'Dashboard', '1', 'profile', NULL, 1, NULL, NULL, '2018-02-24 07:19:53', '2018-02-24 17:15:56'),
           (3, '', 'About', '1', 'profile/about', NULL, 0, NULL, NULL, '2018-02-24 07:20:33', '2018-02-24 17:15:56'),
           (4, '', 'Photos & Videos', '1', 'profile/photos-videos', NULL, 1, NULL, NULL, '2018-02-24 07:21:23', '2018-02-24 17:15:56'),
           (5, '', 'Friends', '1', 'profile/friends', NULL, 1, NULL, NULL, '2018-02-24 07:21:52', '2018-02-24 17:15:56'),
           (6, '', 'Marketplace', '1', 'profile/market', NULL, 1, NULL, NULL, '2018-02-24 07:22:31', '2018-02-24 17:15:56'),
           (7, '', 'Courses & Classes', '', '/university/classes/my-classes', 11, 1, NULL, 2, '2018-02-24 07:24:32', '2018-02-24 17:15:56'),
           (8, '', 'Ranking and Rating', '1', 'profile/rating', NULL, 1, NULL, NULL, '2018-02-24 07:25:22', '2018-02-24 17:15:56'),
           (9, '', 'My  docs', '1', 'docs', NULL, 1, NULL, NULL, '2018-02-24 07:25:47', '2018-02-24 17:15:56'),
           (10, '', 'Credits', '1', 'profile/docs', NULL, 1, NULL, NULL, '2018-02-24 07:26:16', '2018-02-24 17:15:56'),
           (12, '', 'Service', '3', 'service', NULL, 1, NULL, 2, '2018-02-24 16:52:47', '2018-02-24 17:15:56'),
           (13, '', 'Business', '3', 'business', NULL, 1, NULL, 3, '2018-02-24 16:53:18', '2018-02-24 17:15:56'),
           (14, '', 'Trending', '3', 'trending', NULL, 1, NULL, 4, '2018-02-24 16:53:47', '2018-02-24 17:15:56'),
           (15, '', 'Search', '', 'search', 11, 1, NULL, 1, '2018-02-24 17:21:01', '2018-02-24 17:15:56'),
           (31, '', 'Find class', '2', '/university/courses/classes-search', NULL, 1, NULL, 2, '2018-02-25 14:56:59', '2018-02-24 17:15:56'),
           (17, '', 'Virtual Class', '', '/university/courses/virtual-classes-search', 11, 1, NULL, 4, '2018-02-24 17:24:10', '2018-02-24 17:15:56'),
           (18, '', 'Find university', '', 'university/search', NULL, 1, NULL, 4, '2018-02-24 17:25:16', '2018-02-24 17:15:56'),
           (19, '', 'Classified market', '', '', 12, 1, NULL, 1, '2018-02-24 17:27:13', '2018-02-24 17:15:56'),
           (20, '', 'Question bank', '', 'question-bank', 13, 1, NULL, 2, '2018-02-24 17:27:45', '2018-02-24 17:15:56'),
           (21, '', 'Borrow', '', 'Borrow', 12, 1, NULL, 2, '2018-02-24 17:44:11', '2018-02-24 17:15:56'),
           (22, '', 'Job & recruitment', '', 'job', 13, 1, NULL, 1, '2018-02-24 17:46:17', '2018-02-24 17:15:56'),
           (23, '', 'Rent a house', '', '', 12, 1, NULL, 4, '2018-02-24 17:47:24', '2018-02-24 17:15:56'),
           (24, '', 'Ranking', '', '', 14, 1, NULL, 1, '2018-02-24 17:48:06', '2018-02-24 17:15:56'),
           (25, '', 'Files & History', '', '', 14, 1, NULL, 2, '2018-02-24 17:48:30', '2018-02-24 17:15:56'),
           (26, '', 'Profile', '4', '/profile/index', NULL, 1, NULL, 1, '2018-02-24 17:55:45', '2018-02-24 17:15:56'),
           (27, '', 'Tutor', '', '/profile/tutor', NULL, 1, NULL, 2, '2018-02-24 17:56:17', '2018-02-24 17:15:56'),
           (28, '', 'Photos & Video', '4', '/profile/photos-videos', NULL, 1, NULL, 3, '2018-02-24 17:57:31', '2018-02-24 17:15:56'),
           (29, '', 'Courses & Classes', '1', '/profile/courses-classes', NULL, 1, NULL, 5, '2018-02-25 07:31:06', '2018-02-24 17:15:56'),
           (30, '', 'Dashboard', '2', '/university/classes/my-classes', NULL, 1, NULL, 1, '2018-02-25 14:53:07', '2018-02-24 17:15:56'),
           (32, '', 'Dashboard', '5', '/university/courses/my-virtual-classes', NULL, 1, NULL, 1, '2018-05-20 11:28:02', NULL),
           (33, '', 'Search', '5', '/university/courses/virtual-classes-search', NULL, 1, NULL, 2, '2018-05-20 11:28:52', NULL),
           (34, '', 'Favorite', '5', '', NULL, 1, NULL, 3, '2018-05-20 11:29:36', NULL),
           (35, '', 'Become a tutor', '5', '', NULL, 1, 'js-become-tutor', 4, '2018-05-20 11:30:23', '2018-05-20 12:57:39'),
           (36, '', 'Favorite', '6', '', NULL, 1, NULL, 3, '2018-05-20 11:29:36', NULL),
           (37, '', 'Search', '6', '/university/courses/virtual-classes-search', NULL, 1, NULL, 2, '2018-05-20 11:28:52', NULL),
           (38, '', 'Dashboard', '6', '/university/courses/my-virtual-classes', NULL, 1, NULL, 1, '2018-05-20 11:28:02', NULL),
           (39, '', 'Invite', '6', '', NULL, 1, NULL, 4, '2018-05-20 11:32:19', NULL),
           (40, '', 'Archived', '6', '', NULL, 1, NULL, 5, '2018-05-20 11:32:40', NULL),
           (41, '', 'My profile', '6', '/university/virtual-classes/tutor-profile', NULL, 1, '', 6, '2018-05-20 11:32:56', '2018-05-20 17:38:51');
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('menu_items');
    }
}
