<?php

use yii\db\Migration;

/**
 * Class m180713_185204_add_column_to_rent
 */
class m180713_185204_add_column_to_rent extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(common\models\services\Rents::tableName(), 'category_id', $this->integer()->notNull()->after('university_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180713_185204_add_column_to_rent cannot be reverted.\n";

        return false;
    }

}
