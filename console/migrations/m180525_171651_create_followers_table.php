<?php

use yii\db\Migration;

/**
 * Handles the creation of table `followers`.
 */
class m180525_171651_create_followers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('followers', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'object_id' => $this->integer()->notNull(),
            'type' => $this->string(50)->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('followers');
    }
}
