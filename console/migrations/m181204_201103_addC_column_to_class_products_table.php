<?php

use yii\db\Migration,
    common\models\university\ClassProducts;

/**
 * Class m181204_201103_addC_column_to_class_products_table
 */
class m181204_201103_addC_column_to_class_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(ClassProducts::tableName(), 'class_id', $this->smallInteger()->null()->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181204_201103_addC_column_to_class_products_table cannot be reverted.\n";

        return false;
    }

}
