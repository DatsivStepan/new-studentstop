<?php

use yii\db\Migration;

/**
 * Handles the creation of table `classified_market`.
 */
class m180607_203120_create_classified_market_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('classified_market', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'university_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'image_src' => $this->string()->notNull(),
            'content' => $this->text()->null(),
            'address' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('classified_market');
    }
}
