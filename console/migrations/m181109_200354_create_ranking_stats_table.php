<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ranking_stats`.
 */
class m181109_200354_create_ranking_stats_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ranking_stats', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ranking_stats');
    }
}
