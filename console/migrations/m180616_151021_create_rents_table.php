<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rents`.
 */
class m180616_151021_create_rents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rents', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'university_id' => $this->integer()->notNull(),
            'type' => $this->integer()->null(),

            'title' => $this->string()->null(),
            'content' => $this->text()->null(),
            'img_src' => $this->string()->null(),

            'type_pay' => $this->integer()->null(),
            'credits' => $this->float()->null(),
            'money' => $this->float()->null(),
            
            'publish' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            
            'address' => $this->string()->null(),
            'lat' => $this->string()->null(),
            'lng' => $this->string()->null(),
            
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rents');
    }
}
