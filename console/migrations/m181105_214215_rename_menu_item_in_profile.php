<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m181105_214215_rename_menu_item_in_profile
 */
class m181105_214215_rename_menu_item_in_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($model = MenuItems::findOne(['name' => 'Credits', 'type' => MenuItems::TYPE_PROFILE])) {
            $model->url = '/profile/credits';
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181105_214215_rename_menu_item_in_profile cannot be reverted.\n";

        return false;
    }
}
