<?php

use yii\db\Migration,
    common\models\business\QuestionsAnswers;

/**
 * Class m181102_145000_alter_column_in_questions_answers_table
 */
class m181102_145000_alter_column_in_questions_answers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(QuestionsAnswers::tableName(), 'money', $this->decimal(10, 2)->defaultValue(0)->notNull());
        $this->alterColumn(QuestionsAnswers::tableName(), 'credit', $this->decimal(10, 2)->defaultValue(0)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181102_145000_alter_column_in_questions_answers_table cannot be reverted.\n";

        return false;
    }

}
