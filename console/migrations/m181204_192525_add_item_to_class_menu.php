<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m181204_192525_add_item_to_class_menu
 */
class m181204_192525_add_item_to_class_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = new MenuItems();
        $model->name = 'Find Class';
        $model->type = (string)MenuItems::TYPE_CLASS_ONE;
        $model->url = '/university/courses/classes-search';
        $model->sort = 10;
        $model->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181204_192525_add_item_to_class_menu cannot be reverted.\n";

        return false;
    }

}
