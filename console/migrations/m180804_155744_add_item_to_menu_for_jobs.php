<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m180804_155744_add_item_to_menu_for_jobs
 */
class m180804_155744_add_item_to_menu_for_jobs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = MenuItems::find()->where(['like', 'name', 'Job & recruitment'])->one();
        $model->url = '/jobs/default/index';
        $model->save();

        $arrayItemStudent = [
            'Dashboard' => [
                'url' => '/jobs/default/index',
                'class_attr' => null,
            ],
            'Search' => [
                'url' => '/jobs/companies/search',
                'class_attr' => null,
            ],
            'My resume' => [
                'url' => null,
                'class_attr' => 'js-jobs-show-my-resume',
            ]
        ];

        foreach ($arrayItemStudent as $name => $item) {
            $model = new MenuItems();
            $model->name = $name;
            $model->type = (string)MenuItems::TYPE_JOBS_STUDENT;
            $model->url = $item['url'];
            $model->class_attr = $item['class_attr'];
            $model->save();
        }

        $arrayItemCompany = [
            'Dashboard' => [
                'url' => '/jobs/default/index',
                'class_attr' => null,
            ],
            'My company' => [
                'url' => null,
                'class_attr' => 'js-jobs-show-my-company',
            ],
            'My vacancies' => [
                'url' => '/jobs/companies/jobs',
                'class_attr' => null,
            ],
            'My followers' => [
                'url' => '/jobs/companies/followers',
                'class_attr' => null,
            ],
            'Search' => [
                'url' => '/jobs/student/search',
                'class_attr' => null,
            ],
        ];

        foreach ($arrayItemCompany as $name => $item) {
            $model = new MenuItems();
            $model->name = $name;
            $model->type = (string)MenuItems::TYPE_JOBS_COMPANY;
            $model->url = $item['url'];
            $model->class_attr = $item['class_attr'];
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180804_155744_add_item_to_menu_for_jobs cannot be reverted.\n";

        return false;
    }

}
