<?php

use yii\db\Migration;

/**
 * Handles the creation of table `class_files`.
 */
class m181028_131657_create_class_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('class_files', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'class_id' => $this->integer()->notNull(),
            'file_src' => $this->string()->notNull(),
            'name' => $this->string()->null(),
            'typeClass' => $this->integer()->defaultValue(0),
            'mimeType' => $this->string()->null(),
            'status' => $this->integer()->null(),
            'description' => $this->text()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('class_files');
    }
}
