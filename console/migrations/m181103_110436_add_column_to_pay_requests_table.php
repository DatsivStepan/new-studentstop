<?php

use yii\db\Migration,
    common\models\pay\PayRequests;

/**
 * Class m181103_110436_add_column_to_pay_requests_table
 */
class m181103_110436_add_column_to_pay_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(PayRequests::tableName(), 'objectId', $this->integer()->null()->after('to_user_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181103_110436_add_column_to_pay_requests_table cannot be reverted.\n";

        return false;
    }
}
