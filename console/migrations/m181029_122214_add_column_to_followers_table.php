<?php

use yii\db\Migration,
    common\models\Followers;

/**
 * Class m181029_122214_add_column_to_followers_table
 */
class m181029_122214_add_column_to_followers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Followers::tableName(), 'statusF', $this->integer()->defaultValue(1)->after('type'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181029_122214_add_column_to_followers_table cannot be reverted.\n";

        return false;
    }
}
