<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_files`.
 */
class m180525_171328_create_post_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post_files', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'file_src' => $this->string()->null(),
            'type' => $this->string(10)->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post_files');
    }
}
