<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notifications`.
 */
class m190119_125206_create_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notifications', [
            'id' => $this->primaryKey(),
            'module' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'object_id' => $this->integer()->notNull(),
            'text' => $this->integer()->notNull(),
            'status' => $this->integer()->defaultValue(0),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notifications');
    }
}
