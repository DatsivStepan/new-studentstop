<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_role`.
 */
class m180525_171150_create_user_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_role', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'status' => $this->smallInteger(1)->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
                
        $this->batchInsert('{{%user_role}}', ['name', 'status', 'created_at', 'updated_at'], 
            [
                ['Admin', '0', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")],
                ['High-school students', '1', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")],
                ['Professor', '1', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")],
                ['College Freshmen', '1', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")],
                ['Seasoned college students/Graduate students', '1', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")],
                ['Alumni', '1', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")],
                ['Business entities', '1', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")],
                ['Raw Data', '1', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_role');
    }
}
