<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies_jobs_apply`.
 */
class m181201_111511_create_companies_jobs_apply_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies_jobs_apply', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'company_job_id' => $this->integer()->notNull(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('companies_jobs_apply');
    }
}
