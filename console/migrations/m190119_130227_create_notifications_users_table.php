<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notifications_users`.
 */
class m190119_130227_create_notifications_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notifications_users', [
            'id' => $this->primaryKey(),
            'notification_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'status' => $this->integer()->defaultValue(0),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notifications_users');
    }
}
