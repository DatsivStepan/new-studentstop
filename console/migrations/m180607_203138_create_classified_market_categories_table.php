<?php

use yii\db\Migration;

/**
 * Handles the creation of table `classified_market_categories`.
 */
class m180607_203138_create_classified_market_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('classified_market_categories', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'img_src' => $this->string()->null(),
            'parent_id' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('classified_market_categories');
    }
}
