<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m180609_080842_add_item_for_menu
 */
class m180711_190431_add_items_for_menu_rents extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = MenuItems::findOne(21);
        $model->url = 'services/rents/index';
        $model->save();

        $arrayItem = [
            'Dashboard' => [
                'url' => '/services/rents/index',
                'class_attr' => null,
            ],
            'My favorites' => [
                'url' => '/services/rents/favorites',
                'class_attr' => null,
            ],
            'My account' => [
                'url' => '/services/rents/my-account',
                'class_attr' => null,
            ],
            'Add booking' => [
                'url' => '/services/rents/booking',
                'class_attr' => null,
            ],
        ];

        foreach ($arrayItem as $name => $item) {
            $model = new MenuItems();
            $model->name = $name;
            $model->type = (string)MenuItems::TYPE_RENTS;
            $model->url = $item['url'];
            $model->class_attr = $item['class_attr'];
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180711_190431_add_items_for_menu_rents cannot be reverted.\n";

        return false;
    }
}
