<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rents_rating`.
 */
class m181124_135736_create_rents_rating_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rents_rating', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'rent_type' => $this->string()->notNull(),
            'person_type' => $this->integer()->notNull(),
            'rent_booking_id' => $this->integer()->notNull(),
            'rating' => $this->integer()->defaultValue(0),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rents_rating');
    }
}
