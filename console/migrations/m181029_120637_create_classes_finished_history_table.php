<?php

use yii\db\Migration;

/**
 * Handles the creation of table `classes_finished_history`.
 */
class m181029_120637_create_classes_finished_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('classes_finished_history', [
            'id' => $this->primaryKey(),
            'date_finished' => $this->date()->notNull(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('classes_finished_history');
    }
}
