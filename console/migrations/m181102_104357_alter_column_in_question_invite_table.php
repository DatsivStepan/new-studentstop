<?php

use yii\db\Migration,
    common\models\business\QuestionsInvite;

/**
 * Class m181102_104357_alter_column_in_question_invite_table
 */
class m181102_104357_alter_column_in_question_invite_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(QuestionsInvite::tableName(), 'status', $this->integer()->defaultValue(0)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181102_104357_alter_column_in_question_invite_table cannot be reverted.\n";

        return false;
    }

}
