<?php

use yii\db\Migration,
    common\models\pay\UserCredits;

/**
 * Class m190127_091221_add_column_to_UserCredits_table
 */
class m190127_091221_add_column_to_UserCredits_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(UserCredits::tableName(), 'amountM', $this->decimal(10, 2)->defaultValue('0.00')->notNull()->after('amount'));
        $this->renameColumn(UserCredits::tableName(), 'amount', 'amountC');
        $this->alterColumn(UserCredits::tableName(), 'amountC', $this->decimal(10, 2)->defaultValue('0.00')->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190127_091221_add_column_to_UserCredits_table cannot be reverted.\n";

        return false;
    }
}
