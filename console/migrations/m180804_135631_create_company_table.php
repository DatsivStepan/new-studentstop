<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m180804_135631_create_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'founded' => $this->date()->notNull(),

            'description' => $this->text()->null(),
            'img_src' => $this->string()->null(),

            'phone' => $this->string()->null(),
            'email' => $this->string()->null(),
            'fax' => $this->string()->null(),
            'website' => $this->string()->null(),

            'social_link_facebook' => $this->string()->null(),
            'social_link_twitter' => $this->string()->null(),
            'social_link_linkedin' => $this->string()->null(),

            'industry_id' => $this->integer()->null(),
            'type' => $this->integer()->null(),
            'status' => $this->integer()->null(),

            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('companies');
    }
}
