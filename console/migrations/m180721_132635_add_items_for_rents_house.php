<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m180721_132635_add_items_for_rents_house
 */
class m180721_132635_add_items_for_rents_house extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = MenuItems::find()->where(['like','name', 'Rent a house'])->one();
        $model->url = 'services/rents-house/search';
        $model->save();

        $arrayItem = [
            'Search' => [
                'url' => '/services/rents-house/search',
                'class_attr' => null,
            ],
            'My favorites' => [
                'url' => '/services/rents-house/favorites',
                'class_attr' => null,
            ],
            'My account' => [
                'url' => '/services/rents-house/my-account',
                'class_attr' => null,
            ],
            'My booking' => [
                'url' => '/services/rents-house/booking',
                'class_attr' => null,
            ],
        ];

        foreach ($arrayItem as $name => $item) {
            $model = new MenuItems();
            $model->name = $name;
            $model->type = (string)MenuItems::TYPE_RENT_A_HOUSE;
            $model->url = $item['url'];
            $model->class_attr = $item['class_attr'];
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180721_132635_add_items_for_rents_house cannot be reverted.\n";

        return false;
    }

}
