<?php

use yii\db\Migration;

/**
 * Handles the creation of table `majors`.
 */
class m180525_171503_create_majors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('majors', [
            'id' => $this->primaryKey(),
            'university_id' => $this->integer()->null(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->null(),
            'status' => $this->integer()->null(),
            'creator' => $this->integer()->null(),
            'url_name' => $this->string()->null(),
            'img_src' => $this->string()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);

        $this->batchInsert('{{%majors}}', ['id', 'name', 'creator', 'university_id', 'created_at'], 
            [
                [1, 'Architecture', 1, 1, date("Y-m-d H:i:s")],
                [2, 'Arts', 1, 1, date("Y-m-d H:i:s")],
                [3, 'Business', 1, 1, date("Y-m-d H:i:s")],
                [4, 'Design', 1, 1, date("Y-m-d H:i:s")],
                [5, 'Education', 1, 1, date("Y-m-d H:i:s")],
                [6, 'Engineering', 1, 1, date("Y-m-d H:i:s")],
                [7, 'Health Professions', 1, 1, date("Y-m-d H:i:s")],
                [8, 'Health, Sport, and Exercise Sciences', 1, 1, date("Y-m-d H:i:s")],
                [9, 'Humanities & International Studies', 1, 1, date("Y-m-d H:i:s")],
                [10, 'Journalism & Mass Communications', 1, 1, date("Y-m-d H:i:s")],
                [11, 'Law (Prelaw)', 1, 1, date("Y-m-d H:i:s")],
                [12, 'Medicine (Premed)', 1, 1, date("Y-m-d H:i:s")],
                [13, 'Music', 1, 1, date("Y-m-d H:i:s")],
                [14, 'Natural Sciences & Math', 1, 1, date("Y-m-d H:i:s")],
                [15, 'Nursing', 1, 1, date("Y-m-d H:i:s")],
                [16, 'Pharmacy', 1, 1, date("Y-m-d H:i:s")],
                [17, 'Public Affairs & Administration', 1, 1, date("Y-m-d H:i:s")],
                [18, 'Social & Behavioral Sciences', 1, 1, date("Y-m-d H:i:s")],
                [19, 'Social Welfar', 1, 1, date("Y-m-d H:i:s")],
                [20, 'Undecided?', 1, 1, date("Y-m-d H:i:s")],
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('majors');
    }
}
