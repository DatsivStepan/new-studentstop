<?php

use yii\db\Migration;

/**
 * Handles the creation of table `questions`.
 */
class m180722_083306_create_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('questions', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->null(),

            'user_id' => $this->integer()->notNull(),
            'university_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),

            'privacy_status' => $this->integer()->null(),

            'money' => $this->float()->null(),
            'credits' => $this->float()->null(),

            'status' => $this->integer()->null(),
            'class_id' => $this->integer()->null(),

            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('questions');
    }
}
