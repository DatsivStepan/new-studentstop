<?php

use yii\db\Migration;

/**
 * Handles the creation of table `classes_rating`.
 */
class m181029_161322_create_classes_rating_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('classes_rating', [
            'id' => $this->primaryKey(),
            'class_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'professor_id' => $this->integer()->notNull(),
            'rate_professor' => $this->integer()->null(),
            'level_of_difficulty' => $this->text()->null(),
            'prof_again' => $this->integer()->null(),
            'take_for_credit' => $this->integer()->null(),
            'textbook_use' => $this->integer()->null(),
            'attendance' => $this->integer()->null(),
            'hotness' => $this->integer()->null(),
            'more_specific' => $this->text()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('classes_rating');
    }
}
