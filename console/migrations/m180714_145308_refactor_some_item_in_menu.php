<?php

use yii\db\Migration,
    common\models\MenuItems;

/**
 * Class m180714_145308_refactor_some_item_in_menu
 */
class m180714_145308_refactor_some_item_in_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = MenuItems::find()->where(['like', 'url', 'services/rents/booking'])->one();
        if ($model) {
            $model->url = '/services/rents/my-booking';
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180714_145308_refactor_some_item_in_menu cannot be reverted.\n";

        return false;
    }

}
