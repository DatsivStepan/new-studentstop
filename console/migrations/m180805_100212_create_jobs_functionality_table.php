<?php

use yii\db\Migration;

/**
 * Handles the creation of table `jobs_functionality`.
 */
class m180805_100212_create_jobs_functionality_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jobs_functionality', [
            'id' => $this->primaryKey(),
            'name' => $this->integer()->notNull(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('jobs_functionality');
    }
}
