<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_social`.
 */
class m180908_191139_create_user_social_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_social', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'social_id' => $this->integer()->notNull(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_social');
    }
}
