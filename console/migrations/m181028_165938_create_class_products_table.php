<?php

use yii\db\Migration;

/**
 * Handles the creation of table `class_products`.
 */
class m181028_165938_create_class_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('class_products', [
            'id' => $this->primaryKey(),
            'file_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->null(),
            'price_m' => $this->decimal(10,2)->null(),
            'price_c' => $this->decimal(10,2)->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('class_products');
    }
}
