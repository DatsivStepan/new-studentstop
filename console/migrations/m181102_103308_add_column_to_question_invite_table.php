<?php

use yii\db\Migration,
    common\models\business\QuestionsInvite;

/**
 * Class m181102_103308_add_column_to_question_invite_table
 */
class m181102_103308_add_column_to_question_invite_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(QuestionsInvite::tableName(), 'answer_id', $this->integer()->null()->after('question_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181102_103308_add_column_to_question_invite_table cannot be reverted.\n";

        return false;
    }

}
