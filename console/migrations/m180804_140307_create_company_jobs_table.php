<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_jobs`.
 */
class m180804_140307_create_company_jobs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies_jobs', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),

            'gender' => $this->integer()->null(),
            'user_role_need' => $this->integer()->null(),
            'description' => $this->text()->null(),

            'show_in_university' => $this->integer()->null(),
            'major_id' => $this->integer()->null(),

            'jobs_function_id' => $this->integer()->null(),

            'salary' => $this->decimal(10,2)->null(),
            'salary_type' => $this->integer()->null(),
            
            'industry_level' => $this->integer()->null(),
            'skills' => $this->string()->null(),

            'img_src' => $this->string()->null(),

            'type' => $this->integer()->null(),
            'status' => $this->integer()->null(),

            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('companies_jobs');
    }
}
