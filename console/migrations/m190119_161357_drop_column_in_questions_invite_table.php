<?php

use yii\db\Migration,
    common\models\business\QuestionsInvite;

/**
 * Handles the dropping of table `column_in_questions_invite`.
 */
class m190119_161357_drop_column_in_questions_invite_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn(QuestionsInvite::tableName(), 'answer_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('column_in_questions_invite', [
            'id' => $this->primaryKey(),
        ]);
    }
}
