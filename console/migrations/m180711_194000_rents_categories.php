<?php

use yii\db\Migration;

/**
 * Class m180711_194000_rents_categories
 */
class m180711_194000_rents_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rents_categories', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'img_src' => $this->string()->null(),
            'parent_id' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180711_194000_rents_categories cannot be reverted.\n";

        return false;
    }

}
