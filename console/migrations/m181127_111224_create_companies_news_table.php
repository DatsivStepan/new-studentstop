<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies_news`.
 */
class m181127_111224_create_companies_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies_news', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->null(),
            'title' => $this->string()->null(),
            'content' => $this->text()->null(),
            'img_src' => $this->string()->null(),
            'type' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('companies_news');
    }
}
