<?php

use yii\db\Migration;

/**
 * Handles the creation of table `virtual_offices`.
 */
class m181121_124018_create_virtual_offices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('virtual_offices', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('virtual_offices');
    }
}
