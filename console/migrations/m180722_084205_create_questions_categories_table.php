<?php

use yii\db\Migration;

/**
 * Handles the creation of table `questions_categories`.
 */
class m180722_084205_create_questions_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('questions_categories', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'img_src' => $this->string()->null(),
            'parent_id' => $this->integer()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('questions_categories');
    }
}
