<?php

use yii\db\Migration;

/**
 * Handles the creation of table `questions_rating`.
 */
class m181104_103411_create_questions_rating_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('questions_rating', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'answer_id' => $this->integer()->notNull(),
            'rating' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('questions_rating');
    }
}
