<?php

use yii\db\Migration;

/**
 * Class m181104_093910_add_column_to_questions_answers_table
 */
class m181104_093910_add_column_to_questions_answers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\models\business\QuestionsAnswers::tableName(), 'status', $this->integer()->defaultValue(0)->after('credit'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181104_093910_add_column_to_questions_answers_table cannot be reverted.\n";

        return false;
    }
}
