<?php

namespace common\models;

use Yii,
    yii\helpers\Html,
    yii\helpers\Url;

/**
 * This is the model class for table "{{%notifications}}".
 *
 * @property integer $id
 * @property integer $module
 * @property integer $type
 * @property integer $object_id
 * @property string $text
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class Notifications extends \yii\db\ActiveRecord
{
    const MODULE_QUESTION_BANK = 1;
    const MODULE_RENT_BOOKING = 2;
    const MODULE_COURSE_CLASSES = 3;

    public static $moduleArrayTranscripton = [
        self::MODULE_QUESTION_BANK => 'Question bank',
        self::MODULE_RENT_BOOKING => 'Borrow',
        self::MODULE_COURSE_CLASSES => 'Courses & Classes'
    ];

    const TYPE_QUESTION_BANK_USER_INVITE = 1; //користувач отримав invite, від власника питання (після стоврення питання)
    const TYPE_QUESTION_BANK_OWNER_RECEIDES_ANDWER = 2; //власник питання отримав відповідь на нього

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notifications}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module', 'type', 'object_id', 'text'], 'required'],
            [['module', 'type', 'object_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'module' => Yii::t('app', 'Module'),
            'type' => Yii::t('app', 'Type'),
            'object_id' => Yii::t('app', 'Object'),
            'text' => Yii::t('app', 'Text'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param int $userId
     * @return bool
     */
    public static function getNotReadCount($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return NotificationsUsers::find()
            ->where([
                'status' => NotificationsUsers::STATUS_NOT_READ,
                'user_id' => $userId
            ])
            ->count();
    }

    /**
     * @param int $type
     * @param array $data
     * @return string
     */
    public static function generateTextForNotitficationType($type, $data = [])
    {
        $text = '';
        switch ($type) {
            case self::TYPE_QUESTION_BANK_USER_INVITE:
                $text = 'You got an invite on the system module "Question bank" ' . Html::a('Link', Url::to(['/business/questions/invite']), ['target' => '_blank', 'data-pjax' => 0]);
                break;
            case self::TYPE_QUESTION_BANK_OWNER_RECEIDES_ANDWER:
                $text = 'Нou have received an answer on your question - "' . $data['name'] . '" ' . Html::a('Link', Url::to(['/business/questions/my-questions']), ['target' => '_blank', 'data-pjax' => 0]);
                break;
        }
        return $text;
    }

    /**
     * @param int $module
     * @param int $type
     * @param int $object_id
     * @param string $text
     * @param array $usersIds
     */
    public static function addNew($module, $type, $object_id, $text, $usersIds)
    {
        $model = new self;
        $model->module = $module;
        $model->type = $type;
        $model->object_id = $object_id;
        $model->text = $text;
        if ($model->save()) {
            foreach ($usersIds as $userId) {
                $notUser = new NotificationsUsers;
                $notUser->notification_id = $model->id;
                $notUser->user_id = $userId;
                $notUser->status = NotificationsUsers::STATUS_NOT_READ;
                $notUser->save();
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationsUsersModel()
    {
        return $this->hasOne(NotificationsUsers::className(), ['notification_id' => 'id']);
    }
}
