<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%post_comment}}".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $content
 * @property integer $user_id
 * @property string $created_at
 * @property string $updated_at
 */
class PostComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'user_id'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_id' => Yii::t('app', 'Post ID'),
            'content' => Yii::t('app', 'Content'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
        
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getAuthorName()
    {
        return $this->user ? $this->user->getNameAndSurname() : '';
    }

    public function getAuthorLink()
    {
        return $this->user ? $this->user->getProfileLink() : '';
    }

    public function getAuthorAvatar()
    {
        return $this->user ? $this->user->getAvatar() : '';
    }
}
