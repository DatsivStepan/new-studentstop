<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%courses_tutors}}".
 *
 * @property integer $id
 * @property string $courses
 * @property integer $user_id
 * @property double $price
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class CoursesTutors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%courses_tutors}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'price'], 'required'],
            [['user_id'], 'integer'],
            [['price'], 'number'],
            [['courses', 'description'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $attribute = [
            'courses',
        ];
        if (in_array($name, $attribute)) {
            $this->setAttribute($name, serialize($value));
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        $attribute = [
            'courses',
        ];
        if (in_array($name, $attribute)) {
            return unserialize($this->getAttribute($name));
        }

        return parent::__get($name);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'courses' => Yii::t('app', 'Courses'),
            'user_id' => Yii::t('app', 'User ID'),
            'price' => Yii::t('app', 'Price'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
