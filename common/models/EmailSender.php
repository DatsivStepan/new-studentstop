<?php

namespace common\models;

use Yii;

/**
 * Class EmailSender
 * @package system\components
 */
class EmailSender
{

    /**
     * @param $address
     * @param $subject
     * @param $body
     */
    public static function sendEmail($addressTo, $subject, $html, $data = [])
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => $html],
                $data
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'Studentstop Support'])
            ->setTo($addressTo)
            ->setSubject($subject)
            ->send();
    }

}