<?php

namespace common\models;
use Yii,
    common\models\query\PostQuery,
    yii\behaviors\TimestampBehavior,
    yii\db\ActiveRecord,
    yii\helpers\Url,
    yii\web\Linkable;

/**
 * This is the model class for table "{{%post}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $content
 * @property integer $status
 * @property integer $inSlider
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class Post extends ActiveRecord implements Linkable
{
    const TYPE_USER_UNIVERSITY = 0;
    const TYPE_UNIVERSITY = 1;
    const TYPE_CLASS = 2;
    const TYPE_VIRTUAL_CLASS = 3;
    const TYPE_JOBS = 4;
    
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 1;
    
    public $images;

    public static function tableName()
    {
        return '{{%post}}';
    }

    public function rules()
    {
        return [
            [
                [
                    'user_id', 'university_id', 'type', 'status', 'inSlider'
                ],
                'integer'
            ],
            [['images'], 'safe'],
            [['content'], 'string'],
            [['title', 'img_src'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'title' => 'Title',
            'content' => 'Content',
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public function likePost()
    {
        $modelView = PostLikes::findOne(['post_id' => $this->id, 'user_id' => Yii::$app->user->id]);
        if (!$modelView) {
            $modelView = new PostLikes();
            $modelView->post_id = $this->id;
            $modelView->user_id = Yii::$app->user->id;
            $modelView->save();
        } else {
            $modelView->delete();
        }
        return $this->getLikesCount();
    }

    public function showPost()
    {
        $modelView = PostViews::findOne(['post_id' => $this->id, 'user_id' => Yii::$app->user->id]);
        if (!$modelView) {
            $modelView = new PostViews();
            $modelView->post_id = $this->id;
            $modelView->user_id = Yii::$app->user->id;
            $modelView->save();
        }
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }

    public function getIfUserLikes($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return PostLikes::find()->where(['post_id' => $this->id, 'user_id' => $userId])->exists();
    }

    public function getLikesCount()
    {
        return PostLikes::find()->where(['post_id' => $this->id])->count();
    }

    public function getViewsCount()
    {
        return PostViews::find()->where(['post_id' => $this->id])->count();
    }

    public function getCommentsCount()
    {
        return PostComment::find()->where(['post_id' => $this->id])->count();
    }

    public function getAuthorName()
    {
        return $this->user ? $this->user->getNameAndSurname() : '';
    }

    public function getAuthorLink()
    {
        return $this->user ? $this->user->getProfileLink() : '';
    }

    public function getAuthorAvatar()
    {
        return $this->user ? $this->user->getAvatar() : '';
    }

    public function getLastComment()
    {
        return PostComment::find()
                ->where(['post_id' => $this->id])
                ->orderBy(['created_at' => SORT_DESC])
                ->one();
    }

    public function getFilesWithLimit($limit){
        return PostFiles::find()
                ->where(['post_id' => $this->id])
                ->limit($limit)
                ->all();
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversityModel()
    {
        return $this->hasOne(University::className(), ['id' => 'university_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommentModel()
    {
        return $this->hasMany(PostComment::className(), ['post_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilesModel()
    {
        return $this->hasMany(PostFiles::className(), ['post_id' => 'id']);
    }

    /**
     * @return PostQuery
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }

    public function extraFields()
    {
        return [
            'author' => 'user',
        ];
    }

    public function getLinks()
    {
        return [
            'self' => Url::to(['post/view', 'id' => $this->id], true),
        ];
    }
}
