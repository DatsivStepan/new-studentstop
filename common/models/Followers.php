<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%followers}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $object_id
 * @property status $object_id
 * @property string $type
 * @property inteer $statusF
 * @property string $created_at
 * @property string $updated_at
 */
class Followers extends \yii\db\ActiveRecord
{
    const TYPE_CLASSES = 'classes';
    const TYPE_VIRTUAL_CLASSES = 'virtual-classes';
    
    const STATUS_ACTIVE = 1;
    const STATUS_ON_CONFIRM = 2;
    const STATUS_DEACTIVE = 3;
    const STATUS_OLD = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%followers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'object_id', 'statusF'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'type' => Yii::t('app', 'Type'),
            'statusF' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public static function createNew($type, $object_id, $user_id)
    {
        $res = false;
        $model = new self;
        $model->object_id = $object_id;
        $model->user_id = $user_id;
        $model->type = $type;
        $model->statusF = self::STATUS_ACTIVE;
        if ($model->save()) {
            $res = true;
            if (($model->type == self::TYPE_CLASSES) && ($class = university\Classes::findOne($model->object_id)) && ($class->getCountStudent() == 4)) {
                $class->status = university\Classes::STATUS_ACTIVE;
                $class->save();
            }
        }
        return $res;
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getClassesModel()
    {
        return $this->hasOne(university\Classes::className(), ['id' => 'object_id'])
            ->andOnCondition(['type' => self::TYPE_CLASSES]);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
