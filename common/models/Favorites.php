<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%favorites}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $object_id
 * @property string $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class Favorites extends \yii\db\ActiveRecord
{
    const TYPE_CLASSIFIELD_MARKET = 1;
    const TYPE_RENTS = 2;
    const TYPE_RENTS_HOUSE = 3;
    const TYPE_QUESTION = 4;
    const TYPE_JOBS_RESUME = 5;
    const TYPE_JOBS_JOB = 6;
    const TYPE_JOBS_COMPANY = 7;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%favorites}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'object_id'], 'required'],
            [['user_id', 'object_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public static function addNew($type, $objectId, $userId)
    {
        $model = new self;
        $model->object_id = $objectId;
        $model->user_id = $userId;
        $model->type = $type;

        return $model->save();
    }

    public static function getFavoriteStatus($type, $objectId, $userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return self::find()->where(['user_id' => $userId, 'type' => $type, 'object_id' => $objectId])->exists();
    }
}
