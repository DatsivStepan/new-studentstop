<?php

namespace common\models\pay;

use Yii;

/**
 * This is the model class for table "{{%user_credits}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property string $amountC
 * @property string $amountM
 * @property string $created_at
 * @property string $updated_at
 */
class UserCredits extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_credits}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['amountC', 'amountM'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'amountC' => Yii::t('app', 'Amount Credits'),
            'amountM' => Yii::t('app', 'Amount Money'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
}
