<?php

namespace common\models\pay;

use Yii,
    common\models\User,
    common\models\pay\UserCredits;

/**
 * This is the model class for table "{{%pay_requests}}".
 *
 * @property integer $id
 * @property integer $typePay
 * @property integer $module
 * @property string $amount
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property integer $objectId
 * @property string $operationType
 * @property string $note
 * @property string $paypal_id
 * @property string $paypal_hash
 * @property integer $pay_status
 * @property string $created_at
 * @property string $updated_at
 */
class PayRequests extends \yii\db\ActiveRecord
{
    const TYPE_PAY_PAYPAL = 1;
    const TYPE_PAY_CREDIT = 2;
    const TYPE_PAY_MONEY = 3;

    const PAY_STATUS_PENDING = 0;
    const PAY_STATUS_APPROVED = 1;
    const PAY_STATUS_NO_APPROVED = 2;
    const PAY_STATUS_COMFIRM = 3;
    const PAY_STATUS_REJECT = 4;

    const FOR_USER_ADMIN = 99999;

    const PAYPAL_STATUS_COMPLETE = 'PAYMENT.SALE.COMPLETED';

    const OPERATION_TYPE_QUESTION_ANSWER = 'pay_answer';
    const OPERATION_TYPE_RENT_BOOKING = 'pay_rent_booking';
    const OPERATION_TYPE_RENT_HOUSE_BOOKING = 'pay_rent_house_booking';
    const OPERATION_TYPE_ADMIN_SENT = 'admin_sent';
    const OPERATION_TYPE_PAY_PRODUCT = 'pay_product';

    const MODULE_QUESTION_BANK = 1;
    const MODULE_ADMIN_REQUEST = 2;
    const MODULE_RENT_BOOKING = 3;
    const MODULE_COURSE_CLASSES = 4;
    const MODULE_RENT_HOUSE_BOOKING = 5;

    public static $moduleArrayTranscripton = [
        self::MODULE_QUESTION_BANK => 'Question bank',
        self::MODULE_ADMIN_REQUEST => 'Admin request',
        self::MODULE_RENT_BOOKING => 'Borrow',
        self::MODULE_RENT_HOUSE_BOOKING => 'Borrow',
        self::MODULE_COURSE_CLASSES => 'Courses & Classes'
    ];

    public static $typesPay = [
        self::TYPE_PAY_PAYPAL,
        self::TYPE_PAY_CREDIT,
        self::TYPE_PAY_MONEY,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pay_requests}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['typePay', 'module'], 'required'],
            [
                [
                    'typePay', 'module', 'from_user_id', 'to_user_id', 'objectId',
                    'pay_status'
                ],
                'integer'
            ],
            [['amount'], 'number'],
            [
                [
                    'note', 'paypal_hash', 'paypal_id'
                ],
                'string'
            ],
            [['created_at', 'updated_at'], 'safe'],
            [['operationType'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'typePay' => Yii::t('app', 'Type Pay'),
            'module' => Yii::t('app', 'Module'),
            'amount' => Yii::t('app', 'Amount'),
            'from_user_id' => Yii::t('app', 'From User'),
            'to_user_id' => Yii::t('app', 'To User'),
            'objectId' => Yii::t('app', 'Object'),
            'operationType' => Yii::t('app', 'Operation Type'),
            'note' => Yii::t('app', 'Note'),
            'paypal_hash' => Yii::t('app', 'Paypal hash'),
            'paypal_id' => Yii::t('app', 'Paypal ID'),
            'pay_status' => Yii::t('app', 'Pay status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function getModule()
    {
        return $this->module && array_key_exists($this->module, self::$moduleArrayTranscripton) ? self::$moduleArrayTranscripton[$this->module] : '';
    }

    /**
     * @return bool
     */
    public function confirmPaypalPay()
    {
        $transaction = Yii::$app->db->beginTransaction();
        if (($this->pay_status == self::PAY_STATUS_APPROVED) && ($toUser = $this->toUserModel)) {
            $this->pay_status = self::PAY_STATUS_COMFIRM;
            if (!$this->save()) {
                $transaction->rollback();
                return false;
            }
        } else {
            $transaction->rollback();
            return false;
        }
        
        
        $transaction->rollback();
        return true;
    }

    /**
     * @param integer $objectId
     * @param integer $operation
     * @param integer $userId
     * @return bool
     */
    public static function checkPayeed($objectId, $operation, $module, $userId)
    {
        return self::find()
            ->where([
                'from_user_id' => $userId,
                'objectId' => $objectId,
                'operationType' => $operation,
                'module' => $module,
                'pay_status' => self::PAY_STATUS_COMFIRM
            ])
            ->exists();
    }

    /**
     * @param integer $fromUserId
     * @param integer $toUserId
     * @param string $amount
     * @param integer $objectId
     * @param integer $operation
     * @param integer $note
     * @return bool
     */
    public static function creditsPayRequest($fromUserId, $toUserId, $amount, $objectId, $operation, $module, $note = '')
    {
        $fromUser = User::findOne($fromUserId);
        $toUser = User::findOne($toUserId);
        if (($fromUserId != self::FOR_USER_ADMIN) && (!$toUser  || !$fromUser || ($fromUser && !$fromUser->userCreditsModel) || ($fromUser && $fromUser->userCreditsModel && ((float)$amount > (float)$fromUser->userCreditsModel->amountC)))) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        if ($fromUserId != self::FOR_USER_ADMIN) {
            $fromUser->userCreditsModel->amountC = (float)$fromUser->userCreditsModel->amountC - (float)$amount;
            if (!$fromUser->userCreditsModel->save()) {
                $transaction->rollback();
                return false;
            }
        }
        if ($toUser->userCreditsModel) {
            $toUser->userCreditsModel->amountC = (float)$toUser->userCreditsModel->amountC + (float)$amount;
            if (!$toUser->userCreditsModel->save()) {
                $transaction->rollback();
                return false;
            }
        } else {
            $modelC = new UserCredits;
            $modelC->user_id = $toUser->id;
            $modelC->amountC = (float)$amount;

            if (!$modelC->save()){
                $transaction->rollback();
                return false;
            }
        }

        $model = new self();
        $model->from_user_id = $fromUserId;
        $model->to_user_id = $toUserId;
        $model->typePay = self::TYPE_PAY_CREDIT;
        $model->module = $module;
        $model->operationType = $operation;
        $model->amount = (float)$amount;
        $model->objectId = $objectId;
        $model->note = $note;
        $model->pay_status = self::PAY_STATUS_COMFIRM;
        if (!$model->save()) {
            $transaction->rollback();
            return false;
        }
        
        $transaction->commit();
        return true;
    }
    

    /**
     * @param integer $fromUserId
     * @param integer $toUserId
     * @param string $amount
     * @param integer $objectId
     * @param integer $operation
     * @param integer $note
     * @return bool
     */
    public static function moneyPayRequest($fromUserId, $toUserId, $amount, $objectId, $operation, $module, $note = '')
    {
        $fromUser = User::findOne($fromUserId);
        $toUser = User::findOne($toUserId);
        if (($fromUserId != self::FOR_USER_ADMIN) && (!$toUser  || !$fromUser || ($fromUser && !$fromUser->userCreditsModel) || ($fromUser && $fromUser->userCreditsModel && ((float)$amount > (float)$fromUser->userCreditsModel->amountM)))) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        if ($fromUserId != self::FOR_USER_ADMIN) {
            $fromUser->userCreditsModel->amountM = (float)$fromUser->userCreditsModel->amountM - (float)$amount;
            if (!$fromUser->userCreditsModel->save()) {
                $transaction->rollback();
                return false;
            }
        }
        if ($toUser->userCreditsModel) {
            $toUser->userCreditsModel->amountM = (float)$toUser->userCreditsModel->amountM + (float)$amount;
            if (!$toUser->userCreditsModel->save()) {
                $transaction->rollback();
                return false;
            }
        } else {
            $modelC = new UserCredits;
            $modelC->user_id = $toUser->id;
            $modelC->amountM = (float)$amount;

            if (!$modelC->save()){
                $transaction->rollback();
                return false;
            }
        }

        $model = new self();
        $model->from_user_id = $fromUserId;
        $model->to_user_id = $toUserId;
        $model->typePay = self::TYPE_PAY_MONEY;
        $model->module = $module;
        $model->operationType = $operation;
        $model->amount = (float)$amount;
        $model->objectId = $objectId;
        $model->note = $note;
        $model->pay_status = self::PAY_STATUS_COMFIRM;
        if (!$model->save()) {
            $transaction->rollback();
            return false;
        }

        $transaction->commit();
        return true;
    }

    /**
     * @param integer $fromUserId
     * @param integer $toUserId
     * @param string $amount
     * @param integer $objectId
     * @param integer $operation
     * @param integer $note
     * @param integer $paypalId
     * @return bool
     */
    public static function preparePaypalPayRequest($fromUserId, $toUserId, $amount, $objectId, $operation, $module, $note = '', $paypalId)
    {
        $transaction = Yii::$app->db->beginTransaction();

        $model = new self();
        $model->from_user_id = $fromUserId;
        $model->to_user_id = $toUserId;
        $model->typePay = self::TYPE_PAY_PAYPAL;
        $model->module = $module;
        $model->operationType = $operation;
        $model->amount = (float)$amount;
        $model->objectId = $objectId;
        $model->note = $note;
        $model->paypal_id = $paypalId;
        $model->paypal_hash = md5($paypalId);
        $model->pay_status = self::PAY_STATUS_PENDING;

        if (!$model->save()) {
            $transaction->rollback();
            return false;
        }

        $transaction->commit();
        return true;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }
}
