<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper,
    common\models\User;

class UserStatic
{
    /**
     * @return array
     */
    public static function getAllUserInArrayMap($type = null)
    {
        $query = User::find()
                ->alias('user')
                ->select(['id' => 'user.id',  'clientName' => 'IF(userInfo.name IS NULL OR userInfo.name = "", user.email, CONCAT(userInfo.name, " ", userInfo.surname))'])
                ->joinWith(['userInfoModel userInfo']);
        if ($type) {
            $query->andWhere(['userInfo.user_role_id' => $type]);
        }
        return ArrayHelper::map($query->all(), 'id', 'clientName');
    }
    
    public static function checkEmailUnique($email)
    {
        if ($email) {
            return User::findOne(['email' => $email]) ? true : false;
        }
        return false;
    }
    
}