<?php

namespace common\models\services;

use Yii,
    frontend\components\helpers\FunctionHelper,
    common\models\pay\PayRequests,
    common\models\User;

/**
 * This is the model class for table "{{%rents_booking}}".
 *
 * @property integer $id
 * @property integer $rent_id
 * @property integer $user_id
 * @property string $rent_type
 * @property string $date_reserve
 * @property string $reserve_from
 * @property string $reserve_to
 * @property string $rating_count
 * @property string $rating_comment
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class RentsBooking extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_ON_CONFIRM = 2;
    const STATUS_CONFIRM = 3;
    const STATUS_REGECT = 4;
    const STATUS_PAYED = 5;

    const SHOW_TYPE_RECENT = 1;
    const SHOW_TYPE_ARCHIVE = 2;
    const SHOW_TYPE_ALL = 3;

    const TYPE_RENT = 'rent';
    const TYPE_HOUSE = 'rent_house';

    public static $translateStatus = [
        self::STATUS_NEW => 'New',
        self::STATUS_ON_CONFIRM => 'On Confirm',
        self::STATUS_CONFIRM => 'Confirm',
        self::STATUS_REGECT => 'Refused',
        self::STATUS_PAYED => 'Payed',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rents_booking}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rent_id', 'user_id', 'date_reserve', 'reserve_from', 'reserve_to'], 'required'],
            [['rent_id', 'user_id', 'status'], 'integer'],
            [['date_reserve', 'reserve_from', 'reserve_to', 'rating_count', 'rating_comment', 'created_at', 'updated_at'], 'safe'],
            [['rent_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rent_id' => Yii::t('app', 'Rent ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'rent_type' => Yii::t('app', 'Rent Type'),
            'date_reserve' => Yii::t('app', 'Date Reserve'),
            'reserve_from' => Yii::t('app', 'Reserve From'),
            'reserve_to' => Yii::t('app', 'Reserve To'),
            'rating_count' => Yii::t('app', 'Rating Count'),
            'rating_comment' => Yii::t('app', 'Rating Comment'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord){
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        
        return parent::beforeSave($insert);
    }
    
    /**
     * @return integer
     */
    public function getRentHoursCount()
    {
        $interval = date_diff(date_create($this->reserve_from), date_create($this->reserve_to));
        return ($interval->days * 24) + $interval->h;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return FunctionHelper::numberFormat($this->getNeedMoneyPayCount()) . '($)' . ' / ' . FunctionHelper::numberFormat($this->getNeedCreditsPayCount()) . ' (C)';
    }

    /**
     * @return string
     */
    public function getNeedCreditsPayCount()
    {
        if ($rent = $this->rentModel) {
            return $rent->credits * $this->getRentHoursCount();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getNeedMoneyPayCount()
    {
        if ($rent = $this->rentModel) {
            return $rent->money * $this->getRentHoursCount();
        } else {
            return null;
        }
    }
    
    /**
     * @return integer
     */
    public function isPayed()
    {
        return PayRequests::find()
                ->where(['typePay' => PayRequests::$typesPay])
                ->andWhere([
                    'objectId' => $this->id,
                    'module' => PayRequests::MODULE_RENT_BOOKING,
                    'operationType' => PayRequests::OPERATION_TYPE_RENT_BOOKING
                ])->exists();
    }

    /**
     * @return bool
     */
    public function getRatingModel($typePerson = null, $type = null)
    {
        $typePerson = $typePerson ? $typePerson : RentsRating::PERSON_TYPE_OWNER;
        $type = $type ? $type : self::TYPE_RENT;
        return RentsRating::find()->where(['rent_booking_id' => $this->id, 'person_type' => $typePerson, 'rent_type' => $type])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentsRatingModel()
    {
        return $this->hasMany(RentsRating::className(), ['id' => 'rent_booking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentModel()
    {
        return $this->hasOne(Rents::className(), ['id' => 'rent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentHouseModel()
    {
        return $this->hasOne(RentsHouse::className(), ['id' => 'rent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
