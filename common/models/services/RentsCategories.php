<?php

namespace common\models\services;

use Yii,
    yii\helpers\ArrayHelper,
    yii\helpers\Url;

/**
 * This is the model class for table "{{%rents_categories}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $img_src
 * @property integer $parent_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class RentsCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rents_categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'img_src' => Yii::t('app', 'Img Src'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    
    public function getChildCategoryCount()
    {
        return self::find()->where(['parent_id' => $this->id])->count();
    }
    
    public function getChildCategory($limit = 3)
    {
        return self::find()->where(['parent_id' => $this->id])
                ->limit($limit)
                ->all();
    }

    public static function getAllByGroupCategories()
    {
        $modelsC = self::getAllParentCategories();
        $resArray = [];
        foreach ($modelsC as $modelC) {
            $resArray[$modelC->name] = ArrayHelper::map($modelC->childModel, 'id', 'name');
        }

        return $resArray;
    }
    
    public static function getAllParentCategories()
    {
        $query = self::find()
                ->where([
                    'OR',
                    ['parent_id' => null],
                    ['parent_id' => 0],
                ]);

        return $query->all();
    }
    
    public static function getAllParentCategoris()
    {
        $query = self::find()
                ->where([
                    'OR',
                    ['parent_id' => null],
                    ['parent_id' => 0],
                ]);

        return ArrayHelper::map($query->all(), 'id', 'name');
    }

    /**
     * @string
     */
    public function getProductCount()
    {
        return Rents::find()->where(['category_id' => $this->id])->count();
    }

    /**
     * @string
     */
    public function getLink()
    {
        return Url::to(['/services/rents/search/' . $this->id]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildModel()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }
}
