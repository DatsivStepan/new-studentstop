<?php

namespace common\models\services;

use Yii,
    common\models\University,
    common\models\User,
    common\models\Favorites;

/**
 * This is the model class for table "{{%rents_house}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $university_id
 * @property integer $type
 * @property string $title
 * @property string $content
 * @property string $img_src
 * @property integer $house_type
 * @property integer $room_count
 * @property integer $min_rent
 * @property integer $adults
 * @property integer $type_pay
 * @property double $credits
 * @property double $money
 * @property integer $publish
 * @property integer $status
 * @property string $address
 * @property string $lat
 * @property string $lng
 * @property integer $attr_wifi
 * @property integer $attr_parking
 * @property integer $attr_smoking
 * @property integer $attr_tv
 * @property integer $attr_animal
 * @property integer $attr_conditioner
 * @property string $created_at
 * @property string $updated_at
 */
class RentsHouse extends \yii\db\ActiveRecord
{
    const HOUSE_TYPE_PRIVATE = 1;
    const HOUSE_TYPE_SHARED = 2;

    const RENT_TYPE_DAY = 1;
    const RENT_TYPE_WEEK = 2;
    const RENT_TYPE_MONTH = 3;
    
    const PAY_TYPE_1 = 1;
    const PAY_TYPE_2 = 2;

    const PAY_TYPE_INSTANT = 1;
    const PAY_TYPE_REQUEST = 2;
    
    public static $houseTypes = [
        self::HOUSE_TYPE_PRIVATE => 'Private',
        self::HOUSE_TYPE_SHARED => 'Shared'
    ];

    public static $rentTypes = [
        self::RENT_TYPE_DAY => 'Day',
        self::RENT_TYPE_WEEK => 'Week',
        self::RENT_TYPE_MONTH => 'Month'
    ];

    public static $payTypesArray = [
        self::PAY_TYPE_INSTANT => 'Instant Pay',
        self::PAY_TYPE_REQUEST => 'Request'
    ];

    public static $adults = [
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => '5+'
    ];

    public static $payTypes = [
        self::PAY_TYPE_1 => 'Instant Pay',
        self::PAY_TYPE_2 => 'Request'
    ];
    public $images;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rents_house}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'university_id'], 'required'],
            [['user_id', 'university_id', 'type', 'house_type', 'room_count', 'min_rent', 'adults', 'type_pay', 'publish', 'status', 'attr_wifi', 'attr_parking', 'attr_smoking', 'attr_tv', 'attr_animal', 'attr_conditioner'], 'integer'],
            [['content'], 'string'],
            [['credits', 'money'], 'number'],
            [['created_at', 'updated_at', 'images'], 'safe'],
            [['title', 'img_src', 'address', 'lat', 'lng'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'university_id' => Yii::t('app', 'University ID'),
            'type' => Yii::t('app', 'Type'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'img_src' => Yii::t('app', 'Img Src'),
            'house_type' => Yii::t('app', 'House Type'),
            'room_count' => Yii::t('app', 'Room Count'),
            'min_rent' => Yii::t('app', 'Min Rent'),
            'adults' => Yii::t('app', 'Adults'),
            'type_pay' => Yii::t('app', 'Type Pay'),
            'credits' => Yii::t('app', 'Credits'),
            'money' => Yii::t('app', 'Money'),
            'publish' => Yii::t('app', 'Publish'),
            'status' => Yii::t('app', 'Status'),
            'address' => Yii::t('app', 'Address'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'attr_wifi' => Yii::t('app', 'Wifi'),
            'attr_parking' => Yii::t('app', 'Parking'),
            'attr_smoking' => Yii::t('app', 'Smoking'),
            'attr_tv' => Yii::t('app', 'Tv'),
            'attr_animal' => Yii::t('app', 'Animal'),
            'attr_conditioner' => Yii::t('app', 'Conditioner'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }

    public function getRating()
    {
        return 3.5;
    }

    public function getPrice()
    {
        return ($this->money ? $this->money . '$' : '') . ($this->money && $this->credits ? '/' : '') . ($this->credits ? $this->credits . '(credits)' : '');
    }
    
    
    /**
     * @param $personType
     * @return \yii\db\ActiveQuery
     */
    public function getRatingRequest($personType)
    {
        $query = RentsRating::find()
                ->alias('rr')
                ->joinWith(['rentsBookingModel rb'])
                ->where([
                    'rr.user_id' => $this->user_id,
                    'rr.person_type' => $personType,
                    'rr.rent_type' => RentsBooking::TYPE_HOUSE
                ]);

        return $query->all();
    }

    /**
     * @param int|bool $userId
     * @return bool
     */
    public function isOwner($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return $userId == $this->user_id ? true : false;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->user ? $this->user->getNameAndSurname() : '';
    }

    public function getAuthorAvatar()
    {
        return $this->user ? $this->user->getAvatar() : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversityModel()
    {
        return $this->hasOne(University::className(), ['id' => 'university_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoritesModel()
    {
        return $this->hasOne(Favorites::className(), ['object_id' => 'id'])
                ->andOnCondition(['favorites.type' => Favorites::TYPE_RENTS_HOUSE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentsBookingModel()
    {
        return $this->hasMany(RentsBooking::className(), ['rent_id' => 'id'])->andOnCondition(['rent_type' => RentsBooking::TYPE_HOUSE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImagesModel()
    {
        return $this->hasMany(RentsHouseImages::className(), ['rent_house_id' => 'id']);
    }
}
