<?php

namespace common\models\services;

use Yii,
    common\models\University,
    common\models\User,
    common\models\Favorites;

/**
 * This is the model class for table "{{%classified_market}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $university_id
 * @property integer $category_id
 * @property string $title
 * @property string $image_src
 * @property string $content
 * @property string $address
 * @property string $created_at
 * @property string $updated_at
 */
class ClassifiedMarket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%classified_market}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'university_id', 'category_id', 'title', 'address'], 'required'],
            [['user_id', 'university_id', 'category_id'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'image_src', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'university_id' => Yii::t('app', 'University ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'title' => Yii::t('app', 'Title'),
            'image_src' => Yii::t('app', 'Image Src'),
            'content' => Yii::t('app', 'Content'),
            'address' => Yii::t('app', 'Address'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param int|bool $userId
     * @return bool
     */
    public function isOwner($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return $userId == $this->user_id ? true : false;
    }
    
    public function getImages()
    {
        return $this->image_src ? '/' . $this->image_src : '/images/default_avatar.jpg';
    }
    
    public function getRating()
    {
        return $this->user ? $this->user->getRating() : '';
    }
    
    public function getContent()
    {
        return $this->content;
    }

    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }

    public function getAuthorName()
    {
        return $this->user ? $this->user->getNameAndSurname() : '';
    }

    public function getAuthorAvatar()
    {
        return $this->user ? $this->user->getAvatar() : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversityModel()
    {
        return $this->hasOne(University::className(), ['id' => 'university_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoritesModel()
    {
        return $this->hasOne(Favorites::className(), ['object_id' => 'id'])
                ->andOnCondition(['favorites.type' => Favorites::TYPE_CLASSIFIELD_MARKET]);
    }
}
