<?php

namespace common\models\services;

use Yii;

/**
 * This is the model class for table "{{%rents_house_images}}".
 *
 * @property integer $id
 * @property integer $rent_house_id
 * @property string $name
 * @property string $img_src
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class RentsHouseImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rents_house_images}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rent_house_id', 'img_src'], 'required'],
            [['rent_house_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rent_house_id' => Yii::t('app', 'Rent House ID'),
            'name' => Yii::t('app', 'Name'),
            'img_src' => Yii::t('app', 'Img Src'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
