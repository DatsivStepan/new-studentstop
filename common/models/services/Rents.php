<?php

namespace common\models\services;

use Yii,
    common\models\University,
    common\models\User,
    common\models\Favorites;

/**
 * This is the model class for table "{{%rents}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $university_id
 * @property integer $type
 * @property string $title
 * @property string $content
 * @property string $img_src
 * @property integer $type_pay
 * @property double $credits
 * @property double $money
 * @property integer $publish
 * @property integer $status
 * @property integer $category_id
 * @property string $address
 * @property string $lat
 * @property string $lng
 * @property string $created_at
 * @property string $updated_at
 */
class Rents extends \yii\db\ActiveRecord
{
    const PAY_TYPE_INSTANT = 1;
    const PAY_TYPE_REQUEST = 2;

    public static $payTypesArray = [
        self::PAY_TYPE_INSTANT => 'Instant Pay',
        self::PAY_TYPE_REQUEST => 'Request'
    ];
    
    public $images;
    public $bookC;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rents}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'university_id', 'category_id'], 'required'],
            [['user_id', 'university_id', 'category_id', 'type', 'type_pay', 'publish', 'status'], 'integer'],
            [['content'], 'string'],
            [['credits', 'money'], 'number'],
            [['created_at', 'updated_at', 'images'], 'safe'],
            [['title', 'img_src', 'address', 'lat', 'lng'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'university_id' => Yii::t('app', 'University'),
            'university_id' => Yii::t('app', 'Category'),
            'type' => Yii::t('app', 'Type'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'img_src' => Yii::t('app', 'Img Src'),
            'type_pay' => Yii::t('app', 'Type Pay'),
            'credits' => Yii::t('app', 'Credits'),
            'money' => Yii::t('app', 'Money'),
            'publish' => Yii::t('app', 'Publish'),
            'status' => Yii::t('app', 'Status'),
            'address' => Yii::t('app', 'Address'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }

    /**
     * @param int|bool $userId
     * @return bool
     */
    public function isOwner($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return $userId == $this->user_id ? true : false;
    }

    /**
     * @return integer
     */
    public function getRating()
    {
        $query = RentsRating::find()
                ->alias('rr')
                ->joinWith(['rentsBookingModel rb'])
                ->where([
                    'rr.user_id' => $this->user_id,
//                    'rr.person_type' => $personType,
                    'rr.rent_type' => RentsBooking::TYPE_RENT
                ]);
        $allCount = count($query->all());
        $sumCount = $query->sum('rating');

        return  $sumCount ? ($sumCount / $allCount) : 0;
    }

    /**
     * @return integer
     */
    public function getRatingByType()
    {
        return $this->user ? $this->user->id : '';
    }

    /**
     * @param $personType
     * @return \yii\db\ActiveQuery
     */
    public function getRatingRequest($personType)
    {
        $query = RentsRating::find()
                ->alias('rr')
                ->joinWith(['rentsBookingModel rb'])
                ->where([
                    'rr.user_id' => $this->user_id,
                    'rr.person_type' => $personType,
                    'rr.rent_type' => RentsBooking::TYPE_RENT
                ]);

        return $query->all();
    }

    public function getPrice()
    {
        return ($this->money ? $this->money . '$' : '') . ($this->money && $this->credits ? '/' : '') . ($this->credits ? $this->credits . '(credits)' : '');
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }

    public function getAuthorName()
    {
        return $this->user ? $this->user->getNameAndSurname() : '';
    }

    public function getAuthorAvatar()
    {
        return $this->user ? $this->user->getAvatar() : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversityModel()
    {
        return $this->hasOne(University::className(), ['id' => 'university_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImagesModel()
    {
        return $this->hasMany(RentsImages::className(), ['rent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentsBookingModel()
    {
        return $this->hasMany(RentsBooking::className(), ['rent_id' => 'id'])->andOnCondition(['rent_type' => RentsBooking::TYPE_RENT]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoritesModel()
    {
        return $this->hasOne(Favorites::className(), ['object_id' => 'id'])
                ->andOnCondition(['favorites.type' => Favorites::TYPE_RENTS]);
    }
}
