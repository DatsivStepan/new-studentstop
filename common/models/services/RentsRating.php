<?php

namespace common\models\services;

use Yii,
    common\models\User,
    common\models\services\RentsBooking;

/**
 * This is the model class for table "{{%rents_rating}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $rent_type
 * @property integer $person_type
 * @property integer $rent_booking_id
 * @property integer $rating
 * @property string $created_at
 * @property string $updated_at
 */
class RentsRating extends \yii\db\ActiveRecord
{
    const PERSON_TYPE_OWNER = 1;
    const PERSON_TYPE_OWNER_RENT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rents_rating}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'rent_type', 'person_type', 'rent_booking_id'], 'required'],
            [['user_id', 'person_type', 'rent_booking_id', 'rating'], 'integer'],
            [['rent_type'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'rent_type' => Yii::t('app', 'Rent Type'),
            'person_type' => Yii::t('app', 'Person Type'),
            'rent_booking_id' => Yii::t('app', 'Rent Booking ID'),
            'rating' => Yii::t('app', 'Rating'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentsBookingModel()
    {
        return $this->hasOne(RentsBooking::className(), ['id' => 'rent_booking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
