<?php

namespace common\models\university;

use Yii,
    yii\behaviors\TimestampBehavior,
    yii\db\Expression,
    common\models\User,
    common\models\Followers,
    common\models\UserRole;

/**
 * This is the model class for table "{{%classes}}".
 *
 * @property int $id
 * @property int $course_id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property int $status
 * @property int $creator
 * @property int $professor
 * @property int $university_id
 * @property string $url_name
 * @property string $img_src
 * @property string $date_active
 *
 * @property Courses $course
 */
class Classes extends \yii\db\ActiveRecord
{
    const BUTTON_JOIN_TEXT = 'Enroll';
    const BUTTON_VIEW_TEXT = 'View';
    const BUTTON_UNJOIN_TEXT = 'Leave';
    
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const ACTIVE_STUDENT_COUNT = 4;

    public static $filterTop = [
        '1' => 'Students',
        '2' => 'Professor',
        '3' => 'Class'
    ];
    
    public $major_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%classes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'course_id', 'professor'], 'required'],
            [
                [
                    'course_id', 'status', 'creator', 'professor', 'university_id', 'major_id'
                ],
                'integer'
            ],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'date_active'], 'safe'],
            [['name', 'url_name', 'img_src'], 'string', 'max' => 255],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class'         => 'common\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'url_name',
                'translit'      => true
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'course_id' => Yii::t('app', 'Course'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Date Create'),
            'updated_at' => Yii::t('app', 'Date Update'),
            'status' => Yii::t('app', 'Status'),
            'creator' => Yii::t('app', 'Creator'),
            'professor' => Yii::t('app', 'Professor'),
            'university_id' => Yii::t('app', 'University'),
            'url_name' => Yii::t('app', 'Url Name'),
            'img_src' => Yii::t('app', 'Img Src'),
            'date_active' => Yii::t('app', 'Date Active'),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status == self::STATUS_ACTIVE ? 'Active' : 'Pending';
    }

    /**
     * @return integer
     */
    public function getCountStudent()
    {
        return Followers::find()
            ->where([
                'object_id' => $this->id,
                'type' => Followers::TYPE_CLASSES
            ])->count();
    }

    /**
     * @return integer
     */
    public function getRating()
    {
        $query = ClassesRating::find()
            ->where(['class_id' => $this->id]);
        
        return $query->exists() ? $query->sum('rate_professor') / $query->count() : 0;
    }

    /**
     * @return string
     */
    public function getRank()
    {
        $count = Courses::getCountClassesInCourse($this->course_id);
        return '1/' . $count;
    }

    /**
     * @param $classId
     * @return string
     */
    public function checkStudentCreatedStatus()
    {
        return (($follower = $this->isFollower()) && ($follower != Followers::STATUS_OLD)) || $this->isOwner() ? true : false;
    }

    public function getLastFinishedDate()
    {
        $model = ClassesFinishedHistory::find()
                ->where(['status' => ClassesFinishedHistory::STATUS_SUCCESS])
                ->orderBy(['date_finished' => SORT_DESC])
                ->one();

        return $model ? $model->date_finished : null; 
    }
    
    /**
     * @param string $tab
     * @return string
     */
    public function getLink($tab = 'view')
    {
        return '/university/classes/' . $this->url_name . '/' . $tab;
    }

    /**
     * @param $classId
     * @return string
     */
    public static function getTopList($classId)
    {
        if ($class = self::findOne($classId)) {
            $filterTop = self::$filterTop;
            $resultArray = [];
            foreach ($filterTop as $filterId => $filterName) {
                switch ($filterId) {
                    case 1:
                        $resultArray[$filterId] = User::getAll(null, UserRole::ROLE_STUDENT, 8, $classId);
                        break;
                    case 2:
                        $resultArray[$filterId] = User::getAllProfessorByRating(null, 8, $classId);
                        break;
                    case 3:
                        $resultArray[$filterId] = self::getAll(null, $class->course_id, 8);
                        break;
                }
            }
            return $resultArray;
        }

        return null;
    }

    public static function getAll($universityId = null, $courseId = null, $limit = null)
    {
        $query = self::find()
                ->alias('c');

        if ($universityId) {
            $query->andWhere(['c.university_id' => $universityId]);
        }

        if ($courseId) {
            $query->andWhere(['c.course_id' => $courseId]);
        }
        if ($limit) {
            $query->limit($limit);
        }
        return $query->all();
    }

    public static function getStaticLink($classId)
    {
        $model = self::findOne($classId);
        return  $model ? $model->getLink() : null;
    }
    
    public function getStudentsCount()
    {
        return Followers::find()->where([
            'object_id' => $this->id, 'type' => Followers::TYPE_CLASSES
        ])
        ->count();
    }

    /**
     * @param $userId
     * @return bool
     */
    public function isFollower($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        $query = Followers::find()
            ->where([
                'user_id' => $userId,
                'object_id' => $this->id,
                'type' => Followers::TYPE_CLASSES
            ]);

        return $query->exists() ? $query->one()->statusF : null;
    }

    /**
     * @param $userId
     * @return bool
     */
    public function isOwner($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return $this->professor == $userId ? true : false;
    }

    /**
     * @param $userId
     * @param $view
     * @return array
     */
    public function getJoinClassText($userId = null, $view = true)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        $result = ['text' => self::BUTTON_JOIN_TEXT, 'link' => null, 'class' => ' js-join-classes '];
        if ($this->isOwner($userId) || $this->isFollower($userId)) {
            if ($view) {
                $result = ['text' => 'View', 'link' => $this->getLink(), 'class' => ''];
            } else {
                $result = ['text' => 'Leave', 'link' => null, 'class' => ' js-join-classes '];
            }
        }

        return $result;
    }
    
    /**
     * @return string
     */
    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassesFollowModel()
    {
        return $this->hasMany(Followers::className(), ['object_id' => 'id'])
                ->andOnCondition(['type' => Followers::TYPE_CLASSES]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseModel()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessorModel()
    {
        return $this->hasOne(User::className(), ['id' => 'professor']);
    }
}
