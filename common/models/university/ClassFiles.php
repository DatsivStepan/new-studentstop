<?php

namespace common\models\university;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%class_files}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $class_id
 * @property string $file_src
 * @property string $name
 * @property integer $typeClass
 * @property string $mimeType
 * @property integer $status
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class ClassFiles extends \yii\db\ActiveRecord
{
    const TYPE_CLASS = 1;
    const TYPE_VIRTUAL_CLASS = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%class_files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'class_id', 'file_src'], 'required'],
            [['user_id', 'class_id', 'typeClass', 'status'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_src', 'name', 'mimeType'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'class_id' => Yii::t('app', 'Class ID'),
            'file_src' => Yii::t('app', 'File Src'),
            'name' => Yii::t('app', 'Name'),
            'typeClass' => Yii::t('app', 'Type Class'),
            'mimeType' => Yii::t('app', 'Mime Type'),
            'status' => Yii::t('app', 'Status'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public static function getImagesForSider($classId, $limit = null, $dateShowFrom = null)
    {
        $query = self::find()
            ->where(['class_id' => $classId])
            ->andWhere(['like', 'mimeType', 'image']);

        if ($dateShowFrom) {
            $query->andFilterWhere([
                '>', 'created_at', $dateShowFrom
            ]);
        }

        if ($limit) {
            $query->limit($limit);
        }

        return $query->all();
    }

    public static function getMyFileInArrayMap($classId = null, $userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        $query = self::find()
            ->where([
                'user_id' => $userId
            ]);
        if ($classId) {
            $query->andWhere(['class_id' => $classId]);
        }

        return ArrayHelper::map($query->all(), 'id', 'name');
    }
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassProductModel()
    {
        return $this->hasMany(ClassProducts::className(), ['file_id' => 'id']);
    }
}
