<?php

namespace common\models\university;

use Yii;

/**
 * This is the model class for table "{{%classes_finished_history}}".
 *
 * @property integer $id
 * @property string $date_finished
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class ClassesFinishedHistory extends \yii\db\ActiveRecord
{
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%classes_finished_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_finished'], 'required'],
            [['status'], 'integer'],
            [['created_at', 'updated_at', 'date_finished'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date_finished' => Yii::t('app', 'Date Finished'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param $date
     * @return bool
     */
    public static function addFinishedDate($date)
    {
        $model = new self;
        $model->date_finished = date('Y-m-d', strtotime($date));
        $model->status = self::STATUS_SUCCESS;
        return $model->save();
    }
}
