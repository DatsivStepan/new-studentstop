<?php

namespace common\models\university;

use Yii,
    yii\helpers\Html,
    common\models\university\Classes,
    common\models\pay\PayRequests,
    frontend\components\helpers\FunctionHelper;

/**
 * This is the model class for table "{{%class_products}}".
 *
 * @property integer $id
 * @property integer $class_id
 * @property integer $file_id
 * @property string $title
 * @property string $description
 * @property string $price_m
 * @property string $price_c
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class ClassProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%class_products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['file_id', 'status', 'class_id'], 'integer'],
            [['description'], 'string'],
            [['price_m', 'price_c'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'file_id' => Yii::t('app', 'File'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'price_m' => Yii::t('app', 'Price Money'),
            'price_c' => Yii::t('app', 'Price Credits'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
/**
     * @param int|bool $userId
     * @return bool
     */
    public function isOwner($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return $this->classFilesModel && ($this->classFilesModel->user_id == $userId)  ? true : false;
    }

    /**
     * @return bool
     */
    public function getMoney()
    {
        return FunctionHelper::numberFormat($this->price_m) . Html::tag('b', '(P)');
    }

    /**
     * @return bool
     */
    public function getCredits()
    {
        return FunctionHelper::numberFormat($this->price_c) . Html::tag('b', '(C)');
    }
    
    /**
     * @return bool
     */
    public function getPrice()
    {
        return  $this->getMoney() . ' / ' . $this->getCredits();
    }

    /**
     * @param $userId
     * @return bool
     */
    public function isPayed($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;

        return PayRequests::checkPayeed($this->id, PayRequests::OPERATION_TYPE_PAY_PRODUCT, PayRequests::MODULE_COURSE_CLASSES, $userId);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassesModel()
    {
        return $this->hasOne(Classes::className(), ['id' => 'class_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassFilesModel()
    {
        return $this->hasOne(ClassFiles::className(), ['id' => 'file_id']);
    }
}
