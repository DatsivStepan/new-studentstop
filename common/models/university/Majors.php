<?php

namespace common\models\university;

use Yii,
    yii\behaviors\TimestampBehavior,
    yii\db\Expression,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%majors}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property int $status
 * @property int $creator
 * @property string $url_name
 * @property int $university_id
 * @property string $img_src
 *
 * @property Courses[] $courses
 */
class Majors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%majors}}';
    }

    public $rating;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['university_id'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['status', 'creator', 'university_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['url_name'], 'string', 'max' => 50],
            [['img_src'], 'string', 'max' => 200],
            [['name'], 'unique'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class'         => 'common\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'url_name',
                'translit'      => true
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Date Create'),
            'updated_at' => Yii::t('app', 'Date Update'),
            'status' => Yii::t('app', 'Status'),
            'creator' => Yii::t('app', 'Creator'),
            'url_name' => Yii::t('app', 'Url Name'),
            'university_id' => Yii::t('app', 'University'),
            'img_src' => Yii::t('app', 'Image'),
        ];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLink()
    {
        return \yii\helpers\Url::to(['/university/courses/classes-search?majorId=' . $this->id]);
    }

    
    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }

    /**
     * @param $rateNumber
     * @return string
     */
    public function getRank($rateNumber = null)
    {
        $countM = self::getCountMajor($this->university_id);
        return ($rateNumber ? 1 : '') . '/' . $countM;
    }

    /**
     * @return integer
     */
    public function getRating()
    {
        $query = ClassesRating::find()
            ->alias('cr')
            ->joinWith(['classesModel.courseModel cor'])
            ->where(['cor.major_id' => $this->id]);

        return $query->exists() ? $query->sum('rate_professor') / $query->count() : 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Courses::className(), ['major_id' => 'id']);
    }

    public static function getCountMajor($universityId = null)
    {
        $query = self::find();
        if ($universityId) {
            $query->where(['university_id' => $universityId]);
        }
        return $query->count();
    }

    public static function getAllMajorsByRate($universityId = null, $limit = null)
    {
        $query = self::find()
                ->alias('m')
                ->select([
                    'm.*',
                    'rating' => '('. ClassesRating::find()
                        ->alias('cr')
                        ->select('SUM(cr.rate_professor) / COUNT(cr.id)')
                        ->joinWith(['classesModel.courseModel cor'])
                        ->where('cor.major_id = m.id')
                        ->createCommand()
                        ->sql . ')'
                ]);

        if ($universityId) {
            $query->where(['m.university_id' => $universityId]);
        }

        if ($limit) {
            $query->limit($limit);
        }

        return $query->orderBy(['rating' => SORT_DESC])->all();
    }

    public static function getAllMajors($universityId = null, $limit = null)
    {
        $query = self::find();
        if ($universityId) {
            $query->where(['university_id' => $universityId]);
        }
        if ($limit) {
            $query->limit($limit);
        }
        return $query->all();
    }
    
    public static function getAllMajorsInArrayMap($universityId = null)
    {
        return ArrayHelper::map(self::getAllMajors($universityId), 'id', 'name');
    }
}
