<?php

namespace common\models\university;

use Yii,
    common\models\Followers;

/**
 * This is the model class for table "{{%virtual_classes}}".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $tutors_id
 * @property string $name
 * @property string $img_src
 * @property string $description
 * @property string $date_start
 * @property string $slug
 * @property integer $status
 * @property integer $price_type
 * @property integer $price_m
 * @property integer $price_c
 * @property string $created_at
 * @property string $updated_at
 */
class VirtualClasses extends \yii\db\ActiveRecord
{
    const TYPE_OPEN = 1;
    const TYPE_CLOSED = 2;

    public static $typeArray = [
        self::TYPE_OPEN => "Open",
        self::TYPE_CLOSED => 'Closed'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%virtual_classes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'tutors_id', 'name', 'date_start'], 'required'],
            [['course_id', 'tutors_id', 'status', 'type', 'price_type', 'price_m', 'price_c'], 'integer'],
            [['description'], 'string'],
            [['date_start', 'created_at', 'updated_at'], 'safe'],
            [['name', 'img_src', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'course_id' => Yii::t('app', 'Course ID'),
            'tutors_id' => Yii::t('app', 'Tutors ID'),
            'name' => Yii::t('app', 'Name'),
            'img_src' => Yii::t('app', 'Img Src'),
            'description' => Yii::t('app', 'Description'),
            'date_start' => Yii::t('app', 'Date Start'),
            'slug' => Yii::t('app', 'Slug'),
            'status' => Yii::t('app', 'Status'),
            'price_type' => Yii::t('app', 'Price Type'),
            'price_m' => Yii::t('app', 'Price M'),
            'price_c' => Yii::t('app', 'Price C'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }
    
    public function getRankCount()
    {
        return 3.5;
    }

    public function getStudentsCount()
    {
        return 2;
    }

    public function getJoinClassText()
    {
        return 'View';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassesFollowModel()
    {
        return $this->hasMany(Followers::className(), ['object_id' => 'id'])
                ->andOnCondition(['cf.type' => Followers::TYPE_VIRTUAL_CLASSES]);
    }
}
