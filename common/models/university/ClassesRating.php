<?php

namespace common\models\university;

use Yii;

/**
 * This is the model class for table "{{%classes_rating}}".
 *
 * @property integer $id
 * @property integer $class_id
 * @property integer $user_id
 * @property integer $professor_id
 * @property integer $rate_professor
 * @property string $level_of_difficulty
 * @property integer $prof_again
 * @property integer $take_for_credit
 * @property integer $textbook_use
 * @property integer $attendance
 * @property integer $hotness
 * @property string $more_specific
 * @property string $created_at
 * @property string $updated_at
 */
class ClassesRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%classes_rating}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_id', 'user_id', 'professor_id'], 'required'],
            [['class_id', 'user_id', 'rate_professor', 'prof_again', 'take_for_credit', 'textbook_use', 'attendance', 'hotness', 'professor_id'], 'integer'],
            [['level_of_difficulty', 'more_specific'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'class_id' => Yii::t('app', 'Class ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'rate_professor' => Yii::t('app', 'Rate Professor'),
            'level_of_difficulty' => Yii::t('app', 'Level Of Difficulty'),
            'prof_again' => Yii::t('app', 'Prof Again'),
            'take_for_credit' => Yii::t('app', 'Take For Credit'),
            'textbook_use' => Yii::t('app', 'Textbook Use'),
            'attendance' => Yii::t('app', 'Attendance'),
            'hotness' => Yii::t('app', 'Hotness'),
            'more_specific' => Yii::t('app', 'More Specific'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassesModel()
    {
        return $this->hasOne(Classes::className(), ['id' => 'class_id']);
    }
}
