<?php

namespace common\models\university;

use Yii,
    yii\behaviors\TimestampBehavior,
    yii\db\Expression,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%courses}}".
 *
 * @property int $id
 * @property int $major_id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property int $status
 * @property int $creator
 * @property string $url_name
 *
 * @property Classes[] $classes
 * @property Majors $major
 */
class Courses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%courses}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['major_id'], 'required'],
            [['major_id', 'status', 'creator'], 'integer'],
            [['name', 'url_name'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['url_name'], 'string', 'max' => 50],
            [['name'], 'unique'],
            [['major_id'], 'exist', 'skipOnError' => true, 'targetClass' => Majors::className(), 'targetAttribute' => ['major_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class'         => 'common\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'url_name',
                'translit'      => true
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'major_id' => Yii::t('app', 'Major'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Date Create'),
            'updated_at' => Yii::t('app', 'Date Update'),
            'status' => Yii::t('app', 'Status'),
            'creator' => Yii::t('app', 'Creator'),
            'url_name' => Yii::t('app', 'Url Name'),
        ];
    }

    public static function getAllInArrayMapWithMajor($universityId = null)
    {
        $queryM = Majors::find();
        
        if ($universityId) {
            $queryM->where(['university_id' => $universityId]);
        }
        $arrayRes = [];
        if ($queryM->all()) {
            foreach ($queryM->all() as $major) {
                if ($major->courses) {
                    $arrayRes[$major->name] = ArrayHelper::map($major->courses, 'id', 'name');
                }
            }
        }

        return $arrayRes;
    }

    /**
     * @param $courseId
     * @return integer
     */
    public static function getCountClassesInCourse($courseId)
    {
        return Classes::find()->where(['course_id' => $courseId])->count();
    }

    public static function getAllInArrayMap($universityId = null, $major_id = null)
    {
        $query = self::find()
                ->alias('courses')
                ->joinWith(['majorModel major']);

        if ($universityId) {
            $query->where(['major.university_id' => $universityId]);
        }

        if ($major_id) {
            $query->where(['courses.major_id' => $major_id]);
        }

        return ArrayHelper::map($query->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassesModel()
    {
        return $this->hasMany(Classes::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getArrayMapCoursesByMajorId($majorId)
    {
        $query = self::find()->where(['major_id' => $majorId]);
        
        return ArrayHelper::map($query->all(), 'id', 'name');
    }

    public function getMajorModel()
    {
        return $this->hasOne(Majors::className(), ['id' => 'major_id']);
    }
}
