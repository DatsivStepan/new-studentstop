<?php

namespace common\models\university;

use Yii;

/**
 * This is the model class for table "{{%classes_events}}".
 *
 * @property integer $id
 * @property integer $class_id
 * @property integer $user_id
 * @property string $date_start
 * @property string $date_end
 * @property string $title
 * @property string $short_content
 * @property string $content
 * @property string $img_src
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class ClassesEvents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%classes_events}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_id', 'user_id', 'date_start', 'date_end', 'title', 'content'], 'required'],
            [['class_id', 'user_id', 'status'], 'integer'],
            [['date_start', 'date_end', 'created_at', 'updated_at'], 'safe'],
            [['short_content', 'content'], 'string'],
            [['title', 'img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'class_id' => Yii::t('app', 'Class ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'title' => Yii::t('app', 'Title'),
            'short_content' => Yii::t('app', 'Short Content'),
            'content' => Yii::t('app', 'Content'),
            'img_src' => Yii::t('app', 'Img Src'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getDateCreate($format = 'M h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }
    
    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }
    
    public function getAuthorName()
    {
        return $this->user ? $this->user->getNameAndSurname() : '';
    }

    public function getDateStart($format = 'M j')
    {
        return $this->date_start ? date($format, strtotime($this->date_start)) : '';
    }
    
    public function getDateEnd($format = 'M j')
    {
        return $this->date_end ? date($format, strtotime($this->date_end)) : '';
    }

    public function getAuthorAvatar()
    {
        return $this->user ? $this->user->getAvatar() : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
