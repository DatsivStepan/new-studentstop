<?php

namespace common\models\university;

use Yii;

/**
 * This is the model class for table "{{%classes_follow}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $class_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class ClassesFollow extends \yii\db\ActiveRecord
{
    const STATUS_FOLLOW = 1;
    const STATUS_VIEW = 2;

    public static $arrayStatus = [
        self::STATUS_FOLLOW => 'Follow',
        self::STATUS_VIEW => 'View',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%classes_follow}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'class_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

            
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'class_id' => Yii::t('app', 'Class ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public static function saveFollow($userId, $classId) {
        $model = new self();
        $model->user_id = $userId;
        $model->class_id = $classId;
        return $model->save();
    }
    public static function checkJoinType($userId, $classId) {
        $model = self::findOne(['user_id' => $userId, 'class_id' => $classId]);
        $status = self::STATUS_FOLLOW;
        if ($model) {
            $status = self::STATUS_VIEW;
        }
        return $status;
    }
}
