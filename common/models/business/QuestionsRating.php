<?php

namespace common\models\business;

use Yii,
    common\models\User;

/**
 * This is the model class for table "questions_rating".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $answer_id
 * @property integer $rating
 * @property integer $type
 * @property string $created_at
 * @property string $updated_at
 */
class QuestionsRating extends \yii\db\ActiveRecord
{
    const TYPE_ANSWER = 0;
    const TYPE_QUESTION = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions_rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'answer_id'], 'required'],
            [['user_id', 'rating', 'answer_id', 'type'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'user_id' => Yii::t('app', 'User'),
            'answer_id' => Yii::t('app', 'Answer'),
            'rating' => Yii::t('app', 'Rating'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->user ? $this->user->getNameAndSurname() : '';
    }

    /**
     * @return string
     */
    public function getAuthorAvatar()
    {
        return $this->user ? $this->user->getAvatar() : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionsAnswersModel()
    {
        return $this->hasOne(QuestionsAnswers::className(), ['id' => 'answer_id']);
    }
}
