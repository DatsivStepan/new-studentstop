<?php

namespace common\models\business;

use Yii,
    common\models\User,
    common\models\business\Questions;

/**
 * This is the model class for table "{{%questions_invite}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $question_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class QuestionsInvite extends \yii\db\ActiveRecord
{
    const STATUS_NOT_READ = 0;
    const STATUS_READ = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%questions_invite}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'question_id'], 'required'],
            [['user_id', 'question_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'question_id' => Yii::t('app', 'Question'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param $questionId
     * @param $answerId
     * @return bool
     */
    public static function addInvite($questionId, $userId) {
        $model = new self;
        $model->question_id = $questionId;
        $model->user_id = $userId;
        $model->status = self::STATUS_NOT_READ;
        return $model->save();
    }

    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionModel()
    {
        return $this->hasOne(Questions::className(), ['id' => 'question_id']);
    }
}
