<?php

namespace common\models\business;

use Yii,
    common\models\business\QuestionsInvite,
    common\models\User,
    common\models\Favorites,
    frontend\components\helpers\FunctionHelper,
    common\models\Notifications,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%questions}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $user_id
 * @property integer $university_id
 * @property integer $category_id
 * @property integer $privacy_status
 * @property double $money
 * @property double $credits
 * @property integer $status
 * @property integer $class_id
 * @property string $created_at
 * @property string $updated_at
 */
class Questions extends \yii\db\ActiveRecord
{
    const PRIVACY_STATUS_ALL = 1;
    const PRIVACY_STATUS_FRIEND = 2;
    const PRIVACY_STATUS_CLASS = 3;
    const PRIVACY_STATUS_INVITE = 4;

    public static $privacyStatusArray = [
        self::PRIVACY_STATUS_ALL => 'All users',
        self::PRIVACY_STATUS_FRIEND => 'My friend',
        self::PRIVACY_STATUS_CLASS => 'Class mates',
        self::PRIVACY_STATUS_INVITE => 'Invite'
    ];
    
    public $inviteUsers;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%questions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'user_id', 'university_id', 'category_id'], 'required'],
            [['description'], 'string'],
            [['user_id', 'university_id', 'category_id', 'privacy_status', 'status', 'class_id'], 'integer'],
            [['money', 'credits'], 'number'],
            [['created_at', 'updated_at', 'inviteUsers'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'university_id' => Yii::t('app', 'University'),
            'category_id' => Yii::t('app', 'Category'),
            'privacy_status' => Yii::t('app', 'Privacy Status'),
            'money' => Yii::t('app', 'Money'),
            'credits' => Yii::t('app', 'Credits'),
            'status' => Yii::t('app', 'Status'),
            'class_id' => Yii::t('app', 'Class'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        $this->money = $this->money ? $this->money : 0;
        $this->credits = $this->credits ? $this->credits : 0;

        return parent::beforeSave($insert);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        if (($this->privacy_status == self::PRIVACY_STATUS_INVITE) && is_array($this->inviteUsers)) {
//            $invitesUsersId = QuestionsInvite::getAll
            foreach ($this->inviteUsers as $userId) {
                if (!QuestionsInvite::find()->where(['question_id' => $this->id, 'user_id' => $userId])->exists()) {
                    QuestionsInvite::addInvite($this->id, $userId);
                }
            }
            Notifications::addNew(
                Notifications::MODULE_QUESTION_BANK,
                Notifications::TYPE_QUESTION_BANK_USER_INVITE,
                $this->id,
                Notifications::generateTextForNotitficationType(Notifications::TYPE_QUESTION_BANK_USER_INVITE),
                $this->inviteUsers
            );
        }
    }

    /**
     * @param int|bool $userId
     * @return bool
     */
    public function isOwner($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return $userId == $this->user_id ? true : false;
    }

    /**
     * @return integer
     */
    public function getRating()
    {
        $ratings = QuestionsRating::find()->alias('qr')->joinWith(['questionsAnswersModel qa'])->where(['qa.question_id' => $this->id]);
        if ($ratings->exists()) {
            return $ratings->sum('rating') / $ratings->count();
        }
        return 0;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return ($this->money ? FunctionHelper::numberFormat($this->money) . '$' : '') . ($this->money && $this->credits ? ' / ' : '') . ($this->credits ? FunctionHelper::numberFormat($this->credits) . '(c)' : '');
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->user ? $this->user->getNameAndSurname() : '';
    }

    /**
     * @return string
     */
    public function getAuthorAvatar()
    {
        return $this->user ? $this->user->getAvatar() : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversityModel()
    {
        return $this->hasOne(University::className(), ['id' => 'university_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionsInviteModel()
    {
        return $this->hasMany(QuestionsInvite::className(), ['question_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionsAnswersModel()
    {
        return $this->hasMany(QuestionsAnswers::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoritesModel()
    {
        return $this->hasOne(Favorites::className(), ['object_id' => 'id'])
                ->andOnCondition(['favorites.type' => Favorites::TYPE_QUESTION]);
    }
}
