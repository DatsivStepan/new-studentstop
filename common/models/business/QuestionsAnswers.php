<?php

namespace common\models\business;

use Yii,
    common\models\User,
    frontend\components\helpers\FunctionHelper,
    common\models\pay\PayRequests,
    common\models\Notifications;

/**
 * This is the model class for table "{{%questions_answers}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $question_id
 * @property string $answer
 * @property string $show_answer
 * @property string $money
 * @property string $credit
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class QuestionsAnswers extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_PAYED = 1;
    const STATUS_ON_CONFIRM = 2;
    const STATUS_CONFIRM = 3;
    const STATUS_REGECT = 4;
    const STATUS_DELETE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%questions_answers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['answer', 'show_answer'], 'required'],
            [['user_id', 'question_id', 'status'], 'integer'],
            [['money', 'credit'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['answer', 'show_answer'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'question_id' => Yii::t('app', 'Question ID'),
            'answer' => Yii::t('app', 'Answer'),
            'show_answer' => Yii::t('app', 'Show Answer'),
            'money' => Yii::t('app', 'Money'),
            'credit' => Yii::t('app', 'Credit'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        if ($insert && $this->questionModel && $this->questionModel->user_id) {
            Notifications::addNew(
                Notifications::MODULE_QUESTION_BANK,
                Notifications::TYPE_QUESTION_BANK_OWNER_RECEIDES_ANDWER,
                $this->id,
                Notifications::generateTextForNotitficationType(Notifications::TYPE_QUESTION_BANK_OWNER_RECEIDES_ANDWER, ['name' => $this->questionModel->title]),
                [$this->questionModel->user_id]
            );
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        $this->money = $this->money ? $this->money : 0;
        $this->credit = $this->credit ? $this->credit : 0;
        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function hasRating()
    {
        return QuestionsRating::find()->where(['answer_id' => $this->id, 'type' => QuestionsRating::TYPE_ANSWER])->exists();
    }

    /**
     * @param int|bool $userId
     * @return bool
     */
    public function isOwner($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return $userId == $this->user_id ? true : false;
    }

    /**
     * @return integer
     */
    public function isPayed()
    {
        return PayRequests::find()
                ->where(['typePay' => PayRequests::$typesPay])
                ->andWhere([
                    'objectId' => $this->id,
                    'module' => PayRequests::MODULE_QUESTION_BANK,
                    'operationType' => PayRequests::OPERATION_TYPE_QUESTION_ANSWER,
                    'pay_status' => PayRequests::PAY_STATUS_COMFIRM
                ])
                ->exists();
    }

    /**
     * @return integer
     */
    public function getRating()
    {
        return $this->answerRatingModel ? $this->answerRatingModel->rating : 0;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return ($this->money ? FunctionHelper::numberFormat($this->money) . '$' : '') . ($this->money && $this->credit ? ' / ' : '') . ($this->credit ? FunctionHelper::numberFormat($this->credit) . '(c)' : '');
    }

    /**
     * @param $format
     * @return string
     */
    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->user ? $this->user->getNameAndSurname() : '';
    }

    /**
     * @return string
     */
    public function getAuthorAvatar()
    {
        return $this->user ? $this->user->getAvatar() : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionModel()
    {
        return $this->hasOne(Questions::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionsRatingModel()
    {
        return $this->hasOne(QuestionsRating::className(), ['answer_id' => 'id'])
                ->andOnCondition(['type' => QuestionsRating::TYPE_QUESTION]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerRatingModel()
    {
        return $this->hasOne(QuestionsRating::className(), ['answer_id' => 'id'])
                ->andOnCondition(['type' => QuestionsRating::TYPE_ANSWER]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayRequestsModel()
    {
        return $this->hasOne(PayRequests::className(), ['objectId' => 'id'])
                ->andOnCondition([
                    'typePay' => PayRequests::$typesPay,
                    'module' => PayRequests::MODULE_QUESTION_BANK,
                    'operationType' => PayRequests::OPERATION_TYPE_QUESTION_ANSWER
                ]);
    }
}