<?php

namespace common\models\jobs;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%jobs_industries}}".
 *
 * @property integer $id
 * @property integer $name
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class JobsIndustries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%jobs_industries}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public static function getAllInArrayMap()
    {
        $query = self::find();

        return ArrayHelper::map($query->all(), 'id', 'name');
    }
    
}
