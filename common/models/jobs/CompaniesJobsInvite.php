<?php

namespace common\models\jobs;

use Yii,
    common\models\User;

/**
 * This is the model class for table "{{%companies_jobs_invite}}".
 *
 * @property integer $id
 * @property integer $company_job_id
 * @property integer $to_user_id
 * @property string $salary
 * @property integer $salary_type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class CompaniesJobsInvite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%companies_jobs_invite}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_job_id', 'to_user_id'], 'required'],
            [['company_job_id', 'to_user_id', 'salary_type', 'status'], 'integer'],
            [['salary'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_job_id' => Yii::t('app', 'Company Job ID'),
            'to_user_id' => Yii::t('app', 'To User ID'),
            'salary' => Yii::t('app', 'Salary'),
            'salary_type' => Yii::t('app', 'Salary Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesJobsModel()
    {
        return $this->hasOne(CompaniesJobs::className(), ['id' => 'company_job_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }
}
