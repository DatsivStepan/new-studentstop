<?php

namespace common\models\jobs;

use Yii;

/**
 * This is the model class for table "{{%companies_news}}".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $title
 * @property string $content
 * @property string $img_src
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class CompaniesNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%companies_news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'type', 'status'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'img_src' => Yii::t('app', 'Img Src'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
     public function getAuthorName()
    {
        return $this->user ? $this->user->getNameAndSurname() : '';
    }

    public function getAuthorLink()
    {
        return $this->user ? $this->user->getProfileLink() : '';
    }

    public function getAuthorAvatar()
    {
        return $this->user ? $this->user->getAvatar() : '';
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesModel()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommentModel()
    {
        return $this->hasMany(PostComment::className(), ['post_id' => 'id']);
    }
}
