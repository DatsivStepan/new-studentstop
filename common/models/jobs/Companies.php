<?php

namespace common\models\jobs;

use Yii,
    yii\helpers\Url,
    common\models\User,
    common\models\UserRole,
    common\models\Favorites;

/**
 * This is the model class for table "{{%companies}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $address
 * @property string $founded
 * @property string $description
 * @property string $img_src
 * @property string $phone
 * @property string $email
 * @property string $fax
 * @property string $website
 * @property string $social_link_facebook
 * @property string $social_link_twitter
 * @property string $social_link_linkedin
 * @property integer $industry_id
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class Companies extends \yii\db\ActiveRecord
{
    
    public static $filterTop = [
        '1' => 'Students',
        '2' => 'Jobs',
        '3' => 'Company'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%companies}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'address', 'founded'], 'required'],
            [['user_id', 'industry_id', 'type', 'status'], 'integer'],
            [['founded', 'created_at', 'updated_at'], 'safe'],
            [['description'], 'string'],
            [['name', 'address', 'img_src', 'phone', 'email', 'fax', 'website', 'social_link_facebook', 'social_link_twitter', 'social_link_linkedin'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'address' => Yii::t('app', 'Address'),
            'founded' => Yii::t('app', 'Founded'),
            'description' => Yii::t('app', 'Description'),
            'img_src' => Yii::t('app', 'Img Src'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'fax' => Yii::t('app', 'Fax'),
            'website' => Yii::t('app', 'Website'),
            'social_link_facebook' => Yii::t('app', 'Social Link Facebook'),
            'social_link_twitter' => Yii::t('app', 'Social Link Twitter'),
            'social_link_linkedin' => Yii::t('app', 'Social Link Linkedin'),
            'industry_id' => Yii::t('app', 'Industry ID'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        $this->founded = $this->founded ? date('Y-m-d', strtotime($this->founded)) : null;
        return parent::beforeSave($insert);
    }

    
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public static function getTopList()
    {
        $filterTop = self::$filterTop;
        $resultArray = [];
        foreach ($filterTop as $filterId => $filterName) {
            switch ($filterId) {
                case 1:
                    $resultArray[$filterId] = User::getAll(null, UserRole::ROLE_STUDENT, 8);
                    break;
                case 2:
                    $resultArray[$filterId] = CompaniesJobs::getAll(null, 8);
                    break;
                case 3:
                    $resultArray[$filterId] = self::getAll(null, 8);
                    break;
            }
        }
        return $resultArray;

        return null;
    }

    public function getJobsCount()
    {
        return CompaniesJobs::find()->where(['company_id' => $this->id])->count();
    }

    public function getLink()
    {
        return Url::to(['/jobs/default/company/', 'id' => $this->id]);
    }

    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }

    public function getRating()
    {
        return 0;
    }

    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }
    
    public static function getAll($universityId = null, $limit = null)
    {
        $query = self::find()
                ->alias('c');

//        if ($universityId) {
//            $query->andWhere(['c.university_id' => $universityId]);
//        }

        if ($limit) {
            $query->limit($limit);
        }
        return $query->all();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoritesJobModel()
    {
        return $this->hasOne(Favorites::className(), ['object_id' => 'id'])
                ->andOnCondition(['favorites.type' => Favorites::TYPE_JOBS_COMPANY]);
    }
}
