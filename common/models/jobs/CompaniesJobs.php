<?php

namespace common\models\jobs;

use Yii,
    yii\helpers\ArrayHelper,
    common\models\Favorites;

/**
 * This is the model class for table "{{%companies_jobs}}".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $name
 * @property integer $gender
 * @property integer $user_role_need
 * @property string $description
 * @property integer $show_in_university
 * @property integer $major_id
 * @property integer $jobs_function_id
 * @property string $salary
 * @property integer $salary_type
 * @property integer $industry_level
 * @property string $skills
 * @property string $img_src
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class CompaniesJobs extends \yii\db\ActiveRecord
{
    const SALARY_TYPE_DAY = 1;
    const SALARY_TYPE_WEEK = 2;
    const SALARY_TYPE_MONTH = 3;
    const SALARY_TYPE_YEAR = 4;

    const INDUSTRY_LEVEL_JUNIOR = 1;
    const INDUSTRY_LEVEL_MIDDLE = 2;
    const INDUSTRY_LEVEL_SENIOR = 3;

    public static $arrayIndustryLevel = [
        self::INDUSTRY_LEVEL_JUNIOR => 'juniop',
        self::INDUSTRY_LEVEL_MIDDLE => 'miffle',
        self::INDUSTRY_LEVEL_SENIOR => 'senior'
    ];

    public static $arraySalaryTypes = [
        self::SALARY_TYPE_DAY => 'day',
        self::SALARY_TYPE_WEEK => 'week',
        self::SALARY_TYPE_MONTH => 'month',
        self::SALARY_TYPE_YEAR => 'year'
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%companies_jobs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'name'], 'required'],
            [['company_id', 'gender', 'user_role_need', 'show_in_university', 'major_id', 'jobs_function_id', 'salary_type', 'industry_level', 'type', 'status'], 'integer'],
            [['description'], 'string'],
            [['salary'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'skills', 'img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'name' => Yii::t('app', 'Name'),
            'gender' => Yii::t('app', 'Gender'),
            'user_role_need' => Yii::t('app', 'User Role Need'),
            'description' => Yii::t('app', 'Description'),
            'show_in_university' => Yii::t('app', 'Show In University'),
            'major_id' => Yii::t('app', 'Major ID'),
            'jobs_function_id' => Yii::t('app', 'Jobs Function ID'),
            'salary' => Yii::t('app', 'Salary'),
            'salary_type' => Yii::t('app', 'Salary Type'),
            'industry_level' => Yii::t('app', 'Industry Level'),
            'skills' => Yii::t('app', 'Skills'),
            'img_src' => Yii::t('app', 'Img Src'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    
    public function getLink()
    {
        return '/jobs/companies/job/' . $this->id;
    }

    
    public function getSalary()
    {
        return $this->salary  . (array_key_exists($this->salary_type, self::$arraySalaryTypes) ? '/' . self::$arraySalaryTypes[$this->salary_type] : '');
    }

    /**
     * @param $userId
     * @return bool
     */
    public function getApplyStatus($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return CompaniesJobsApply::find()->where(['company_job_id' => $this->id, 'user_id' => $userId])->exists();
    }

    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }

    public static function getAll($companyId = null, $limit = null)
    {
        $query = self::find()
                ->alias('c');

//        if ($universityId) {
//            $query->andWhere(['c.university_id' => $universityId]);
//        }
        if ($companyId) {
            $query->andWhere(['c.company_id' => $companyId]);
        }

        if ($limit) {
            $query->limit($limit);
        }
        return $query->all();
    }
    
    public static function getAllInArrayMap($companyId = null, $limit = null)
    {
        return ArrayHelper::map(self::getAll($companyId), 'id', 'name');
    }

    public function getRating()
    {
        return 0;
    }

    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesModel()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesJobsInviteModel()
    {
        return $this->hasMany(CompaniesJobsInvite::className(), ['company_job_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesJobsApplyModel()
    {
        return $this->hasMany(CompaniesJobsApply::className(), ['company_job_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoritesModel()
    {
        return $this->hasOne(Favorites::className(), ['object_id' => 'id'])
                ->andOnCondition(['favorites.type' => Favorites::TYPE_JOBS_RESUME]);
    }
}
