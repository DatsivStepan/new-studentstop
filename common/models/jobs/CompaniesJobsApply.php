<?php

namespace common\models\jobs;

use Yii,
    common\models\User;

/**
 * This is the model class for table "{{%companies_jobs_apply}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $company_job_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class CompaniesJobsApply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%companies_jobs_apply}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_job_id'], 'required'],
            [['user_id', 'company_job_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'company_job_id' => Yii::t('app', 'Company Job ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesJobsModel()
    {
        return $this->hasOne(CompaniesJobs::className(), ['id' => 'company_job_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }
}
