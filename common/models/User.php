<?php
namespace common\models;

use Yii,
    common\models\query\UserQuery,
    yii\base\NotSupportedException,
    yii\behaviors\TimestampBehavior,
    yii\db\ActiveRecord,
    yii\web\IdentityInterface,
    yii\helpers\ArrayHelper,
    common\models\user\UserInfo,
    common\models\University,
    common\models\user\UserFriend,
    common\models\jobs\Companies,
    common\models\user\UserJobsResume,
    common\models\Followers,
    common\models\university\ClassesRating,
    common\models\pay\UserCredits,
    common\models\university\Classes;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    
    const STATUS_EMAIL_ACTIVE = 1;
    const STATUS_EMAIL_NOT_ACTIVE = 0;
    
    public $profile;
    public $password;
    public $rememberMe;
    public $userNameSurname;
    public $rating;
    
    private static $stclass = null;
    public static function __callStatic($name, $args)
    {
        return call_user_func_array([!self::$stclass ? self::$stclass = new UserStatic : self::$stclass, $name], $args);
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            //TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['username', 'string'],
            ['active_status', 'integer'],
            ['email', 'email'],
            ['social_email', 'match', 'pattern' => '/.com$/', 'message' => 'email must be org.'],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        if (Yii::$app->getSession()->has('user-'.$id)) {
            return new self(Yii::$app->getSession()->get('user-'.$id));
        }else {
            return self::findOne($id);
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()
            ->joinWith('tokens t')
            ->andWhere(['t.token' => $token])
            ->andWhere(['>', 't.expired_at', time()])
            ->one();
    }
    
     public static function findByEAuth($service) {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName().'-'.$service->getId();
        $attributes = [
            'id' => $id,
            'username' => $service->getAttribute('name'),
            'auth_key' => md5($id),
            'profile' => $service->getAttributes(),
        ];
        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-'.$id, $attributes);
        return new self($attributes);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @return string
     */
    public function getProfileLink()
    {
        return \yii\helpers\Url::to(['/profile/index/'.$this->id .'?openPopup=1']);
    }

    /**
     * @return string
     */
    public function getNameAndSurname()
    {
        return  $this->userInfoModel ? $this->userInfoModel->getNameAndSurname() : ($this->username ? $this->username : $this->email); 
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return  $this->userInfoModel ? $this->userInfoModel->address : ''; 
    }

    /**
     * @param null|int $universityId
     * @param null|int $role
     * @return bool
     */
    public static function getAllInArrayMap($universityId=null, $role=null)
    {
        return ArrayHelper::map(self::getAll($universityId, $role), 'id', function($item){ return $item->getNameAndSurname(); });
    }

    /**
     * @param null|int $universityId
     * @param null|int $role
     * @param null|int $limit
     * @param null|int $classId
     * @return bool
     */
    public static function getAll($universityId=null, $role=null, $limit = null, $classId = null)
    {
        $query = self::find()
                ->alias('u')
                ->select(['u.*', 'userNameSurname' => 'IF(ui.surname, CONCAT(ui.name, " ", ui.surname), u.email)'])
                ->joinWith([
                    'userInfoModel ui',
                    'classesFollowModel cf',
                    'classesProfessorModel cpf'
                ]);
        if ($universityId) {
            $query->andWhere(['ui.university_id' => $universityId]);
        }

        if ($role) {
            $query->andWhere(['ui.user_role_id' => $role]);
        }
        if ($classId) {
            $query->andWhere(['cf.object_id' => $classId]);
        }
        if ($limit) {
            $query->limit($limit);
        }

        return $query->all();
    }

    public static function getAllProfessorByRating($universityId=null, $limit = null, $classId = null)
    {
        $query = self::find()
                ->alias('u')
                ->select([
                    'u.*',
                    'userNameSurname' => 'IF(ui.surname, CONCAT(ui.name, " ", ui.surname), u.email)',
                    'rating' => '('. ClassesRating::find()
                        ->alias('cr')
                        ->select('SUM(cr.rate_professor) / COUNT(cr.id)')
                        ->where('cr.professor_id = u.id')
                        ->createCommand()
                        ->sql . ')'
                ])
                ->joinWith([
                    'userInfoModel ui',
                    'classesFollowModel cf',
                    'classesProfessorModel cpf'
                ])
                ->andWhere(['ui.user_role_id' => UserRole::ROLE_PROFFESOR]);

        if ($universityId) {
            $query->andWhere(['ui.university_id' => $universityId]);
        }

        if ($classId) {
            $query->andWhere(['cpf.id' => $classId]);
        }
        if ($limit) {
            $query->limit($limit);
        }
        return $query->orderBy(['rating' => SORT_DESC])->all();
    }

    public static function getCountUser($universityId=null, $role=null)
    {
        $query = self::find()
                ->alias('u')
                ->select(['u.*', 'userNameSurname' => 'IF(ui.surname, CONCAT(ui.name, " ", ui.surname), u.email)'])
                ->joinWith(['userInfoModel ui']);
        if ($universityId) {
            $query->andWhere(['ui.university_id' => $universityId]);
        }

        if ($role) {
            $query->andWhere(['ui.user_role_id' => $role]);
        }

        return $query->count();
    }
    
    public function getAvatar()
    {
        return $this->userInfoModel ? $this->userInfoModel->getAvatar() : '/images/default_avatar.jpg';
    }
    
    public function getFriends()
    {
        
    }
    
    public function checkFriendRequestType($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        $type = UserFriend::checkFriendRequestType($userId, $this->id);
        return UserFriend::$arrayStatus[$type];
    }
    
    public function getRole()
    {
        return $this->userInfoModel ? $this->userInfoModel->getRole() : '';
    }
    
    public function getRankCount($rateNumber = null)
    {
        $countU = $this->userInfoModel ? self::getCountUser($this->userInfoModel->university_id, $this->userInfoModel->user_role_id) : 0;
        return ($rateNumber ? 1 : '') . '/' . $countU;
    }
    
    public function getRating()
    {
        return 0;
    }
    
    public function getProfessorRating()
    {
        $query = ClassesRating::find()
            ->where(['professor_id' => $this->id]);
//        var_dump($query->all());exit;
        
        return $query->exists() ? $query->sum('rate_professor') / $query->count() : 0;
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @param $returnModel
     * @return bool
     */
    public function hasCompany($return = false)
    {
        return $this->companiesModel ? ($return ? $this->companiesModel : true) : false;
    }

    /**
     * @return bool
     */
    public function hasJobResume()
    {
        return $this->jobsResumeModel ? true : false;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return integer
     */
    public function getMoneyAmount()
    {
        return $this->userCreditsModel ? $this->userCreditsModel->amountM : 0;
    }

    /**
     * @return integer
     */
    public function getCreditsAmount()
    {
        return $this->userCreditsModel ? $this->userCreditsModel->amountC : 0;
    }

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFriedsModels()
    {
        return $this->hasMany(UserFriend::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToFriedsModels()
    {
        return $this->hasMany(UserFriend::className(), ['friend_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfoModel()
    {
        return $this->hasOne(UserInfo::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsResumeModel()
    {
        return $this->hasOne(UserJobsResume::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSocialModel()
    {
        return $this->hasMany(user\UserSocial::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesModel()
    {
        return $this->hasOne(Companies::className(), ['user_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorModel()
    {
        return $this->hasOne(CoursesTutors::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreditsModel()
    {
        return $this->hasOne(UserCredits::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassesFollowModel()
    {
        return $this->hasMany(Followers::className(), ['user_id' => 'id'])
                ->andOnCondition(['type' => Followers::TYPE_CLASSES]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassesProfessorModel()
    {
        return $this->hasOne(Classes::className(), ['professor' => 'id']);
    }

    public function fields()
    {
        return [
            'id' => 'id',
            'username' => 'username',
            'active_status' => 'Status',
            'email' => 'email',
        ];
    }

}
