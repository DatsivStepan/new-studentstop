<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 */
class UserRole extends \yii\db\ActiveRecord
{
    const ROLE_STUDENT = 2;
    const ROLE_PROFFESOR = 3;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
    
    public static function getAllRoleInArrayMap()
    {
        return ArrayHelper::map(self::findAll(['status' => 1]), 'id', 'name');
    }
}
