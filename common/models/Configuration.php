<?php

namespace common\models;

use Yii,
    common\models\Configuration;

/**
 * This is the model class for table "{{%configuration}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $key
 * @property string $value
 * @property string $created_at
 * @property string $updated_at
 */
class Configuration extends \yii\db\ActiveRecord
{
    const CLASS_FIHISHED_DATE1 = 'fihishedDate1';
    const CLASS_FIHISHED_DATE2 = 'fihishedDate2';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%configuration}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'key', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
            'created_at' => Yii::t('app', 'Date Create'),
            'updated_at' => Yii::t('app', 'Date Update'),
        ];
    }

    public static function getClassFihishedDates(){
        $res = [];
        if ($date1 = self::findOne(['key' => self::CLASS_FIHISHED_DATE1])) {
            $res[] = date('Y-m-d', strtotime(date('Y').'-' . $date1->value));
        }
        if ($date2 = self::findOne(['key' => self::CLASS_FIHISHED_DATE2])) {
            $res[] = date('Y-m-d', strtotime(date('Y').'-' . $date2->value));
        }

        return $res;
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
}
