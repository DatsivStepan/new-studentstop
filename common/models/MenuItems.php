<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%menu_items}}".
 *
 * @property integer $id
 * @property string $logo
 * @property string $name
 * @property string $type
 * @property string $url
 * @property integer $parent_id
 * @property integer $status
 * @property string $class_attr
 * @property string $created_at
 * @property string $updated_at
 */
class MenuItems extends \yii\db\ActiveRecord
{
    const TYPE_PROFILE = 1;
    const TYPE_CLASSES = 2;
    const TYPE_CLASS_ONE = 13;
    const TYPE_HOME = 3;
    const TYPE_PROFILE_HEADER = 4;
    const TYPE_VIRTUAL_CLASSES = 5;
    const TYPE_VIRTUAL_CLASSES_ACTIVE = 6;
    const TYPE_VIRTUAL_CLASSIFIED_MARKET = 7;
    const TYPE_RENTS = 8;
    const TYPE_RENT_A_HOUSE = 9;
    const TYPE_QUESTION_BANK = 10;
    const TYPE_JOBS_COMPANY = 11;
    const TYPE_JOBS_STUDENT = 12;

    public static $arrayTypes = [
        self::TYPE_PROFILE => 'Profile',
        self::TYPE_CLASSES => 'Classes',
        self::TYPE_CLASS_ONE => 'Class',
        self::TYPE_HOME => 'Home',
        self::TYPE_PROFILE_HEADER => 'Profile in HEADER',
        self::TYPE_VIRTUAL_CLASSES => 'Virtual classes',
        self::TYPE_VIRTUAL_CLASSES_ACTIVE => 'Virtual classes with tutor',
        self::TYPE_VIRTUAL_CLASSIFIED_MARKET => 'Clsssified market',
        self::TYPE_RENTS => 'Borrow',
        self::TYPE_RENT_A_HOUSE => 'Rent a house',
        self::TYPE_QUESTION_BANK => 'Question bank',
        self::TYPE_JOBS_COMPANY => 'Jobs company',
        self::TYPE_JOBS_STUDENT => 'Jobs student',
    ];
    
    public static $arrayProfilePageNotShow = [
        'Marketplace', 'My  docs', 'Credits'
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menu_items}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'status', 'sort'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['logo', 'name', 'type', 'url', 'class_attr'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'logo' => Yii::t('app', 'Logo'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'url' => Yii::t('app', 'Url'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'status' => Yii::t('app', 'Status'),
            'class_attr' => Yii::t('app', 'Class attribute'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
        
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public static function getParentArray()
    {
        $query = self::find()->where(['parent_id' => null])->orWhere(['parent_id' => 0]);
        
        return ArrayHelper::map($query->all(), 'id', 'name');
    }
    
    public function getChildItem()
    {
        return self::find()->where(['parent_id' => $this->id])->orderBy(['sort' => SORT_ASC])->all();
    }
    
    public static function getAllItemByType($type)
    {
        return self::find()->where(['type' => $type])->orderBy(['sort' => SORT_ASC])->all();
    }

    public function getImages()
    {
        return $this->logo ? '/' . $this->logo : '/images/default_avatar.jpg';
    }
}
