<?php

namespace common\models\user;

use Yii,
    common\models\University,
    common\models\User,
    common\models\UserRole,
    common\models\university\Classes;

/**
 * This is the model class for table "{{%user_info}}".
 *
 * @property int $id
 * @property int $id_user
 * @property int $user_role_id
 * @property string $birthday
 * @property string $mobile
 * @property string $address
 * @property string $website
 * @property string $facebook
 * @property string $twitter
 * @property int $gender
 * @property string $avatar
 * @property string $avatar_wrapper
 * @property int $university_id
 * @property string $position_lat
 * @property string $position_lng
 * @property int $major_id
 * @property string $about
 * @property string $date_end_education
 * @property int $industry_id
 * @property int $jobs_function_id
 * @property string $linkedin
 * @property int $country_id
 * @property int $states_id
 * @property int $city_id
 * @property string $date_begin_education
 */
class UserInfo extends \yii\db\ActiveRecord
{
    const GENDER_MAN = 1;
    const GENDER_WOMAN = 0;

    public static $arrayGender = [
        self::GENDER_MAN => 'Man',
        self::GENDER_WOMAN => 'Woman'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_info}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['university_id', 'user_role_id', 'name', 'surname'], 'required'],
            [
                [
                    'id_user',  'user_role_id', 'gender', 'university_id',
                    'major_id', 'industry_id', 'jobs_function_id', 'country_id',
                    'states_id', 'city_id'
                ],
                'integer'
            ],
            [
                [
                    'birthday', 'date_end_education', 'date_begin_education'
                ],
                'safe'
            ],
            [['about'], 'string'],
            [
                [
                    'address', 'avatar', 'avatar_wrapper', 'position_lat',
                    'position_lng', 'linkedin', 'name', 'surname'
                ],
                'string',
                'max' => 255
            ],
            [['mobile'], 'string', 'max' => 20],
            [['website'], 'string', 'max' => 50],
            [['facebook', 'twitter'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'name' => Yii::t('app', 'First name'),
            'surname' => Yii::t('app', 'Last name'),
            'user_role_id' => Yii::t('app', 'Academic Status'),
            'birthday' => Yii::t('app', 'Date of birth'),
            'mobile' => Yii::t('app', 'Mobile'),
            'address' => Yii::t('app', 'Address'),
            'website' => Yii::t('app', 'Website'),
            'facebook' => Yii::t('app', 'Facebook'),
            'twitter' => Yii::t('app', 'Twitter'),
            'gender' => Yii::t('app', 'Gender'),
            'avatar' => Yii::t('app', 'Avatar'),
            'avatar_wrapper' => Yii::t('app', 'Avatar Wrapper'),
            'university_id' => Yii::t('app', 'University'),
            'position_lat' => Yii::t('app', 'Position Lat'),
            'position_lng' => Yii::t('app', 'Position Lng'),
            'major_id' => Yii::t('app', 'Major ID'),
            'about' => Yii::t('app', 'About'),
            'date_end_education' => Yii::t('app', 'Date End Education'),
            'industry_id' => Yii::t('app', 'Industry ID'),
            'jobs_function_id' => Yii::t('app', 'Jobs Function ID'),
            'linkedin' => Yii::t('app', 'Linkedin'),
            'country_id' => Yii::t('app', 'Country ID'),
            'states_id' => Yii::t('app', 'States ID'),
            'city_id' => Yii::t('app', 'City ID'),
            'date_begin_education' => Yii::t('app', 'Date Begin Education'),
        ];
    }
    
     public function beforeSave($insert)
    {
//        if ($this->isNewRecord)
//        {
//            $this->created_at = date("Y-m-d H:i:s");
//        } else {
//            $this->updated_at = date("Y-m-d H:i:s");
//        }
         
        $this->date_begin_education = $this->date_begin_education ? date('Y-m-d', strtotime($this->date_begin_education)) : null;
        $this->date_end_education = $this->date_end_education ? date('Y-m-d', strtotime($this->date_end_education)) : null;
        $this->birthday = $this->birthday ? date('Y-m-d', strtotime($this->birthday)) : null;

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes) {
        
        if ($changedAttributes) {
            $message = '';

            foreach($changedAttributes as $attribute => $value) {
                if ($attribute == 'avatar') {
                    $model = new UserProfileEvents();
                    $model->user_id = $this->id_user;
                    $model->events_type = UserProfileEvents::TYPE_CHANGE_PROFILE_AVATAR;
                    $model->events_id = $this->id;
                    $model->note = $this->{$attribute};
                    $model->save();

                    continue;
                }
                $message .= $attribute . ' changed from ' . $value . ' to ' . $this->{$attribute} . '; ';
            }

            if ($message) {
                $model = new UserProfileEvents();
                $model->user_id = $this->id_user;
                $model->events_type = UserProfileEvents::TYPE_CHANGE_PROFILE_DATA;
                $model->events_id = $this->id;
                $model->note = $message;
                $model->save();
            }
        }
        
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param $userId
     * @return string
     */
    public function getClassCount($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        
        return Classes::find()
                ->alias('c')
                ->joinWith(['classesFollowModel cf'])
                ->andWhere([
                    'OR',
                    ['cf.user_id' => $userId],
                    ['c.professor' => $userId]
                ])->count();
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->userRoleModel ? $this->userRoleModel->name : '';
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar ? '/' . $this->avatar : '/images/default_avatar.jpg';
    }

    /**
     * @return bool
     */
    public function isUniversityManager()
    {
        return $this->universityModel && ($this->universityModel->id_owner == $this->id_user);
    }

    /**
     * @return string
     */
    public function getUniversityName()
    {
        return $this->universityModel ? $this->universityModel->name : '';
    }

    /**
     * @return string
     */
    public function getNameAndSurname()
    {
        return  $this->name || $this->surname ? $this->name . ' ' . $this->surname : ($this->userModel ? $this->userModel->email : '' ); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoleModel()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'user_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversityModel()
    {
        return $this->hasOne(University::className(), ['id' => 'university_id']);
    }
}

