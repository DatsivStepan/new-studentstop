<?php

namespace common\models\user;

use Yii,
    common\models\User,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%users_friends}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $friend_id
 * @property string $created_at
 * @property string $updated_at
 */
class UserFriend extends \yii\db\ActiveRecord
{
    const STATUS_FOLLOW = 1;
    const STATUS_UNFOLLOW = 2;
    const STATUS_CONFIRM = 3;

    public static $arrayStatus = [
        self::STATUS_FOLLOW => 'follow',
        self::STATUS_UNFOLLOW => 'unfollow',
        self::STATUS_CONFIRM => 'confirm',
    ];

    public $usernameS;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_friend}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'friend_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'friend_id' => Yii::t('app', 'Friend ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public static function getFriendsIds($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return ArrayHelper::getColumn(self::findAll(['user_id' => $userId]), 'id');
    }
    
    public static function saveFollow($userId, $friendId)
    {
        $statusFriend = self::find()->where(['user_id' => $userId, 'friend_id' => $friendId])->exists();
        if (!$statusFriend) {
            $statusFriend = new self();
            $statusFriend->user_id = $userId;
            $statusFriend->friend_id = $friendId;
            $statusFriend->save();
        }
        return $statusFriend;
    }
    
    public static function saveUnFollow($userId, $friendId)
    {
        $model = self::find()->where(['user_id' => $userId, 'friend_id' => $friendId])->exists();
        $status = true;
        if ($model) {
            $status = self::deleteAll(['user_id' => $userId, 'friend_id' => $friendId]);
        }

        return $status;
    }
    
    /*
     * param int $userId
     * param null|int $universityId
     * @return array
    */
    public static function getFriendCount($userId, $universityId = null)
    {
        $query = self::find()
            ->alias('uf')
            ->select(['uf.*', 'usernameS' => 'CONCAT(fi.name, " ", fi.surname, " ")'])
            ->joinWith(['friendModel f', 'friendInfoModel fi'])
            ->where(['uf.user_id' => $userId]);

        if ($universityId) {
            $query->andWhere(['fi.university_id' => $universityId]);
        }
        return $query->count();
    }
    
    /*
     * param $userId
     * param $searchValue
     * @return array
    */
    public static function getMyFriendArray($userId, $searchValue)
    {
        $query = self::find()
            ->alias('uf')
            ->select(['uf.*', 'usernameS' => 'CONCAT(fi.name, " ", fi.surname, " ")'])
            ->joinWith(['friendModel f', 'friendInfoModel fi'])
            ->where(['uf.user_id' => $userId]);

        if ($searchValue) {
            $query->andFilterHaving(['like', 'usernameS', $searchValue]);
        }

        $friends = [];
        foreach ($query->all() as $model) {
            if ($friend = $model->friendModel) {
                $friends[] = [
                    'name' => $friend->getNameAndSurname(),
                    'link' => $friend->getProfileLink(),
                    'avatar' => $friend->getAvatar(),
                    'type' => 'friend',
                ];
            }
        }
        return $friends;
    }
    
    public static function checkFriendRequestType($userId, $friendId)
    {
        $status = self::STATUS_FOLLOW;

        $statusFriend = self::find()->where(['user_id' => $userId, 'friend_id' => $friendId])->exists();
        $statusFriendRequest = self::find()->where(['friend_id' => $userId, 'user_id' => $friendId])->exists();

        if ($statusFriendRequest) {
            $status = self::STATUS_CONFIRM;
        }

        if ($statusFriend) {
            $status = self::STATUS_UNFOLLOW;
        }

        return $status;
    }
    
    public function getFriendModel()
    {
        return $this->hasOne(User::className(), ['id' => 'friend_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFriendInfoModel()
    {
        return $this->hasOne(UserInfo::className(), ['id_user' => 'friend_id']);
    }
    
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
