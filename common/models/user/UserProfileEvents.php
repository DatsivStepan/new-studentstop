<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "{{%user_profile_events}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $events_id
 * @property integer $events_type
 * @property integer $note
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class UserProfileEvents extends \yii\db\ActiveRecord
{
    const TYPE_CHANGE_PROFILE_DATA = 1;
    const TYPE_ADD_PHOTO = 2;
    const TYPE_ADD_VIDEO = 3;
    const TYPE_CHANGE_PROFILE_AVATAR = 4;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_profile_events}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'events_id', 'events_type'], 'required'],
            [['user_id', 'events_id', 'events_type', 'status'], 'integer'],
            [['created_at', 'updated_at', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'events_id' => Yii::t('app', 'Events ID'),
            'events_type' => Yii::t('app', 'Events Type'),
            'note' => Yii::t('app', 'Note'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }

    public function getUserFilesModel()
    {
        return $this->hasOne(UserFiles::className(), ['id' => 'events_id']);
    }
    
    public function getUserModel()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id']);
    }
    
    public function getUserInfoModel()
    {
        return $this->hasOne(UserInfo::className(), ['id_user' => 'user_id']);
    }
}
