<?php

namespace common\models\user;

use Yii,
    common\models\user\UserInfo,
    common\models\User,
    common\models\Favorites,
    common\models\jobs\CompaniesJobsInvite,
    common\models\jobs\CompaniesJobsApply,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_jobs_resume}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $avatar
 * @property string $about
 * @property string $skills
 * @property string $current_gpa
 * @property string $overall_gpa
 * @property string $created_at
 * @property string $updated_at
 */
class UserJobsResume extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_jobs_resume}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['current_gpa', 'overall_gpa'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['avatar', 'about', 'skills'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'avatar' => Yii::t('app', 'Avatar'),
            'about' => Yii::t('app', 'About'),
            'skills' => Yii::t('app', 'Skills'),
            'current_gpa' => Yii::t('app', 'Current Gpa'),
            'overall_gpa' => Yii::t('app', 'Overall Gpa'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    
    public function getDateCreate($format = 'Y-m-d h:i:s')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }
    
    public function getAvatar()
    {
        return $this->avatar ? '/' . $this->avatar : ($this->userInfoModel ? $this->userInfoModel->getAvatar() : '/images/default_avatar.jpg');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoriteCompaniesIds()
    {
        return ArrayHelper::getColumn(Favorites::find()->where(['type' => Favorites::TYPE_JOBS_COMPANY, 'user_id' => $this->user_id])->all(), 'object_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfoModel()
    {
        return $this->hasOne(UserInfo::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesJobsInviteModel()
    {
        return $this->hasMany(CompaniesJobsInvite::className(), ['to_user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesJobsApplyModel()
    {
        return $this->hasMany(CompaniesJobsApply::className(), ['user_id' => 'user_id']);
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoritesModel()
    {
        return $this->hasOne(Favorites::className(), ['object_id' => 'id'])
                ->andOnCondition(['favorites.type' => Favorites::TYPE_JOBS_JOB]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoritesJobModel()
    {
        return $this->hasOne(Favorites::className(), ['user_id' => 'user_id'])
                ->andOnCondition(['favorites.type' => Favorites::TYPE_JOBS_RESUME]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoritesCompanyModel()
    {
        return $this->hasOne(Favorites::className(), ['user_id' => 'user_id'])
                ->andOnCondition(['favorites.type' => Favorites::TYPE_JOBS_COMPANY]);
    }
}
