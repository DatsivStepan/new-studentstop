<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "{{%user_files}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $file_src
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class UserFiles extends \yii\db\ActiveRecord
{
    const TYPE_PHOTO = 'image';
    const TYPE_VIDEO = 'video';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'file_src', 'type'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_src', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'file_src' => Yii::t('app', 'File Src'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes) {
        
        if ($insert) {
            $model = new UserProfileEvents();
            $model->user_id = $this->user_id;
            $model->events_type = $this->type == self::TYPE_PHOTO ? UserProfileEvents::TYPE_ADD_PHOTO : UserProfileEvents::TYPE_ADD_VIDEO;
            $model->events_id = $this->id;
            $model->save();
        }
        
        return parent::afterSave($insert, $changedAttributes);
    }
}
