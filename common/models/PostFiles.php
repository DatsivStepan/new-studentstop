<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%post_files}}".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $file_src
 * @property integer $type
 * @property string $created_at
 * @property string $updated_at
 */
class PostFiles extends \yii\db\ActiveRecord
{
    const TYPE_VIDEO = 'video';
    const TYPE_IMAGE = 'image';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post_files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 10],
            [['file_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_id' => Yii::t('app', 'Post ID'),
            'file_src' => Yii::t('app', 'File Src'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public static function getAllForSlider($university)
    {
        $query = self::find()
                ->alias('f')
                ->joinWith(['postModel p'])
                ->where(['p.inSlider' => 1, 'p.university_id' => $university])
                ->orderBy(['p.id' => SORT_DESC])
                ->limit(5);

        return $query->all();
    }
    
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostModel()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

}
