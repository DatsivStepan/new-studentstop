<?php

namespace common\models\location;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%location_states}}".
 *
 * @property int $id
 * @property string $name
 * @property int $country_id
 * @property string $lat
 * @property string $lng
 */
class States extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%location_states}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'lat', 'lng'], 'required'],
            [['country_id'], 'integer'],
            [['name'], 'string', 'max' => 30],
            [['lat', 'lng'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'country_id' => Yii::t('app', 'Country ID'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
        ];
    }

    /**
     * @param integer|null $countryId
     * @return array
     */
    public static function getAll($countryId = null)
    {
        $query = self::find();
        if ($countryId) {
            $query->where(['country_id' => $countryId]);
        }

        return $query->all();
    }

    /**
     * @param integer $id
     * @return array
     */
    public static function getAllByCountryId($id)
    {
        return ArrayHelper::map(self::findAll(['country_id' => $id]), 'id', 'name');
    }
}
