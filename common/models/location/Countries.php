<?php

namespace common\models\location;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%location_countries}}".
 *
 * @property int $id
 * @property string $sortname
 * @property string $name
 */
class Countries extends \yii\db\ActiveRecord
{
    const CODE_USA = 231;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%location_countries}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sortname', 'name'], 'required'],
            [['sortname'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sortname' => Yii::t('app', 'Sortname'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return array
     */
    public static function getAll()
    {
        return self::find()->all();
    }

    /**
     * @return array
     */
    public static function getAllInMapArray()
    {
        return ArrayHelper::map(self::getAll(), 'id', 'name');
    }
}
