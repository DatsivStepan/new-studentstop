<?php

namespace common\models;

use Yii,
    yii\db\Expression,
    yii\helpers\ArrayHelper,
    yii\behaviors\TimestampBehavior,
    common\models\university\ClassesRating,
    common\models\university\Majors;

/**
 * This is the model class for table "{{%university}}".
 *
 * @property integer $id
 * @property integer $id_owner
 * @property string $name
 * @property string $history
 * @property integer $status
 * @property string $slug
 * @property string $date
 * @property string $image
 * @property string $location
 * @property string $location_lat
 * @property string $location_lng
 * @property string $year_founded
 * @property string $facebook_link
 * @property string $linkedin_link
 * @property string $twitter_link
 * @property integer $country_id
 * @property integer $region_id
 * @property string $telephone
 * @property string $email
 * @property string $website
 * @property string $created_at
 * @property string $updated_at
 */
class University extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;
    
    public static $filterTop = [
        '1' => 'Students',
        '2' => 'Professor',
        '3' => 'Major',
        '4' => 'University',
    ];

    public static $statusArray = [
        self::STATUS_ACTIVE => 'active',
        self::STATUS_NOT_ACTIVE => 'not active',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%university}}';
    }

    public $rating;
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class'         => 'common\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [
                [
                    'id_owner', 'status', 'country_id', 'region_id'
                ],
                'integer'
            ],
            [['history'], 'string'],
            [
                [
                    'date', 'created_at', 'updated_at'
                ],
                'safe'
            ],
            [
                [
                    'name', 'slug', 'image', 'location', 'year_founded', 'facebook_link',
                    'linkedin_link', 'twitter_link', 'location_lat', 'location_lng'
                ],
                'string',
                'max' => 255
            ],
            [['telephone', 'email', 'website'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_owner' => Yii::t('app', 'Owner'),
            'name' => Yii::t('app', 'Name'),
            'history' => Yii::t('app', 'History'),
            'status' => Yii::t('app', 'Status'),
            'slug' => Yii::t('app', 'Slug'),
            'date' => Yii::t('app', 'Date'),
            'image' => Yii::t('app', 'Image'),
            'location' => Yii::t('app', 'Location'),
            'year_founded' => Yii::t('app', 'Year Founded'),
            'facebook_link' => Yii::t('app', 'Facebook Link'),
            'linkedin_link' => Yii::t('app', 'Linkedin Link'),
            'twitter_link' => Yii::t('app', 'Twitter Link'),
            'country_id' => Yii::t('app', 'Country'),
            'region_id' => Yii::t('app', 'Region'),
            'telephone' => Yii::t('app', 'Telephone'),
            'email' => Yii::t('app', 'Email'),
            'website' => Yii::t('app', 'Website'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
        
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param $strCount
     * @return string
     */
    public function getHistory($strCount = null)
    {
        return $strCount && (strlen($this->history) > $strCount) ? substr($this->history, 0, $strCount) . '...' : $this->history;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->location;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|int $userId
     * @return string
     */
    public function getMyFriendCount($userId = null)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        return user\UserFriend::getFriendCount($userId, $this->id);
    }
    
    /**
     * @param $universityId
     * @return string
     */
    public static function getTopList($universityId)
    {
        $filterTop = self::$filterTop;
        $resultArray = [];
        foreach ($filterTop as $filterId => $filterName) {
            switch ($filterId) {
                case 1:
                    $resultArray[$filterId] = User::getAll($universityId, UserRole::ROLE_STUDENT, 8);
                    break;
                case 2:
                    $resultArray[$filterId] = User::getAllProfessorByRating($universityId, 8);
                    break;
                case 3:
                    $resultArray[$filterId] = university\Majors::getAllMajorsByRate($universityId, 8);
                    break;
                case 4:
                    $resultArray[$filterId] = self::getAllUniversityByRate(8);
                    break;
            }
        }
        return $resultArray;
    }
    
    /**
     * @return string
     */
    public function getClassCount()
    {
        return university\Classes::find()
                ->alias('c')
                ->joinWith(['courseModel.majorModel m'])
                ->where(['m.university_id' => $this->id])
                ->count();
    }
    
    /**
     * @return string
     */
    public function getUserCount()
    {
        return User::find()
                ->alias('c')
                ->joinWith(['userInfoModel ui'])
                ->where(['ui.university_id' => $this->id])
                ->count();
    }
    
    /**
     * @param $rateNumber
     * @return string
     */
    public function getRate($rateNumber = null)
    {
        $universityCount = University::find()->count();
        return ($rateNumber ? 1 : '') . '/' . $universityCount;
    }
    
    /**
     * return string
     */
    public function getRating()
    {
        $query = ClassesRating::find()
            ->alias('cr')
            ->joinWith(['classesModel.courseModel.majorModel m'])
            ->where(['m.university_id' => $this->id]);

        return $query->exists() ? $query->sum('rate_professor') / $query->count() : 0;
    }

    public function getImages()
    {
        return $this->image ? '/' . $this->image : '/images/default_avatar.jpg';
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getPostsModel()
    {
        return $this->hasMany(Post::className(), ['university_id' => 'id'])->
                orderBy(['created_at' => SORT_DESC]);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getOwnerModel()
    {
        return $this->hasOne(User::className(), ['id' => 'id_owner']);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getMajorsModel()
    {
        return $this->hasMany(Majors::className(), ['university_id' => 'id']);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getRegionModel()
    {
        return $this->hasOne(location\States::className(), ['id' => 'region_id']);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public static function getAllUniversityByRate($limit = null)
    {
        $query = self::find()
                ->alias('u')
                ->select([
                    'u.*',
                    'rating' => '('. ClassesRating::find()
                        ->alias('cr')
                        ->select('SUM(cr.rate_professor) / COUNT(cr.id)')
                        ->joinWith(['classesModel.courseModel.majorModel m'])
                        ->where('m.university_id = u.id')
                        ->createCommand()
                        ->sql . ')'
                ]);

        if ($limit) {
            $query->limit($limit);
        }

        return $query->orderBy(['rating' => SORT_DESC])->all();
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public static function getAllUniversity($limit = null)
    {
        $query = self::find();
        if ($limit) {
            $query->limit($limit);
        }
        return $query->all();
    }
    
    public static function getAllUniversityInArrayMap()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
