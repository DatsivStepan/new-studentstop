<?php
use yii\helpers\Html,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$activateLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate-user-email/' . $user->id . '?key=' . $user->auth_key]);
$username = $user->userInfoModel ? $user->userInfoModel->getNameAndSurname() : ($user->username ? $user->username : $user->email);
?>
<style>
</style>

<div class="password-reset">
    <div style="text-align:center;">
        <img src="http://student.codereview.site/images/student_logonewcolor.png" style="width:150px;margin:0 auto;">
    </div>
    <p  style="text-align:center;">Hey, <?= $username; ?></p>

    <p style="text-align:center;">We received a request to set your StudentStop email to <?= $user->email; ?>. If this is correct, please confirm by clicking the button below.</p>

    <p style="text-align:center;">
        <?= Html::a('Activate link',
            $activateLink,
            [
                'class' => ' button-class btn_n',
                'style' => 'display: inline-block;
                font-weight: 400;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                border: 1px solid transparent;
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                line-height: 1.5;
                border-radius: 0.25rem;
                transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                 color: white !important;
                background-color: #31e7c5 !important;
                border: 0px;
                font-size: 17px;
                font-weight: bold;
                font-family: "Candara";
                height: 40px;
                box-shadow: 0 4px 12px rgba(61, 227, 196, 0.5) !important;
                padding-top: 6px;
                letter-spacing: 1px;'
            ]) ?>
    </p>
</div>
