<?php
use yii\helpers\Html,
    yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>

<div class="password-reset">
    <div style="text-align:center;">
        <img src="http://student.codereview.site/images/student_logonewcolor.png" style="width:150px;margin:0 auto;">
    </div>
    <h5 style="text-align:center;">Congratulations! Your registration is confirmed!</h5>
    <p style="text-align:center;">You’ve successfully completed registration for The Student Stop.</p>
    <p style="text-align:center;">Your email: <?= $user->email; ?></p>
</div>
