<?php
return [
    'adminEmail' => 'roket.kaktus.team@gmail.com',
    'supportEmail' => 'roket.kaktus.team@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
];
